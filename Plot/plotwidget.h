#ifndef PLOTWIDGET_H
#define PLOTWIDGET_H

#include "../cfg.h"
#include "../Parser/modelparserbase.h"
#include "mycurve.h"
#include "myplot.h"
#include "../Plot/datacurve.h"

class PlotWidget : public QWidget
{
    Q_OBJECT
public:
    explicit PlotWidget(QThread* th);
    ~PlotWidget(void);

    void dropEvent(QDropEvent * event);
    void dragEnterEvent(QDragEnterEvent *denev){denev->acceptProposedAction();} // для обеспечения дропа

private:
    // элементы управления
    QGridLayout* gl; // главный компоновщик

    MyPlot* plot; // холст графиков

    QPushButton* pb_help; // кнопка справки по командам
    QTextBrowser *l_help; // виджет справки по командам

    QStatusBar *statusbar;

    struct StCurve // структура графика
    {
        ModelParserBase* mp; // модель анализатора (через него есть доступ к каналу и ПИВ)
        MyCurve* curve; // сам график
        DataCurve* dcurve; // данные графика
    };

    QList<StCurve> list_st_curve; // список сруктур графиков

    QThread *thread_from_parser; // поток парсера

};

#endif // PLOTWIDGET_H
