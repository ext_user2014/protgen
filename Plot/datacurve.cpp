#include "datacurve.h"

DataCurve::DataCurve(int rowdata, QObject *sender) :
    QObject(0)
{
    first_msec = -1;
    row_of_data = rowdata;
    src = (ModelParserBase*)sender;
    counter = 0;
}
DataCurve::~DataCurve(void)
{
    this->disconnect();
}

/******************************************************************
*  Слот добавления данных
*  Вход: время в мс и в QTime'е
*  Выход: нет
******************************************************************/
void DataCurve::slotDataAdded(double td,QTime t)
{
    ModelParserBase* s = (ModelParserBase*)sender(); // получаем указатель на отправителя сигнала
    const double *d = s->dblData(); // указатель на массив данных полей
    if( !d )
        return;

    if( src != s ) // настоящий экземпляр класса не относится к отправителю
        return;

    if( first_msec == -1 ) // первичный приход данных по времени
        first_msec = td;

    myX.append(td);
    myY.append(*(d+row_of_data)); // значение, смещенное на нужную строку
    myXt.append(t);

    counter++;
}
