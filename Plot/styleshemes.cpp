#include "styleshemes.h"

StyleShemes::StyleShemes(void)
{
    h = 0;
    s = 255;
    v = 255;

    cb_list_shemes = new QComboBox(0);
    cb_list_shemes->setHidden(true); // временно скрываем до полной реализации
    cb_list_shemes->addItems(StaticData::settings->value("Styles",QStringList()<<"Default").toStringList());

    //
    default_curve = new StyleCurve();

    default_curve->map_color[PLNT_CURVE] = QColor(Qt::red);
    default_curve->map_color[PLNT_MARKER] = QColor(Qt::red);
    default_curve->map_color[PLNT_AXISY] = QColor(Qt::red);
    default_curve->map_color[PLNT_SYMBOL] = QColor(Qt::red);
    default_curve->map_color[PLNT_SYMBOL_BORDER] = QColor(Qt::red);
    default_curve->map_color[PLNT_TXT] = QColor(Qt::black);
    default_curve->map_color[PLNT_TXT_BGROUND] = QColor(Qt::white);
    default_curve->map_color[PLNT_TXT_BORDER] = QColor(Qt::white);

    default_curve->map_pen[PLNT_CURVE] = Qt::SolidLine;
    default_curve->map_pen[PLNT_SYMBOL_BORDER] = Qt::NoPen;
    default_curve->map_pen[PLNT_MARKER] = Qt::DashLine;
    default_curve->map_pen[PLNT_TXT_BORDER] = Qt::SolidLine;

    default_curve->map_size[PLNT_CURVE] = QSizeF(1.5,0.0);
    default_curve->map_size[PLNT_SYMBOL] = QSizeF(4.0,0.0);
    default_curve->map_size[PLNT_SYMBOL_BORDER] = QSizeF(0.0,0.0);
    default_curve->map_size[PLNT_MARKER] = QSizeF(1.0,0.0);
    default_curve->map_size[PLNT_TXT_BORDER] = QSizeF(4.0,4.0);

    default_curve->map_style[PLNT_SYMBOL] = (int)QwtSymbol::Ellipse;
    default_curve->map_style[PLNT_CURVE] = (int)QwtPlotCurve::Lines;
    //
    //
    default_canvas = new StyleCanvas();

    default_canvas->map_color[PLNT_CANVAS] = QColor(Qt::black);
    default_canvas->map_color[PLNT_GRID] = QColor(Qt::green);

    default_canvas->map_pen[PLNT_GRID] = Qt::DotLine;

    default_canvas->map_size[PLNT_GRID] = QSizeF(0.8,0.0);
    //
    //
    default_marker = new StyleMarker();

    default_marker->map_color[PLNT_MARKER] = QColor(Qt::cyan);
    default_marker->map_color[PLNT_SYMBOL] = QColor(Qt::cyan);
    default_marker->map_color[PLNT_SYMBOL_BORDER] = QColor(Qt::cyan);
    default_marker->map_color[PLNT_TXT] = QColor(Qt::black);
    default_marker->map_color[PLNT_TXT_BGROUND] = QColor(Qt::white);
    default_marker->map_color[PLNT_TXT_BORDER] = QColor(Qt::white);

    default_marker->map_pen[PLNT_TXT_BORDER] = Qt::SolidLine;
    default_marker->map_pen[PLNT_SYMBOL_BORDER] = Qt::SolidLine;
    default_marker->map_pen[PLNT_MARKER] = Qt::SolidLine;

    default_marker->map_size[PLNT_MARKER] = QSizeF(1.0,0.0);
    default_marker->map_size[PLNT_TXT_BORDER] = QSizeF(4.0,4.0);
    default_marker->map_size[PLNT_SYMBOL] = QSizeF(4.0,0.0);
    default_marker->map_size[PLNT_SYMBOL_BORDER] = QSizeF(0.0,0.0);

    default_marker->map_style[PLNT_SYMBOL] = (int)QwtSymbol::Star1;
    //
}
StyleShemes::~StyleShemes(void)
{
    DelAll(default_canvas);
    DelAll(default_curve);
    DelAll(default_marker);
    delete cb_list_shemes;
}

/******************************************************************
*  Статическая функция вычисления контрастного цвета
*  Вход: исходный цвет
*  Выход: контрастный цвет
******************************************************************/
QColor StyleShemes::ContrastColor(QColor color)
{
    QColor c = color;

    int h = color.hue();
    int s = color.saturation();
    int v = color.value();

    h += 180;
    h = h>359?h-359:h;

    s += 127;
    s = s>255?s-255:s;

    v += 127;
    v = v>255?v-255:v;

    c.setHsv(h,s,v);

    return c;
}

/**************************************************************************
 *
 * Применение стиля к графику
 *
 * ***********************************************************************/
void StyleShemes::Apply(QwtPlotCurve* c, QwtScaleWidget* w)
{
    StyleCurve *stc = 0x0;
    if( c ) // ищем по c
        stc = Search(default_curve,c); // поиск в связанном списке структур
    if( !stc && w ) // не найдено, ищем по w
        stc = Search(default_curve,w);

    if( !stc && (c || w) ) // таки не найдено ничего, создаем новую структуру, если есть исходные указатели (хотя бы один)
    {
        stc = new StyleCurve();
        default_curve->CopyMapsTo(stc);
        stc->curve = c;
        stc->scale = w;

        StyleCurve* bot = Bot(default_curve);
        bot->next = stc;
        stc->back = bot;
        stc->next = 0x0;
    }
    else if( !stc ) // ничего не найдено и на входе нулевые указателы, выходим
        return;

    // далее stc содержит дейсвтительный указатель

    if( c && !stc->curve )
        stc->curve = c;
    if( w && !stc->scale )
        stc->scale = w;

    QwtSymbol *s = 0x0;
    if( stc->curve ) // применяем стиль к графику
    {
        stc->curve->setPen(stc->map_color[PLNT_CURVE],
                           stc->map_size[PLNT_CURVE].width(),
                           stc->map_pen[PLNT_CURVE]);
        stc->curve->setStyle((QwtPlotCurve::CurveStyle)stc->map_style[PLNT_CURVE]);
        s = const_cast<QwtSymbol*>(stc->curve->symbol());
    }
    if( stc->scale ) // применяем стиль к оси Y
    {
        stc->scale->setStyleSheet("color: " + stc->map_color[PLNT_AXISY].name() + ";");
    }
    if( s ) // применяем стиль к точкам графика
    {
        s->setPen(stc->map_color[PLNT_SYMBOL_BORDER],
                  stc->map_size[PLNT_SYMBOL_BORDER].width(),
                  stc->map_pen[PLNT_SYMBOL_BORDER]);
        s->setStyle((QwtSymbol::Style)stc->map_style[PLNT_SYMBOL]);
        s->setColor(stc->map_color[PLNT_SYMBOL]);
        s->setSize(stc->map_size[PLNT_SYMBOL].width());
    }
}

/**************************************************************************
 *
 * Применение стиля к холсту
 *
 * ***********************************************************************/
void StyleShemes::Apply(QwtPlotCanvas* c, QwtPlotGrid* g)
{
    StyleCanvas *stc = 0x0;
    if( c ) // ищем по c
        stc = Search(default_canvas,c); // поиск в связанном списке структур
    if( !stc && g ) // не найдено, ищем по g
        stc = Search(default_canvas,g);

    if( !stc && (c || g) ) // таки не найдено ничего, создаем новую структуру, если есть исходные указатели (хотя бы один)
    {
        stc = new StyleCanvas();
        default_canvas->CopyMapsTo(stc);
        stc->canvas = c;
        stc->grid = g;

        StyleCanvas* bot = Bot(default_canvas);
        bot->next = stc;
        stc->back = bot;
        stc->next = 0x0;
    }
    else if( !stc ) // ничего не найдено и на входе нулевые указателы, выходим
        return;

    // далее stc содержит дейсвтительный указатель

    if( c && !stc->canvas )
        stc->canvas = c;
    if( g && !stc->grid )
        stc->grid = g;

    if( stc->canvas ) // применяем стиль к холсту
    {
        stc->canvas->setStyleSheet("background-color: " + stc->map_color[PLNT_CANVAS].name() + ";");
    }

    if( stc->grid ) // применяем стиль к сетке
    {
        stc->grid->setPen(stc->map_color[PLNT_GRID],
                          stc->map_size[PLNT_GRID].width(),
                          stc->map_pen[PLNT_GRID]);
    }
}

/**************************************************************************
 *
 * Применение стиля к маркеру
 *
 * ***********************************************************************/
void StyleShemes::Apply(QwtPlotMarker* m, QwtPlotCurve *friend_curve)
{
    StyleCurve *stc = 0x0;
    if( friend_curve ) // ищем по c
        stc = Search(default_curve,friend_curve); // поиск в связанном списке структур

    StyleMarker *stm = 0x0;
    if( m ) // ищем по c
        stm = Search(default_marker,m); // поиск в связанном списке структур

    if( !stm && m ) // не найдено ничего, создаем новую структуру, если есть исходные указатели (хотя бы один)
    {
        stm = new StyleMarker();
        default_marker->CopyMapsTo(stm);
        stm->marker = m;
        stm->extern_style = stc;

        StyleMarker* bot = Bot(default_marker);
        bot->next = stm;
        stm->back = bot;
        stm->next = 0x0;
    }
    else if( !stm ) // ничего не найдено и на входе нулевые указателы, выходим
        return;

    // далее stm содержит дейсвтительный указатель

    if( m && !stm->marker )
        stm->marker = m;

    StyleBase* st = 0x0;
    if( stm->extern_style )
        st = stm->extern_style;
    else
        st = stm;

    QwtSymbol *s = 0x0;
    if( stm->marker ) // применяем стиль к маркеру и его тексту
    {
        stm->marker->setLinePen(st->map_color[PLNT_MARKER],
                                st->map_size[PLNT_MARKER].width(),
                                st->map_pen[PLNT_MARKER]);
        QwtText txt = stm->marker->label();
        txt.setBorderRadius(st->map_size[PLNT_TXT_BORDER].height());
        txt.setBorderPen(QPen(QBrush(st->map_color[PLNT_TXT_BORDER]),
                         st->map_size[PLNT_TXT_BORDER].width(),
                         st->map_pen[PLNT_TXT_BORDER]));
        txt.setColor(st->map_color[PLNT_TXT]);
        txt.setBackgroundBrush(QBrush(st->map_color[PLNT_TXT_BGROUND]));
        stm->marker->setLabel(txt);

        s = const_cast<QwtSymbol*>(stm->marker->symbol());
    }
    if( s ) // применяем стиль к точкам маркера
    {
        s->setPen(st->map_color[PLNT_SYMBOL_BORDER],
                  st->map_size[PLNT_SYMBOL_BORDER].width(),
                  st->map_pen[PLNT_SYMBOL_BORDER]);
        s->setStyle((QwtSymbol::Style)st->map_style[PLNT_SYMBOL]);
        s->setColor(st->map_color[PLNT_SYMBOL]);
        s->setSize(st->map_size[PLNT_SYMBOL].width());
    }

}

/**************************************************************************
 *
 * Обновление цвета
 *
 * ***********************************************************************/
void StyleShemes::Update(void* target_ptr, int plot_element, QColor color, bool auto_apply)
{
    // ищем, к какому роду относится указатель target_ptr
    StyleCanvas* stcs = Search(default_canvas,target_ptr);
    StyleCurve* stcv = Search(default_curve,target_ptr);
    StyleMarker* stm = Search(default_marker,target_ptr);

    if( stcs )
    {
        UpdateColor(stcs,plot_element,color);
        if( auto_apply )
            Apply(stcs->canvas);
    }
    else if( stcv )
    {
        UpdateColor(stcv,plot_element,color);
        if( auto_apply )
            Apply(stcv->curve);
    }
    else if( stm )
    {
        UpdateColor(stm,plot_element,color);
        if( auto_apply )
            Apply(stm->marker);
    }
}
void StyleShemes::Update(void* target_ptr, int plot_element, QSizeF size, bool auto_apply)
{
    // ищем, к какому роду относится указатель target_ptr
    StyleCanvas* stcs = Search(default_canvas,target_ptr);
    StyleCurve* stcv = Search(default_curve,target_ptr);
    StyleMarker* stm = Search(default_marker,target_ptr);

    if( stcs )
    {
        UpdateSize(stcs,plot_element,size);
        if( auto_apply )
            Apply(stcs->canvas);
    }
    else if( stcv )
    {
        UpdateSize(stcv,plot_element,size);
        if( auto_apply )
            Apply(stcv->curve);
    }
    else if( stm )
    {
        UpdateSize(stm,plot_element,size);
        if( auto_apply )
            Apply(stm->marker);
    }
}

void StyleShemes::Update(void* target_ptr, int plot_element, Qt::PenStyle pen, bool auto_apply)
{
    // ищем, к какому роду относится указатель target_ptr
    StyleCanvas* stcs = Search(default_canvas,target_ptr);
    StyleCurve* stcv = Search(default_curve,target_ptr);
    StyleMarker* stm = Search(default_marker,target_ptr);

    if( stcs )
    {
        UpdatePen(stcs,plot_element,pen);
        if( auto_apply )
            Apply(stcs->canvas);
    }
    else if( stcv )
    {
        UpdatePen(stcv,plot_element,pen);
        if( auto_apply )
            Apply(stcv->curve);
    }
    else if( stm )
    {
        UpdatePen(stm,plot_element,pen);
        if( auto_apply )
            Apply(stm->marker);
    }
}

void StyleShemes::Update(void* target_ptr, int plot_element, int style, bool auto_apply)
{
    // ищем, к какому роду относится указатель target_ptr
    StyleCanvas* stcs = Search(default_canvas,target_ptr);
    StyleCurve* stcv = Search(default_curve,target_ptr);
    StyleMarker* stm = Search(default_marker,target_ptr);

    if( stcs )
    {
        UpdateStyle(stcs,plot_element,style);
        if( auto_apply )
            Apply(stcs->canvas);
    }
    else if( stcv )
    {
        UpdateStyle(stcv,plot_element,style);
        if( auto_apply )
            Apply(stcv->curve);
    }
    else if( stm )
    {
        UpdateStyle(stm,plot_element,style);
        if( auto_apply )
            Apply(stm->marker);
    }
}

