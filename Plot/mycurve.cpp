#include "mycurve.h"

/******************************************************************
*  Конмтруктор
*  Вход: нет
*  Выход: экземпляр класса
******************************************************************/
MyCurve::MyCurve() :
    QwtPlotCurve()
{
    this->setRenderHint(QwtPlotItem::RenderAntialiased);

    pos_msec = 0;

    // маркеры
    symbol = new QwtSymbol;
    this->setSymbol(symbol);
}
/******************************************************************
*  Деструктор
*  Вход: нет
*  Выход: нет
******************************************************************/
MyCurve::~MyCurve(void)
{
}
/******************************************************************
*  Обновление кривой зависимости
*  Вход: нет
*  Выход: нет
******************************************************************/
void MyCurve::Update(void)
{
    int offset = 0;
    if( pos_msec != -1 )
        offset = OffsetOfPosMsec(pos_msec,myX->data(),0,myX->size()-1);

    this->setRawSamples(myX->data()+offset,myY->data()+offset,myX->size()-offset); // обновляем данные графика
}

/******************************************************************
*  Установка начальной видимой координаты Х
*  Вход: координата Х (в мсек)
*  Выход: нет
******************************************************************/
void MyCurve::SetPosT(double msec)
{
    pos_msec = msec;
}
/******************************************************************
*  Вычисление смещения буфера по значению координаты Х (в мсек)
*  Вход: Х(в мсек), буфер оси Х, смещение начала просмотра, смещение конца просмотра
*  Выход: смещение (значение < мин, то смещение = 0, значение > макс, то смещение = макс индекс)
******************************************************************/
int MyCurve::OffsetOfPosMsec(double msec, double *data_time, int pb, int pe)
{
    if( pe < 0 )
        return 0;

    if( pe < pb )
        return pe;

    int m = (pe+pb)/2;

    if( *(data_time+m) > msec )
        return OffsetOfPosMsec(msec,data_time,pb,m-1);
    else if( *(data_time+m) < msec )
        return OffsetOfPosMsec(msec,data_time,m+1,pe);
    else
        return m;
}

/******************************************************************
*  Вычисление позиции на графике по координатам указателя
*  Вход: положение указателя мыши на полотне (обновится после исполнения)
*  Выход: знак производной по Y
******************************************************************/
int MyCurve::PosOnCurve(QPointF *pos)
{
    bool sign = false;
    // координата Х останется прежней, Y пересчитается
    //pos->setX(pos->rx());
    pos->setY(FindY(pos->rx(),&sign));
    return sign?-1:+1;
}
/******************************************************************
*  Вычисление Y по X
*  Вход: X, указатель для записи флага отрицательной производной по Y
*  Выход: Y
******************************************************************/
double MyCurve::FindY(double &x, bool *dy_minus)
{
    int index = OffsetOfPosMsec(x,myX->data(),0,myX->size()-1);

    if( x > myX->last() )
        return myY->last();

    if( x < myX->first() )
        return myY->first();

    if( x == myX->at(index) ) // координата Х точно совпала с значением в массиве
        return myY->at(index);

    double dx = myX->at(index+1) - myX->at(index);
    double dy = myY->at(index+1) - myY->at(index);
    double y = dy/dx*(x - myX->at(index)) + myY->at(index);

    if( dy_minus )
        *dy_minus = dy<0.0?true:false;
    return y;
}


