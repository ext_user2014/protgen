#ifndef MYCURVE_H
#define MYCURVE_H

#include <QQueue>
#include <QTime>
#include <QPoint>
#include <qwt_plot_curve.h>
#include <qwt_symbol.h>
#include <qwt_plot_marker.h>

#include "datacurve.h"

#define MAX_SIZEXY 100

class MyCurve : public QwtPlotCurve
{
public:
    explicit MyCurve();
    virtual ~MyCurve(void);

    void SetData(DataCurve* dc){myXt = dc->Xt(); myX = dc->X(); myY = dc->Y();}

    void Update(void); // ф-я обновления графика

    void SetPosT(double msec = -1); // ф-я установка координаты Х первой видимой точки

    void Select(bool on){b_select = on;}
    bool Select(void){return b_select;}

    int PosOnCurve(QPointF *pos); // текущая позиция на графике

    void SetLineWidth(int w){this->setPen(this->pen().color(),w,this->pen().style());}
    void SetLineStyle(Qt::PenStyle ps){this->setPen(this->pen().color(),this->pen().width(),ps);}

private:
    QVector<QTime> *myXt; // абсцисса
    QVector<double> *myX; // абсцисса
    QVector<double> *myY; // ордината

    QwtSymbol *symbol; // маркер

    double pos_msec; // координата Х первой видимой точки

    bool b_select; // флаг выделения (выбора) кривой для участия в функциях инструментария графика

    int OffsetOfPosMsec(double msec, double* data_time, int pb, int pe); // ф-я вычисдения смещения в буфере по координате Х

//    QPointF current_pos; // текущие координаты указателя мыши

    double FindY(double &x, bool *dy_minus = 0x0); // поиск координаты Y по координате X
};

#endif // MYCURVE_H
