#ifndef MYPLOT_H
#define MYPLOT_H

#include "../cfg.h"
#include "styleshemes.h"

#include <QWheelEvent>

#include <qwt_plot.h>
#include <qwt_plot_curve.h>
#include <qwt_plot_grid.h>
#include <qwt_legend.h>
#include <qwt_legend_label.h>
#include <qwt_symbol.h>
#include <qwt_plot_magnifier.h>
#include <qwt_plot_panner.h>
#include <qwt_plot_picker.h>
#include <qwt_picker_machine.h>
#include <qwt_plot_zoomer.h>
#include <qwt_slider.h>
#include <qwt_knob.h>
#include <qwt_scale_widget.h>
#include <qwt_scale_draw.h>
#include <qwt_plot_layout.h>
#include <qwt_plot_canvas.h>
#include <qwt_plot_renderer.h>
#include <qwt_plot_zoneitem.h>
#include <QColormap>

#include <qwt_plot_marker.h>

#include "mycurve.h"

#define OFFSET_FROM_END_CANVAS ((double)0.1) // процент смещения конца графика от конца полотна
#define MOUSE_WHEEL_SCALE ((double)0.0002) // масштабный коэффициент прокрутки колесом мыши

#define UGOL_NAKLONA_TXT_PO_X ((float)-30.0) // угол наклона подписей оси Х

#define DEF_DX 10000 // интервал по оси Х по-умолчанию (видимая область графика)

#define STYLE_FORCURVE (StyleShemes::PLNT_CURVE|\
                        StyleShemes::PLNT_AXISY|\
                        StyleShemes::PLNT_MARKER|\
                        StyleShemes::PLNT_SYMBOL|\
                        StyleShemes::PLNT_SYMBOL_BORDER)

class MyPlot : public QwtPlot
{
    Q_OBJECT
public:
    explicit MyPlot(QWidget *parent = 0);
    virtual ~MyPlot(void);

    MyCurve* NewCurve(QString name, QString ax_label);

    // типы непосредственных дочек контекстного меню
    enum Ctx
    {
        CXT_MENU_LINECOLOR,
        CXT_MENU_LINEWIDTH,
        CXT_MENU_LINETYPES,
        CXT_ACT_LINECOLOR,
        CXT_ACT_LINEWIDTH,
        CXT_ACT_LINETYPES,
        CXT_ACT_LINECOLOREX,
        CXT_ACT_AXISFONT,
        CXT_ACT_GRID
    };
    // типы инструментов
    enum TOOLS
    {
        TB_ZOOMER,
        TB_PANNER,
        TB_MAGNX,
        TB_MAGNY,
        TB_MAGNXY,
        TB_EXPPDF,
        TB_MARKER,
        TB_INTERV,
        TB_MAX
    };

protected:
    virtual void wheelEvent(QWheelEvent *e);
    virtual void mouseDoubleClickEvent(QMouseEvent *e);
    virtual void keyPressEvent(QKeyEvent *e);
    virtual void contextMenuEvent(QContextMenuEvent *cme);

private slots:
    void slotZoomed(QRectF);
    void slotPanned(int, int);
    void slotMagnEnabled(bool on); // вкл\выкл лупы
    void slotPannEnabled(bool on); // вкл\выкл перемещения
    void slotZoomEnabled(bool on); // вкл\выкл масштабер

    void slotLegChecked(const QVariant &itemInfo, bool on); // клик по легенде

    void slotExport(void); // экспорт графика

    void slotPickerMoved(QPointF);
    void slotPickerEnabled(bool b);

    void slotContextMenuAction(QAction*);

signals:
    void signalPickerChanged(QString);

private:
    StyleShemes *styles;

    bool b_tools; // флаг использованного интсрумента

    QToolBar *toolBar;

    QwtLegend *leg; // легенда графика
    QwtPlotGrid *grid; // сетка

    QwtPlotMagnifier *magnifier; // лупа
    QwtPlotPanner *panner; // перетаскивание
    QwtPlotZoomer *zoom; // масштабер
    QwtPlotPicker* picker; // указатель координат
    QAction *markers; // маркеры
    QAction *intervals; // интервалы (зональные)
    bool interval_change; // флаг выделения интервала
    double interval_begin;

    QList<MyCurve*> list_curve; // список кривых
    QList<QwtAxisId> list_axisY; // список осей ординат кривых
    QList<QwtPlotMarker*> list_markerY; // список маркеров кривых для Picker'а

    QList<QwtPlotMarker*> list_marker; // маркеры
    QList<QwtSymbol*> list_symbol; // символы
    QList<QwtPlotZoneItem*> list_zone; // интервалы (зональные)

    QwtAxisId ax_x; // ось абсцисс
    double dx; // настройка размера графика по оси Х

    void timerEvent(QTimerEvent* ev); // обработчик события таймера
    int timerid; // идентификатор графика

    // экстремумы по осям
    double MaxAxisX(void);
    double MinAxisX(void);
    double MaxAxisY(void);
    double MinAxisY(void);

    // автомасштаб по Y
    void AutoScaleY(void)
    {
        for( int i=0;i<list_axisY.size();i++ )
            this->setAxisAutoScale(list_axisY.at(i));
    }
    // автоустановка базового масштаба по осям
    void AutosetBaseZoom(void)
    {
        if( zoom )
            zoom->setZoomBase(QRectF(MinAxisX(),MinAxisY(),\
                                     MaxAxisX()-MinAxisX(),MaxAxisY()-MinAxisY()));
    }
    bool IsToolOn(void)
    {
        return zoom||magnifier||panner||markers->isChecked()||intervals->isChecked();
    }

    bool CurveSelect(void); // диалог выделения кривых

    // доступные цвета для графиков
    QList<QColor> colormap;

    // Контекстное меню
    QMenu* menu;

};

#endif // MYPLOT_H
