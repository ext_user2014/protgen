#include "plotwidget.h"

/******************************************************************
*  Конструктор
*  Вход: нет
*  Выход: экземпляр класса
******************************************************************/
PlotWidget::PlotWidget(QThread *th) :
    QWidget(0)
{
    QTextCodec::setCodecForCStrings(QTextCodec::codecForName("UTF-8"));

    this->setAttribute(Qt::WA_DeleteOnClose);
    this->setAcceptDrops(true);
    this->setMinimumSize(400,400);
    list_st_curve.clear();

    gl = new QGridLayout(this);
    plot = new MyPlot(this);

    pb_help = new QPushButton("Справка...",this);
    QFile f;
    f.setFileName(":/plot/h");
    l_help = new QTextBrowser(this);
    l_help->setFont(QFont("Tahoma",12));
    l_help->setMinimumSize(600,600);
    l_help->setContentsMargins(10,10,10,10);
    if( f.open(QFile::ReadOnly) )
        l_help->setText(f.readAll());
    l_help->setParent(0);
    l_help->setWindowTitle("Справка по управлению графиками");
    connect(pb_help,SIGNAL(clicked()),l_help,SLOT(show()));

    gl->addWidget(0x0,0,0); // место для панели инструментов от MyPlot-а
    gl->addWidget(pb_help,0,1);
    gl->addWidget(plot,1,0,1,2);

    this->setWindowTitle("График");

    this->setLayout(gl);

    statusbar = new QStatusBar(this);
    connect(plot,SIGNAL(signalPickerChanged(QString)),statusbar,SLOT(showMessage(QString)));
    gl->addWidget(statusbar,3,0);

    thread_from_parser = th; // поток парсера
}
/******************************************************************
*  Деструктор
*  Вход: нет
*  Выход: нет
******************************************************************/
PlotWidget::~PlotWidget(void)
{
    disconnect();

    list_st_curve.clear();

    delete l_help;
    delete pb_help;
}
/******************************************************************
*  Обработчик падения на виджет по технологии d&d
*  Вход: событие drop
*  Выход: нет
******************************************************************/
void PlotWidget::dropEvent(QDropEvent * event)
{
    QByteArray data = event->mimeData()->data(MIME_STRUCT_MP_FOR_PLOT); // дешифрируем миме данные в байтовый массив
    QDataStream stream(&data, QIODevice::ReadOnly); // организуем поток чтения

    int row = 0; // строка таблицы парсера
    quint64 mp_addr = 0; // адрес модели парсера
    while (!stream.atEnd()) // читаем байтовый массив до конца
    {
        stream >> row >> mp_addr; // считываем данные из потока
        StCurve cur;
        cur.mp = (ModelParserBase*)mp_addr; // преобразуем адрес в указатель

        bool exist = false;
        for( int i=0;i<list_st_curve.size();i++ ) // перебор имеющихся графиков
        {
            if( (list_st_curve.at(i).mp == cur.mp) && \
                (list_st_curve.at(i).dcurve->Row() == row) )
                exist = true;
        }
        if( exist ) // пропускаем повторы
            continue;

        cur.dcurve = new DataCurve(row,cur.mp);
        cur.dcurve->moveToThread(thread_from_parser);
        connect(cur.mp,SIGNAL(signalAddedInternalQueue(double,QTime)),cur.dcurve,SLOT(slotDataAdded(double,QTime)));

        // создаем график и устанавливаем его имя
        cur.curve = plot->NewCurve(cur.mp->strl_header_v.at(row),
                                   cur.mp->Piv()->info.Name + "/" + cur.mp->strl_header_v.at(row));

        cur.curve->SetData(cur.dcurve);

        list_st_curve.append(cur);
    }
}
