#ifndef DATACURVE_H
#define DATACURVE_H

#include "../cfg.h"
#include "../Parser/modelparserbase.h"

class DataCurve : public QObject
{
    Q_OBJECT
public:
    explicit DataCurve(int rowdata, QObject *sender);
    virtual ~DataCurve(void);

    int Row(void){return row_of_data;}

    QVector<QTime>* Xt(void){return &myXt;}
    QVector<double>* X(void){return &myX;}
    QVector<double>* Y(void){return &myY;}

signals:

public slots:
    void slotDataAdded(double td,QTime t); // слот обработки добавления данных в модели парсера


private:
    QVector<QTime> myXt; // абсцисса
    QVector<double> myX; // абсцисса
    QVector<double> myY; // ордината

    int row_of_data; // порядковый номер данных в массиве данных, приходящих с сигналом
    int first_msec; // временнОй старт рисования графика

    int counter;

    ModelParserBase *src;
};

#endif // DATACURVE_H
