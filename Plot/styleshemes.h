#ifndef STYLESHEMES_H
#define STYLESHEMES_H

#include "../cfg.h"

#include <qwt_plot.h>
#include <qwt_plot_curve.h>
#include <qwt_plot_grid.h>
#include <qwt_legend.h>
#include <qwt_legend_label.h>
#include <qwt_symbol.h>
#include <qwt_plot_magnifier.h>
#include <qwt_plot_panner.h>
#include <qwt_plot_picker.h>
#include <qwt_picker_machine.h>
#include <qwt_plot_zoomer.h>
#include <qwt_scale_widget.h>
#include <qwt_plot_canvas.h>
#include <qwt_plot_zoneitem.h>
#include <qwt_plot_marker.h>

class StyleShemes
{
public:
    StyleShemes(void);
    virtual ~StyleShemes(void);

//    QComboBox* WListShemes(QObject* parent){return 0x0;} // виджет-список стилей
//    void SaveSheme(QString name_sheme = "Sheme1", bool save_as = false){} // сохранение схемы
//    bool OpenSheme(QString name_sheme){return 0;} // загрузка схемы стилей

    enum PlotElement
    {
        PLNT_CURVE =          0x1 << 0,
        PLNT_SYMBOL =         0x1 << 1,
        PLNT_MARKER =         0x1 << 2,
        PLNT_AXISY =          0x1 << 3,
        PLNT_CANVAS =         0x1 << 4,
        PLNT_GRID =           0x1 << 5,
        PLNT_TXT =            0x1 << 6,
        PLNT_SYMBOL_BORDER =  0x1 << 7,
        PLNT_TXT_BORDER =     0x1 << 8, // QSize(толщина,радиус)
        PLNT_TXT_BGROUND =    0x1 << 9,

        PLNT_COUNT =            10
    };

    void Apply(QwtPlotCurve* c = 0x0, QwtScaleWidget* w = 0x0); // применение стиля к графику
    void Apply(QwtPlotCanvas* c = 0x0, QwtPlotGrid* g = 0x0); // применение стиля к холсту
    void Apply(QwtPlotMarker* m = 0x0, QwtPlotCurve* friend_curve = 0x0); // применение стиля к графику

    void Update(void* target_ptr, int plot_element, QColor color, bool auto_apply = true);
    void Update(void* target_ptr, int plot_element, QSizeF size, bool auto_apply = true);
    void Update(void* target_ptr, int plot_element, Qt::PenStyle pen, bool auto_apply = true);
    void Update(void* target_ptr, int plot_element, int style, bool auto_apply = true);

    QColor NextColor(void)
    {
        QColor color;
        color.setHsv(h,s,v);
        h += 24.0;
        return color;
    }

    static QColor ContrastColor(QColor);

    QComboBox* WListShemes(QWidget* parent) const {cb_list_shemes->setParent(parent); return cb_list_shemes;}

private:
    QComboBox *cb_list_shemes;

    qreal h;
    qreal s;
    qreal v;

    typedef QMap<int,QColor> MapColor;
    typedef QMap<int,QSizeF> MapSize;
    typedef QMap<int,Qt::PenStyle> MapPen;
    typedef QMap<int,int> MapOtherStyle;

    class StyleBase
    {
    public:
        StyleBase(void)
        {
            // инициализация структур Map***
            for( int i=0;i<PLNT_COUNT;i++ )
            {
                map_color.insert(0x1<<i,QColor(Qt::red));
                map_pen.insert(0x1<<i,Qt::SolidLine);
                map_size.insert(0x1<<i,QSizeF(0.0,0.0));
                map_style.insert(0x1<<i,0x0);
            }
        }
        virtual ~StyleBase(void){}

        MapColor map_color;
        MapSize map_size;
        MapPen map_pen;
        MapOtherStyle map_style;

        virtual bool Compare(void*) = 0;

        void CopyMapsTo(StyleBase* dest)
        {
            for( int i=0;i<PLNT_COUNT;i++ )
            {
                dest->map_color[0x1<<i] = map_color[0x1<<i];
                dest->map_pen[0x1<<i] = map_pen[0x1<<i];
                dest->map_size[0x1<<i] = map_size[0x1<<i];
                dest->map_style[0x1<<i] = map_style[0x1<<i];
            }
        }
    };

    class StyleCurve : public StyleBase // структура графика
    {
    public:
        StyleCurve(void) : StyleBase(),
            next(0x0),back(0x0),curve(0x0), scale(0x0)
        {
        }
        StyleCurve* next;
        StyleCurve* back;

        QwtPlotCurve* curve;
        QwtScaleWidget* scale;

        virtual bool Compare(void* ptr)
        {
            return (void*)curve == ptr || (void*)scale == ptr;
        }
    }*default_curve;

    class StyleCanvas : public StyleBase // структура холста
    {
    public:
        StyleCanvas(void) : StyleBase(),
            next(0x0),back(0x0),canvas(0x0), grid(0x0)
        {
        }
        StyleCanvas* next;
        StyleCanvas* back;

        QwtPlotCanvas* canvas;
        QwtPlotGrid* grid;

        virtual bool Compare(void* ptr)
        {
            return (void*)canvas == ptr || (void*)grid == ptr;
        }
    }*default_canvas;

    class StyleMarker : public StyleBase // структура маркера
    {
    public:
        StyleMarker(void) : StyleBase(),
            next(0x0),back(0x0),marker(0x0),extern_style(0x0)
        {
        }
        StyleMarker* next;
        StyleMarker* back;

        QwtPlotMarker *marker;

        StyleCurve* extern_style;

        virtual bool Compare(void* ptr)
        {
            return (void*)marker == ptr;
        }
    }*default_marker;

    // поиск указателя findptr в связанном списке структур (trend = 0 - инициатор)
    template<typename T>
    T* Search(T* beginptr, void* findptr, int trend = 0)
    {
        if( beginptr->Compare(findptr) ) // поиск завершен, найдено совпадение
            return beginptr;
        else
        {
            T* ret = 0x0;

            if( beginptr->next && (trend >= 0) )
                ret = Search(beginptr->next,findptr,1);
            else if( !ret && beginptr->back && (trend <= 0) )
                ret = Search(beginptr->back,findptr,-1);

            return ret;
        }
    }
    // верхний элемент связанного списка
    template<typename T>
    T* Top(T* ptr)
    {
        T* tmp = ptr;
        while( tmp->back )
            tmp = tmp->back;
        return tmp;
    }
    // нижний элемент связанного списка
    template<typename T>
    T* Bot(T* ptr)
    {
        T* tmp = ptr;
        while( tmp->next )
            tmp = tmp->next;
        return tmp;
    }
    // удаление связанного списка
    template<typename T>
    void DelAll(T* ptr)
    {
        T* bot = Bot(ptr); // получаем самый нижний элемент
        while( bot->back ) // пока не дошли до самого верха
        {
            bot = bot->back;
            delete bot->next;
            bot->next = 0x0;
        }
        if( bot )
            delete bot; // удаляемся окончательно
    }
    // удаление из связанного списка
    template<typename T>
    void Del(T* ptr)
    {
        if( ptr )
        {
            if( ptr->back )
                ptr->back->next = ptr->next;
            if( ptr->next )
                ptr->next->back = ptr->back;
        }
    }
    template<typename T>
    void UpdateColor(T* ptr, int elements, QColor c)
    {
        if( ptr )
            for( int i =0;i<PLNT_COUNT;i++ )
                if( (0x1<<i) & elements )
                    ptr->map_color[(PlotElement)0x1<<i] = c;
    }
    template<typename T>
    void UpdatePen(T* ptr, int elements, Qt::PenStyle p)
    {
        if( ptr )
            for( int i =0;i<PLNT_COUNT;i++ )
                if( (0x1<<i) & elements )
                    ptr->map_pen[(PlotElement)0x1<<i] = p;
    }
    template<typename T>
    void UpdateSize(T* ptr, int elements, QSizeF s)
    {
        if( ptr )
            for( int i =0;i<PLNT_COUNT;i++ )
                if( (0x1<<i) & elements )
                    ptr->map_size[(PlotElement)0x1<<i] = s;
    }
    template<typename T>
    void UpdateStyle(T* ptr, int elements, int s)
    {
        if( ptr )
            for( int i =0;i<PLNT_COUNT;i++ )
                if( (0x1<<i) & elements )
                    ptr->map_style[(PlotElement)0x1<<i] = s;
    }
};

#endif // STYLESHEMES_H
