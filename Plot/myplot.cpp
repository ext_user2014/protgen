#include "myplot.h"

/******************************************************************
*  Класс шкалы X для отображения времени
******************************************************************/
class TimeScaleDraw: public QwtScaleDraw
{
public:
    TimeScaleDraw( const QTime &base ):
        baseTime( base )
    {
    }
    virtual QwtText label( double dv ) const
    {
        int v = (int)dv;
        return TimeScaleDraw::TimeToString(v);
    }
    static double StartAxisX(QFont font)
    {
        QFontMetrics fm(font);
        double fms = fm.height()/2 + fm.width("00:00:00:000")*cos(3.14*UGOL_NAKLONA_TXT_PO_X/180);
        return fms;
    }
    static double EndAxisX(QFont font)
    {
        QFontMetrics fm(font);
        double fms = fm.height()/2;
        return fms;
    }
    static QString TimeToString(int msec)
    {
        return QTime(msec/3600000,(msec%3600000)/60000,(msec%60000)/1000,msec%1000).toString("hh:mm:ss:zzz");
    }
private:
    QTime baseTime;
};

/******************************************************************
*  Конструктор
*  Вход: родитель
*  Выход: экземпляр класса
******************************************************************/
MyPlot::MyPlot(QWidget *parent) :
    QwtPlot(parent), ax_x(QwtPlot::xBottom,0)
{
    styles = new StyleShemes(); // класс стилей

    b_tools = false; // инициализация флагов

    interval_change = false;

    magnifier = 0x0;
    panner = 0x0;
    zoom = 0x0;

    // инициализация оси Х
    ax_x.id = 0;
    ax_x.pos = QwtPlot::xBottom;
    dx = DEF_DX; // регулятор по оси Х
    this->setAxisScaleDraw(ax_x,new TimeScaleDraw(QTime(0,0,0,0)));
    this->setAxisLabelRotation(ax_x,UGOL_NAKLONA_TXT_PO_X);
    this->setAxisLabelAlignment(ax_x,Qt::AlignLeft|Qt::AlignBottom);
    this->setAxisFont(ax_x,QFont("Tahoma",10));
    this->setAxisTitle(ax_x,"t [ч:мин:сек:мсек]");
    QwtScaleWidget *scaleWidget = this->axisWidget(ax_x);
    scaleWidget->setMinBorderDist(TimeScaleDraw::StartAxisX(scaleWidget->font()),TimeScaleDraw::EndAxisX(scaleWidget->font()));

    // указатель координат по X
    picker = new QwtPlotPicker(ax_x, 0, QwtPlotPicker::VLineRubberBand, QwtPicker::ActiveOnly, this->canvas());
    picker->setRubberBandPen(QColor(Qt::white)); // Цвет перпендикулярных линий
    picker->setTrackerPen(QColor(0,0,0,0)); // цвет координат положения указателя
    picker->setStateMachine(new QwtPickerDragPointMachine());
    connect(picker,SIGNAL(moved(QPointF)),this,SLOT(slotPickerMoved(QPointF)));
    connect(picker,SIGNAL(activated(bool)),this,SLOT(slotPickerEnabled(bool)));

    // полотно графика
    this->setTitle("");
    this->setAutoReplot(true);

    // легенда
    leg = new QwtLegend(this);
    leg->setDefaultItemMode(QwtLegendData::Checkable);
    this->insertLegend(leg,QwtPlot::RightLegend);
    connect(leg,SIGNAL(checked(QVariant,bool,int)),SLOT(slotLegChecked(QVariant,bool)));

    // сетка
    grid = new QwtPlotGrid();
    grid->enableXMin(true);
    grid->setVisible(false);
    grid->attach(this);

    // панель инструментов
    toolBar = new QToolBar(this);
    toolBar->setContentsMargins(2,2,2,2);
    toolBar->addSeparator();
    toolBar->addSeparator();
    toolBar->setIconSize(QSize(42,42));
    toolBar->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
    QAction *act_tool = NULL;

    act_tool = toolBar->addAction(QIcon(":/plot/zoom"),"Масштаб");
    act_tool->setCheckable(true);
    act_tool->setProperty("TYPE",TB_ZOOMER);
    connect(act_tool,SIGNAL(toggled(bool)),this,SLOT(slotZoomEnabled(bool)));

    act_tool = toolBar->addAction(QIcon(":/plot/pann"),"Переместить");
    act_tool->setCheckable(true);
    act_tool->setProperty("TYPE",TB_PANNER);
    connect(act_tool,SIGNAL(toggled(bool)),this,SLOT(slotPannEnabled(bool)));

    toolBar->addSeparator();
    act_tool = toolBar->addAction(QIcon(":/plot/magnx"),"Лупа X");
    act_tool->setCheckable(true);
    act_tool->setProperty("TYPE",TB_MAGNX);
    connect(act_tool,SIGNAL(toggled(bool)),this,SLOT(slotMagnEnabled(bool)));
    act_tool = toolBar->addAction(QIcon(":/plot/magny"),"Лупа Y");
    act_tool->setCheckable(true);
    act_tool->setProperty("TYPE",TB_MAGNY);
    connect(act_tool,SIGNAL(toggled(bool)),this,SLOT(slotMagnEnabled(bool)));
    act_tool = toolBar->addAction(QIcon(":/plot/magnxy"),"Лупа XY");
    act_tool->setCheckable(true);
    act_tool->setProperty("TYPE",TB_MAGNXY);
    connect(act_tool,SIGNAL(toggled(bool)),this,SLOT(slotMagnEnabled(bool)));

    toolBar->addSeparator();
    markers = toolBar->addAction(QIcon(":/plot/marker"),"Маркер");
    markers->setCheckable(true);
    markers->setProperty("TYPE",TB_MARKER);
    intervals = toolBar->addAction(QIcon(":/plot/interval"),"Интервал");
    intervals->setCheckable(true);
    intervals->setProperty("TYPE",TB_INTERV);

    toolBar->addSeparator();
    act_tool = toolBar->addAction(QIcon(":/plot/exp_pdf"),"Экспорт");
    act_tool->setProperty("TYPE",TB_EXPPDF);
    connect(act_tool,SIGNAL(triggered()),SLOT(slotExport()));

    toolBar->addWidget(styles->WListShemes(toolBar)); // добавляем список стилей

    timerid = startTimer(200); // запуск таймера

    QGridLayout *glp = (QGridLayout*)parent->layout(); // компоновщик родителя
    glp->addWidget(toolBar,0,0); // добавляем панель инструментов

    // загружаем цвета
    colormap.clear();
    QFile f;
    f.setFileName(":/app/palette");
    if( f.open(QFile::ReadOnly) )
    {
        QString s_color;
        while( !s_color.append(f.readLine()).isEmpty() )
        {
            QStringList sl = s_color.split(" ");
            colormap.append(QColor(sl[0].toInt(),sl[1].toInt(),sl[2].toInt()));
            s_color.clear();
        }
    }

    // формируем контекстное меню
    menu = new QMenu(this);
    connect(menu,SIGNAL(triggered(QAction*)),this,SLOT(slotContextMenuAction(QAction*)));
    QMenu *m_line_color = menu->addMenu("Цвет");
    m_line_color->setProperty("TYPE",CXT_MENU_LINECOLOR);
    QMenu *m_line_width = menu->addMenu("Толщина линии");
    m_line_width->setProperty("TYPE",CXT_MENU_LINEWIDTH);
    QMenu *m_line_type = menu->addMenu("Тип линии");
    m_line_type->setProperty("TYPE",CXT_MENU_LINETYPES);
//    QMenu *m_symbol_form = menu->addMenu("Форма маркера");
//    QMenu *m_symbol_type = menu->addMenu("Тип маркера");

    menu->setStyleSheet("background-color: Gainsboro; color: black; ");

    QAction *ac;
    for( int i=0;i<colormap.size();i++ )
    {
        ac =  m_line_color->addAction(colormap[i].name());
        ac->setProperty("TYPE",CXT_ACT_LINECOLOR);
        ac->setProperty("VALUE",colormap[i].name());
        QPixmap pm(10,10);
        pm.fill(colormap[i]);
        ac->setIcon(QIcon(pm));
    }
    ac =  m_line_color->addAction("Выбрать...");
    ac->setProperty("TYPE",CXT_ACT_LINECOLOREX);
    ac->setProperty("VALUE","");
    for( int i=0;i<10;i++ )
    {
        ac =  m_line_width->addAction(QString().sprintf("%d px",i+1));
        ac->setProperty("TYPE",CXT_ACT_LINEWIDTH);
        ac->setProperty("VALUE",i+1);
        QPixmap pm(10,i);
        pm.fill(QColor("gray"));
        ac->setIcon(QIcon(pm));
    }
    ac = m_line_type->addAction("Нет"); ac->setProperty("TYPE",CXT_ACT_LINETYPES); ac->setProperty("VALUE",Qt::NoPen);
    ac = m_line_type->addAction("Сплошная"); ac->setProperty("TYPE",CXT_ACT_LINETYPES); ac->setProperty("VALUE",Qt::SolidLine);
    ac = m_line_type->addAction("Штриховая"); ac->setProperty("TYPE",CXT_ACT_LINETYPES); ac->setProperty("VALUE",Qt::DashLine);
    ac = m_line_type->addAction("Пунктирная"); ac->setProperty("TYPE",CXT_ACT_LINETYPES); ac->setProperty("VALUE",Qt::DotLine);
    ac = m_line_type->addAction("Штрихпунктирная1"); ac->setProperty("TYPE",CXT_ACT_LINETYPES); ac->setProperty("VALUE",Qt::DashDotLine);
    ac = m_line_type->addAction("Штрихпунктирная2"); ac->setProperty("TYPE",CXT_ACT_LINETYPES); ac->setProperty("VALUE",Qt::DashDotDotLine);

    menu->addSeparator();
    ac = menu->addAction("Шрифт...");
    ac->setProperty("TYPE",CXT_ACT_AXISFONT);

    menu->addSeparator();
    ac = menu->addAction("Сетка");
    ac->setProperty("TYPE",CXT_ACT_GRID);
    ac->setCheckable(true);

    // стилизуемся
    styles->Apply((QwtPlotCanvas*)this->canvas(),grid);
    this->setStyleSheet("background-color: black; color:white;");
}
/******************************************************************
*  Виртуальный деструктор
*  Вход: нет
*  Выход: нет
******************************************************************/
MyPlot::~MyPlot(void)
{
    disconnect();

    if( magnifier )
        delete magnifier;
    if( panner )
        delete panner;
    if( zoom )
        delete zoom;
    if( picker )
        delete picker;

    list_axisY.clear();

    qDeleteAll(list_curve);
    list_curve.clear();

    qDeleteAll(list_marker);
    list_marker.clear();

    qDeleteAll(list_zone);
    list_zone.clear();

//    qDeleteAll(list_symbol);
//    list_symbol.clear();

    delete menu;
    delete styles;
}
/******************************************************************
*  Слот перемотки осей колесом мышки
*  Вход: событие переметки
*  Выход: нет
******************************************************************/
void MyPlot::wheelEvent(QWheelEvent *e)
{
    QRect rectX = this->axisWidget(ax_x)->geometry();
    QRect rectY;
    QPoint pos = e->pos();
    double delta = e->delta();

    if( rectX.contains(pos) ) // перемотка оси Х
    {
        delta *= this->axisInterval(ax_x).width()*MOUSE_WHEEL_SCALE;
        e->accept();
        this->setAxisScale(ax_x,\
                           this->axisInterval(ax_x).minValue()+delta,\
                           this->axisInterval(ax_x).maxValue()+delta);

        b_tools = true;
        return;
    }
    for( int i=0;i<list_axisY.size();i++ )
    {
        rectY = this->axisWidget(list_axisY.at(i))->geometry();
        if( rectY.contains(pos) ) // перемотка оси Y
        {
            delta *= this->axisInterval(list_axisY.at(i)).width()*MOUSE_WHEEL_SCALE;
            e->accept();
            this->setAxisScale(list_axisY.at(i),\
                               this->axisInterval(list_axisY.at(i)).minValue()+delta,\
                               this->axisInterval(list_axisY.at(i)).maxValue()+delta);

            return;
        }

    }

    e->ignore();
}
/******************************************************************
*  Слот двоного клика мышкой
*  Вход: событие мышки
*  Выход: нет
******************************************************************/
void MyPlot::mouseDoubleClickEvent(QMouseEvent *e)
{
    QRect rectX = this->axisWidget(ax_x)->geometry();
    QRect rectY;
    QRect rect = this->canvas()->geometry();
    QPoint pos = e->pos();

    if( rect.contains(pos) ) // обработка действий мышы на полотне
    {
        e->accept();
        if( e->button() == Qt::LeftButton )
        {
            b_tools = false; // сьрасываем флаг использования инструментария
            for( int i=0;i<toolBar->children().size();i++ ) // перебор дочек панели инструментов
            {
                QAction *act_tmp = (QAction*)toolBar->children()[i];
                if( act_tmp->property("TYPE").isValid() )
                    act_tmp->setChecked(false); // отжимаем все кнопки
            }

            dx = DEF_DX;
            AutoScaleY();
            AutosetBaseZoom();
        }
    }
    else if( rectX.contains(pos) ) // обработка действий мышы на оси Х
    {
        e->accept();
        b_tools = false; // сьрасываем флаг использования инструментария
        dx = DEF_DX;
        AutosetBaseZoom();
    }
    else
    {
        for( int i=0;i<list_axisY.size();i++ ) // обработка действий мышы на оси Y
        {
            rectY = this->axisWidget(list_axisY.at(i))->geometry();
            if( rectY.contains(pos) )
            {
                this->setAxisAutoScale(list_axisY.at(i));
                break;
            }
        }
    }

    e->ignore();
}
/******************************************************************
*  Слот нажатия клавиши на клаве
*  Вход: событие нажатия кнопки клавы
*  Выход: нет
******************************************************************/
void MyPlot::keyPressEvent(QKeyEvent *e)
{
    int k = e->key();
    e->accept();
    switch(k)
    {
    case Qt::Key_M: // новый маркер
    {
        if( !markers->isChecked() || !picker->isActive() )
            break;

        QPointF pf;
        pf.setX(this->invTransform(ax_x, picker->trackerPosition().rx())); // получаем координату Х

        // добавляем вертикальный линию-маркер
        QwtPlotMarker *ml = new QwtPlotMarker();
        ml->setLineStyle(QwtPlotMarker::VLine);
        ml->setXValue(pf.rx());
        ml->setLabelOrientation(Qt::Vertical);
        ml->setLabelAlignment(Qt::AlignLeft | Qt::AlignBottom);
        QwtText txt(TimeScaleDraw::TimeToString((int)pf.x()));
        ml->setLabel(txt);
        styles->Apply(ml);
        ml->attach(this);
        list_marker.append(ml);

        // добавляем текстовые маркеры для каждого видимого графика
        for( int i=0;i<list_curve.size();i++ )
        {
            if( list_curve[i]->isVisible() )
            {
                int sign = list_curve[i]->PosOnCurve(&pf); // получаем точку с учетом знака
                QwtPlotMarker *mt = new QwtPlotMarker(); // маркер-текст
                mt->setAxes(ax_x,list_axisY[i]);
                mt->setValue(pf);
                mt->setLineStyle(QwtPlotMarker::NoLine);
                QwtSymbol *symbol = new QwtSymbol(); // символ маркера
                mt->setSymbol(symbol);
                if( sign < 0 ) // учтем знак:
                    mt->setLabelAlignment(Qt::AlignRight | Qt::AlignTop); // график спадает, текст сверху
                else
                    mt->setLabelAlignment(Qt::AlignRight | Qt::AlignBottom); // график возрастает, текст снизу
                QwtText txt(QString::number(pf.ry()));
                mt->setLabel(txt);
                mt->attach(this);
                styles->Apply(mt);
                list_marker.append(mt);
                list_symbol.append(symbol);
            }
        }
        break;
    }
    case Qt::Key_I: // новый интервал
    {
        if( !intervals->isChecked() || !picker->isActive() )
            break;

        QPointF pf;
        pf.setX(this->invTransform(ax_x, picker->trackerPosition().rx())); // получаем координату Х

        interval_change = !interval_change; // смена флага выделения интервала
        QwtPlotZoneItem *zone = 0x0;
        if( interval_change ) // флаг установлен, это начало интервала
        {
            zone = new QwtPlotZoneItem(); // новый интервал
            list_zone.append(zone);

            zone->setXAxis(ax_x);
            zone->setOrientation(Qt::Vertical);
            zone->setPen(QColor(Qt::cyan));
            zone->setInterval(pf.rx(),pf.rx());
            zone->attach(this);
            interval_begin = pf.rx();

            QwtPlotMarker *zm = new QwtPlotMarker();
            zm->setLineStyle(QwtPlotMarker::VLine);
            zm->setLinePen(QColor(0,0,0,0));
            zm->setXValue(pf.rx());
            zm->setLabelOrientation(Qt::Horizontal);
            zm->setLabelAlignment(Qt::AlignRight | Qt::AlignTop);
            QwtText txt("");
            zm->setLabel(txt);
            styles->Apply(zm);
            zm->attach(this);
            list_marker.append(zm);
        }
        else // повтор нажатия кнопки, завершение создания интервала
        {
            // см. ф-ю slotPickerMoved
        }
        break;
    }
    case Qt::Key_Escape:
    {
        if( interval_change ) // флаг говорит о процессе создания интервала, удаляем его
        {
            interval_change = false; // сбрасываем флаг
            QwtPlotZoneItem *zone = list_zone.last(); // выбираем из списка интервал
            zone->detach(); // удаляем с холста
            delete zone; // удаляем из памяти
            list_zone.removeLast(); // удаляем из списка
            QwtPlotMarker *zone_marker = list_marker.last(); // выбираем из списка маркер
            zone_marker->detach(); // удаляем с холста
            delete zone_marker; // удаляем из памяти
            list_marker.removeLast(); // удаляем из списка
        }
        }
    }
}
/******************************************************************
*  Слот увеличения
*  Вход: границы области увеличения
*  Выход: нет
******************************************************************/
void MyPlot::slotZoomed(QRectF)
{
    b_tools = true;
}
/******************************************************************
*  Слот перемещения канваса
*  Вход: откуда, куда
*  Выход: нет
******************************************************************/
void MyPlot::slotPanned(int, int)
{
    b_tools = true;
}
/******************************************************************
*  Слот лупы
*  Вход: вкл/выкл
*  Выход: нет
******************************************************************/
void MyPlot::slotMagnEnabled(bool on)
{
    if( !on ) // в какое состояние переходим
    {
        if( !magnifier ) // лупа уже удалена выходим
            return;

        dx = this->axisInterval(ax_x).width(); // меняем интервал просмотра по оси Х
        // удаляем лупу и выходим
        magnifier->setEnabled(on);
        delete magnifier;
        magnifier = 0x0;
        return;
    }
    else
    {
        QAction *act = (QAction*)sender();
        TOOLS tool = (TOOLS)act->property("TYPE").toInt(); // вытаскиеваем пользовательское свойство

        if( magnifier ) // лупа есть => переключение луп
        {
            magnifier->setEnabled(false);
            delete magnifier;
            magnifier = 0x0;
            for( int i=0;i<act->parent()->children().size();i++ ) // перебор дочек панели инструментов
            {
                QAction *act_tmp = (QAction*)act->parent()->children()[i];
                if( act_tmp != act && act_tmp->property("TYPE").isValid() ) // пропускаем источник сигнала
                    if( ((TOOLS)act_tmp->property("TYPE").toInt() == TB_MAGNX) ||
                        ((TOOLS)act_tmp->property("TYPE").toInt() == TB_MAGNY) ||
                        ((TOOLS)act_tmp->property("TYPE").toInt() == TB_MAGNXY) )
                        act_tmp->setChecked(false); // здесь произойдет рекурсия
            }
        }

        if( (tool == TB_MAGNY) || (tool == TB_MAGNXY) )
        {
            if( !CurveSelect() ) // отмена выбора списка выделенных кривых, выходим
            {
                act->setChecked(false);
                return;
            }
        }

        // создаем лупу для выделенных кривых
        magnifier = new QwtPlotMagnifier(this->canvas());

        // включаем лупу
        magnifier->setAxisEnabled(ax_x,(tool == TB_MAGNX) || (tool == TB_MAGNXY));
        for( int i=0;i<list_axisY.size();i++ )
        {
            if( list_curve.at(i)->Select() && list_curve.at(i)->isVisible() ) // кривая выделена и видима
                magnifier->setAxisEnabled(list_axisY[i],(tool == TB_MAGNY) || (tool == TB_MAGNXY));
            else
                magnifier->setAxisEnabled(list_axisY[i],false);
        }
        magnifier->setMouseButton(Qt::MidButton);
        magnifier->setEnabled(on);
    }
}
/******************************************************************
*  Слот перетаскивателя
*  Вход: вкл/выкл
*  Выход: нет
******************************************************************/
void MyPlot::slotPannEnabled(bool on)
{
    if( !on ) // в какое состояние переходим
    {
        if( !panner ) // лупа уже удалена выходим
            return;

        // удаляем лупу и выходим
        panner->setEnabled(on);
        disconnect(panner,SIGNAL(panned(int,int)),this,SLOT(slotPanned(int,int)));
        delete panner;
        panner = 0x0;
        return;
    }
    else
    {
        QAction *act = (QAction*)sender();

        if( panner ) // перетаскиватель есть => переключение его
        {
            panner->setEnabled(false);
            disconnect(panner,SIGNAL(panned(int,int)),this,SLOT(slotPanned(int,int)));
            delete panner;
            panner = 0x0;
        }
        for( int i=0;i<act->parent()->children().size();i++ ) // перебор дочек панели инструментов
        {
            QAction *act_tmp = (QAction*)act->parent()->children()[i];
            if( act_tmp != act && act_tmp->property("TYPE").isValid() )  // пропускаем источник сигнала
                if( (TOOLS)act_tmp->property("TYPE").toInt() == TB_ZOOMER )
                    act_tmp->setChecked(false);  // выключаем масштабер (иначе каша в управлении)
        }

        if( !CurveSelect() ) // отмена выбора списка выделенных кривых, выходим
        {
            act->setChecked(false);
            return;
        }

        // создаем перетаскиватель для выделенных кривых
        panner = new QwtPlotPanner(this->canvas());
        connect(panner,SIGNAL(panned(int,int)),this,SLOT(slotPanned(int,int)));
        // включаем перетаскиватель
        panner->setAxisEnabled(ax_x,true);
        for( int i=0;i<list_axisY.size();i++ )
        {
            if( list_curve.at(i)->Select() && list_curve.at(i)->isVisible() ) // кривая выделена и видима
                panner->setAxisEnabled(list_axisY[i],true);
            else
                panner->setAxisEnabled(list_axisY[i],false);
        }
        panner->setMouseButton(Qt::LeftButton);
        panner->setEnabled(on);
    }
}
/******************************************************************
*  Слот масштабера
*  Вход: вкл/выкл
*  Выход: нет
******************************************************************/
void MyPlot::slotZoomEnabled(bool on)
{
    if( !on ) // в какое состояние переходим
    {
        if( !zoom ) // масштабер уже удалена выходим
            return;

        // удаляем масштабер и выходим
        zoom->setEnabled(on);
        disconnect(zoom,SIGNAL(zoomed(QRectF)),this,SLOT(slotZoomed(QRectF)));
        delete zoom;
        zoom = 0x0;
        return;
    }
    else
    {
        QAction *act = (QAction*)sender();

        if( zoom ) // масштабер есть => переключение его
        {
            zoom->setEnabled(false);
            disconnect(zoom,SIGNAL(zoomed(QRectF)),this,SLOT(slotZoomed(QRectF)));
            delete zoom;
            zoom = 0x0;
        }

        for( int i=0;i<act->parent()->children().size();i++ ) // перебор дочек панели инструментов
        {
            QAction *act_tmp = (QAction*)act->parent()->children()[i];
            if( act_tmp != act && act_tmp->property("TYPE").isValid() )  // пропуск источника сигнала
                if( (TOOLS)act_tmp->property("TYPE").toInt() == TB_PANNER )
                    act_tmp->setChecked(false);  // выключаем перетаскиватель
        }

        // создаем масштабер для выделенных кривых
        zoom = new QwtPlotZoomer(this->canvas());
        zoom->setRubberBandPen(QPen(QBrush(Qt::white),2));
        zoom->setMaxStackDepth(-1);
        zoom->setMousePattern(QwtEventPattern::MouseSelect1,Qt::LeftButton);  // увеличение
        zoom->setMousePattern(QwtEventPattern::MouseSelect2,Qt::RightButton,Qt::ControlModifier); // базовый зум
        zoom->setMousePattern(QwtEventPattern::MouseSelect3,Qt::RightButton); // уменьшение на предыдущий зум
        zoom->setEnabled(false);
        connect(zoom,SIGNAL(zoomed(QRectF)),this,SLOT(slotZoomed(QRectF)));
        // включаем масштабер
        zoom->setXAxis(ax_x);
        for( int i=0;i<list_axisY.size();i++ )
        {
            if( list_curve.at(i)->isVisible() ) // кривая видима
                zoom->setYAxis(list_axisY[i]);
        }
        zoom->setEnabled(on);
    }
}

/******************************************************************
*  Слот-обработчик событий таймера
*  Вход: событие
*  Выход: нет
******************************************************************/
void MyPlot::timerEvent(QTimerEvent* ev)
{
    if( ev->timerId() != timerid ) // не наш таймер, выходим
        return;

    for( int i=0;i<list_curve.size();i++ ) // в цикле обновляем данные графиков
        list_curve[i]->Update();

    if( IsToolOn() ) // включен инструментарий, выходим
        return;

    if( b_tools ) // применен инструментарий, выходим
        return;

    double _dx = dx*(1-OFFSET_FROM_END_CANVAS); // интервал по Х видимой части графика
    if( MaxAxisX() - MinAxisX() < _dx )
        this->setAxisScale(QwtPlot::xBottom,MinAxisX(),MinAxisX() + dx);
    else
        this->setAxisScale(QwtPlot::xBottom,MaxAxisX() - _dx,MaxAxisX() - _dx + dx);
}
/******************************************************************
*  Вычисление экстремумов кривых (4 функции)
*  Вход: нет
*  Выход: нет
******************************************************************/
double MyPlot::MaxAxisX(void)
{
    double max = 0;
    for( int i=0;i<list_curve.size();i++ )
    {
        if( !i || max < list_curve[i]->maxXValue() )
            max = list_curve[i]->maxXValue();
    }
    return max;
}

double MyPlot::MinAxisX(void)
{
    double min = 0;
    for( int i=0;i<list_curve.size();i++ )
    {
        if( !i || min > list_curve[i]->minXValue() )
            min = list_curve[i]->minXValue();
    }
    return min;
}

double MyPlot::MaxAxisY(void)
{
    double max = 0;
    for( int i=0;i<list_curve.size();i++ )
    {
        if( !i || max < list_curve[i]->maxYValue() )
            max = list_curve[i]->maxYValue();
    }
    return max;
}

double MyPlot::MinAxisY(void)
{
    double min = 0;
    for( int i=0;i<list_curve.size();i++ )
    {
        if( !i || min > list_curve[i]->minYValue() )
            min = list_curve[i]->minYValue();
    }
    return min;
}
/******************************************************************
*  Создание новой кривой
*  Вход: имя, подпись оси Y
*  Выход: указатель объект кривой
******************************************************************/
MyCurve* MyPlot::NewCurve(QString name, QString ax_label)
{
    // создаем и сохраняем в списке ось Y
    QwtAxisId ax_y(QwtPlot::yLeft,list_axisY.size());
    list_axisY.append(ax_y);

    this->setAxesCount(QwtPlot::yLeft,list_axisY.size()); // изменение кол-ва осей Y

    this->axisWidget(ax_y)->setFont(QFont("Tahoma",10,-1,false)); // настройка шрифта

    // создаем кривую, привязываем ее к осям и сохраняем в списке
    MyCurve *curve = new MyCurve();
    QwtText title;
    title.setFont(QFont("Tahoma",10,-1,false));

    title.setText(name);
    curve->setTitle(title); // наименование кривой в легенде

    title.setText(ax_label);
    this->setAxisTitle(ax_y,title); // наименование оси Y

    curve->setAxes(ax_x,ax_y); // установка осей
    curve->attach(this); // помещаем кривую на полотно
    styles->Apply(curve,this->axisWidget(ax_y));
    styles->Update(curve,STYLE_FORCURVE,styles->NextColor());
    list_curve.append(curve);

    // создание и настройка маркера для picker'а
    QwtPlotMarker *marker = new QwtPlotMarker();
    marker->setLabelAlignment(Qt::AlignRight | Qt::AlignTop); // выравнивание текста значения Y
    marker->setLineStyle(QwtPlotMarker::HLine); // тип маркера
    marker->setAxes(ax_x,ax_y); // установка осей
    marker->setYValue(0.0); // установка начальной позиции
    marker->setVisible(false); // скрываем маркер
    QwtText txt;
    marker->setLabel(txt);
    marker->attach(this); // помещаем на полотно
    styles->Apply(marker,curve);
    list_markerY.append(marker);

    // настройка элемента легенды (нажатый вид)
    QwtLegendLabel *legLabel = (QwtLegendLabel*)leg->legendWidget(this->itemToInfo(curve)); // получам объект через инфу о нем
    if( legLabel )
        legLabel->setChecked(true);

    return curve;
}
/******************************************************************
*  Слот обработки клика по легенде
*  Вход: информация об объекте, состояние нажатия элемента легенды
*  Выход: нет
******************************************************************/
void MyPlot::slotLegChecked(const QVariant &itemInfo, bool on)
{
    QwtPlotItem *plotItem = this->infoToItem(itemInfo); // получам объект через инфу о нем
    if( plotItem->rtti() == QwtPlotItem::Rtti_PlotCurve ) // проверяем тип объекта
    {
        plotItem->setVisible(on); // скрываем/показываем кривую
        this->setAxisVisible(plotItem->yAxis(),on); // скрываем/показываем ось Y кривой
        this->replot();
    }
}
/******************************************************************
*  Выбор крывых через диалоговое окно
*  Вход: нет
*  Выход: флаг нажатия кнопки ОК в диалоговом окне
******************************************************************/
bool MyPlot::CurveSelect(void)
{
    // создаем гуи
    QDialog *dialog = new QDialog();
    QListWidget *lw = new QListWidget(dialog);
    QPushButton *pb_ok = new QPushButton("ОК",dialog);
    QPushButton *pb_cncl = new QPushButton("Отмена",dialog);
    QGridLayout *gl = new QGridLayout(dialog);
    gl->addWidget(lw,0,0,1,2);
    gl->addWidget(pb_ok,1,0);
    gl->addWidget(pb_cncl,1,1);

    // настройка гуи
    lw->setSelectionMode(QListWidget::MultiSelection);
    for( int i=0;i<list_curve.size();i++ )
        lw->addItem(this->axisTitle(list_axisY.at(i)).text());
    lw->selectAll();
    pb_ok->setDefault(true);
    connect(pb_ok,SIGNAL(clicked(bool)),dialog,SLOT(accept()));
    connect(pb_cncl,SIGNAL(clicked()),dialog,SLOT(reject()));

    // запуск диалогового окна
    int dc = QDialog::Accepted;
    if( list_curve.size() > 1 )
        dc = dialog->exec();
    if( dc == QDialog::Accepted )
        for( int i=0;i<list_curve.size();i++ )
            list_curve.at(i)->Select(lw->item(i)->isSelected()); // считываем в цикле выделенный нуект и помечаем кривую соотв. флагом

    // выходим
    dialog->close();
    return dc;
}
/******************************************************************
*  Экспорт графика
*  Вход: нет
*  Выход: нет
******************************************************************/
void MyPlot::slotExport(void)
{
    QwtPlotRenderer renderer;
    QString style_canvas = this->canvas()->styleSheet();
    QString style_plot = this->styleSheet();

    this->canvas()->setStyleSheet("background-color:white;color:black;");
    this->setStyleSheet("background-color:white;color:black;");

    renderer.exportTo(this,"legends.pdf");

    this->setStyleSheet(style_plot); // установка стиля виджета
    this->canvas()->setStyleSheet(style_canvas); // установка стиля виджета
}
/******************************************************************
*  Слот отслеживания перемещения указателя координат
*  Вход: точка на графике в координатах XY (Y нулевой)
*  Выход: нет
******************************************************************/
void MyPlot::slotPickerMoved(QPointF pos)
{
    QPointF pf = pos;
    for( int i=0;i<list_curve.size();i++ )
    {
        list_curve[i]->PosOnCurve(&pf); // точка на графике
        list_markerY[i]->setValue(pf); // смена положения маркера по Y
        QwtText ytxt = list_markerY[i]->label();
        ytxt.setText(QString::number(pf.y()));
        list_markerY[i]->setLabel(ytxt); // смена надписи маркера

        if( intervals->isChecked() && interval_change && list_zone.size() ) // режим создания интервала?
        {
            QwtPlotZoneItem *zone = list_zone.last();

            double min, max;
            if( pf.rx() < interval_begin )
            {
                max = interval_begin;
                min = pf.rx();
            }
            else
            {
                max = pf.rx();
                min = interval_begin;
            }
            zone->setInterval(min,max);
            double interval = max - min;
            list_marker.last()->setXValue(min);
            QwtText txt = list_marker.last()->label();
            txt.setText(QString::number(interval) + "мс");
            list_marker.last()->setLabel(txt);
        }
    }
    emit signalPickerChanged("X: " + TimeScaleDraw::TimeToString((int)pos.x()));
}
/******************************************************************
*  Слот активации маркеров по Y
*  Вход: флаг активности picker'а
*  Выход: нет
******************************************************************/
void MyPlot::slotPickerEnabled(bool b)
{
    bool visible = (IsToolOn() || b_tools) && b; // видимость при активации и использовании инструментария
    for( int i=0;i<list_markerY.size();i++ ) // перебираем маркеры
        list_markerY[i]->setVisible(visible && list_curve[i]->isVisible()); // маркер виден при видимом графике

    if( !visible )
        emit signalPickerChanged("");
}
/******************************************************************
*  Слот контекстного меню
*  Вход: событие
*  Выход: нет
******************************************************************/
void MyPlot::contextMenuEvent(QContextMenuEvent *cme)
{
    if( zoom ) // при включенном масштабере, правая кнопка мыши работает на него, выхоим
        return;

    QPoint pos = cme->pos();
    QRect rect;
    QObjectList obl = menu->children();

    for( int i=0;i<list_axisY.size();i++ ) // оси Y
    {
        rect = this->axisWidget(list_axisY[i])->geometry();
        if( rect.contains(pos) )
        {
            for( int j=0;j<obl.size();j++ )
            {
                QString st = obl[j]->property("TYPE").toString();
                if( st.isEmpty() )
                    continue;

                switch(st.toInt())
                {
                case CXT_MENU_LINECOLOR:
                    ((QMenu*)obl[j])->menuAction()->setText("Цвет линии");
                case CXT_MENU_LINEWIDTH:
                case CXT_MENU_LINETYPES:
                    ((QMenu*)obl[j])->menuAction()->setVisible(true);
                    break;
                case CXT_ACT_AXISFONT:
                    ((QAction*)obl[j])->setVisible(true);
                    break;
                case CXT_ACT_GRID:
                    ((QAction*)obl[j])->setVisible(false);
                    break;
                }
            }

            break;
        }
    }

    rect = this->axisWidget(ax_x)->geometry();
    if( rect.contains(pos) ) // ось Х
    {
        for( int j=0;j<obl.size();j++ )
        {
            QString st = obl[j]->property("TYPE").toString();
            if( st.isEmpty() )
                continue;

            switch(st.toInt())
            {
            case CXT_MENU_LINECOLOR:
            case CXT_MENU_LINEWIDTH:
            case CXT_MENU_LINETYPES:
                ((QMenu*)obl[j])->menuAction()->setVisible(false);
                break;
            case CXT_ACT_AXISFONT:
                ((QAction*)obl[j])->setVisible(true);
                break;
            case CXT_ACT_GRID:
                ((QAction*)obl[j])->setVisible(false);
                break;
            }
        }
    }

    rect = leg->geometry();
    if( rect.contains(pos) ) // легенда
    {
        for( int j=0;j<obl.size();j++ )
        {
            QString st = obl[j]->property("TYPE").toString();
            if( st.isEmpty() )
                continue;

            switch(st.toInt())
            {
            case CXT_MENU_LINECOLOR:
            case CXT_MENU_LINEWIDTH:
            case CXT_MENU_LINETYPES:
                ((QMenu*)obl[j])->menuAction()->setVisible(false);
                break;
            case CXT_ACT_AXISFONT:
                ((QAction*)obl[j])->setVisible(true);
                break;
            case CXT_ACT_GRID:
                ((QAction*)obl[j])->setVisible(false);
                break;
            }
        }
    }

    rect = this->canvas()->geometry();
    if( rect.contains(pos) ) // полотно графиков
    {
        for( int j=0;j<obl.size();j++ )
        {
            QString st = obl[j]->property("TYPE").toString();
            if( st.isEmpty() )
                continue;

            switch(st.toInt())
            {
            case CXT_MENU_LINECOLOR:
                ((QMenu*)obl[j])->menuAction()->setText("Цвет фона");
                ((QMenu*)obl[j])->menuAction()->setVisible(true);
                break;
            case CXT_MENU_LINEWIDTH:
            case CXT_MENU_LINETYPES:
                ((QMenu*)obl[j])->menuAction()->setVisible(false);
                break;
            case CXT_ACT_AXISFONT:
                ((QAction*)obl[j])->setVisible(false);
                break;
            case CXT_ACT_GRID:
                ((QAction*)obl[j])->setVisible(true);
                break;
            }
        }
    }

    menu->exec(cme->globalPos());
}
/******************************************************************
*  Слот действия в контекстном меню
*  Вход: действие (пункт меню)
*  Выход: нет
******************************************************************/
void MyPlot::slotContextMenuAction(QAction* a)
{
    QPoint poss = this->mapFromGlobal(((QMenu*)sender())->pos());
    QWidget* w = this->childAt(poss); // виджет, на котором вызвано контекстное меню
    QColor color("#000000");

    switch( a->property("TYPE").toInt() )
    {
    case CXT_ACT_LINECOLOREX:
    {
        QColorDialog cdlg;
        cdlg.setCurrentColor(w->palette().color(QPalette::Background));
        if( cdlg.exec() )
            color = cdlg.selectedColor();

        if( color.name() == "#000000" ) // черный цвет недопустим
            break;
    } // оптимизация - нет break в case'е
    case CXT_ACT_LINECOLOR:
    {
        if( color.name() == "#000000" ) // выбран цвет из меню
            color.setNamedColor(a->property("VALUE").toString());

        // перебираем виджеты осей
        for( int i=0;i<list_axisY.size();i++ )
        {
            if( w == this->axisWidget(list_axisY[i]) )
            {
                styles->Update((void*)w,STYLE_FORCURVE,color);
                styles->Update((void*)list_markerY[i],0x0,color);
                break;
            }
        }

        if( this->canvas() == w )
        {
            styles->Update((void*)w,StyleShemes::PLNT_CANVAS,color);
            styles->Update((void*)w,StyleShemes::PLNT_GRID,StyleShemes::ContrastColor(color));
        }

        break;
    }
    case CXT_ACT_LINEWIDTH:
    {
        // перебираем виджеты осей
        for( int i=0;i<list_axisY.size();i++ )
        {
            if( w == this->axisWidget(list_axisY[i]) )
            {
                styles->Update((void*)list_curve[i],StyleShemes::PLNT_CURVE,QSizeF(a->property("VALUE").toFloat(),0));
                break;
            }
        }
        break;
    }
    case CXT_ACT_LINETYPES:
    {
        // перебираем виджеты осей
        for( int i=0;i<list_axisY.size();i++ )
        {
            if( w == this->axisWidget(list_axisY[i]) )
            {
                styles->Update((void*)w,StyleShemes::PLNT_CURVE,(Qt::PenStyle)a->property("VALUE").toInt());
                break;
            }
        }
        break;
    }
    case CXT_ACT_AXISFONT:
    {
        QFont font = w->font();
        QFontDialog fdlg(font);
        if( fdlg.exec() )
            font = fdlg.selectedFont();
        else
            break;

        bool is_ax_y = false;
        bool is_ax_x = this->axisWidget(ax_x) == w;
        bool is_leg = leg->geometry().contains(poss);

        // перебираем виджеты осей
        for( int i=0;i<list_axisY.size();i++ )
        {
            QwtScaleWidget *sw = this->axisWidget(list_axisY[i]);
            if( w == sw )
            {
                is_ax_y = true;
                break;
            }
        }

        if( is_ax_y )
        {
            for( int j=0;j<list_axisY.size();j++ )
            {
                this->axisWidget(list_axisY[j])->setFont(font);
                QwtText t = this->axisTitle(list_axisY[j]);
                t.setFont(font);
                this->setAxisTitle(list_axisY[j],t);
            }
        }
        else if( is_ax_x )
        {
            w->setFont(font);
            QwtText t = this->axisTitle(ax_x);
            t.setFont(font);
            this->setAxisTitle(ax_x,t);

            // далее от скочков оси (косяк qwt)
            QwtScaleWidget *sw = (QwtScaleWidget*)this->axisWidget(ax_x);
            sw->setMinBorderDist(TimeScaleDraw::StartAxisX(font),TimeScaleDraw::EndAxisX(font));
        }
        else if( is_leg )
        {
            for( int j=0;j<list_curve.size();j++ )
            {
                QwtText t = list_curve[j]->title();
                t.setFont(font);
                list_curve[j]->setTitle(t);
            }
        }
        break;
    }
    case CXT_ACT_GRID:
    {
        grid->setVisible(a->isChecked());
        break;
    }
    }
}

