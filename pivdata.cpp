/*****************************************************************

    Структуры данных

    Мордвинкин Ю.Ю.
    26.10.2013

*****************************************************************/
#include "pivdata.h"

/******************************************************************
* Конструктор класса данных
* вход: узел-родитель, объект-родитель
* выход: экземпляр класса
******************************************************************/
PivData::PivData(PivData *_parent, QObject *p) :
    QObject(p)
{
    version = CLASS_PIVDATA_CURRENT_VER;
    iscolorV2 = false;

    // заполняем вспомогательные элементы
    tnode.insert(N_PIV,tnodestr().at(N_PIV));
    tnode.insert(N_LINE,tnodestr().at(N_LINE));
    tnode.insert(N_GROUP,tnodestr().at(N_GROUP));
    tnode.insert(N_PARAM,tnodestr().at(N_PARAM));
    tnode.insert(N_ELP_GBIT,tnodestr().at(N_ELP_GBIT));
    tnode.insert(N_ELP_BIT,tnodestr().at(N_ELP_BIT));

    en_t_node.insert(N_PIV,N_PIV);
    en_t_node.insert(N_PIV,N_LINE);
    en_t_node.insert(N_LINE,N_GROUP);
    en_t_node.insert(N_LINE,N_PARAM);
    en_t_node.insert(N_GROUP,N_GROUP);
    en_t_node.insert(N_GROUP,N_PARAM);
    en_t_node.insert(N_PARAM,N_ELP_GBIT);
    en_t_node.insert(N_PARAM,N_ELP_BIT);
    en_t_node.insert(N_ELP_GBIT,N_ELP_GBIT);
    en_t_node.insert(N_ELP_GBIT,N_ELP_BIT);

    st_n_piv.clear();
    st_n_line.clear();
    st_n_group.clear();
    st_n_param.clear();
    st_n_elp_gbit.clear();
    st_n_elp_bit.clear();

    // сохраняем родителя
    this->parent = _parent;

    // устанавливаем значение по умолчанию основной структуры
    if ( this->parent == NULL ) // создается корневой узел
    {
        this->info.type = PivData::N_PIV;
        this->info.Name = this->tnode.value(this->info.type);
        this->info.dt = this->info.dt.currentDate();
        this->st_n_piv.desc = this->Path();
    }
    else    //не корневые узлы
    {
        if( this->parent->info.type != N_MAX )  // максимальная глубина ветви не достигнута
        {
            this->info.type = this->en_t_node.value(this->parent->info.type);
            this->info.Name = this->tnode.value(this->info.type);
            this->info.dt = this->info.dt.currentDate();
            this->st_n_piv.desc = this->Path();
        }
        else // достигли максимальной вложенности
        {
            this->parent = NULL;
            return; // выходим
        }
    }
    this->children.clear();
}
/******************************************************************
*  Деструктор класса данных
******************************************************************/
PivData::~PivData(void)
{
    emit this->signalDistroyed(this); // сообщаем всем, что собираемся разрушиться

    // удаляем всех дочек
    for( int i=0;i<this->children.size();i++ )
        if( this->children.at(i) !=0 )
            delete this->children.at(i); // рекурсивное удаление

    this->children.clear();

    this->parent = NULL;

}
/******************************************************************
* Проверка ниличия дочернего узла
* вход: row - сюда запишется номер строки pd в векторе children
*       path - сюда запишется путь к pd как в файловой системе
* выход: факт наличия дочернего узла
******************************************************************/
bool PivData::Contains(PivData *pd, int* row, QString *path)
{
    if( this->children.contains(pd) ) // ищем в ближайших дочерних узлах
    {
        if( path )
        {
            PivData *tmp = pd->parent;
            *path = "";
            while( tmp )
            {
                *path = "/" + tmp->info.Name + *path;
                tmp = tmp->parent;
            }
        }
        if( row )
            *row = this->children.indexOf(pd);
        return true;
    }

    for( int i=0; i<this->children.size(); i++ )  // ищем в каждом дочернем узле
        if( this->children.at(i)->Contains(pd,row,path) )
            return true;

    return false;

}
/******************************************************************
* Проверка доступности типа n для this
* вход: тип узла
* выход: доступность
******************************************************************/
bool PivData::CtrlTypeNode(Nodes n)
{
    // определяем минимальный тип дочерних узлов
    Nodes nchild = N_MAX;
    for( int i=0;i<this->children.size();i++ )
        if( this->children.at(i)->info.type < nchild )
            nchild = this->children.at(i)->info.type;

    if( !this->parent ) // условия для корневого узла немного отличаются
    {
        if( n != N_PIV )  // корневой может быть только N_PIV
            return false;
        else
            if( this->children.size() && !this->en_t_node.contains(n,nchild) )  // проверка разрешений для дочерних узлов, если они имеются
                return false;
    }
    else  // условия для остальных узлов
    {
        if( !this->en_t_node.contains(n,nchild) && this->children.size() )  // может ли запрашиваемый тип иметь существующих дочек
            return false;
        if( !this->en_t_node.contains(this->parent->info.type,n) ) // может ли родитель содержать запрашиваемый тип
            return false;
    }

    // все условияне выполнились, значит все ОК
    return true;
}
/******************************************************************
* Вычисление пути в дереве текщего экземпляра
* вход: нет
* выход: путь к узлу this
******************************************************************/
QString PivData::Path(void) const
{
    const PivData *tmp = this;
    QString path = "";
    while( tmp )
    {
        path = "/" + tmp->info.Name + path;
        tmp = tmp->parent;
    }
    return path;
}

/******************************************************************
* Загрузка данных из потока
* вход: организованный поток, версия класса
* выход: успешность загрузки данных
******************************************************************/
bool PivData::DataLoad(QDataStream &ds, uint ver)
{
    switch( ver )
    {
    case CLASS_PIVDATA_VER_1: // загрузка для файла данных версии 1
    case CLASS_PIVDATA_VER_2: // загрузка для файла данных версии 2
    {
        int s;
        ds >> s;
        info.operator >>(ds);
        st_n_piv.operator >>(ds);
        st_n_line.operator >>(ds);
        st_n_group.operator >>(ds);
        st_n_param.operator >>(ds);
        st_n_elp_gbit.operator >>(ds);
        st_n_elp_bit.operator >>(ds);

        if(this->iscolorV2)
        {
            st_n_elp_bit.color0.setNamedColor("yellow");
            st_n_elp_bit.color1.setNamedColor("green");
            st_n_elp_bit.v_0ss = "background-color: yellow;";
            st_n_elp_bit.v_1ss = "background-color: green;";
        }

        for( int i=0; i < s; i++ )
        {
            this->children.append(new PivData(this));
            this->children.at(i)->ColorV2(this->iscolorV2);
            if( !this->children.at(i)->DataLoad(ds, ver) )
                return false;
        }
        return true;
    }
    default:
        return false;
    }

    return false;
}
/******************************************************************
* Выгрузка данных в поток
* вход: организованный поток для выгрузки данных
* выход: успешность выгрузки
******************************************************************/
bool PivData::DataSave(QDataStream &ds)
{
    int s = this->children.size();
    ds << s;
    info.operator <<(ds);
    st_n_piv.operator <<(ds);
    st_n_line.operator <<(ds);
    st_n_group.operator <<(ds);
    st_n_param.operator <<(ds);
    st_n_elp_gbit.operator <<(ds);
    st_n_elp_bit.operator <<(ds);

    for( int i=0; i < s; i++ )
        this->children.at(i)->DataSave(ds); // запись ветви

    return true;
}
/******************************************************************
* Самый верхний элемент заданного типа относительно текущего
* вход: заданный тип
* выход: указатель на вышестоящий узел заданного типа
******************************************************************/
PivData *PivData::TopPDOfType(PivData::Nodes tn)
{
    PivData *pd = this->parent;
    PivData *pdp = 0x0;

    while( !pdp && pd )
    {
        if( pd->info.type == tn )
            pdp = pd;
        pd = pd->parent;
    }
    return pdp;
}
/******************************************************************
* Самый нижний элемент заданного типа относительно текущего
* вход: заданный тип
* выход: указатель на нижестоящий узел заданного типа
******************************************************************/
PivData *PivData::ChildPDOfType(PivData::Nodes tn)
{
    for(int i=0; i<this->children.size();i++)
        if(this->children.at(i)->info.type == tn)
            return this->children.at(i);

    return 0x0;
}
/******************************************************************
* Обновление даты изменения узла (рекурсивный вызов при изменении данных)
* вход: нет
* выход: нет
******************************************************************/
void PivData::updateDateNodes(void)
{
    this->info.dt = this->info.dt.currentDate();
    if( this->parent )
        this->parent->updateDateNodes();
}
/******************************************************************
* Вставка src в позицию row, если row = -1, то копирование в себя
* вход: указатель на вставляемый узел, номер строки для вставки
* выход: факт вставки
******************************************************************/
bool PivData::InsertData(PivData* src, int row)
{
    if( row == -1 ) // копированая src в себя
    {
        // проверяем, можем ли мы стать типом как у src
        if( this->parent == NULL && src->info.type != PivData::N_PIV ) // условие для корневого узла
            return false;
        if( this->parent != NULL )
            if( !this->parent->en_t_node.values(this->parent->info.type).contains(src->info.type) ) // условия для остальных узлов
                return false;

        for( int i=0; i<this->children.size(); i++ )
            delete this->children.at(i); // удаляем дочек ветки
        this->children.clear(); // очищаем ветку
        // копируем свойства
        this->info = src->info;
        this->st_n_piv = src->st_n_piv;
        this->st_n_line = src->st_n_line;
        this->st_n_group = src->st_n_group;
        this->st_n_param = src->st_n_param;
        this->st_n_elp_gbit = src->st_n_elp_gbit;
        this->st_n_elp_bit = src->st_n_elp_bit;
        // копируем ветку
        for( int i=0; i<src->children.size(); i++ )
        {
            this->children.append(new PivData(this)); // создаем элемент
            if( !this->children.at(i)->InsertData(src->children.at(i),-1) ) // копируем из src
                return false;
        }

    }
    else // копирование внутрь на позицию row
    {
        // проверяем, можем ли мы содержать тип узла src
        if( !this->en_t_node.values(this->info.type).contains(src->info.type) )
            return false;

        this->children.insert(row,new PivData(this));
        this->children.last()->InsertData(src,-1);
    }
    return true;
}
/******************************************************************
* Запрос размера ветки, включая узловые элементы
* вход: нет
* выход: размер ветки
******************************************************************/
int PivData::Size(void)
{
    int size = this->children.size();

    for( int i=0; i<this->children.size(); i++ )
        size += this->children.at(i)->Size();

    return size;
}
/******************************************************************
* Запрос указателя на PivData по порядковому номеру в ветке, включая узловые элементы
* вход: порядковый номер в ветке
* выход: указатель на узел
******************************************************************/
PivData* PivData::GetPD(int n)
{
    if( n >= this->Size() )
        return 0x0;
    if( n < 0 )
        return 0x0;

    int childrensize = this->children.size();

    if( n < childrensize )
        return this->children.at(n);

    PivData* pd = 0x0;

    for( int i=0; i<childrensize; i++ )
    {
        pd = this->children.at(i)->GetPD(n - childrensize);
        if( pd )
            break;
    }

    return pd;
}
/******************************************************************
* Запрос максимальной глубины элементы
* вход: нет
* выход: глубина элемента
******************************************************************/
int PivData::Depth(void)
{
    if( !this->children.size() )
        return 0;

    int depth = 0;
    for( int i=0; i<this->children.size(); i++ )
    {
        int tmp = this->children.at(i)->Depth();
        if( depth < tmp )
            depth = tmp;
    }
    return depth + 1;
}


