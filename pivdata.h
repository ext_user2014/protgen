/*****************************************************************

    Структуры данных ПИВ

    Мордвинкин Ю.Ю.
    26.10.2013

    16.02.2014: введено наследование от QObject из-за
            необходимости отправлять сигнал разрушения

*****************************************************************/
#ifndef PIVDATA_H
#define PIVDATA_H

#include "cfg.h"

// версии класса PivData
#define CLASS_PIVDATA_VER_1   0x1
#define CLASS_PIVDATA_VER_2   0x2 // добавлен цвет в узле типа "бит" для значений 0 и 1

// текущая версия класса PivData
#define CLASS_PIVDATA_CURRENT_VER   CLASS_PIVDATA_VER_2

class PivData : public QObject
{
    Q_OBJECT
public:
    explicit PivData(PivData *_parent = 0, QObject* p = 0);
    ~PivData(void);

    uint GetVersion(void) const { return this->version;}

    // типы узлов в иерархическом дереве
    enum Nodes{
        N_PIV,      // ПИВ
        N_LINE,     // Линия (канал)
        N_GROUP,    // Группа параметров или файл в линии
        N_PARAM,    // Параметр в группе или файле
        N_ELP_GBIT, // группа битов
        N_ELP_BIT,   // бит (признак)
        N_MAX = N_ELP_BIT
    };
    QMap<Nodes,QString> tnode;  // массив типов и их названий
    static QStringList tnodestr()
    {
        return QStringList() << "ПИВ" << "Канал" << "Группа" << "Параметр" << "Элемент" << "Признак";
    }
    QMultiMap<Nodes,Nodes> en_t_node;   // массив разрешенных значений дочерних узлов

    // общая для всех структура информации об узле
    struct st_info
    {
        // нужные данные
        QString Name;   // имя узла
        Nodes type;     // тип узла
        QDate dt;       // дата изменения
        // сервис структуры
        QDataStream &operator<<(QDataStream &out)   // стандартный вывод
        {
            out << Name << (quint32)type << dt;
            return out;
        }
        QDataStream &operator>>(QDataStream &in) // стандартный ввод
        {
            quint32 _type;
            QDate _dt;
            in >> Name >> _type >> _dt;
            type = (Nodes)_type;
            dt = _dt;
            return in;
        }
        void clear(void)
        {
            Name = "";
            type = (Nodes)0;
            dt = QDate::fromString("2013.10.26","YYYY.MM.DD");
        }
    };

    // структуры Nodes-ов -->>
    struct st_N_PIV      // ПИВ
    {
        // нужные данные
        QString desc;   // описание
        // сервис структуры
        QDataStream &operator<<(QDataStream &out)   // стандартный вывод
        {
            out << desc;
            return out;
        }
        QDataStream &operator>>(QDataStream &in) // стандартный ввод
        {
            in >> desc;
            return in;
        }
        void clear(void)
        {
            desc = "";
        }

    };
    struct st_N_LINE     // Линия (канал)
    {
        // нужные данные
        enum en{A429,RK,MKIO, size=3} t;   // тип линии
        uint r;   // разрядность слов (0 - переменное значение)
        uint n;   // номер линии
        QString desc;   // описание линии
        // сервис структуры
        QMap<en, QString> t_map;
        QDataStream &operator<<(QDataStream &out)   // стандартный вывод
        {
            out << (quint32)t << r << n << desc;
            return out;
        }
        QDataStream &operator>>(QDataStream &in) // стандартный ввод
        {
            quint32 _t;
            in >> _t >> r >> n >> desc;
            t = (en)_t;
            return in;
        }
        void clear(void)
        {
            t = A429; r = 32; n = 0; desc = "";
            t_map.insert(A429,"А429");
//!!!            t_map.insert(RK,"РК");        // убрана поддержка РК
//!!!            t_map.insert(MKIO,"МКИО");    // убрана поддержка МКИО
        }
    };
    struct st_N_GROUP    // Группа параметров/файлов в линии
    {
        // нужные данные
        enum en{LINK,NOLINK, size=2} t;  // тип группы
        uint id;    // идентификатор группы
        uint id_offset;    // смещение
        uint id_mask;    // маска
        uint b_addr;    // базовый адрес группы
        uint b_addr_offset;  // смещение
        uint b_addr_mask;  // маска
        uint size_max;  // максимальный размер группы (к-во слов разрядности st_N_LINE::r)
        uint size_offset;  // смещение размера группы
        uint size_mask;  // маска размерности группы
        bool link_control;  // контроль целостности группы
        // сервис структуры
        QMap<en, QString> t_map;
        QDataStream &operator<<(QDataStream &out)   // стандартный вывод
        {
            out << (quint32)t << id << id_offset << id_mask << b_addr << b_addr_offset << b_addr_mask << size_max << size_offset << size_mask << link_control;
            return out;
        }
        QDataStream &operator>>(QDataStream &in) // стандартный ввод
        {
            quint32 _t;
            in >> _t >> id >> id_offset >> id_mask >> b_addr >> b_addr_offset >> b_addr_mask >> size_max >> size_offset >> size_mask >> link_control;
            t = (en)_t;
            return in;
        }
        void clear(void)
        {
            t = NOLINK; id = id_offset = id_mask = b_addr = b_addr_mask = b_addr_offset = size_max = size_offset = size_mask = 0;
            t_map.insert(NOLINK,"Группа параметров");
            t_map.insert(LINK,"Файл");
            link_control = 1;
        }
    };
    struct st_N_PARAM    // Параметр в линии или группе
    {
        // нужные данные
        uint f; // частота обновления в Гц
        uint id; // идентификатор (адрес)
        uint offset; // расположение идентификатора отностительно начала слова
        uint mask; // маска идентификатора
        bool control;   // контроль наличия параметра
        // сервис структуры
        QDataStream &operator<<(QDataStream &out)   // стандартный вывод
        {
            out << f << id << offset << mask << control;
            return out;
        }
        QDataStream &operator>>(QDataStream &in) // стандартный ввод
        {
            in >> f >> id >> offset >> mask >> control;
            return in;
        }
        void clear(void)
        {
            f = id = offset = 0; mask = 0xFF; control = 1;
        }
    };
    struct st_N_ELP_GBIT      // группа битов
    {
        // нужные данные
        enum en1{GB,    // группировка st_N_ELP_BIT/st_N_ELP_GBIT
             CSR,   // цена старшего разряда
             K,   // линейный коэффициент
             DDK,   // двоично-десятичый код
             CONST,  // константа
             VALUES  // значения
            } t;  // тип группы

        uint offset; // смещение
        uint mask;  // маска
        float val; // опорное значение
        QMap<uint,QString> values; // значения типа группы VALUES
        enum en2{N, SUM, SUB, MULT, DIV, CON, FUN} fnc;   // ничегоНЕделать/суммировать/вычитать/перемножать/делить/сцепить(символьно)/функция значения подгрупп st_N_ELP_GBIT
        QString si; // единица измерения
        QString fun; // функция значений групп
        // сервис структуры
        QMap<en1,QString> t_map;
        QMap<en2,QString> fnc_map;
        QDataStream &operator<<(QDataStream &out)   // стандартный вывод
        {
            out << (quint32)t << offset << mask << val << values << (quint32)fnc << si << fun;
            return out;
        }
        QDataStream &operator>>(QDataStream &in) // стандартный ввод
        {
            quint32 _t, _fnc;
            in >> _t >> offset >> mask >> val >> values >> _fnc >> si >> fun;
            t = (en1)_t;
            fnc = (en2)_fnc;
            return in;
        }
        void clear(void)
        {
            t = GB; offset = 8; mask = 0x3; val = 0.0; fnc = N; si = fun = "";
            t_map.insert(GB,"Объединение");
            t_map.insert(CSR,"ЦСР");
            t_map.insert(K,"Коэффициент");
            t_map.insert(DDK,"ДДК");
            t_map.insert(CONST,"Константа");
            t_map.insert(VALUES,"Значения");
            fnc_map.insert(N,"Нет");
            fnc_map.insert(SUM,"Сумма");
            fnc_map.insert(SUB,"Разность");
            fnc_map.insert(MULT,"Умножение");
            fnc_map.insert(DIV,"Деление");
            fnc_map.insert(CON,"Сцепить");
//!!!!            fnc_map.insert(FUN,"Функция"); // убрана поддержка расширенной функции
        }
    };
    struct st_N_ELP_BIT       // бит
    {
        // нужные данные
        uint offset; // расположение отностительно начала слова
        QString v_1; // значение, если 1
        QString v_0; // значение, если 0

        QString v_1ss; // значение StyleSheet, если 1
        QColor color1;
        QString v_0ss; // значение StyleSheet, если 0
        QColor color0;
        // сервис структуры
        QDataStream &operator<<(QDataStream &out)   // стандартный вывод
        {
            out << offset << (v_1+"&&"+v_1ss) << (v_0+"&&"+v_0ss); // сохраняем данные с стилями их отображения (стил кодируем после символов &&)
            return out;
        }
        QDataStream &operator>>(QDataStream &in) // стандартный ввод
        {
            in >> offset >> v_1 >> v_0;

            QRegExp rg;
            rg.setPattern("background-color: ([a-z]{1,});"); // регулярное выражения для цвета фона

            if( v_1.contains("&&") ) // для значения 1, есть ли в параметре его стиль
            {
                v_1ss = v_1.right(v_1.size() - v_1.indexOf("&&") - 2); // выделяем текст стиля
                v_1 = v_1.left(v_1.indexOf("&&")); // выделяем значение
                rg.indexIn(v_1ss); // парсим текст
                color1.setNamedColor(rg.cap(1)); // выделяем цвет по его текстовому названию и сохраняем его
            }
            if( v_0.contains("&&") ) // для значения 0 ....
            {
                v_0ss = v_0.right(v_0.size() - v_0.indexOf("&&") - 2);
                v_0 = v_0.left(v_0.indexOf("&&"));
                rg.indexIn(v_0ss);
                color0.setNamedColor(rg.cap(1));
            }
            return in;
        }
        void clear(void)
        {
            offset = 0;
            v_1 = v_0 = "";
            v_0ss = v_1ss = "background-color: white;";
            color1.setNamedColor("white");
            color0.setNamedColor("white");
        }
    };

    // <<-- структуры Nodes-ов

    st_info info;               // информация об узле
    QVector<PivData*> children;  // список дочерних узлов
    PivData* parent;            // указатель на родительский узел
    // данные
    st_N_PIV        st_n_piv;
    st_N_LINE       st_n_line;
    st_N_GROUP      st_n_group;
    st_N_PARAM      st_n_param;
    st_N_ELP_GBIT   st_n_elp_gbit;
    st_N_ELP_BIT    st_n_elp_bit;

    // функции
    bool Contains(PivData *pd, int *row = 0x0, QString *path = 0x0);    // проверка наличия дочернего элемента

    bool CtrlTypeNode(Nodes n); // проверка доступности типа для узла

    bool DataLoad(QDataStream &ds, uint ver); // загрузка данных
    bool DataSave(QDataStream &ds); // выгрузка данных

    QString Path(void) const;   // запрос пути к элементу
    PivData* TopPDOfType(PivData::Nodes tn);   // запрос родительского элемента заданного типа (максимально удаленный по дереву)
    PivData* ChildPDOfType(PivData::Nodes tn);   // запрос дочернего элемента заданного типа
    void updateDateNodes(void);   // обновление даты изменения узла (рекурсивный вызов при изменении данных)

    bool InsertData(PivData* src, int row); // вставка элемента

    int Size(void); // возвращает количество элементов вниз по дереву, включая узловые
    int Depth(void); // возвращает максимальную глубину элемента

    PivData* GetPD(int n); // возвращает указатель на PivData по порядковому номеру в ветке, включая узловые элементы

    void ColorV2(bool b){iscolorV2 = b;} // установка признака цветности

signals:
    void signalDistroyed(PivData*);

private:

    uint version; // версия класса

    bool iscolorV2; // признак установки цветности для класса версии 2
};

typedef QVector<PivData*> PivDataList;

#endif // PIVDATA_H
