#ifndef SRCDATAFILE_H
#define SRCDATAFILE_H

#include "../cfg.h"

class SrcDataFile : public QObject
{
    Q_OBJECT
public:
    explicit SrcDataFile(QObject *parent = 0);
    
    void SetListWidget(QListWidget* lw = 0) {this->listwidget = lw;}

signals:
    
public slots:
    void AddFileToList(void);
    void RemFileFromList(void);

private:
    QListWidget* listwidget;
    
};

#endif // SRCDATAFILE_H
