#include "delegatesrcdata.h"

DelegateSrcData::DelegateSrcData(QObject *parent) :
    QItemDelegate(parent)
{
}
DelegateSrcData::~DelegateSrcData(void)
{
}

/******************************************************************
*
*  Создание редактора делегата
*
******************************************************************/
QWidget* DelegateSrcData::createEditor(QWidget *parent, const QStyleOptionViewItem &, const QModelIndex &index) const
{
    A429_Channel* dc = (A429_Channel*)index.internalPointer();

    switch (index.column())
    {
    case 2:
        return new QCheckBox(parent);
    case 3:
        return new QComboBox(parent);
    case 5:
    {
        return new WinData(dc, parent);
    }
    default:
        return 0;
    }

}
/******************************************************************
*
*  загрузка данных в редактор делегата
*
******************************************************************/
void DelegateSrcData::setEditorData(QWidget *editor, const QModelIndex &index) const
{
    A429_Channel* dc = (A429_Channel*)index.internalPointer();
    if( !dc ) return;

    switch (index.column())
    {
    case 2:
    {
        QCheckBox* cb = (QCheckBox*)editor;
        cb->setChecked(dc->Open());
        return;
    }
    case 3:
    {
        QComboBox* cb = (QComboBox*)editor;
        cb->setEnabled(true);
        if( ((Device*)dc->parent())->GetDevType() == D_A429 )
        {
            cb->addItems(QStringList()<<"12.5"<<"50"<<"100");
            cb->setCurrentIndex((int)dc->Freq());
        }
        else
            cb->setEnabled(false);
        return;
    }
    case 5:
    {
        WinData* wd = (WinData*)editor;
        wd->SetValue(dc->Size());
    }

    }
}
/******************************************************************
*
*  Изъятие данных из редактора делегата и запись их в модель данных
*
******************************************************************/
void DelegateSrcData::setModelData(QWidget *editor, QAbstractItemModel *, const QModelIndex &index) const
{
    A429_Channel* dc = (A429_Channel*)index.internalPointer();
    if( !dc ) return;

    switch (index.column())
    {
    case 2:
    {
        QCheckBox* cb = (QCheckBox*)editor;
        ((ModelSrcData*)this->parent())->setData(index, cb->isChecked(), Qt::EditRole);
        break;
    }
    case 3:
    {
        QComboBox* cb = (QComboBox*)editor;
        ((ModelSrcData*)this->parent())->setData(index, cb->currentIndex(), Qt::EditRole);
        break;
    }
    case 5:
    {
        WinData* wd = (WinData*)editor;
        ((ModelSrcData*)this->parent())->setData(index, wd->GetValue(), Qt::EditRole);
    }
    }

}

/******************************************************************/
WinData::WinData(A429_Channel *d, QWidget *parent) : QFrame(parent)
{
    if (!d)
    {
        this->setEnabled(false);
        return;
    }

    this->setMinimumSize(250,100);
    this->setFrameStyle(QFrame::Box | QFrame::Plain);
    this->setAutoFillBackground(true);
    this->setLineWidth(2);
    this->sbms = new QSpinBox(this);
    this->sbw = new QSpinBox(this);
    this->sms = new QSlider(Qt::Horizontal,this);
    this->sw = new QSlider(Qt::Horizontal,this);

    QGridLayout* gl = new QGridLayout(this);
    gl->setMargin(10);
    gl->setSpacing(20);
    gl->addWidget(new QLabel("Слов: "),0,0);
    gl->addWidget(this->sbw,0,1);
    gl->addWidget(this->sw,0,2);
    gl->addWidget(new QLabel("мс: "),1,0);
    gl->addWidget(this->sbms,1,1);
    gl->addWidget(this->sms,1,2);

    this->SetDevChannel(d);

    connect(this->sbms,SIGNAL(valueChanged(int)),this->sms,SLOT(setValue(int)));
    connect(this->sms,SIGNAL(valueChanged(int)),this->sbms,SLOT(setValue(int)));

    connect(this->sbw,SIGNAL(valueChanged(int)),this->sw,SLOT(setValue(int)));
    connect(this->sw,SIGNAL(valueChanged(int)),this->sbw,SLOT(setValue(int)));
}
WinData::~WinData(void)
{
    delete this->sbms;
    delete this->sbw;
    delete this->sms;
    delete this->sw;
}

void WinData::SetDevChannel(A429_Channel * const v)
{
    dc = v;

    sbw->setMaximum(MAX_SIZECHANNEL);
    sbw->setMinimum(MIN_SIZECHANNEL);
    sw->setMaximum(MAX_SIZECHANNEL);
    sw->setMinimum(MIN_SIZECHANNEL);

    if( ((Device*)dc->parent())->GetDevType() == D_A429 )
    {
        if(dc->Freq() == FREQ_12)
        {
            sbms->setMaximum(2.56*MAX_SIZECHANNEL);
            sbms->setMinimum(2.56*MIN_SIZECHANNEL);
            sms->setMaximum(2.56*MAX_SIZECHANNEL);
            sms->setMinimum(2.56*MIN_SIZECHANNEL);
        }
        else if(dc->Freq() == FREQ_50)
        {
            sbms->setMaximum(0.64*MAX_SIZECHANNEL);
            sbms->setMinimum(0.64*MIN_SIZECHANNEL+1);
            sms->setMaximum(0.64*MAX_SIZECHANNEL);
            sms->setMinimum(0.64*MIN_SIZECHANNEL+1);
        }
        else
        {
            sbms->setMaximum(0.32*MAX_SIZECHANNEL);
            sbms->setMinimum(0.32*MIN_SIZECHANNEL+1);
            sms->setMaximum(0.32*MAX_SIZECHANNEL);
            sms->setMinimum(0.32*MIN_SIZECHANNEL+1);
        }
    }
    else if( ((Device*)dc->parent())->GetDevType() == D_MKIO )
    {
        sbms->setMaximum(0.02*MAX_SIZECHANNEL);
        sbms->setMinimum(0.02*MIN_SIZECHANNEL+1);
        sms->setMaximum(0.02*MAX_SIZECHANNEL);
        sms->setMinimum(0.02*MIN_SIZECHANNEL+1);
    }

    connect(this->sbms,SIGNAL(valueChanged(int)),this,SLOT(SetValW(int)));
    connect(this->sms,SIGNAL(valueChanged(int)),this,SLOT(SetValW(int)));

    connect(this->sbw,SIGNAL(valueChanged(int)),this,SLOT(SetValMS(int)));
    connect(this->sw,SIGNAL(valueChanged(int)),this,SLOT(SetValMS(int)));

}

void WinData::SetValMS(int v)
{
    disconnect(this->sbms,SIGNAL(valueChanged(int)),this,SLOT(SetValW(int)));
    disconnect(this->sms,SIGNAL(valueChanged(int)),this,SLOT(SetValW(int)));

    if( ((Device*)dc->parent())->GetDevType() == D_A429 )
    {
        if(dc->Freq() == FREQ_12)
        {
            sbms->setValue(2.56*v);
            sms->setValue(2.56*v);
        }
        else if(dc->Freq() == FREQ_50)
        {
            sbms->setValue(0.64*v);
            sms->setValue(0.64*v);
        }
        else
        {
            sbms->setValue(0.32*v);
            sms->setValue(0.32*v);
        }
    }
    else if( ((Device*)dc->parent())->GetDevType() == D_MKIO )
    {
        sbms->setValue(0.02*v);
        sms->setValue(0.02*v);
    }

    connect(this->sbms,SIGNAL(valueChanged(int)),this,SLOT(SetValW(int)));
    connect(this->sms,SIGNAL(valueChanged(int)),this,SLOT(SetValW(int)));

}

void WinData::SetValW(int v)
{
    disconnect(this->sbw,SIGNAL(valueChanged(int)),this,SLOT(SetValMS(int)));
    disconnect(this->sw,SIGNAL(valueChanged(int)),this,SLOT(SetValMS(int)));
    if( ((Device*)dc->parent())->GetDevType() == D_A429 )
    {
        if(dc->Freq() == FREQ_12)
        {
            sbw->setValue(v/2.56);
            sw->setValue(v/2.56);
        }
        else if(dc->Freq() == FREQ_50)
        {
            sbw->setValue(v/0.64);
            sw->setValue(v/0.64);
        }
        else
        {
            sbw->setValue(v/0.32);
            sw->setValue(v/0.32);
        }
    }
    else if( ((Device*)dc->parent())->GetDevType() == D_MKIO )
    {
        sbw->setValue(v/0.02);
        sw->setValue(v/0.02);
    }
    connect(this->sbw,SIGNAL(valueChanged(int)),this,SLOT(SetValMS(int)));
    connect(this->sw,SIGNAL(valueChanged(int)),this,SLOT(SetValMS(int)));
}
