#include "device.h"

Device::Device(QObject *parent) :
    QObject(parent)
{
}


DevType Device::GetDevType(void) const
{
    return this->dev_type;
}

QString Device::GetDevID  (void) const
{
    return this->dev_id;
}
