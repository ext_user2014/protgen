#ifndef A429_WORD_H
#define A429_WORD_H

#include "../cfg.h"

class A429_Word : public QObject
{
    Q_OBJECT
public:
    // конструктор
    explicit A429_Word(QTime t, quint32 d, bool* sav, QObject *parent = 0);
    explicit A429_Word(A429_Word *wrd, QObject *parent = 0);
    // деструктор
    ~A429_Word(void);

    // установка данных
    void SetData(QTime& t, quint32& d);

    // получение данных
    quint32 GetTimeMS(void){QMutexLocker(&this->mutex);return this->time.msec() + this->time.second()*1000 + this->time.minute()*60000 + this->time.hour()*3600000;} // время прихода слова в мс
    QTime GetTime(void)    {QMutexLocker(&this->mutex);return this->time;} // время прихода слова
    quint32 GetData(void)  {QMutexLocker(&this->mutex);return this->data;} // данные
    bool GetReady(void)    {QMutexLocker(&this->mutex);return this->ready;} // готовность данных по таймеру

    bool CompareT(A429_Word* w, quint32 delta = 0);
    bool CompareT(QTime t, quint32 delta = 0);
    bool CompareW(A429_Word* w);
    bool CompareW(quint32 w);
    bool CompareA(A429_Word* w);
    bool CompareA(quint8 addr);

private:
    // событие таймера
    void timerEvent(QTimerEvent *ev);

    QObject* channel; // канал
    quint32 data;
    QTime time;
    bool ready;
    bool* saveable;

    int timerid;
    int counter;

    // синхронизация доступа к данным
    QMutex mutex;

};

#endif // A429_WORD_H
