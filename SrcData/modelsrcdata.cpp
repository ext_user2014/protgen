#include "modelsrcdata.h"

ModelSrcData::ModelSrcData(QObject *parent) :
    QAbstractTableModel(parent)
{
    this->columns_header << "ID" << "Канал" << "Вкл" << "Частота" << "Счетчик слов" << "Окно" << "Линия";
    this->srcdatatype = SDT_Net;

    map_pivdata.clear();
}

/******************************************************************
*
*  Заголовки столбцов
*
******************************************************************/
QVariant ModelSrcData::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (role != Qt::DisplayRole) // если не для отображения
        return QVariant();

    // для отображения
    if (orientation == Qt::Horizontal && role == Qt::DisplayRole && section < this->columns_header.size())
        return this->columns_header.at(section);

    return QVariant();
}
/******************************************************************
*
*  Запрос количества столбцов
*
******************************************************************/
int ModelSrcData::columnCount(const QModelIndex &) const
{
    return this->columns_header.size();
}
/******************************************************************
*
*  Запрос количества строк
*
******************************************************************/
int ModelSrcData::rowCount(const QModelIndex &) const
{
    if ( this->srcdatatype == SDT_Net && this->srctcp != 0x0 )
    {
        int ctmp = 0;
        for( int i=0; i<this->srctcp->devices.size(); i++ )
            ctmp += this->srctcp->devices[i]->GetDevSize();

        return ctmp + (srctcp->debuginf.show?2:0); // +2 - это для отладки - две дополнительне строки
    }

    return 0;
}

/******************************************************************
*
*  Флаги записи
*
******************************************************************/
Qt::ItemFlags ModelSrcData::flags(const QModelIndex &index) const
{
    if (!index.isValid())
        return Qt::ItemIsEnabled;

    if( index.column() == 2 || index.column() == 3 || index.column() == 5 )
        return QAbstractTableModel::flags(index) | Qt::ItemIsEditable;
    else if( index.column() == 6 )
        return QAbstractTableModel::flags(index) | Qt::ItemIsDropEnabled;
    else
        return QAbstractTableModel::flags(index);
}
/******************************************************************
*
*  Запрос модельного индекса
*
******************************************************************/
QModelIndex ModelSrcData::index(int row, int column, const QModelIndex &parent) const
{
    if ( !this->hasIndex(row, column, parent) ) // проверка существования индека по родителю, строке и столбцу
        return QModelIndex();

    if ( this->srcdatatype == SDT_Net && this->srctcp != 0x0 ) // проверка источника данных
    {
        return createIndex(row,column,(void*)this->srctcp->GetChannel(row)); // в индекс зашиваем указатель на канал
    }
    return QModelIndex();
}

/******************************************************************
*
*  Запрос данных
*
******************************************************************/
QVariant ModelSrcData::data(const QModelIndex &index, int role) const
{
    if ( !index.isValid() )   // проверка индекса
        return QVariant();

//справка: this->columns_header << "ID" << "Канал" << "Вкл" << "Частота" << "Счетчик слов" << "Окно";
    if ( this->srcdatatype == SDT_Net && this->srctcp != 0x0 ) // проверка источника данных
    {
        A429_Channel* dc = (A429_Channel*)index.internalPointer();

        if ( role == Qt::DisplayRole && srctcp->debuginf.show && index.row() >= (this->rowCount()-2) ) // последняя строка - отладочная
        {
            if( index.row() == (this->rowCount()-1) )
            {
                switch( index.column() )
                {
                case 0:
                    return this->srctcp->debuginf.bytesavailable;
                case 1:
                    return this->srctcp->debuginf.endsimbol3;
                case 2:
                    return this->srctcp->debuginf.msgcount;
                case 3:
                    return this->srctcp->debuginf.strendlist;
                default:
                    return QVariant();
                }
            }
            if( index.row() == (this->rowCount()-2) )
            {
                switch( index.column() )
                {
                case 0:
                    return "bytesavailable";
                case 1:
                    return "endsimbol3";
                case 2:
                    return "msgcount";
                case 3:
                    return "strendlist";
                default:
                    return QVariant();
                }
            }
        }
        else if( role == Qt::DisplayRole )   // данные для отображения
        {
            if( !dc ) return QVariant();

            switch( index.column() )
            {
            case 0:
                return ((A429_Device*)dc->parent())->GetDevID();
            case 1:
                return dc->Num()+1;
            case 2:
                return dc->Open()?"Вкл":"Выкл";
            case 3:
            {
                if( dc->Freq() == FREQ_12 )
                    return "12.5";
                else if( dc->Freq() == FREQ_50 )
                    return "50";
                else
                    return "100";
            }
            case 4:
                return dc->Counter();
            case 5:
                return dc->Size();
            case 6:
            {
                if( this->map_pivdata.contains(index.internalPointer()) )
                {
                    return this->map_pivdata[index.internalPointer()].piv->Path();
                }
                else
                    return QVariant();
            }
            default:
                return QVariant();
            }
        }
        else if( role == Qt::ToolTipRole )   // данные для всплывающей подсказки
        {
            if( !dc ) return QVariant();

            if( index.column() == 4 && dc->DataWord() )
                return "Мгновенное значение:\nВремя: " + dc->DataWord()->GetTime().toString("hh:mm:ss.zzz") + "\n" + "Данные: " + QString::number(dc->DataWord()->GetData(),16);
        }
    }

    return QVariant();
}
/******************************************************************
*
*  Заполнение данными
*
******************************************************************/
bool ModelSrcData::setData(const QModelIndex &index, const QVariant &value, int role)
{
    if ( !index.isValid() )   // проверка индекса
        return false;

    A429_Channel* dc = (A429_Channel*)index.internalPointer();
    if( !dc ) return false;

    if( role != Qt::EditRole )
        return false;

    switch ( index.column() )
    {
    case 2: // вкл/выкл
    {
        this->srctcp->SetChOnOff(((A429_Device*)dc->parent())->GetDevID(),dc->Num(),value.toBool());
        break;
    }
    case 3: // частота
    {
        this->srctcp->SetChFreq(((A429_Device*)dc->parent())->GetDevID(),dc->Num(),(FreqA429)value.toInt());
        break;
    }
    case 5: // глубина канала
    {
        dc->Size(value.toInt());
        break;
    }
    }
    return true;
}
/******************************************************************
*
*  Доступные режимы перетаскивания
*
******************************************************************/
Qt::DropActions ModelSrcData::supportedDropActions() const
{
    return Qt::CopyAction | Qt::MoveAction;
}
/******************************************************************
*
*  Вставка данных при перетаскивании
*
******************************************************************/
bool ModelSrcData::dropMimeData(const QMimeData *data, Qt::DropAction action, int, int, const QModelIndex &parent)
{
    if( !parent.isValid() )
        return false;

    if( !data->hasFormat(MIME_PTR_PIVDATA) )// проверяем тип MIME
        return false;

    if( action != Qt::CopyAction && action != Qt::MoveAction )// проверяем действие с перетаскиваемыми данными
        return false;

    // считываем перетаскиваемые данные
    QByteArray encodedData = data->data(MIME_PTR_PIVDATA);
    QDataStream stream(&encodedData, QIODevice::ReadOnly);
    QList<quint64> ptrs; // список указателей на перетаскиваемые данные
    while (!stream.atEnd())
    {
        quint64 ui = 0x0;
        stream >> ui;
        ptrs.append(ui);
    }

    PivSrc src;
    // получаем указатель на данные и проверяем его
    src.piv = (PivData*)ptrs.at(0); // в ячейке 0 ptrs располагается указатель на PivData
    if( !src.piv )
        return false;
    // получаем указатель на модель-источник PivData
    src.model = (MyItemModel*)ptrs.at(1);// в ячейке 0 ptrs располагается указатель на MyItemModel
    if( !src.model )
        return false;

    //проверяем, что перетащили данные линии
    if( src.piv->info.type != PivData::N_LINE )
        return false;

    // сохраняем указатель
    if( map_pivdata.keys().contains(parent.internalPointer()) ) // проверка наличия данных в карте
    {
        PivData* pdtmp = map_pivdata[parent.internalPointer()].piv;
        disconnect(pdtmp,SIGNAL(signalDistroyed(PivData*)),this,SLOT(slotDeletePivData(PivData*)));
        map_pivdata[parent.internalPointer()].piv = src.piv;
        map_pivdata[parent.internalPointer()].model = src.model;
        connect(src.piv,SIGNAL(signalDistroyed(PivData*)),this,SLOT(slotDeletePivData(PivData*)));
    }
    else
    {
        map_pivdata.insert(parent.internalPointer(),src);
        connect(src.piv,SIGNAL(signalDistroyed(PivData*)),this,SLOT(slotDeletePivData(PivData*)));
    }

    return true;
}
/******************************************************************
*
*  Типы MIME
*
******************************************************************/
QStringList ModelSrcData::mimeTypes(void) const
{
    QStringList sl;
    sl << MIME_PTR_PIVDATA;
    return sl;
}
/******************************************************************
*
*  Обработка сигнала разрушения PivData
*
******************************************************************/
void ModelSrcData::slotDeletePivData(PivData* pd)
{
    for( int i=0;i<map_pivdata.values().size();i++ )
    {
        PivSrc src = map_pivdata.values().at(i);
        if( src.piv == pd )
        {
            disconnect(pd,SIGNAL(signalDistroyed(PivData*)),this,SLOT(slotDeletePivData(PivData*)));
            map_pivdata.remove(map_pivdata.key(src));
            return;
        }
    }
}
A429_Channel* ModelSrcData::GetSrcData(QModelIndex& index)
{
    if( !index.isValid() )
        return 0;

    if( this->srcdatatype == SDT_Net )
    {
        A429_Channel* ch = (A429_Channel*)index.internalPointer();
        return ch;
    }
    else
        return 0x0;
}

PivData* ModelSrcData::GetSrcPiv(QModelIndex& index)
{
    if( !index.isValid() )
        return 0x0;

    if( this->map_pivdata.keys().contains(index.internalPointer()) )
        return this->map_pivdata[index.internalPointer()].piv;
    else
        return 0x0;
}
MyItemModel* ModelSrcData::GetSrcPivModel(QModelIndex& index)
{
    if( !index.isValid() )
        return 0x0;

    if( this->map_pivdata.keys().contains(index.internalPointer()) )
        return this->map_pivdata[index.internalPointer()].model;
    else
        return 0x0;
}
