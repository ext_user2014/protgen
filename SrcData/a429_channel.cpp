#include "a429_channel.h"

// конструктор
A429_Channel::A429_Channel(quint32 num, quint32 s, FreqA429 f, QObject *parent) :
    QObject(parent)
{
    this->device = parent;

    this->Size(s+1); // инициализация очереди +1 на мгновенное значение
    this->data = this->dataofword.dequeue(); // инициализируем указатель на мгновенное значение
    size--;

    this->freq = f;
    this->numchannel = num;
    this->wordcounter = 0x0;
    this->dataofaddr.resize(256);
    for( int i=0; i<256; i++)
    {
        this->listaddrfilter.append(i&0xFF);
        this->dataofaddr[i] = new A429_Word(QTime::currentTime(),0x0,&this->saveable,this);
    }

    this->timerid = this->startTimer(1000);
    this->counter = 0;
    this->saveable = true;
    this->ready = false;
}
// деструктор
A429_Channel::~A429_Channel(void)
{
    this->disconnect();

    this->killTimer(this->timerid);

    this->device = 0x0;

    this->Size(0);
    delete this->data;

    qDeleteAll(this->dataofaddr); // такой способ вызывает оибку, т.к. указатель удаляется сразу, а не в начале следующего цикла обработки
//    for( int i=0; i<256; i++)
//        this->dataofaddr.at(i)->deleteLater();
    this->dataofaddr.clear();
}
// добавление данные в буфер канала
void A429_Channel::AddData(QTime &t, quint32 &d)
{
    QMutexLocker(&this->mutex);

    if( !this->saveable ) return;
    if( !this->listaddrfilter.contains(d&0xFF) ) return;

    this->data->SetData(t,d); // мгновенное значение
    this->dataofword.enqueue(this->data); // вставка в очередь
    this->data = this->dataofword.dequeue(); // выброс из очереди
    this->dataofaddr[d&0xFF]->SetData(t,d); // расклад по адресам

    this->counter = 2; // выставка счётчика готовности
    this->ready = true;
    this->wordcounter++;

    emit this->DataAdded(this->dataofword.last());
}
// установка размера очереди (буфера)
void A429_Channel::Size(int s)
{
    QMutexLocker(&this->mutex);

    while( dataofword.size() < s ) // заполняем очередь пустышками
        dataofword.enqueue(new A429_Word(QTime(), 0x0, &this->saveable, this));

    while( dataofword.size() > s ) // очищаем очередь
        dataofword.dequeue()->deleteLater();

    size = dataofword.size();
}
// очистка данных канала
void A429_Channel::ClearData(void)
{
    QMutexLocker(&this->mutex);

    bool b = this->Saveable();
    this->Saveable(false);
    this->ready = false;
    int s = size;
    this->Size(0);
    this->Size(s);
    this->Saveable(b);
}
// событие таймера
void A429_Channel::timerEvent(QTimerEvent *ev)
{
    if( ev->timerId() != this->timerid )return;

    if( !this->counter )
    {
        this->ready = false; // сброс готовности
        emit this->signalChannelEmpty();
    }
    else
        this->counter--;

}

// функции поиска
int A429_Channel::FindOfAddr(quint8 addr) const
{
    for( int i=0; i<this->dataofword.size(); i++ )
        if( this->dataofword.at(i)->CompareA(addr) )
            return i;
    return -1;

}
int A429_Channel::FindOfTime(QTime t, int deltat) const
{
    for( int i=0; i<this->dataofword.size(); i++ )
        if( this->dataofword.at(i)->CompareT(t,deltat) )
            return i;
    return -1;
}
int A429_Channel::FindOfWord(quint32 w) const
{
    for( int i=0; i<this->dataofword.size(); i++ )
        if( this->dataofword.at(i)->CompareW(w) )
            return i;
    return -1;
}
int A429_Channel::FindOfTimeEx(int pb, int pe, A429_Word* w, int deltat) const
{
    if( pb > pe ) return -1;
    if( !w ) return -1;
    if( pe > this->size ) return -1;

    int c = (pe+pb)/2;
    int dt = this->dataofword[c]->GetTimeMS() - w->GetTimeMS();

    if( abs(dt) <= deltat )
        return c;
    else if( dt > 0 )
        return this->FindOfTimeEx(pb,c-1,w,deltat);
    else if( dt < 0 )
        return this->FindOfTimeEx(c+1,pe,w,deltat);
    else
        return -1;
}

QList<quint8>& A429_Channel::ListAddrFilter(void)
{
    return listaddrfilter;
}
void A429_Channel::ListAddrFilter(QList<quint8>& list)
{
    listaddrfilter.clear();
    listaddrfilter.append(list);
}
void A429_Channel::ListAddrFilter(quint8 addr)
{
    if( !listaddrfilter.contains(addr&0xFF) )
        listaddrfilter.append(addr&0xFF);
}

void A429_Channel::ListAddrFilterReset(void)
{
    listaddrfilter.clear();
    int i=256;
    while(i--)
        listaddrfilter.append(i);
}
/*************************************************************************************
 * получить данные по индеку из буфера канала
 * вход: индекс
 * выход: указатель на класс слова
 * **********************************************************************************/
A429_Word* A429_Channel::DataWord(int i)
{
    QMutexLocker(&this->mutex);

    if( i == -1 )
        return dataofword.last();

    if( i >= this->Size() || i < 0 || i >= this->dataofword.size() )
        return 0x0;

    return dataofword[i];
}


