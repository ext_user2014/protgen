#include "a429_word.h"

// конструктор
A429_Word::A429_Word(QTime t, quint32 d, bool *sav, QObject *parent) :
    QObject(parent)
{
    this->channel = parent;
    this->time = t;
    this->data = d;
    this->saveable = sav;

    this->timerid = this->startTimer(1000);
    this->counter = 2;
    this->ready = true;
}
A429_Word::A429_Word(A429_Word* wrd, QObject *parent)
{
    this->channel = parent;
    this->time.setHMS(wrd->GetTime().hour(),wrd->GetTime().minute(),wrd->GetTime().second(),wrd->GetTime().msec());
    this->data = wrd->GetData();

    this->timerid = this->startTimer(1000);
    this->counter = 2;
    this->ready = true;
}
// деструктор
A429_Word::~A429_Word(void)
{
    QMutexLocker(&this->mutex);
    this->killTimer(this->timerid);
    this->ready = false;
    this->channel = 0x0;
}
// событие таймера
void A429_Word::timerEvent(QTimerEvent *ev)
{
    if( ev->timerId() != this->timerid )
        return;
    if( !*this->saveable )
        return;

    if( !this->counter )
        this->ready = false;
    else
        this->counter--;

}
// установка данных
void A429_Word::SetData(QTime& t, quint32& d)
{
    QMutexLocker(&this->mutex);
    this->time = t;
    this->data = d;
    this->counter = 2;
    this->ready = true;
}

// функции сравнения
bool A429_Word::CompareT(A429_Word* w, quint32 delta)
{
    if( !w ) return false;
    return (quint32)abs(this->GetTimeMS() - w->GetTimeMS()) <= delta;
}
bool A429_Word::CompareT(QTime t, quint32 delta)
{
    int ms = t.msec()+t.second()*1000+t.minute()*60000+t.hour()*3600000;
    return (quint32)abs(ms - this->GetTimeMS()) <= delta;
}

bool A429_Word::CompareW(A429_Word *w)
{
    if( !w ) return false;
    return this->data == w->data;
}
bool A429_Word::CompareW(quint32 w)
{
    return this->data == w;
}
bool A429_Word::CompareA(A429_Word* w)
{
    if( !w ) return false;
    return (this->data & 0xFF) == (w->data & 0xFF);
}
bool A429_Word::CompareA(quint8 addr)
{
    return (this->data & 0xFF) == addr;
}


