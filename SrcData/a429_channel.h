#ifndef A429_CHANNEL_H
#define A429_CHANNEL_H

#include "a429_word.h"
#include "../cfg.h"

enum FreqA429{FREQ_12, FREQ_50, FREQ_100, FREQ_NONE};


class A429_Channel : public QObject
{
    Q_OBJECT
public:
    // конструктор
    explicit A429_Channel(quint32 num, quint32 s, FreqA429 f, QObject *parent = 0);
    // деструктор
    ~A429_Channel(void);


    void AddData(QTime& t, quint32& d); // добавление данные в буфер канала
    void ClearData(void); // очистка данных канала

    void Size(int s); // установка размера очереди (буфера)
    int Size(void) {QMutexLocker(&this->mutex); return this->size;} // возвращает реальный размер буфера

    void Freq(FreqA429 f) {this->freq = f;} // установка частоты канала А429
    FreqA429 Freq(void) const {return this->freq;} // частота канала А429

    void Saveable(bool b) {this->saveable = b;} // установка признака необходимости сохраненя данных в очереди
    bool Saveable(void) const {return this->saveable;}

    A429_Word* DataWord(int i=-1); // получить данные по индеку из буфера канала

    const QVector<A429_Word*>* DataAddr(void) {QMutexLocker(&this->mutex); return &this->dataofaddr;} // возврат ссылки буфера адресов

    bool Ready(void) const {return this->ready;} // флаг приема
    quint32 Num(void) const {return this->numchannel;} // номер канала
    quint32 Counter(void) const {return this->wordcounter;} // счетчик слов

    bool Open(void) const {return this->isopen;} // установка открытия/закрытия приема
    void Open(bool op) {this->isopen = op;}

    int FindOfAddr(quint8 addr) const;
    int FindOfTime(QTime t, int deltat) const;
    int FindOfTimeEx(int pb, int pe, A429_Word* w, int deltat) const;
    int FindOfWord(quint32 w) const;

    QList<quint8>& ListAddrFilter(void);
    void ListAddrFilter(QList<quint8>& list);
    void ListAddrFilter(quint8 addr);
    void ListAddrFilterReset(void);

signals:
    void DataAdded(A429_Word*); // сигнал добавления новых данных
    void signalChannelEmpty(void); // сигнал о пустом канале
private:
    // событие таймера
    void timerEvent(QTimerEvent *ev);

    QObject* device; // плата
    FreqA429 freq;
    QQueue<A429_Word*> dataofword; // данные в очереди (буфер)
    QVector<A429_Word*> dataofaddr; // расклад по адресам
    A429_Word* data; // мгновенное значение
    int size; // размер буфера
    bool ready; // готовность канала (идет прием данных)
    bool saveable; // флаг необходимости сохранения данных
    bool isopen; //  // флаг открытия канала
    quint32 numchannel; // номер канала
    quint32 wordcounter; // счетчик слов
    QList<quint8> listaddrfilter;

    int timerid; // ид. внутреннего таймера
    int counter; // счетчик таймера

    QMutex mutex;
};

#endif // A429_CHANNEL_H
