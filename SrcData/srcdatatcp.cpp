/*****************************************************************

    Модуль сетевого источника данных

    Мордвинкин Ю.Ю.
    17.11.2013

*****************************************************************/
/*****************************************************************

Описание протокола обмена по сети между Программой и сервером - сетевым источнико данных

От клиента к серверу идут три запроса: get_id, set_status, get_status
От сервера клиенту: id, status, in

$get_id фильтр#
    фильтр - MKIO A429 RK
    может быть несколько фильтров через пробел
    если фильтра нет, то считается запрос всех имеющихся плат

$set_status идентификатор вкл парметры#
    идентификатор - "тип паты"_"серийный номер":"номер канала"
    вкл - on или off
    парметры - зависит от платы, для A429 12.5 50 100
    параметров может не быть (зависит от платы)

$get_status идентификатор#
    идентификатор - "тип паты"_"серийный номер":"номер канала"
    если :"номер канала" не указан, то возвратиться статус всей платы

#id "идентификатор"="количество каналов"$

#status идентификатор вкл параметры$
    идентификатор - "тип паты"_"серийный номер":"номер канала"
    вкл - on или off
    параметры - через пробел, зависит от платы, для A429 - 12.5 50 100

#in идентификатор время значения$
    идентификатор - "тип паты"_"серийный номер":"номер канала"
    время - время в формате ч:мин:сек:мсек (ч, мин, сек по 2 символа, мсек - 3 символа)
    Пример:
    #in A429_12409:5 12:00:01.102 F0C68951$


*****************************************************************/


#include "srcdatatcp.h"
#include <math.h>

//#define DEBUGMODE

enum ViewMode{MODE_ADDR,MODE_TIME};

SrcDataTCP::SrcDataTCP(QObject *parent) :
    QTcpSocket(parent)
{
    // инициализация типов устройств
    this->devtypedesc.insert(D_UNKNOWN,"");
    this->devtypedesc.insert(D_A429,"A429");
    this->devtypedesc.insert(D_MKIO,"MKIO");
    this->devtypedesc.insert(D_RK,"RK");

    // инициализация статуса устройств
    this->devices.clear();

    QString pattern; // регулярное выражение для фильтрации входящих сообщений
    // инициализация фильтра
    //                  1тип    2ид  3тип_платы  4номер_платы  5номер_канала      6время                        7данные
    pattern +=     "(?:(#in) (([A429RKMIO]{2,4})_([A-z0-9]+)):([0-9]+) ([0-9]{2}:[0-9]{2}:[0-9]{2}\\.[0-9]{1,3}) ([ABCDEFabcdef0-9]+)\\$)|";
    //                  8       9    10          11            12кол-во_каналов
    pattern +=     "(?:(#id) (([A429RKMIO]{2,4})_([A-z0-9]+))=([0-9]+)\\$)|";
    //                 13       14   15          16            17№канала   18вкл\выкл 19частотаА429
    pattern += "(?:(#status) (([A429RKMIO]{2,4})_([A-z0-9]+)):([0-9]+) (on|off{1}) ?(12\\.5|50|100?)\\$)";
    regdata.setPattern(pattern);
    regmsg.setPattern("(#[^#\\$]+\\$)"); // захват любых символов между # и $

    this->connect(this,SIGNAL(connected()),this,SLOT(slotConnected())); // факт подключения
    this->connect(this,SIGNAL(disconnected()),this,SLOT(slotDisconnected())); // факт отключения
    this->connect(this,SIGNAL(error(QAbstractSocket::SocketError)),this,SLOT(slotError(QAbstractSocket::SocketError))); // обработка ошибок
    this->connect(this,SIGNAL(readyRead()),this,SLOT(slotRead())); // чтение с порта
    this->connect(this,SIGNAL(stateChanged(QAbstractSocket::SocketState)),this,SLOT(slotState(QAbstractSocket::SocketState))); // изменение состояния

    this->isconnected = true;
    this->hostport = 0;

    timerAutoConnect.start(1000);
    connect(&timerAutoConnect,SIGNAL(timeout()),this,SLOT(slotAutoConnect()));
    timerAutoGetStatus.start(500);
    connect(&timerAutoGetStatus,SIGNAL(timeout()),this,SLOT(slotAutoGetStatus()));

    connect(this,SIGNAL(ReadyDataForParser(int)),SLOT(slotParser(int)));

    // Загружаем список серверов
    StaticData::settings->beginGroup("SrcDataTCP");
    slist_ipp.append(StaticData::settings->value("servers","").toStringList());
    StaticData::settings->endGroup();

    // инициализация отладочной структуры
    debuginf.show = false;
    debuginf.bytesavailable = 0x0;
    debuginf.endsimbol3 = "";
    debuginf.msgcount = 0x0;
    debuginf.strendlist = "";
}
SrcDataTCP::~SrcDataTCP()
{
    this->disconnectFromHost();
    qDeleteAll(devices);
    devices.clear();
}

/******************************************************************
* Установка адреса сервера
* вход: IP и порт
* выход: нет
******************************************************************/
void SrcDataTCP::SetHostIPPort(QString ipp)
{
    QRegExp reg;
    reg.setPattern("([0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}):([0-9]{1,5})");
    if( !ipp.contains(reg) )
        return;

    reg.indexIn(ipp);

    this->hostaddr.setAddress(reg.cap(1));
    this->hostport = (quint16)(reg.cap(2).toUInt() & 0xFFFF);
}
/******************************************************************
* Отправка сообщения серверу
* вход: посылаемое сообщение
* выход: нет
******************************************************************/
void SrcDataTCP::SendMessToHost(QByteArray mess)
{
    if( this->isWritable() )
    {
        this->write(mess);
        emit this->MessTrn(mess);
    }
}
void SrcDataTCP::SendMessToHost(void) // отправка текущей команды (по таймеру)
{
    if( this->isWritable() )
    {
        this->SendMessToHost(this->currentcommand.toAscii());
        this->currentcommand = "";
    }
}
/******************************************************************
* Соединение с сервером
* вход: нет
* выход: результат соединения
******************************************************************/
bool SrcDataTCP::ConnectToHost(void)
{
    this->disconnectFromHost();
    this->connectToHost(this->hostaddr, this->hostport);
    return true;
}
/******************************************************************
* Обработка сигнала соединения
* вход: нет
* выход: нет
******************************************************************/
void SrcDataTCP::slotConnected(void)
{
    this->SendMessToHost(this->GetCommandString(D_ALL));
    this->isconnected = true;
    QString str = hostaddr.toString() + ":" + QString().number(hostport);
    emit signalConnected(str);

    // сохраняем параметры сервера
    if( !slist_ipp.contains(str) ) // в списке нет, сохраняем
        slist_ipp.append(str);
    StaticData::settings->beginGroup("SrcDataTCP");
    StaticData::settings->setValue("servers",slist_ipp); // выгружаем в ини-файл
    StaticData::settings->endGroup();
}
/******************************************************************
* Обработка сигнала разъдинения
* вход: нет
* выход: нет
******************************************************************/
void SrcDataTCP::slotDisconnected(void)
{
    qDeleteAll(devices);
    devices.clear();

    this->isconnected = false;
    emit this->ChangeStatus();
    this->ChangeStatus("Пытаемся соединиться");

}
/******************************************************************
* Обработка сигналов ошибок
* вход: идентификатор ошибки
* выход: нет
******************************************************************/
void SrcDataTCP::slotError(QAbstractSocket::SocketError err)
{
    switch( err )
    {
    case ConnectionRefusedError:
        emit this->ChangeStatus("Соединение было разорвано другим узлом (или по тайм-ауту)");
        break;
    case RemoteHostClosedError:
        emit this->ChangeStatus("Удалённый узел закрыл соединение");
        break;
    case HostNotFoundError:
        emit this->ChangeStatus("Адрес узла не найден");
        break;
    case SocketAccessError:
        emit this->ChangeStatus("Операция с сокетом была прервана, так как приложение не получило необходимых прав");
        break;
    case SocketResourceError:
        emit this->ChangeStatus("У текущей системы не хватило ресурсов (например, слишком много сокетов)");
        break;
    case SocketTimeoutError:
        emit this->ChangeStatus("Время для операции с сокетом истекло");
        break;
    case UnknownSocketError:
        emit this->ChangeStatus("Произошла неопределённая ошибка");
        break;
    default:
        emit this->ChangeStatus("Остальные ошибки");
        break;
    }

}
/******************************************************************
* Обработка готовности к чтению из сокета
* вход: нет
* выход: нет
******************************************************************/
void SrcDataTCP::slotRead(void)
{
    QByteArray data;

#ifdef DEBUGMODE
    data = testdata.toAscii();
#else
    data = this->readAll();
#endif


    // захват данных и разделение сообщений
    int pos = 0;
    int count = 0;
    while( (pos = regmsg.indexIn(data,pos)) != -1 )
    {
        list_msg.enqueue(regmsg.cap(1));
        pos += regmsg.matchedLength();
        count++;
    }

    this->debuginf.bytesavailable = data.size();
    this->debuginf.endsimbol3 = data.right(3);
    this->debuginf.msgcount = list_msg.size();
    this->debuginf.strendlist = list_msg.last();

    emit this->ReadyDataForParser(count);
}
/******************************************************************
* Обработка принятых сообщений
* вход: кол-во принятых сообщений в slotRead
* выход: нет
******************************************************************/
void SrcDataTCP::slotParser(int count)
{
    int cnt = count;
    QString strdata = "";
    while(cnt--)
    {
        strdata = list_msg.dequeue();
        // фильтруем
        this->regdata.indexIn(strdata);
        QStringList listdata = this->regdata.capturedTexts();
        if( listdata.at(0).isEmpty() )
        {
            emit this->MessRcv("Неверный формат: " + strdata);
            continue;
        }

        if( !listdata.at(1).isEmpty() ) // это #in
        {
            if( this->devtypedesc.key(listdata.at(3)) == D_A429 )
            {
                A429_Channel* channel = (A429_Channel*)this->GetChannel(listdata.at(2),(quint8)listdata.at(5).toUInt());
                if( !channel )
                    emit this->MessRcv("Канал " + listdata.at(5) +  " на устройстве '"+ listdata.at(2) + "' не инициализирован");
                else
                {
                    QTime t = QTime::fromString(listdata.at(6),"hh:mm:ss.zzz");
                    quint32 d = listdata.at(7).toUInt(0,16);
                    channel->AddData(t, d);
                }
            }
            else
                emit this->MessRcv("Устройство не поддерживается: " + listdata.at(2));
        }
        else if( !listdata.at(8).isEmpty() ) // это #id
        {
            if( this->devtypedesc.values().contains(listdata.at(10)) ) // при известном устройстве сохраняем его и инициализируем статус
            {
                if( this->devtypedesc.key(listdata.at(10)) == D_A429 )
                {
                    A429_Device* dev = new A429_Device(listdata.at(9), this->devtypedesc.key(listdata.at(10)), (quint8)listdata.at(12).toUInt(), this);
                    for( int i=0; i<this->devices.size(); i++ )
                    {
                        if( this->devices[i]->GetDevID() == dev->GetDevID() )
                        {
                            delete this->devices[i];
                            this->devices.removeAt(i);
                            break;
                        }
                    }
                    this->devices.append(dev);
                    emit this->MessRcv("Идентификатор: " + listdata.at(9) + " к=" + listdata.at(12));
                }
                else
                    emit this->MessRcv("Устройство не поддерживается: " + listdata.at(10));
            }
            else
                emit this->MessRcv("Устройство не опознано: " + listdata.at(10));
        }
        else if( !listdata.at(13).isEmpty() ) // это #status
        {
            if( this->devtypedesc.key(listdata.at(15)) == D_A429 )
            {
                A429_Channel* channel = (A429_Channel*)this->GetChannel(listdata.at(14),(quint8)listdata.at(17).toUInt());
                if( !channel )
                    emit this->MessRcv("Канал " + listdata.at(17) +  " на устройстве '"+ listdata.at(14) + "' не инициализирован");
                else
                {
                    if( listdata.at(19) == "12.5" )
                        channel->Freq(FREQ_12);
                    else if( listdata.at(19) == "50" )
                        channel->Freq(FREQ_50);
                    else
                        channel->Freq(FREQ_100);

                    channel->Open(listdata.at(18)=="on");

                    emit this->MessRcv("Статус изменен: к" + listdata.at(17) + "; " + listdata.at(18) + "; " + listdata.at(19));
                    emit this->ChangeStatus();
                }
            }
            else
                emit this->MessRcv("Устройство не поддерживается: " + listdata.at(14));
        }
        else
        {
            emit this->MessRcv("Неизвестное сообщение");
        }
        emit this->MessRcv();
    }
}

/******************************************************************
* Обработка сигнала изменения статуса соединения
* вход: статус соединения
* выход: нет
******************************************************************/
void SrcDataTCP::slotState(QAbstractSocket::SocketState stat)
{
    switch( stat )
    {
    case UnconnectedState:
        emit this->ChangeStatus("Сокет не соединён");
        break;
    case HostLookupState:
        emit this->ChangeStatus("Сокет выполняет поиск имени узла");
        break;
    case ConnectingState:
        emit this->ChangeStatus("Сокет начинает устанавливать соединение");
        break;
    case ConnectedState:
        emit this->ChangeStatus("Соединение установлено");
        break;
    case BoundState:
        emit this->ChangeStatus("Сокет связан с адресом и портом (для серверов)");
        break;
    case ClosingState:
        emit this->ChangeStatus("Сокет готовится к закрытию (данные всё ещё ожидают записи)");
        break;
    case ListeningState:
        emit this->ChangeStatus("Только для внутреннего использования");
        break;
    }
}
/******************************************************************
* Формирование команд (перегруз функций)
* вход: тип устройств
*       идентификатор устройства, канал
*       идентификатор устройства, канал, вкл/выкл, частота канала
*       идентификатор устройства, канал, вкл/выкл
*       массив строк для тестирования
* выход: команда для отправки в сокет
******************************************************************/
QByteArray SrcDataTCP::GetCommandString(DevType filter) // get_id
{
    QString str = "";
    switch( filter )
    {
    case D_ALL:
        str = "#get_id$";
        break;
    case D_A429:
        str = "#get_id A429$";
        break;
    case D_MKIO:
        str = "#get_id MKIO$";
        break;
    case D_RK:
        str = "#get_id RK$";
        break;
    case D_UNKNOWN:
        str = "#$";
        break;
    }
    return str.toAscii();
}

QByteArray SrcDataTCP::GetCommandString(QString id, int channel) // get_status
{
    QString str = "";
    str = "#get_status " + id + ":" + QString::number(channel) + "$";
    return str.toAscii();
}

QByteArray SrcDataTCP::GetCommandString(QString id, int channel, bool onoff, FreqA429 freq) // set_status для А429
{
    QString str = "";

    str = "#set_status " + id + ":" + QString::number(channel) + (onoff?" on ":" off ");
    if( freq == FREQ_12 )
        str += "12.5";
    else if( freq == FREQ_50 )
        str += "50";
    else
        str += "100";

    str += "$";
    return str.toAscii();
}

QByteArray SrcDataTCP::GetCommandString(QString id, int channel, bool onoff) // set_status
{
    QString str = "";
    str = "#set_status " + id + ":" + QString::number(channel) + (onoff?"on ":"off ") + "$";
    return str.toAscii();
}

QByteArray SrcDataTCP::GetCommandString(QStringList param) // test
{
    QString str = "";
    str = "#test ";
    foreach (QString string, param)
        str += string;
    str += "$";
    return str.toAscii();
}


/******************************************************************
* Изменение частоты работы канала А429
* вход: идентификатор устройства, канал, частота
* выход: нет
******************************************************************/
void SrcDataTCP::SetChFreq(QString devid, quint8 channel, FreqA429 freq)
{
    A429_Channel* dc = (A429_Channel*)this->GetChannel(devid,channel);

    if( !dc ) return;

    this->SendMessToHost(this->GetCommandString(devid,channel,dc->Open(),freq)); // настройка канала
}
/******************************************************************
* Вкл/выкл канал
* вход: устройство, канал, вкл/выкл
* выход: нет
******************************************************************/
void SrcDataTCP::SetChOnOff(QString devid, quint8 channel, bool onoff)
{
    A429_Channel* dc = (A429_Channel*)this->GetChannel(devid,channel);

    if( !dc ) return;

    if( ((A429_Device*)dc->parent())->GetDevType() == D_A429 )
        this->SendMessToHost(this->GetCommandString(devid,channel,onoff,dc->Freq())); // настройка канала
    else
        this->SendMessToHost(this->GetCommandString(devid,channel,onoff)); // настройка канала
}
/******************************************************************
* Получить указатель на канал
* вход: устройство, канал
*       порядковый номер
* выход: указатель на канал
******************************************************************/
QObject* SrcDataTCP::GetChannel(QString id, quint8 ch)
{
    for( int i=0; i< this->devices.size(); i++ )
        if( (this->devices[i]->GetDevID() == id) && (ch < this->devices[i]->GetDevSize()) )
            return this->devices[i]->GetChPtr(ch);
    return 0;
}
QObject *SrcDataTCP::GetChannel(quint32 pos) // по порядковому номеру, начиная с 0
{
    quint32 _pos = pos;
    for( int i=0; i<this->devices.size(); i++ )
    {
        if( _pos < this->devices.at(i)->GetDevSize() )
            return this->devices.at(i)->GetChPtr(_pos);
        else
            _pos -= this->devices.at(i)->GetDevSize();
    }
    return 0;
}

/******************************************************************
* Отладочные слоты
* выход: нет
* выход: нет
******************************************************************/
void SrcDataTCP::slotDebug(void)
{
    static int i = 0;

    if(i==0)
        testdata = "#id A429_12568=16$#id A429_12548=8$";
    else if(i==1)
        testdata = "#status A429_12568:1 on 12.5$";
    else
    {
        QTime t1, t2;
        t1 = QTime::currentTime();
        t2.setHMS(t1.hour(),t1.minute(),t1.second(),t1.msec()/*+(rand()&0x3)*/);
//        t1.setHMS(10,32,33,rand()&0xF);
//        t2.setHMS(10,32,33,rand()&0xF);
        testdata =  "#in A429_12568:2 " + t1.toString("hh:mm:ss.zzz") + " " + QString::number((rand()&0xFFC03F00)|0x20|(0x41<<14),16)+"$";
        testdata +=  "#in A429_12568:2 " + t1.toString("hh:mm:ss.zzz") + " " + QString::number((rand()&0xFFFFFF00)|0x21,16)+"$";
        testdata += "#in A429_12568:4 " + t2.toString("hh:mm:ss.zzz") + " " + QString::number(rand()&0xFFFFFFFF,16)+"$";
    }
    this->slotRead();

    i++;
}
void SrcDataTCP::slotDebugStart(void)
{
#ifdef DEBUGMODE

    timerDebug.start(2);
    connect(&timerDebug,SIGNAL(timeout()),this,SLOT(slotDebug()));

#endif
}
