#ifndef MODELSRCDATA_H
#define MODELSRCDATA_H

#include "../cfg.h"
#include "srcdatatcp.h"
#include "srcdatafile.h"
#include "../pivdata.h"
#include "../PivEditor/MyItemModel.h"


class ModelSrcData : public QAbstractTableModel
{
    Q_OBJECT
public:
    explicit ModelSrcData(QObject *parent = 0);

    // функции интерфейса
    int rowCount(const QModelIndex &parent = QModelIndex()) const;              // количество строк
    int columnCount(const QModelIndex &parent = QModelIndex()) const;           // количество столбцов
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const;  // выборка данных по роли
    Qt::ItemFlags flags(const QModelIndex &index) const;                        // установка флагов
    QVariant headerData(int section, Qt::Orientation orientation, int role) const; // установка заголовков
    bool setData(const QModelIndex &index, const QVariant &value, int role);    // установка данных записи
    QModelIndex index(int row, int column, const QModelIndex &parent) const;    // модельный индекс

    enum SrcDataType{SDT_Net,SDT_File};
    void SetSrcDataPointer(SrcDataTCP* src = 0x0) {srctcp = src;}
    void SetSrcDataPointer(SrcDataFile* src = 0x0) {srcfile = src;}

    Qt::DropActions supportedDropActions() const; // поддерживаемые типы вставки
    bool dropMimeData(const QMimeData *data, Qt::DropAction action, int row, int column, const QModelIndex &parent); // вставка данных при перетаскивании
    QStringList mimeTypes(void) const; // Список миме типов

    // запрос источника данных и привязанного ПИВ
    A429_Channel *GetSrcData(QModelIndex&);
    PivData* GetSrcPiv(QModelIndex&);
    MyItemModel *GetSrcPivModel(QModelIndex&);
signals:
    
public slots:
    void SetSrcDataType(int sdt = (int)SDT_Net) {this->srcdatatype = (SrcDataType)sdt; this->layoutChanged();}
    void slotDeletePivData(PivData*);
private:
    SrcDataTCP* srctcp;
    SrcDataFile* srcfile;

    SrcDataType srcdatatype; // тип источника

    QStringList columns_header; // заголовки таблицы

    class PivSrc
    {
    public:
        PivSrc(void):piv(0x0),model(0x0){}

        PivData* piv;
        MyItemModel* model;

        friend bool operator==(const PivSrc& l, const PivSrc& r)
        {
            return (l.model == r.model) && (l.piv == r.piv);
        }
    };
    QMap<void*,PivSrc> map_pivdata; // карта привязки данных к каналу (<указатель_на_канал,класс с источниками>)

};
#endif // MODELSRCDATA_H
