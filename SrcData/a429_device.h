#ifndef DEVA429_H
#define DEVA429_H

#include "device.h"
#include "a429_channel.h"
#include "../cfg.h"


class A429_Device : public Device
{
    Q_OBJECT
friend class Device;
public:
    explicit A429_Device(QString id, DevType type, quint8 ch, QObject *parent = 0);
    ~A429_Device();

    quint8  GetDevSize(void) const;
    A429_Channel* GetChPtr(quint8 num) const;

private:
    quint8 dev_ch_count; // кол-во каналов на плате
    QVector<A429_Channel*> dev_ch; // список указателей каналов
};


#endif // DEVA429_H
