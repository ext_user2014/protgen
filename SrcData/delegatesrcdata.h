#ifndef DELEGATESRCDATA_H
#define DELEGATESRCDATA_H

#include "../cfg.h"
#include "srcdatatcp.h"
#include "srcdatafile.h"
#include "modelsrcdata.h"

class DelegateSrcData : public QItemDelegate
{
    Q_OBJECT
public:
    explicit DelegateSrcData(QObject *parent = 0);
    ~DelegateSrcData(void);

    QWidget *createEditor(QWidget *parent, const QStyleOptionViewItem &option, const QModelIndex &index) const;    // создание редактора
    void setEditorData(QWidget *editor, const QModelIndex &index) const;         // загрузка данных в редактор
    void setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const;  // загрузка данных из редактора в модель данных

};

class WinData : public QFrame
{
    Q_OBJECT
public:
    explicit WinData(A429_Channel* d, QWidget *parent = 0);
    ~WinData(void);

    int GetValue(void){return sbw->value();}
    void SetValue(int w){sbw->setValue(w);}

public slots:
    void SetValMS(int v);
    void SetValW(int v);

private:
    QSpinBox* sbms;
    QSpinBox* sbw;
    QSlider* sms;
    QSlider* sw;

    A429_Channel* dc;
    void SetDevChannel(A429_Channel* const v);
};


#endif // DELEGATESRCDATA_H
