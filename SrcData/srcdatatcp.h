/*****************************************************************

    Модуль сетевого источника данных

    Мордвинкин Ю.Ю.
    17.11.2013

*****************************************************************/
#ifndef SRCDATATCP_H
#define SRCDATATCP_H

#include "../cfg.h"
#include "a429_device.h"
#include "device.h"

#define MAX_SIZECHANNEL 1000
#define MIN_SIZECHANNEL 1

class SrcDataTCP : public QTcpSocket
{
    Q_OBJECT
public:
    explicit SrcDataTCP(QObject *parent = 0);
    virtual ~SrcDataTCP();

    // данные
    QList<A429_Device*> devices; // список устройств

    // функции
    QObject* GetChannel(QString id, quint8 ch); // получить указатель на канал
    QObject* GetChannel(quint32 pos); // получить указатель на канал по порядковому номеру, начиная с 0

    void SetChFreq(QString devid, quint8 channel, FreqA429 freq); // изменение частоты работы канала А429
    void SetChOnOff(QString devid, quint8 channel, bool onoff); // вкл/выкл канала А429

    struct dinf
    {
        bool show;
        int bytesavailable;
        QString endsimbol3;
        int msgcount;
        QString strendlist;
    }debuginf;

    QStringList slist_ipp; // список ранее подключенных серверов (адрес и порт)

signals:
    void ChangeStatus(QString); // сигнал изменения статуса (сетевой канал)
    void ChangeStatus(void); // сигнал изменения статуса (канал платы)
    void MessRcv(QString); // сигнал сообщения приемника (для отладки)
    void MessRcv(); // сигнал приемника о завершении обработки одной посылки
    void MessTrn(QString); // сигнал сообщения передатчика  (для отладки)

    void ReadyDataForParser(int);

    void signalConnected(QString);
public slots:
    void SetHostIPPort(QString ipp); // установка адреса и порта
    void SendMessToHost(QByteArray mess); // отпрака сообщений серверу
    bool ConnectToHost(void); // соединение с сервером

    void slotDebugStart(void); // отладочный слот

    void slotDebugShow(bool b){debuginf.show = b;}

private slots:
    void slotConnected(void); // обработка сигнала соединения
    void slotDisconnected(void); // обработка сигнала разъдинения
    void slotError(QAbstractSocket::SocketError err); // обработка ошибок
    void slotRead(void); // обработка сигнала готовности чтения
    void slotState(QAbstractSocket::SocketState stat); // обработка сигнала изменения статуса соединения

    void slotParser(int count);

    void slotAutoConnect(void) // автоподключение при отсутствии соединения
    {
        if( !this->isconnected && !this->hostaddr.isNull() && this->hostport )
            this->ConnectToHost();
    }

    void slotAutoGetStatus(void) // автоопределение статуса
    {
        this->currentcommand = "";

        for( int i=0; i< devices.size(); i++ )
            for( int j=0; j<devices[i]->GetDevSize(); j++ )
                this->currentcommand += this->GetCommandString(devices[i]->GetDevID(),j);

        this->SendMessToHost(); // отправка текущей команды
    }

    void slotDebug(void); // отладочный слот

private:

    void SendMessToHost(void); // отпрака сообщений серверу (текущая команда из списка)

    QMap<DevType,QString> devtypedesc; // типы устройств и их названия в сети
    QString currentcommand; // текущая отправляемая команда серверу
    QHostAddress hostaddr; // адрес сервера
    quint16 hostport; // порт на сервере

    QByteArray GetCommandString(DevType filter = D_ALL); // get_id
    QByteArray GetCommandString(QString id, int channel); // get_status
    QByteArray GetCommandString(QString id, int channel, bool onoff, FreqA429 freq); // set_status для А429
    QByteArray GetCommandString(QString id, int channel, bool onoff); // set_status
    QByteArray GetCommandString(QStringList param); // test

    QRegExp regdata; // регулярное выражение для фильтрации входящих сообщений
    QRegExp regmsg; // регулярное выражение для захвата сообщений

    // это для отладки
    QString testdata;

    bool isconnected;

    QTimer timerAutoConnect;
    QTimer timerAutoGetStatus;

    QTimer timerDebug;

    QQueue<QString> list_msg;
};



#endif // SRCDATATCP_H
