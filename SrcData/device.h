#ifndef DEVICE_H
#define DEVICE_H

#include <QObject>

enum DevType{D_ALL, D_UNKNOWN, D_A429, D_MKIO, D_RK};

class Device : public QObject
{
    Q_OBJECT
public:
    explicit Device(QObject *parent = 0);
    
    DevType GetDevType(void) const;
    QString GetDevID  (void) const;
protected:
    DevType dev_type; // тип платы
    QString dev_id; // идентификатор платы
    
};

#endif // DEVICE_H
