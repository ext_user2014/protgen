#include "a429_device.h"


A429_Device::A429_Device(QString id, DevType type, quint8 ch, QObject *parent) :
    Device(parent)
{
    this->dev_id = id;
    this->dev_type = type;
    this->dev_ch_count = ch;

    for( int i=0; i<this->dev_ch_count; i++ )
        this->dev_ch.append(new A429_Channel(i, 256, FREQ_NONE, this));
}
A429_Device::~A429_Device()
{
    for( int i=0; i<this->dev_ch_count; i++ )
        this->dev_ch.at(i)->deleteLater();

    this->dev_type = D_UNKNOWN;
    this->dev_id = "";
    this->dev_ch_count = 0x0;
}

quint8  A429_Device::GetDevSize(void) const
{
    return this->dev_ch_count;
}

A429_Channel *A429_Device::GetChPtr(quint8 num) const
{
    if( num < this->dev_ch_count )
        return this->dev_ch.at(num);
    return 0x0;
}
