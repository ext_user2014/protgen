#include "modelA429mon.h"

/******************************************************************
*
*  Конструктор и деструктор модели
*
******************************************************************/
ModelA429Mon::ModelA429Mon(QList<A429_Channel *> &dc, QObject *parent) :
    QAbstractTableModel(parent)
{
    this->list_devch = dc;
    this->currentdevch = 0x0;
    this->pausa = false;
    this->mode = MODE_TIME;
    this->syncon = false;
    this->deltat = 0;

    for( int i=0;i<dc.size();i++ ) // цепляем сигнал обновления представления при поступлении слова в к-л канал
        connect(dc[i],SIGNAL(DataAdded(A429_Word*)),this,SIGNAL(layoutChanged()));
}
ModelA429Mon::~ModelA429Mon(void)
{
    disconnect();
    // очищаем фильтр адресов в канале
    for( int i=0; i<this->list_devch.size(); i++ )
        this->list_devch.at(i)->ListAddrFilterReset();
}

/******************************************************************
*
*  Флаги
*
******************************************************************/
Qt::ItemFlags ModelA429Mon::flags(const QModelIndex &index) const
{
    if (!index.isValid())
        return Qt::ItemIsEnabled;

    if( !index.row() && (index.column()%2) )
        return QAbstractTableModel::flags(index) | Qt::ItemIsEditable;
    else
        return QAbstractTableModel::flags(index);
}

/******************************************************************
*
*  Индекс
*
******************************************************************/
QModelIndex ModelA429Mon::index(int row, int column, const QModelIndex &parent) const
{
    if ( !this->hasIndex(row, column, parent) ) // проверка существования индека по родителю, строке и столбцу
        return QModelIndex();

    if( this->mode == MODE_TIME ) // режим буфера
    {
        int numdevch = column / 2; // номер канала

        if( numdevch >= this->list_devch.size() ) // номер канала выпадает из списка
            return QModelIndex();

        A429_Channel* dc = this->list_devch.at(numdevch); // указатель на канал

        if(dc) // указатель существует
            return createIndex(row,column,(void*)dc); // в индекс зашиваем указатель на канал
        else
            return QModelIndex();

    }
    else if( this->mode == MODE_ADDR ) // режим  адресов
    {
        if( !this->currentdevch ) // текущий канал не установлен
            return QModelIndex();

        int subnumcol = column % 2; // все нечетные (остаток = 1) - это данные, иначе текст с адресом
        quint32 addr = (row + (column/2)*MODEADRR_ROWCOUNT) & 0xFF;

        A429_Word* wrd = this->currentdevch->DataAddr()->at(addr); // указатель на данные

        if( subnumcol ) // данные
            return createIndex(row,column,(void*)wrd); // в индекс зашиваем указатель на слово
        else // текст с адресом
            return createIndex(row,column,addr); // в индекс зашиваем адрес слова
    }
    else
        return QModelIndex();

}

/******************************************************************
*
*  Количество строк
*
******************************************************************/
int ModelA429Mon::rowCount(const QModelIndex &) const
{
    int r = 0;

    if( this->mode == MODE_TIME )
    {
        for( int i=0; i< this->list_devch.size(); i++ )
            if( r < this->list_devch.at(i)->Size() )
                r = this->list_devch.at(i)->Size();
        r += 1; // первая строка - настройки фильтрации
    }
    else if( this->mode == MODE_ADDR )
    {
        if( !this->currentdevch ) return 0;
        r = MODEADRR_ROWCOUNT;
    }

    return r;
}
/******************************************************************
*
*  Количество столбцов
*
******************************************************************/
int ModelA429Mon::columnCount(const QModelIndex &) const
{
    int c = 0;

    if( this->mode == MODE_TIME )
    {
        c = this->list_devch.size() * 2; // т.к. данные занимают два столбца (время и данные)
    }
    else if( this->mode == MODE_ADDR )
    {
        if( !this->currentdevch ) return 0;
        c = 2*(256/MODEADRR_ROWCOUNT);
    }

    return c;
}
/******************************************************************
*
*  Запрос данных по модельному индексу
*
******************************************************************/
QVariant ModelA429Mon::data(const QModelIndex &index, int role) const
{
    if( role == Qt::DisplayRole ) // отрисовка ключевых данных
    {
        if( this->mode == MODE_TIME )
        {
            if( !index.isValid() )
                return QVariant();

            int subnumcol = index.column() % 2; // тип столбеца (0 - время, 1 - данные)

            A429_Channel* dc = (A429_Channel*)index.internalPointer(); // указатель на канал

            if( index.row() ) // начинаем со второй строки, т.к. первая - это настройки фильтра
            {
                A429_Word* wrd = 0x0; // указатель на данные

                if( this->syncon && this->currentdevch != dc && this->currentdevch != 0x0 ) // синхронизирующийся канал
                {
                    wrd = this->currentdevch->DataWord(this->currentdevch->Size() - index.row()); // данные для синхронизации

                    int pos = dc->FindOfTimeEx(0,dc->Size()-1,wrd,this->deltat); // ищем синхронные данные
                    if( pos == -1 ) // не найдено, данных нет
                        wrd = 0x0;
                    else // найдено
                        wrd = dc->DataWord(pos);
                }
                else
                    wrd = dc->DataWord(dc->Size() - index.row());


                if( wrd && (this->pausa || dc->Ready()) ) // есть слово и (пауза или данные готовы)
                {
                    if( subnumcol == 0 )
                        return wrd->GetTime().toString("hh:mm:ss.zzz");
                    else if( subnumcol == 1 )
                        return QString().sprintf("%0*X",8,wrd->GetData());
                    else
                        return "---";
                }
                else // нет слова, нет данных при выключенной паузе
                    return "---";
            }
            else // фильтр адреса
            {
                int subnumcol = index.column() % 2; // тип столбеца (0 - время, 1 - данные)
                if( subnumcol && (dc->ListAddrFilter().size() == 256) )
                    return "Все Адреса";
                if( subnumcol && (dc->ListAddrFilter().size() == 0) )
                    return "Нет выбраных";
                if( subnumcol )
                    return "Адреса...";
                else
                    return "Фильтр А:";
            }
        }
        else if( this->mode == MODE_ADDR )
        {
            if( !index.isValid() )
                return QVariant();

            int subnumcol = index.column() % 2;

            if( subnumcol ) // колонка данных
            {
                A429_Word* wrd = (A429_Word*)index.internalPointer(); // указатель на данные
                if( wrd->GetReady() || (this->pausa && wrd->GetReady()) )
                    return QString().sprintf("%0*x",8,wrd->GetData()).toUpper();
                else
                    return "---";
            }
            else // колонка адреса
            {
                int addr = index.internalId();
                return "A" + QString::number(addr,8) + "(" + QString().sprintf("%0*x",2,addr).toUpper() + ")";
            }
        }

    }
    else if( role == Qt::BackgroundColorRole ) // окраска фона ячеек
    {
        int subnumcol = index.column() % 2;

        if( this->mode == MODE_TIME )
        {
            if( !subnumcol && index.row() )
                return QColor(143,255,143); //окраска столбцов с временем
            else if( !subnumcol )
                return QColor(Qt::cyan); // первая строка в столбце времени
            else if( !index.row() )
                return QColor(Qt::cyan); // первая строка в столбце значения
        }
        else if( this->mode == MODE_ADDR )
        {
            quint32 addr = (index.row() + (index.column()/2)*MODEADRR_ROWCOUNT) & 0xFF;

            if( !this->currentdevch )
                return QVariant();

            if( !subnumcol )
                return QColor(143,255,143); // окраска столбцов с адресом
            else if( (this->currentdevch->DataWord()->GetData() & 0xFF) == addr && this->currentdevch->Saveable() )
                return QColor(255,255,137); // подсвет мгновенного значения, если идет прием
            else if( !this->currentdevch->DataAddr()->at(addr)->GetReady() )
                return QColor(192,192,192); // подсвет неготовых адресов

        }
    }


    return QVariant();
}
/******************************************************************
*
*  Отображение заголовков таблицы
*
******************************************************************/
QVariant ModelA429Mon::headerData(int section, Qt::Orientation orientation, int role) const
{
    if( role != Qt::DisplayRole )
        return QVariant();

    if( orientation == Qt::Vertical )
        return QVariant();

    if( this->mode == MODE_TIME )
    {
        int numdevch = section / 2;
        int subnumcol = section % 2;

        if( numdevch >= this->list_devch.size() )
            return QVariant();

        A429_Channel* dc = this->list_devch.at(numdevch);

        if( subnumcol == 0 )
            return "К" + QString::number(dc->Num()+1) + " Время";

        if( subnumcol == 1 )
            return "К" + QString::number(dc->Num()+1) + " Данные";
    }
    else if( this->mode == MODE_ADDR )
    {
        if( this->currentdevch == 0x0 )
            return QVariant();

        int subnumcol = section % 2;

        if( subnumcol == 0 )
            return "Адрес";

        if( subnumcol == 1 )
            return "К" + QString::number(this->currentdevch->Num()+1) + " Данные";
    }

    return QVariant();
}
/******************************************************************
*
*  Установить текущий канал
*
******************************************************************/
void ModelA429Mon::SetCurrentDevch(QModelIndex index)
{
    if( this->mode == MODE_TIME )
        this->currentdevch = (A429_Channel*)index.internalPointer();
    else
        this->currentdevch = 0x0;
}
/******************************************************************
*
*  Установить выделенные слова в режиме просмотра адресов
*
******************************************************************/
void ModelA429Mon::SetCurrentWords(QModelIndexList indexes)
{
    if( indexes.isEmpty() ) // очищаем выделенные слова, если список пуст
    {
        this->currentwords.clear();
        qDeleteAll(this->currentwordpos);
        this->currentwordpos.clear();
        return;
    }

    if( this->Mode() == MODE_ADDR )
    {
        if( !this->currentdevch ) return; // канал не выбран, выходим

        this->currentwords.clear();
        qDeleteAll(this->currentwordpos);
        this->currentwordpos.clear();
        for( int i=0; i<indexes.size(); i++ )
        {
            if( indexes.at(i).column() % 2 )
                this->currentwords.append((A429_Word*)indexes.at(i).internalPointer()); // в режиме адресов в индексе имеется указатель на слово
        }
        emit this->SelectWords(&this->currentwords); // отправка сигнала с указателем обновленного списка выделенных слов
    }
    else if( this->Mode() == MODE_TIME )
    {
        this->currentwords.clear(); // очищаем список выделенных слов в карте адресов
        qDeleteAll(this->currentwordpos);
        this->currentwordpos.clear(); // очищаем список выделенных элементов в буфере
        for( int i=0; i<indexes.size(); i++ )
        {
            if( indexes.at(i).column() % 2 )
            {
                A429_Channel* dc = (A429_Channel*)indexes.at(i).internalPointer(); // указатель на канал
                if( dc && indexes.at(i).row() ) // указатель на канал имеется в индексе и не первая строка (фильтр адресов)
                {
                    ListWP* wp = new ListWP;
                    wp->channelptr = dc;
                    wp->posbuf = dc->Size() - (indexes.at(i).row()-1) - 1; // -1 т.к. в первой строке - фильтр адресов (также учтена инверсия отображения буфера)
                    this->currentwordpos.append(wp);
                }
            }
        }
        emit this->SelectWords(&this->currentwordpos); // отправка сигнала с позициями выделенных слов в буфере
    }

}
