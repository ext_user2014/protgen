#include "modela429monword.h"

ModelA429MonWord::ModelA429MonWord(QObject *parent) :
    QAbstractTableModel(parent)
{
    this->words = 0x0;
    this->orderofbit = ORDER_32_1;
    this->bitgroup = 1;
    this->words = 0x0;
    this->wordspos = 0x0;

    font_size = 17;
}

int ModelA429MonWord::rowCount(const QModelIndex &parent) const
{
    if( parent.isValid() )
        return 0;

    if( this->words )
        return this->words->size();
    else if( this->wordspos )
        return this->wordspos->size();
    else
        return 0;
}
int ModelA429MonWord::columnCount(const QModelIndex &parent) const
{
    if( parent.isValid() )
        return 0;

    if( this->bitgroup == 1 )
        return 24+1;
    else if( this->bitgroup == 2 )
        return 12+1;
    else if( this->bitgroup == 4 )
        return 6+1;
    else if( this->bitgroup == 8 )
        return 3+1;
    return 0;
}
QVariant ModelA429MonWord::data(const QModelIndex &index, int role) const
{
    if( !index.isValid() ) return QVariant();

    if( role == Qt::FontRole )
        return QFont("Arial",font_size);

    if( role == Qt::BackgroundRole && index.column() == (this->columnCount()-1) )
        return QColor(Qt::cyan);

    if( role != Qt::DisplayRole ) return QVariant();

    A429_Word* wrd = 0x0; //указатель на слово
    if( this->words ) // слова в адресном режиме монитора
    {
        if( index.row() < this->words->size() )
            wrd = this->words->at(index.row());
    }
    else if( this->wordspos ) // слова в временном режиме монитора
    {
        if( index.row() < this->wordspos->size() ) // каждая строка - это позиция в списке, проверяем на наличие в списке нужной позиции
        {
            A429_Channel* channel = this->wordspos->at(index.row())->channelptr;
            int pos = this->wordspos->at(index.row())->posbuf;
            wrd = channel->DataWord(pos);
        }
    }
    if( !wrd ) return QVariant(); // проверка указателя

    // вычисляем значение выделенных битов
    if( index.column() == (this->columnCount()-1) )  // столбец "Значение"
    {
        int offset = 8;
        while( (((this->selectedbits.at(index.row()) >> offset) & 0x1) != 1) && (offset < 31) )
            offset++;
        return (wrd->GetData() & this->selectedbits.at(index.row())) >> offset;
    }

    // номер бита с учетом группировки
    int bit = 0;
    if( this->orderofbit == ORDER_1_32 )
        bit = index.column()*this->bitgroup + 8;
    else
        bit = 32 - (index.column()+1)*this->bitgroup;

    // маска для выборки битов
    quint8 maska = 0x1;
    if( this->bitgroup == 1 )
        maska = 0x1;
    else if( this->bitgroup == 2 )
        maska = 0x3;
    else if( this->bitgroup == 4 )
        maska = 0xF;
    else if( this->bitgroup == 8 )
        maska = 0xFF;

    // вычисление бит на отображение
    int data = (wrd->GetData() >> bit) & maska;
    if( this->orderofbit == ORDER_1_32 ) // обратный порядок бит
    {
        int res = 0;
        for( int i=0; i<this->bitgroup; i++ )
            res |= ((data >> (this->bitgroup-1-i)) & 0x1) << i;
        data = res;
    }

    // отображение через десятичное представление (так проще всего вначале бинарного кода выставить нули)
    // - это костыль, т.к. ф-я sprintf не поддерживает форматного вывода двоичных чисел
    QString str = QString::number(data,2);
    data = str.toInt();
    return QString().sprintf("%0*d",this->bitgroup,data);

}
QVariant ModelA429MonWord::headerData(int section, Qt::Orientation orientation, int role) const
{
    if( role == Qt::FontRole )
    {
        return QFont("Arial",font_size);
    }

    if( role != Qt::DisplayRole ) return QVariant();

    if( orientation == Qt::Vertical ) // заголовки строк - адреса слов
    {
        quint8 addr = 0x0;
        if( this->words ) // вывод по указателю на слово
        {
            if( section >= this->words->size() ) return QVariant(); // выделенная строка вне адресного пространства канала
            if( !this->words->at(section)->GetReady() ) return QVariant(); // выделенное слово не готово
            addr = this->words->at(section)->GetData()&0xFF;
        }
        else if( this->wordspos ) // вывод по позиции слова в очереди
        {
            if( section >= this->wordspos->size() ) return QVariant();// выделенная строка вне буфера канала

            int posbuf = this->wordspos->at(section)->posbuf; // позиция в буфере
            A429_Channel* ch = this->wordspos->at(section)->channelptr; // указатель на канал
            A429_Word* wrd = ch->DataWord(posbuf);

            if( wrd )
                addr = wrd->GetData() & 0xFF;
            else
                return QVariant();
        }
        else
            return QVariant();

        QString str = "A" + QString::number(addr,8) + "(h:" + QString::number(addr,16).toUpper() + ")";
        return str;
    }
    else if( orientation == Qt::Horizontal ) // заголовки стобцов - номера битов (группы битов)
    {
        if( section == (this->columnCount()-1) ) // последний слобец - значенгие выделенных бит
        {
            return "Значение";
        }

        if( this->orderofbit == ORDER_1_32 )
        {
            if( this->bitgroup == 1 )
                return QString::number(section+8+1); // отображаем номер бита с 1
            else
                return QString::number(section*this->bitgroup+8+1) + "-" + QString::number(section*this->bitgroup+8+this->bitgroup); // отображаем группу бит с 1
        }
        else if( this->orderofbit == ORDER_32_1 )
        {
            if( this->bitgroup == 1 )
                return QString::number(32 - section); // отображаем номер бита с 1 в обратном порядке
            else
                return QString::number(32-section*this->bitgroup) + "-" + QString::number(32-section*this->bitgroup-this->bitgroup+1); // отображаем группу бит с 1 в обратном порядке
        }
    }
    return QVariant();
}

/******************************************************************
*
*  Слот выделения бит в представлении
*  вход: список модельных индексов выделенных бит (ячеек по сути)
*  выход: нет
*
******************************************************************/
void ModelA429MonWord::BitSelected(QModelIndexList mil)
{
    this->selectedbits.clear();
    // определяемся с размером вектора слов
    if( this->words )
        this->selectedbits.resize(this->words->size());
    else if( this->wordspos )
        this->selectedbits.resize(this->wordspos->size());
    else
        return;

    // вычисляем выделенные биты в словах
    QVector< QVector<int> > vecwrd; // вектор слов, разложенных в вектор бит
    vecwrd.resize(this->selectedbits.size()); // размер под выделенное кол-во слов
    for( int i=0; i<mil.size(); i++ ) // переюор по выделенным битам (столбцам)
        vecwrd[mil.at(i).row()].append(mil.at(i).column()); // в соответствующий слову вектор вносим номер выделенного столбца (это и есть номер выделенного бита)
    for( int i=0; i<vecwrd.size(); i++ ) // перебор по словам
    {
        for( int j=0; j<vecwrd.at(i).size(); j++ ) // перебор по битам
        {
            int offset = 0; // вычисляем смещение в зависимости от группировки и порядка отображения
            if( this->orderofbit == ORDER_1_32 )
                offset = vecwrd.at(i).at(j)*this->bitgroup + 8;
            else
                offset = 31 - vecwrd.at(i).at(j)*this->bitgroup - (this->bitgroup - 1);

            // вычисляем маски в зависимости от группировки бит и полученного смещения первого бита
            if( this->bitgroup == 1 )
                this->selectedbits[i] |= 0x1 << offset;
            else if( this->bitgroup == 2 )
                this->selectedbits[i] |= 0x3 << offset;
            else if( this->bitgroup == 4 )
                this->selectedbits[i] |= 0xF << offset;
            else if( this->bitgroup == 8 )
                this->selectedbits[i] |= 0xFF << offset;

        }
    }

     emit layoutChanged();
}
