#ifndef MODELA429MONWORD_H
#define MODELA429MONWORD_H

#include "../cfg.h"
#include "../SrcData/a429_word.h"
#include "modelA429mon.h"

class ModelA429MonWord : public QAbstractTableModel
{
    Q_OBJECT
public:
    explicit ModelA429MonWord(QObject *parent = 0);
    
    // функции интерфейса
    int rowCount(const QModelIndex &parent = QModelIndex()) const;              // количество строк
    int columnCount(const QModelIndex &parent = QModelIndex()) const;           // количество столбцов
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const;  // выборка данных по роли
    QVariant headerData(int section, Qt::Orientation orientation, int role) const; // установка заголовков

    int font_size;

signals:
    void ChangeBitGroup(void);
    
public slots:
    void SetWord(QList<A429_Word*>* wrd) {wordspos = 0x0; words = wrd; selectedbits.clear();selectedbits.resize(wrd->size());emit layoutChanged();}
    void SetWord(QList<ListWP*>* wrd)    {wordspos = wrd; words = 0x0; selectedbits.clear();selectedbits.resize(wrd->size());emit layoutChanged();}

    void SetOrder(bool order) {orderofbit = order?ORDER_1_32:ORDER_32_1; emit layoutChanged();}
    void SetBitGroup(int bg) {bitgroup = 0x1 << (quint8)bg; emit ChangeBitGroup();  emit layoutChanged();}
    void BitSelected(QModelIndexList mil);
    void SetFontSize(int s){font_size = s; emit layoutChanged();}

private:
    QList<A429_Word*>* words; // список слов для отображения
    QList<ListWP*>* wordspos; // список выделенных элементов (только для режиме времени, т.к. указатели динамически удалаются)
    QVector<unsigned> selectedbits; // маска выделенных бит в слове

    enum OrderOfBit{ORDER_32_1, ORDER_1_32} orderofbit; // порядок отображения бит

    quint8 bitgroup; // группировки бит

};

#endif // MODELA429MONWORD_H
