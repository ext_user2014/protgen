#ifndef DELEGATEA429MON_H
#define DELEGATEA429MON_H

#include "../cfg.h"

class DelegateA429Mon : public QItemDelegate
{
    Q_OBJECT
public:
    explicit DelegateA429Mon(QObject *parent = 0);

    QWidget *createEditor(QWidget *parent, const QStyleOptionViewItem &option, const QModelIndex &index) const;    // создание редактора
    void setEditorData(QWidget *editor, const QModelIndex &index) const;         // загрузка данных в редактор
    void setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const;  // загрузка данных из редактора в модель данных
    
};

// класс элемента управления - выбор адресов для фильтрации
class FilterAddrEditor : public QFrame
{
    Q_OBJECT
public:
    explicit FilterAddrEditor(QWidget* parent = 0) : QFrame(parent)
    {
        this->setMinimumSize(300,300);
        this->setFrameStyle(QFrame::Box | QFrame::Plain);
        this->setAutoFillBackground(true);
        this->setLineWidth(2);

        this->gl = new QGridLayout(this);
        this->pb_selectall =  new QPushButton("Выделить все",this);
        this->pb_deselectall =  new QPushButton("Снять все",this);
        this->tw = new QTableWidget(this);

        this->gl->setMargin(5);
        this->gl->setSpacing(5);
        this->gl->addWidget(this->tw,0,0,0,1,Qt::AlignTop);
        this->gl->addWidget(this->pb_selectall,1,0);
        this->gl->addWidget(this->pb_deselectall,1,1);

        this->tw->setColumnCount(4);
        this->tw->setRowCount(256);
        this->tw->setHorizontalHeaderLabels(QStringList()<<"v"<<"oct"<<"hex"<<"bin");
        this->tw->setSelectionBehavior(QAbstractItemView::SelectRows);
        this->tw->verticalHeader()->setVisible(false);
        this->tw->setColumnWidth(0,50);
        this->tw->setColumnWidth(1,50);
        this->tw->setColumnWidth(2,50);
        this->tw->setColumnWidth(3,90);
        this->tw->setMinimumWidth(290);
        this->tw->setMinimumHeight(300-this->pb_selectall->height()-10);

        for( int i=0; i<256; i++)
        {
            QCheckBox* cb = new QCheckBox(tw);
            QTableWidgetItem* item8 = new QTableWidgetItem(QString::number(i,8));
            QTableWidgetItem* item16 = new QTableWidgetItem(QString::number(i,16));
            QTableWidgetItem* item2 = new QTableWidgetItem(QString::number(i,2));

            tw->setCellWidget(i,0,cb);
            tw->setItem(i,1,item8);
            tw->setItem(i,2,item16);
            tw->setItem(i,3,item2);

            connect(this->pb_selectall,SIGNAL(clicked()),this,SLOT(Select()));
            connect(this->pb_deselectall,SIGNAL(clicked()),this,SLOT(DeSelect()));
            connect(this,SIGNAL(SelectChange(bool)),cb,SLOT(setChecked(bool)));
        }

        this->pb_selectall->setMinimumWidth(this->width()/2-10);
        this->pb_deselectall->setMinimumWidth(this->width()/2-10);

        connect(this->tw,SIGNAL(doubleClicked(QModelIndex)),this,SLOT(close()));
    }

    ~FilterAddrEditor(void)
    {
        delete gl;
        delete pb_selectall;
        delete pb_deselectall;
        delete tw;
    }

    const QTableWidget* TableAddr(void) const {return this->tw;}

signals:
    void SelectChange(bool);

public slots:
    void Select(void)
    {
        emit SelectChange(true);
    }

    void DeSelect(void)
    {
        emit SelectChange(false);

    }

private:
    QGridLayout* gl;
    QPushButton* pb_selectall;
    QPushButton* pb_deselectall;
    QTableWidget* tw;

};

#endif // DELEGATEA429MON_H
