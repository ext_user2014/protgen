#ifndef MONITORA429_H
#define MONITORA429_H

#include "../cfg.h"
#include "../SrcData/srcdatatcp.h"
#include "../SrcData/modelsrcdata.h"
#include "modelA429mon.h"
#include "delegatea429mon.h"
#include "modela429monword.h"

class MonitorA429 : public QWidget
{
    Q_OBJECT
public:
    explicit MonitorA429(QModelIndexList* indexlist, QWidget *parent = 0);
    ~MonitorA429(void);
    
signals:
    
public slots:

private slots:
    void ViewWidgetChange(void); // слот переключения режима виджета (в отдельном окне)
    void StateChange(void); // слот переключения режима просмотра канала (адреса или буфер)
    void Pausa(void); // пауза в отображении данных на каналах
    void Clear(void); // очистка данных на канале
    void SetSyncON(void); // вкл/выкл синхронизатора
    void SelectWrd(QModelIndex index); // обработчик выделенных элементов
    void SelectBit(QModelIndex index); // обработчик выделенных элементов

    void WriteToFile(A429_Word*); // функция записи в файл
    void Rec(void); // слот обработки кнопки записи в файл
    void FileSave(void); // слот обработки кнопки выбора фала для записи

private:
    QTabWidget* parent; // родитель

    // далее элементы управления
    QPushButton* pb_close;
    QPushButton* pb_state;
    QPushButton* pb_view;
    QPushButton* pb_pausa;
    QPushButton* pb_cleardata;
    QTableView* tw_data; // табличное представление данных по адресам
    QCheckBox* chb_sync;
    QSpinBox* sb_sync;
    QLabel* l_sync;
    QTableView* tw_word; // представление слова
    QCheckBox* chb_order; // порядок бит в представлении слова
    QComboBox* cb_bitwordgroup; // группировка бит в представлении слова
    QLineEdit* le_file;
    QPushButton* pb_file;
    QPushButton* pb_filerecstop;
    QLabel* l_fontsize;
    QSpinBox* sb_fontsize;

    ////

    QList<A429_Channel*> list_devch; // хранилище выбранных каналов

    ModelA429Mon* modeldata; // модель данных монитора
    ModelA429MonWord* modelword; // модель данных слова

    QModelIndexList savemil_modeaddr; // сохраненные выделенные объекты в режиме адреса
    QModelIndexList savemil_modetime; // сохраненные выделенные объекты в режиме время

    QThread* th; // поток для моделей

    bool view; // режим адреса или буфера
    bool state; // режим окна или вкладки

    QFile* file; // файл для записи

    QMap<int,QString> stroka; // текущаяя строка для записи в файл

};

#endif // MONITORA429_H
