#ifndef MONA429DATA_H
#define MONA429DATA_H

#include "../cfg.h"
#include "../SrcData/srcdatatcp.h"

 // !!! д.б. число степени 2
#define MODEADRR_ROWCOUNT   (quint32)32

enum ViewMode{MODE_ADDR,MODE_TIME};

struct ListWP
{
    A429_Channel* channelptr;
    int posbuf;
};

class ModelA429Mon : public QAbstractTableModel
{
    Q_OBJECT
public:
    explicit ModelA429Mon(QList<A429_Channel*> &dc, QObject *parent = 0);
    ~ModelA429Mon(void);

    // функции интерфейса
    int rowCount(const QModelIndex &parent = QModelIndex()) const;              // количество строк
    int columnCount(const QModelIndex &parent = QModelIndex()) const;           // количество столбцов
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const;  // выборка данных по роли
    QVariant headerData(int section, Qt::Orientation orientation, int role) const; // установка заголовков
    Qt::ItemFlags flags(const QModelIndex &index) const;
    QModelIndex index(int row, int column, const QModelIndex &parent) const;

    void Pausa(bool p){pausa = p; for(int i=0;i<list_devch.size();i++)list_devch.at(i)->Saveable(!p);} // пауза в обновлении данных
    bool Pausa(void){return pausa;}
    void Mode(ViewMode m){mode = m;emit layoutChanged();} // смена режима модели
    ViewMode Mode(void){return mode;} // режим модели
    void SetCurrentDevch(QModelIndex index); // установка текущего канала для адресного просмотра
    void SetCurrentDevch(A429_Channel* dc){currentdevch = dc;}
    const A429_Channel* CurrentDevch(void) const {return currentdevch;}
    void Sync(bool b){syncon = b;} // Вкл синхронизацию
    bool Sync(void){return syncon;}
    int SyncDeltaT(void){return deltat;}

public slots:
    void SyncDeltaT(int delta){deltat = delta;} // уровень синхронизации
    void SetCurrentWords(QModelIndexList indexes); // обработка выделенных элементов
signals:
    void SelectWords(QList<A429_Word*>*); // сигнал выделенных слов
    void SelectWords(QList<ListWP*>*); // сигнал выделенных слов
private:
    QList<A429_Channel*> list_devch;

    A429_Channel* currentdevch;
    QList<A429_Word*> currentwords; // список выделенных элементов (только для режима просмотра адресов, т.к. указатели динамически удалаются)
    QList<ListWP*> currentwordpos; // список выделенных элементов (только для режиме времени, т.к. указатели динамически удалаются)

    bool pausa;
    ViewMode mode;

    // синхронизатор и всё для него
    bool syncon;
    int deltat;
};

#endif // MONA429DATA_H
