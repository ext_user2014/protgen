#include "delegatea429mon.h"
#include "../SrcData/a429_channel.h"
#include "modelA429mon.h"

DelegateA429Mon::DelegateA429Mon(QObject *parent) :
    QItemDelegate(parent)
{
}

QWidget* DelegateA429Mon::createEditor(QWidget *parent, const QStyleOptionViewItem &, const QModelIndex &index) const
{
    if( index.row() )
        return 0;

    return new FilterAddrEditor(parent);
}

void DelegateA429Mon::setEditorData(QWidget *editor, const QModelIndex &index) const
{
    if( !index.isValid() )
        return;

    FilterAddrEditor* ed = (FilterAddrEditor*)editor;
    A429_Channel* ch = (A429_Channel*)index.internalPointer();
    for( int i=0; i<256; i++)
    {
        QCheckBox* cb = (QCheckBox*)ed->TableAddr()->cellWidget(i,0);
        bool b = ch->ListAddrFilter().contains(i);
        cb->setChecked(b);
    }
}

void DelegateA429Mon::setModelData(QWidget *editor, QAbstractItemModel *, const QModelIndex &index) const
{
    if( !index.isValid() )
        return;

    FilterAddrEditor* ed = (FilterAddrEditor*)editor;
    A429_Channel* ch = (A429_Channel*)index.internalPointer();
    ch->ListAddrFilter().clear();
    for( int i=0; i<256; i++)
    {
        QCheckBox* cb = (QCheckBox*)ed->TableAddr()->cellWidget(i,0);
        if( cb->isChecked() )
            ch->ListAddrFilter(i);
    }
}
