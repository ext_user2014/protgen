#include "monitora429.h"

/******************************************************************
*
* Конструктор класса
* вход: список индексов модели ModelSrcData, родитель
* выход: экземпляр класса
*
******************************************************************/
MonitorA429::MonitorA429(QModelIndexList* indexlist, QWidget *parent) :
    QWidget(parent)
{
    this->parent = (QTabWidget*)parent;
    this->parent->addTab(this,"Монитор А429");

    this->setAttribute(Qt::WA_DeleteOnClose);

    // разметка элементов управления
    QVBoxLayout* vbl = new QVBoxLayout(this);
    vbl->setMargin(5);
    vbl->setSpacing(5);

    // кнопки
    this->pb_close = new QPushButton("Закрыть",this);
    connect(pb_close,SIGNAL(clicked()),this,SLOT(deleteLater()));
    this->pb_view = new QPushButton("В отдельном окне",this);
    connect(pb_view,SIGNAL(clicked()),this,SLOT(ViewWidgetChange()));
    this->pb_state = new QPushButton("Вид: Адреса",this);
    connect(pb_state,SIGNAL(clicked()),this,SLOT(StateChange()));
    this->pb_pausa = new QPushButton("Пауза",this);
    connect(pb_pausa,SIGNAL(clicked()),this,SLOT(Pausa()));
    this->pb_cleardata = new QPushButton("Очистить",this);
    connect(pb_cleardata,SIGNAL(clicked()),this,SLOT(Clear()));
    this->tw_data = new QTableView(this);
    this->chb_sync = new QCheckBox("Синхронизация, мс (диапазон от 0 мс до 10 сек):",this);
    connect(this->chb_sync,SIGNAL(clicked()),this,SLOT(SetSyncON()));
    this->sb_sync = new QSpinBox(this);
    this->sb_sync->setHidden(false);
    this->chb_sync->setHidden(false);
    this->sb_sync->setMaximum(10000); // максимум уровня синхронизации = 10000 мс
    this->l_sync = new QLabel("Нет канала синхронизации",this);

    this->tw_word = new QTableView(this);
    this->tw_word->setMaximumHeight(600);
    this->tw_word->verticalHeader()->setResizeMode(QHeaderView::ResizeToContents);
    this->tw_word->horizontalHeader()->setResizeMode(QHeaderView::ResizeToContents);
    this->tw_word->setAutoScroll(true);

    this->chb_order = new QCheckBox("9-32",this);
    this->chb_order->setMaximumWidth(90);
    this->cb_bitwordgroup = new QComboBox(this);
    this->cb_bitwordgroup->addItems(QStringList()<<"По 1 биту"<<"По 2 бита"<<"По 4 бита"<<"По 8 бит");
    this->cb_bitwordgroup->setCurrentIndex(0);
    this->cb_bitwordgroup->setMaximumWidth(90);
    // элементы для записи в файл
    this->le_file = new QLineEdit(QDateTime::currentDateTime().toString("yyyy-MM-dd_HH-mm"),this);
    this->pb_file = new QPushButton("Записывать в файл...",this);
    connect(this->pb_file,SIGNAL(clicked()),this,SLOT(FileSave())); // обработка нажатия на кнопку
    this->pb_filerecstop = new QPushButton("Старт запись",this);
    connect(this->pb_filerecstop,SIGNAL(clicked()),this,SLOT(Rec())); // обработка нажатия на кнопку

    this->l_fontsize = new QLabel("Р-р шрифта",this);
    this->l_fontsize->setMaximumWidth(90);
    this->l_fontsize->setWordWrap(true);
    this->sb_fontsize = new QSpinBox(this);
    this->sb_fontsize->setMinimum(12);
    this->sb_fontsize->setMaximum(50);
    this->sb_fontsize->setMaximumWidth(90);

    // верхняя строка - кнопки
    QHBoxLayout *hbl_s1 = new QHBoxLayout(this);
    vbl->addLayout(hbl_s1);
    hbl_s1->addWidget(pb_state);
    hbl_s1->addWidget(pb_pausa);
    hbl_s1->addWidget(pb_cleardata);
    hbl_s1->addWidget(pb_view);
    hbl_s1->addWidget(pb_close);
    // синхронизатор
    QHBoxLayout *hbl_s2 = new QHBoxLayout(this);
    vbl->addLayout(hbl_s2);
    hbl_s2->addWidget(chb_sync);
    hbl_s2->addWidget(sb_sync);
    hbl_s2->addWidget(l_sync);
    // запись в файл
    QHBoxLayout *hbl_s3 = new QHBoxLayout(this);
    vbl->addLayout(hbl_s3);
    hbl_s3->addWidget(le_file);
    hbl_s3->addWidget(pb_file);
    hbl_s3->addWidget(pb_filerecstop);
    // сплиттер
    QSplitter *splitter = new QSplitter(Qt::Vertical,this);
    vbl->addWidget(splitter);
    // представление данных
    splitter->addWidget(tw_data);
    QWidget* w_word = new QWidget(this);
    splitter->addWidget(w_word);
    splitter->setSizes(QList<int>()<<parent->height()<<100);
    QHBoxLayout *hbl_s4 = new QHBoxLayout(w_word);
    QVBoxLayout *vbl_s4 = new QVBoxLayout(w_word);
    hbl_s4->addWidget(tw_word);
    hbl_s4->addLayout(vbl_s4);
    // настройка представления слов
    vbl_s4->addWidget(chb_order);
    vbl_s4->addWidget(cb_bitwordgroup);
    vbl_s4->addWidget(l_fontsize);
    vbl_s4->addWidget(sb_fontsize);
    vbl_s4->addStretch(1);

    // заполняем список каналов
    for( int i=0; i<indexlist->size(); i++ )
    {
        if( indexlist->at(i).column() )
            continue;

        A429_Channel* dc = (A429_Channel*)indexlist->at(i).internalPointer();
        this->list_devch << dc;
        this->le_file->setText(this->le_file->text() + "_K" + QString::number(dc->Num()+1)); // формируем название файла по умолчанию (для записи данных каналов)
    }
    this->le_file->setText(this->le_file->text() + ".csv"); // добавляем к имени файла расширение

    // создаём модель данных и настраиваем её
    this->modeldata = new ModelA429Mon(this->list_devch, this);
    connect(this->tw_data,SIGNAL(clicked(QModelIndex)),this,SLOT(SelectWrd(QModelIndex))); // обработка выделенных элементов по клику мышкой

    // создаём модель слова и настраиваем её
    this->modelword = new ModelA429MonWord(this);
    connect(this->modeldata,SIGNAL(layoutChanged()),this->modelword,SIGNAL(layoutChanged())); // обновление представлений данных
    connect(this->modelword,SIGNAL(ChangeBitGroup()),this->tw_word,SLOT(resizeColumnsToContents())); // выравнивание колонок при изменении руппировки бит
    connect(this->modeldata,SIGNAL(SelectWords(QList<A429_Word*>*)),this->modelword,SLOT(SetWord(QList<A429_Word*>*))); // установка выделенных слов в модель
    connect(this->modeldata,SIGNAL(SelectWords(QList<ListWP*>*)),this->modelword,SLOT(SetWord(QList<ListWP*>*))); // установка выделенных слов в модель
    connect(this->chb_order,SIGNAL(toggled(bool)),this->modelword,SLOT(SetOrder(bool))); // установка порядка бит по галочке
    connect(this->cb_bitwordgroup,SIGNAL(currentIndexChanged(int)),this->modelword,SLOT(SetBitGroup(int))); // выбор из списка способа группировки бит
    connect(this->tw_word,SIGNAL(clicked(QModelIndex)),this,SLOT(SelectBit(QModelIndex))); // установка выделенных бит
    connect(this->sb_fontsize,SIGNAL(valueChanged(int)),this->modelword,SLOT(SetFontSize(int))); // установка размера шрифта
    connect(this->sb_fontsize,SIGNAL(valueChanged(int)),this->tw_word,SLOT(resizeColumnsToContents())); // отслеживание изменения шрифта
    connect(this->sb_fontsize,SIGNAL(valueChanged(int)),this->tw_word,SLOT(resizeRowsToContents())); // отслеживание изменения шрифта

    // настройка представления-таблицы
    this->tw_data->setModel(this->modeldata);
    this->tw_data->setItemDelegate(new DelegateA429Mon(this));
    this->tw_word->setModel(this->modelword);

    // перенос модели в отдельный поток
    this->th = new QThread();
    th->start();
    this->modeldata->setParent(0);
    this->modeldata->moveToThread(this->th);
    this->modelword->setParent(0);
    this->modelword->moveToThread(this->th);

    // открываемся пользователю
    this->parent->setCurrentIndex(this->parent->count()-1);
    this->view = false; // вид окна - как вкладка
    this->state = false; // режим просмотра - буфер

    sb_fontsize->setValue(modelword->font_size); // установка размера шрифта по умолчанию

    this->file = new QFile();
}
/******************************************************************
*
*  Деструктор класса
*
******************************************************************/
MonitorA429::~MonitorA429(void)
{
    // закрываем файл
    this->file->close();
    delete this->file;

    th->exit();
    if( !th->wait(1000) )
        th->terminate();
    delete th;

    delete this->pb_close;
    delete this->pb_view;
    delete this->pb_state;
    delete this->pb_pausa;
    delete this->pb_cleardata;
    delete this->tw_data;
    delete this->sb_sync;
    delete this->chb_sync;
    delete this->l_sync;
    delete this->tw_word;

    delete this->modeldata;
    delete this->modelword;
    delete this->chb_order;
    delete this->cb_bitwordgroup;

    delete this->le_file;
    delete this->pb_file;
    delete this->pb_filerecstop;

}
/******************************************************************
*
*  Переключение режима окна
*
******************************************************************/
void MonitorA429::ViewWidgetChange(void)
{
    if( !this->view )
    {
        this->pb_view->setText("Как вкладка");
        this->setParent(0);
        this->setWindowTitle("Монитор А429");
        this->show();
    }
    else
    {
        this->pb_view->setText("В отдельном окне");
        this->setParent(this->parent);
        this->parent->addTab(this,"Монитор А429");
        this->parent->setCurrentIndex(this->parent->count()-1);
    }
    this->view = !this->view;
}
/******************************************************************
*
*  Нажатие кнопки паузы по приему данных каналов
*
******************************************************************/
void MonitorA429::Pausa()
{
    this->modeldata->Pausa(!this->modeldata->Pausa());

    if( this->modeldata->Pausa() )
        this->pb_pausa->setText("Старт");
    else
        this->pb_pausa->setText("Пауза");


}
/******************************************************************
*
*  Слот переключения режима просмотра канала (адреса или буфер)
*
******************************************************************/
void MonitorA429::StateChange(void)
{
    if( this->state ) // режим адресов
    {
        if( this->modeldata->Mode() == MODE_TIME )
            return;

        this->modeldata->SetCurrentWords(QModelIndexList()); // по сути - сброс выделенных элементов (пустой List)

        // сохраняем выделенные объекты
        this->savemil_modeaddr = this->tw_data->selectionModel()->selectedIndexes();

        this->modeldata->Mode(MODE_TIME); // меняем режим отображения
        this->modeldata->SetCurrentDevch(QModelIndex()); // сброс текущего канала

        this->sb_sync->setHidden(false);
        this->l_sync->setHidden(false);
        this->chb_sync->setHidden(false);

        // восстанавливаем выделенные объекты
        this->tw_data->selectionModel()->reset();
        if( !this->savemil_modetime.isEmpty() )
            this->tw_data->setCurrentIndex(this->savemil_modetime.at(0));
        for( int i=0; i<this->savemil_modetime.size(); i++ )
            this->tw_data->selectionModel()->select(this->savemil_modetime.at(i),QItemSelectionModel::Select);
        this->SelectWrd(this->tw_data->currentIndex());

        this->pb_state->setText("Вид: Адреса");
    }
    else // режим буфера
    {
        if( this->modeldata->Mode() == MODE_ADDR )
            return;

        this->modeldata->SetCurrentWords(QModelIndexList()); // по сути - сброс выделенных элементов (пустой List)

        // сохраняем выделенные объекты
        this->savemil_modetime = this->tw_data->selectionModel()->selectedIndexes();

        this->modeldata->SetCurrentDevch(this->tw_data->currentIndex()); // установка текущего канала через модельный индекс
        this->modeldata->Mode(MODE_ADDR); // меняем режим отображения

        this->sb_sync->setHidden(true);
        this->chb_sync->setHidden(true);
        this->l_sync->setHidden(true);

        // восстанавливаем выделенные объекты
        this->tw_data->selectionModel()->reset();
        if( !this->savemil_modeaddr.isEmpty() )
            this->tw_data->setCurrentIndex(this->savemil_modeaddr.at(0));
        for( int i=0; i<this->savemil_modeaddr.size(); i++ )
            this->tw_data->selectionModel()->select(this->savemil_modeaddr.at(i),QItemSelectionModel::Select);
        this->SelectWrd(this->tw_data->currentIndex());

        this->pb_state->setText("Вид: Время");
    }
    this->state = !this->state;
}
/******************************************************************
*
*  Очистить данные каналов
*
******************************************************************/
void MonitorA429::Clear(void)
{
    this->modeldata->SetCurrentWords(QModelIndexList()); // очищаем выделенные слова по выделенным индексам

    for( int i=0; i<this->list_devch.size(); i++ )
        this->list_devch.at(i)->ClearData();
}
/******************************************************************
*
*  Вкл/выкл режима синхронизации
*
******************************************************************/
void MonitorA429::SetSyncON(void)
{
    this->modeldata->Sync(this->chb_sync->isChecked());
    if( this->modeldata->Sync() )
    {
        this->modeldata->SetCurrentDevch(this->tw_data->currentIndex());
        if( !this->modeldata->CurrentDevch() )
            return;
        this->modeldata->SyncDeltaT(this->sb_sync->value());
        connect(this->sb_sync,SIGNAL(valueChanged(int)),this->modeldata,SLOT(SyncDeltaT(int)));
        this->l_sync->setText("Канал синхронизации: К" + QString::number(this->modeldata->CurrentDevch()->Num()+1));
    }
    else
    {
        this->modeldata->SetCurrentDevch(0x0);
        this->modeldata->SyncDeltaT(0x0);
        disconnect(this->sb_sync,SIGNAL(valueChanged(int)),this->modeldata,SLOT(SyncDeltaT(int)));
        this->l_sync->setText("Нет канала синхронизации");
    }
}
/******************************************************************
*
*  Слот обработки выделения элементов по клику мышкой в tw_data
*
******************************************************************/
void MonitorA429::SelectWrd(QModelIndex index)
{
    if( !index.isValid() )
        this->modeldata->SetCurrentWords(QModelIndexList()); // по сути - сброс выделенных элементов (пустой List)
    else
        this->modeldata->SetCurrentWords(this->tw_data->selectionModel()->selectedIndexes()); // установка выделенных слов по выделенным индексам
}
/******************************************************************
*
*  Слот обработки выделения элементов по клику мышкой в tw_word
*
******************************************************************/
void MonitorA429::SelectBit(QModelIndex index)
{
    if( !index.isValid() )
        this->modelword->BitSelected(QModelIndexList()); // по сути - сброс выделенных элементов (пустой List)
    else
        this->modelword->BitSelected(this->tw_word->selectionModel()->selectedIndexes()); // установка выделенных бит по выделенным индексам
}

/******************************************************************
*
*  Слот записи данных из канала channel в файл
*
******************************************************************/
void MonitorA429::WriteToFile(A429_Word *pw)
{
    static bool first = true;

    A429_Channel* channel = (A429_Channel*)sender();
    int pos = this->list_devch.indexOf(channel);

    // организуем поток записи
    QTextStream ts(this->file);

    if( first )
    {
        first = false;
        for( int i=0; i<this->list_devch.size(); i++ ) // запись строки
        {
            ts << "K" << QString::number(this->list_devch.at(i)->Num()+1) << " time;";
            ts << "K" << QString::number(this->list_devch.at(i)->Num()+1) << " msec;";
            ts << "K" << QString::number(this->list_devch.at(i)->Num()+1) << " addr(oct);";
            ts << "K" << QString::number(this->list_devch.at(i)->Num()+1) << " data(hex);";
            ts << "K" << QString::number(this->list_devch.at(i)->Num()+1) << " data(bin);";
        }
        ts << "\n"; // переводим строку
    }

    if( this->stroka.keys().contains(pos) ) // ячейка заполнена
    {
        for( int i=0; i<this->list_devch.size(); i++ ) // запись строки
        {
            if( this->stroka.keys().contains(i) )
                ts << this->stroka.value(i);
            else
                ts << ";;;;;";
        }
        ts << "\n"; // переводим строку
        this->stroka.clear(); // очщаем строку
    }

    QString str = "";
    quint32 w = pw->GetData();
    QTime t = pw->GetTime();
    quint32 tms = pw->GetTimeMS();

    str += t.toString("hh:mm:ss.zzz") + ";";
    str += QString::number(tms) + ";";
    str += QString::number(w & 0xFF,8) + ";";
    str += "0x" + QString::number(w >> 8,16) + ";";

    QString str_bin = QString::number((w >> 24) & 0xFF,2);
    str +=  QString().sprintf("%08ld",str_bin.toLong()) + " ";
    str_bin = QString::number((w >> 16) & 0xFF,2);
    str +=  QString().sprintf("%08ld",str_bin.toLong()) + " ";
    str_bin = QString::number((w >> 8) & 0xFF,2);
    str +=  QString().sprintf("%08ld",str_bin.toLong());

    str += ";";

    this->stroka.insert(pos,str);
}

/******************************************************************
*
*  Слот нажатия на кнопку записи
*
******************************************************************/
void MonitorA429::Rec(void)
{
    if( this->pb_filerecstop->text() == "Стоп запись" )
    {
        this->pb_filerecstop->setText("Старт запись");
        for( int i=0; i<this->list_devch.size(); i++ )
            disconnect(this->list_devch.at(i),SIGNAL(DataAdded(A429_Word*)),this,SLOT(WriteToFile(A429_Word*)));
    }
    else
    {
        if( !this->file->isOpen() )
        {
            this->file->setFileName(this->le_file->text());
            if( !this->file->open(QIODevice::WriteOnly | QIODevice::Append) )
                return;
        }

        this->pb_filerecstop->setText("Стоп запись");
        for( int i=0; i<this->list_devch.size(); i++ )
            connect(this->list_devch.at(i),SIGNAL(DataAdded(A429_Word*)),this,SLOT(WriteToFile(A429_Word*)));
    }
}
/******************************************************************
*
*  Слот нажатия на кнопку сохранения
*
******************************************************************/
void MonitorA429::FileSave(void)
{
    // выбираем файл
    QString strfile = this->le_file->text();
    strfile = QFileDialog::getSaveFileName(0,"Выберите файл",strfile,"Текст CSV (*.csv)");

    if( strfile.isEmpty() ) // ничего не выбрано, выходим
        return;

    this->le_file->setText(strfile);
}


