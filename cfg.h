#ifndef CFG_H
#define CFG_H

#include <QtGui>
#include <QtNetwork>

class StaticData
{
public:
    static QSettings *settings;
};

//!!! идентификатор приложения !!!
#define APPID 0x5A1A

// MIME типы
#define MIME_PTR_PIVDATA "application/ProtGen-&PivData"
#define MIME_INDEX_MYITEMMODEL "application/ProtGen-MyItemModel-ModelIndex.PivData"
#define MIME_STRUCT_MP_FOR_PLOT "application/ProtGen-ModelParser-StructForPlot"

#endif // CFG_H
