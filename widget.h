#ifndef WIDGET_H
#define WIDGET_H

#include "ui_widget.h"

#include "cfg.h"
#include "PivEditor/MyItemModel.h"
#include "PivEditor/piveditor.h"
#include "PivEditor/myitemdelegat.h"
#include "SrcData/srcdatatcp.h"
#include "SrcData/srcdatafile.h"
#include "SrcData/modelsrcdata.h"
#include "SrcData/delegatesrcdata.h"
#include "Monitor/monitora429.h"
#include "Parser/parser.h"
#include "SrcPIV/modelsrcpiv.h"

namespace Ui {
class Widget;
}

class Widget : public QWidget
{
    Q_OBJECT
    
public:
    explicit Widget(QWidget *parent = 0);
    ~Widget();

public slots:
    void keyPressEvent(QKeyEvent *ev);
    void closeEvent(QCloseEvent *ev);

    void slotAddIPPort(QString ipp){if(ui->cbIPPort->findText(ipp)==-1)ui->cbIPPort->addItem(ipp);}
private slots:

    void on_pbOpenMonSelCh_clicked();

    void on_pbOpenParSelCh_clicked();

private:
    Ui_Widget *ui;

    QTimer timer250ms;

    PivEditor* piveditor;  // редактор ПИВ
    SrcDataFile* srcdatafile; // источник данных из файла
    ModelSrcData* modelsrcdata; // модель источника данных
    QMap<QString,int> counters; // счетчики
    SrcDataTCP* srcdatatcp; // источник данных по сети

    ModelSrcPIV* srcpivdata;

    QThread* th_srcdata; // поток источника данных

signals:
    void IsClosed(void);

    void ShowDebug(bool); // показать/скрыть отладочную инфу
};

#endif // WIDGET_H
