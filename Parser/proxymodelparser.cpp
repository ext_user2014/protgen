#include "proxymodelparser.h"

ProxyModelPivForParser::ProxyModelPivForParser(PivData *pivsrc, QObject *parent) :
    QSortFilterProxyModel(parent)
{
    piv = pivsrc;
    root_index = QModelIndex();
}

bool ProxyModelPivForParser::filterAcceptsRow(int source_row, const QModelIndex &source_parent) const
{
    PivData* pd = (PivData*)source_parent.internalPointer();

    if( pd == piv )
    {
        QModelIndex ri = mapFromSource(source_parent);
        memcpy((void*)&root_index,(void*)&ri,sizeof(QModelIndex)); // копируем индекс через костыль, т.к. находимся в константной функции
        emit this->signalSetRootIndex(root_index); // высылаем сигнал для установки корневого элемента
    }

    if( !pd )
        return true;
    else
        if( pd->children.at(source_row)->info.type <= PivData::N_PARAM )
            return true;

    return false;
}
bool ProxyModelPivForParser::filterAcceptsColumn(int source_column, const QModelIndex &) const
{
    return (source_column != 1 && source_column != 2 ); // фильтруем тип и дату изменения
}

/******************************************************************
* Флаги записи
* вход: модельный индекс
* выход: флаги записи
******************************************************************/
Qt::ItemFlags ProxyModelPivForParser::flags(const QModelIndex &index) const
{
    if (!index.isValid())
        return Qt::ItemIsEnabled;

    return QAbstractItemModel::flags(index);
}

