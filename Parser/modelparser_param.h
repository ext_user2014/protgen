#ifndef MOEDEPARSER_PARAM_H
#define MOEDEPARSER_PARAM_H

#include "modelparserbase.h"

class ModelParser_Param : public ModelParserBase
{
    Q_OBJECT
public:
    ModelParser_Param(A429_Channel* ch, PivData* pd);
    ~ModelParser_Param(void);

    virtual QVariant data(const QModelIndex&, int) const;  // выборка данных по роли

    virtual int columnCount(const QModelIndex&) const;           // количество столбцов
    virtual QVariant headerData(int, Qt::Orientation, int) const; // установка заголовков


    virtual int Size(void);

    virtual QString csvData(void);

public slots:
    virtual void slotUpdateInternalData(A429_Word*);
    virtual void slotResetInternalData(void);

    virtual void Size(int s);

private:
    int static_size;

    struct StParam
    {
        quint32 id;
        quint32 word;
        QTime time;
        quint32 _time;
        float f;
        bool ready;
    };

    QQueue<StParam*> params; // очередь параметров

    StParam* p; // буферная переменная

    virtual void dblDataUpdate(void);
};

#endif // MOEDEPARSER_PARAM_H
