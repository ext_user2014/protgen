#include "modelparser_delegatebase.h"

ModelParser_DelegateBase::ModelParser_DelegateBase(QObject *parent) :
    QItemDelegate(parent)
{
}
QWidget *ModelParser_DelegateBase::createEditor(QWidget *parent, const QStyleOptionViewItem &, const QModelIndex &index) const
{
    QLabel *l = new QLabel("",parent); // элемент для отображения данных
    MapLabel *pml = const_cast<MapLabel*> (&map_label); // карта элементов
    pml->insert(index,l); // добавляем в элемент в карту (делегат 1, кол-во вызовов ф-ии = кол-во столбцов модели => готовая карта элементов)
    return l;
}
void ModelParser_DelegateBase::setEditorData(QWidget *, const QModelIndex &) const
{
}
void ModelParser_DelegateBase::setModelData(QWidget *, QAbstractItemModel *, const QModelIndex &) const
{
}
void ModelParser_DelegateBase::slotUpdateLabels(QModelIndex index,QString str)
{
    if( !map_label.keys().contains(index) ) // индекс в карте не найден - выходим
        return;

    map_label[index]->setText(str); // обновляем содержимое элемента
}
