#include "modelparser_group.h"

ModelParser_Group::ModelParser_Group(A429_Channel *ch, PivData *pd) :
    ModelParserBase(ch,pd)
{
    this->strl_header_v.append("Наличие адресов");
    this->strl_header_v.append("Целостность группы");
    this->strl_header_v.append("Рекомендации");

    // формируем список адресов
    for( int i=0;i<this->pivdata->children.size();i++ )
    {
        if( this->pivdata->children[i]->info.type == PivData::N_PARAM && this->pivdata->children[i]->st_n_param.control )
        {
            StGroup a;
            a.addr = this->pivdata->children[i]->st_n_param.id;
            a.m = this->pivdata->children[i]->st_n_param.mask;
            a.mb = this->pivdata->children[i]->st_n_param.offset;
            a.exist = false;
            list_addrexist.append(a);
            this->strl_header_h.append(QString().sprintf("A%o(8)",a.addr));
        }
    }
}
/******************************************************************
*  Обновление данных
*  Вход: нет
*  Выход: нет
******************************************************************/
void ModelParser_Group::slotUpdateInternalData(A429_Word *)
{
    for( int num=0;num<list_addrexist.size();num++ )
    {
        bool ex = false;
        for(int i=this->channel->Size()-2; i>=0; i--)
        {
            if( this->channel->Size() <= i )continue; // на случай незаполненной очереди

            A429_Word* wptr = this->channel->DataWord(i); // данные из очереди, начиная с первого
            if( !wptr )continue;

            quint32 wrd = wptr->GetData(); // текущее просматриваемое слово в буфере

            // сравниваем адресом
            ex = ((wrd >> list_addrexist[num].mb) & list_addrexist[num].m) == list_addrexist[num].addr;
            ex = ex && wptr->GetReady();
            if( ex )break; //адрес найден
        }
        list_addrexist[num].exist = ex;
    }
    this->counter = 4;

    if( !this->Stop() )
        emit this->layoutChanged();
}
/******************************************************************
*  Сброс данных
*  Вход: нет
*  Выход: нет
******************************************************************/
void ModelParser_Group::slotResetInternalData(void)
{
    for( int i=0;i<list_addrexist.size();i++ )
        list_addrexist[i].exist = false;

    emit this->layoutChanged();
}
/******************************************************************
*  Вывод данных
*  Вход: индекс, роль
*  Выход: значение
******************************************************************/
QVariant ModelParser_Group::data(const QModelIndex& index, int role) const
{
    // фильтруем роли
    if( role != Qt::DisplayRole && role != Qt::BackgroundRole )
        return QVariant();


    if( index.row() == 0 )
    {
        if( list_addrexist[index.column()].exist ) // слово есть в входном буфере
        {
            if( role == Qt::DisplayRole )
                return "ДА";
            if( role == Qt::BackgroundRole )
                return Qt::green;
        }
        else // слова нет в буфере
        {
            if( role == Qt::DisplayRole )
                return "НЕТ";
            if( role == Qt::BackgroundRole )
                return Qt::red;
        }
    }
    else if( index.row() == 1 )
    {
        if( index.column() )
            return QVariant();

        if( !this->pivdata->st_n_group.link_control && role == Qt::DisplayRole )
            return "Нет контроля целостности";

        bool ex = true;
        for( int i=0;i<this->list_addrexist.size();i++ )
            if( !(ex = list_addrexist[i].exist) )
                break;

        if( ex )
        {
            if( role == Qt::DisplayRole )
                return "ДА";
            if( role == Qt::BackgroundRole )
                return Qt::green;
        }
        else
        {
            if( role == Qt::DisplayRole )
                return "НЕТ";
            if( role == Qt::BackgroundRole )
                return Qt::red;
        }
    }
    else
    {
        if( this->channel->Size() >= this->pivdata->children.size() && role == Qt::DisplayRole )
            return "Нет рекомендаций";
        else if( role == Qt::DisplayRole )
            return QString().sprintf("Увеличьте размер буфера до %d",this->pivdata->children.size());
    }
    return QVariant();
}
/******************************************************************
*  Получить размер буфера групп
*  Вход: нет
*  Выход: размер буфера
******************************************************************/
int ModelParser_Group::Size(void)
{
    return list_addrexist.size();
}
/******************************************************************
*  Установить размер буфера групп
*  Вход: размер
*  Выход: нет
******************************************************************/
void ModelParser_Group::Size(int)
{
}
