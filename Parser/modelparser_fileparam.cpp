#include "modelparser_fileparam.h"

ModelParser_FileParam::ModelParser_FileParam(A429_Channel *ch, PivData *pd, ModelParserBase *mpb) :
    ModelParserBase(ch,pd,mpb)
{
    this->strl_header_v.clear();
    this->strl_header_v.append("Адрес");
    this->strl_header_v.append("№ пп (для отладки)");
    for(int i=0;i<pivdata->children.size();i++)
        this->strl_header_v.append(pivdata->children.at(i)->info.Name);

    Size(10+1); // буфер параметров
    fp = fileparam.dequeue(); // выделяем промежуточный указатель
    static_size--;

    numpp = 0;

    this->dblDataInit();
}
ModelParser_FileParam::~ModelParser_FileParam(void)
{
    this->Size(0); // очищаем буфер файлов
    delete fp; // удаляем промежуточный указатель
}

/******************************************************************
*  Получить кол-во столбцов
*  Вход: индекс родителя
*  Выход: кол-во
******************************************************************/
int ModelParser_FileParam::columnCount(const QModelIndex&) const
{
    return fileparam.size();
}
/******************************************************************
*  Получить данные заголовков представления
*  Вход: секция, ориентация, роль
*  Выход: данные
******************************************************************/
QVariant ModelParser_FileParam::headerData(int section, Qt::Orientation orientation, int role) const
{
    if( role == Qt::DisplayRole )
    {
        if( orientation == Qt::Vertical )
            return this->strl_header_v.at(section);
        if( orientation == Qt::Horizontal )
        {
            int size = fileparam.size();
            int col = size - section - 1;
            if( col < 0 || col >= size || size > static_size )
                return QVariant();

            return fileparam[col]->time.toString("hh:mm:ss:zzz");
        }
    }
    return QVariant();
}

/******************************************************************
*  Вывод данных
*  Вход: индекс, роль
*  Выход: значение
******************************************************************/
QVariant ModelParser_FileParam::data(const QModelIndex& index, int role) const
{
    int size = fileparam.size();
    int col = size - index.column() - 1;
    if( col < 0 || col >= size || size > static_size )
        return QVariant();

    if( !this->fileparam[col]->ready && (index.row() == 0 || index.column() != 0) )
        if( role == Qt::BackgroundRole )
            return QColor(200,200,200);

    if( index.row() == 0 ) // строка Адрес
    {
        if( role == Qt::DisplayRole )
            return QString().sprintf("A%o(8)",pivdata->st_n_param.id);
        else if( role == Qt::BackgroundRole )
        {
            if( !this->fileparam[col]->ready )
                return QColor(200,200,200);
            else
                return QVariant();
        }
        else
            return QVariant();
    }
    else if( index.row() == 1 ) // для отладки
    {
        if( role == Qt::DisplayRole )
            return QString().sprintf("%ld(гр: %ld)",this->fileparam[col]->numpp,this->fileparam[col]->group_numpp);
        else
            return QVariant();
    }
    else
    {
        PivData* pd = pivdata->children.at(index.row()-2); // получаем указатель на элемент параметра

        if( pd->info.type == PivData::N_ELP_GBIT ) // элемент слова
        {
            return Fnc_GBIT(pd,fileparam[col]->word,role);
        }
        else if( pd->info.type == PivData::N_ELP_BIT ) // признак
        {
            return Fnc_BIT(pd,fileparam[col]->word,role);
        }
        else
            return "Элемент не распознан";
    }
}

/******************************************************************
*  Разбор параметра группы файла
*  Вход: нет
*  Выход: нет
******************************************************************/
void ModelParser_FileParam::slotUpdateInternalData(A429_Word *)
{
    ModelParser_FileGroup* mpg = (ModelParser_FileGroup*)sender();

    bool is_update = false; // флаг обновления данных

    for( int i=0;i<mpg->Size();i++ )
    {
        ModelParser_FileGroup::FileGroup* fg = mpg->Group(i);
        for( int j=0;j<fg->size;j++ )
        {
            fp->time = fg->time;
            fp->_time = fg->_time;
            fp->word = fg->file[j];
            fp->id = (fp->word >> pivdata->st_n_param.offset) & pivdata->st_n_param.mask;
            fp->group_numpp = fg->numpp;
            fp->ready = fg->ready;

            if( fp->id != pivdata->st_n_param.id ) // адрес не найден в файле, продолжаем поиск
                continue;

            // поиск параметра этой группы в сохраненных параметрах
            bool param_exist = false;
            for( int k=0;k<Size() && !param_exist;k++ )
                param_exist = fp->group_numpp == fileparam[k]->group_numpp;
            if( param_exist ) // параметр нашли, нет смысла далее смотреть этот файл, продолжаем в следующей группе
                continue;

            fp->numpp = ++numpp;

            this->mutex.lock();
            fileparam.enqueue(fp); // добавляем в очередь
            emit this->signalAddedInternalQueue(fp->_time,fp->time);
            fp = fileparam.dequeue();
            this->mutex.unlock();

            is_update = true;
            if( this->RecToFile() )
                emit this->signalWriteToFile(this->csvData());
        }
    }

    if( is_update )
    {
        emit this->signalUpdatedInternalData(0); // сигнал изменения данных
        this->counter = 4; // счетчик актуальности данных
        if( !this->Stop() )
            emit this->layoutChanged(); // обновить представления
    }
}
/******************************************************************
*  Сброс данных
*  Вход: нет
*  Выход: нет
******************************************************************/
void ModelParser_FileParam::slotResetInternalData(void)
{
    QMutexLocker(&this->mutex);

    int s = fileparam.size();
    while( --s >= 0 )
        fileparam[s]->ready = false;

    emit this->layoutChanged();
}
/******************************************************************
*  Получить размер буфера параметров
*  Вход: нет
*  Выход: размер буфера
******************************************************************/
int ModelParser_FileParam::Size(void)
{
    QMutexLocker(&this->mutex);
    return this->fileparam.size();
}
/******************************************************************
*  Установить размер буфера параметров
*  Вход: размер
*  Выход: нет
******************************************************************/
void ModelParser_FileParam::Size(int s)
{
    QMutexLocker(&this->mutex);

    while( fileparam.size() < s ) // заполняем очередь пустышками
    {
        fileparam.enqueue(new FileParam);
        memset(fileparam.last(),0x0,sizeof(FileParam));
    }

    while( fileparam.size() > s ) // очищаем очередь файлов
    {
        FileParam* f = fileparam.dequeue();
        delete f;
    }
    static_size = fileparam.size();
}
/******************************************************************
*  Возврат строки с данными в формате csv
*  Вход: нет
*  Выход: данные
******************************************************************/
QString ModelParser_FileParam::csvData(void)
{
    QString str = "";
    FileParam* param = fileparam.last();

    str += param->time.toString("hh:mm:ss:zzz") + ";";
    str += QString().sprintf("%d;",param->_time);

    str += QString().sprintf("%o(8);",param->id); // запись адреса
    str += QString().sprintf("%ld(гр: %ld);",param->numpp,param->group_numpp); // запись №пп
    for(int i=0;i<this->pivdata->children.size();i++)
    {
        PivData* pd = this->pivdata->children.at(i); // получаем указатель на элемент параметра

        if( pd->info.type == PivData::N_ELP_GBIT ) // элемент слова
            str += Fnc_GBIT(pd,param->word,Qt::DisplayRole,false).toString() + ";";
        else if( pd->info.type == PivData::N_ELP_BIT ) // признак
            str += Fnc_BIT(pd,param->word,Qt::DisplayRole).toString() + ";";
        else
            str += "Не распознано;";

        str = str.replace(".",",");
    }

    return str;
}
/******************************************************************
*  Обновление данных в массиве double (для графика)
*  Вход: нет
*  Выход: нет
******************************************************************/
void ModelParser_FileParam::dblDataUpdate(void)
{
    double *d = const_cast<double*>(this->dblData());
    FileParam* param = fileparam.last();

    *(d+0) = (double)param->id;
    *(d+1) = (double)param->numpp; // №пп

    for(int i=0;i<this->pivdata->children.size();i++)
    {
        PivData* pd = this->pivdata->children.at(i); // получаем указатель на элемент параметра

        if( pd->info.type == PivData::N_ELP_GBIT ) // элемент слова
            *(d+2+i) = Fnc_GBIT(pd,param->word,ModelParserBase::DoubleRole).toDouble();
        else if( pd->info.type == PivData::N_ELP_BIT ) // признак
            *(d+2+i) = Fnc_BIT(pd,param->word,ModelParserBase::DoubleRole).toDouble();
        else
            *(d+2+i) = 0.0;
    }
}
