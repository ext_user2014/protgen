#ifndef MODELPARSER_DELEGATEBASE_H
#define MODELPARSER_DELEGATEBASE_H

#include <QItemDelegate>
#include <QLabel>
#include <QMap>

typedef QMap<QModelIndex,QLabel*> MapLabel;

class ModelParser_DelegateBase : public QItemDelegate
{
    Q_OBJECT
public:
    explicit ModelParser_DelegateBase(QObject *parent = 0);

    QWidget *createEditor(QWidget *parent, const QStyleOptionViewItem &option, const QModelIndex &index) const;    // создание редактора
    void setEditorData(QWidget *editor, const QModelIndex &) const;         // загрузка данных в редактор
    void setModelData(QWidget *, QAbstractItemModel *, const QModelIndex &) const;  // загрузка данных из редактора в модель данных

    MapLabel map_label;

public slots:
    void slotUpdateLabels(QModelIndex,QString);
};

#endif // MODELPARSER_DELEGATEBASE_H
