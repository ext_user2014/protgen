#include "modelparser_file.h"

ModelParser_File::ModelParser_File(A429_Channel *ch, PivData *pd) :
    ModelParserBase(ch,pd)
{
    this->strl_header_v.append("Базовый адрес");
    this->strl_header_v.append("Группа (oct)");
    this->strl_header_v.append("Размер");
    this->strl_header_v.append("№ пп (для отладки)");

    Size(10+1); // буфер файлов
    file = files.dequeue(); // выделяем промежуточный указатель
    static_size--;

    numpp = 0x0; // номер файла

    this->dblDataInit();
}
ModelParser_File::~ModelParser_File(void)
{
    this->Size(0); // очищаем буфер файлов
    free(file->file);
    delete file; // удаляем промежуточный файл
}

/******************************************************************
*  Получить кол-во столбцов
*  Вход: индекс родителя
*  Выход: кол-во
******************************************************************/
int ModelParser_File::columnCount(const QModelIndex&) const
{
    return files.size(); // кол-во столбцов = количество файлов
}
/******************************************************************
*  Получить данные заголовков представления
*  Вход: секция, ориентация, роль
*  Выход: данные
******************************************************************/
QVariant ModelParser_File::headerData(int section, Qt::Orientation orientation, int role) const
{
    if( role == Qt::DisplayRole )
    {
        if( orientation == Qt::Vertical )
            return this->strl_header_v.at(section);
        if( orientation == Qt::Horizontal )
        {
            int size = files.size();
            int col = size - section - 1;
            if( col < 0 || col >= size || col >= static_size )
                return QVariant();

            return files.at(col)->time.toString("hh:mm:ss:zzz");
        }
    }
    return QVariant();
}
/******************************************************************
*  Вывод данных
*  Вход: индекс, роль
*  Выход: значение
******************************************************************/
QVariant ModelParser_File::data(const QModelIndex& index, int role) const
{
    int size = files.size();
    int col = size - index.column() - 1;
    if( col < 0 || col >= size || col >= static_size )
        return QVariant();

    if( role == Qt::DisplayRole )
    {
        switch( index.row() )
        {
        case 0: // базовый адрес
            return QString().sprintf("A%o(8)",this->pivdata->st_n_group.b_addr);
        case 1: // идентификатор группы
            return QString().sprintf("%o",this->files[col]->group);
        case 2: // размер
            return this->files[col]->size;
        case 3: // для отладки
            return (quint64)this->files[col]->numpp;
        default:
            return QVariant();
        }
    }
    else if( role == Qt::BackgroundRole && (index.row() == 0 || index.column() != 0) ) // перекрашиваем строку адреса
    {
        if( !this->files[col]->ready ) // если данные устарели
            return QColor(200,200,200);
        else
            return QVariant();
    }
    else if( role == Qt::ToolTipRole )
    {
        switch( index.row() )
        {
        case 1: // идентификатор группы
        {
            int idg = this->files[col]->group;
            return QString().sprintf("oct: %o\nhex: 0x%x\ndec: %d",idg,idg,idg);
        }
        case 0: // базовый адрес
        case 2: // размер
        case 3: // для отладки
        default:
            return QVariant();
        }
    }

    return QVariant();
}
/******************************************************************
*  Обновление данных
*  Вход: нет
*  Выход: нет
******************************************************************/
void ModelParser_File::slotUpdateInternalData(A429_Word *)
{
    ModelParser_File::StFile* f; // файл из хранилища

    bool is_update = false; // флаг обновления данных

    // ищем адрес в входном буфере
    for(int i=0; i<this->channel->Size(); i++)
    {
        A429_Word* wptr = this->channel->DataWord(i); // данные из очереди, начиная с первого
        if( !wptr )continue;

        quint32 wrd = wptr->GetData(); // вытаскиваем данные
        quint32 addr = (wrd >> this->pivdata->st_n_group.b_addr_offset) & this->pivdata->st_n_group.b_addr_mask; // смотрим адрес

        if( addr == this->pivdata->st_n_group.b_addr ) // базовый адрес файла найден
        {
            quint32 _time = wptr->GetTimeMS(); // вытаскиеваем время прихода файла по базовому адресу
            quint32 _size = (wrd >> this->pivdata->st_n_group.size_offset) & this->pivdata->st_n_group.size_mask; // размер файла

            f = file; // самый младший файл
            if( f->_time > _time ) // старые файлы в буфере канала не интересны, продолжаем
            {
                i += _size; // на следующем цикле i будет на позиции слова, следующим сразу за файлом
                continue;
            }

            f = File(Size()-1); // самый свежий файл
            if( f ) // файл есть, смотрим время его создания
            {
                if( f->_time >= _time ) // существующие файлы не интересны, продолжаем
                {
                    i += _size;
                    continue;
                }
            }

            i++; // смещаемся на одно слово, пропуская служебное слово файла
            // проверки пройдены, новый файл сохраняем
            file->base_word = wrd;
            file->group = (wrd >> this->pivdata->st_n_group.id_offset) & this->pivdata->st_n_group.id_mask;
            file->size = _size;
            file->time = wptr->GetTime(); // время прихода слова
            file->_time = _time; // время прихода слова
            file->small_buffer = (i + file->size) >= (uint)this->channel->Size(); // файл НЕ влезает в буфер?
            file->ready = wptr->GetReady();

            if( !file->small_buffer ) // файл влез в буфер, сохраняем его
            {
                this->mutex.lock();

                for( uint j=i;j<=(i + file->size);j++ ) // сохраняем файл
                    file->file[j-i] = this->channel->DataWord(j)->GetData();

                file->numpp = ++numpp;
                i += file->size; // смещаемся на размер файла в буфере (ускоряет поиск!), считая, что файл не битый во временной шкале

                files.enqueue(file); // в конец очереди (т.о. в конце очереди - самые свежие данные)
                emit this->signalAddedInternalQueue(file->_time,file->time);
                file = files.dequeue(); // промежуточное значение теперь бывший первый в очереди

                this->mutex.unlock();

                is_update = true;
                if( this->RecToFile() )
                    emit this->signalWriteToFile(this->csvData());
            }
        }
    }

    if( is_update )
    {
        emit this->signalUpdatedInternalData(0); // сигнал изменения данных
        this->counter = 4; // счетчик актуальности данных
        if( !this->Stop() )
            emit this->layoutChanged(); // обновить представления
    }
}
/******************************************************************
*  Сброс данных
*  Вход: нет
*  Выход: нет
******************************************************************/
void ModelParser_File::slotResetInternalData(void)
{
    QMutexLocker(&this->mutex);

    int s = files.size();
    while( --s >= 0 )
        files[s]->ready = false;

    emit this->layoutChanged();
}
/******************************************************************
*  Получить указатель на файл по индексу
*  Вход: индекс
*  Выход: указатель на файл
******************************************************************/
ModelParser_File::StFile* ModelParser_File::File(int i)
{
    return files[i];
}
/******************************************************************
*  Получить размер буфера файлов
*  Вход: нет
*  Выход: размер буфера
******************************************************************/
int ModelParser_File::Size(void)
{
    QMutexLocker(&this->mutex);
    return this->files.size();
}
/******************************************************************
*  Установить размер буфера файлов
*  Вход: размер
*  Выход: нет
******************************************************************/
void ModelParser_File::Size(int s)
{
    QMutexLocker(&this->mutex);

    while( files.size() < s ) // заполняем очередь файлами-пустышками
    {
        files.enqueue(new StFile);
        memset(files.last(),0x0,sizeof(StFile));
        files.last()->file = (quint32*)calloc(this->Piv()->st_n_group.size_max,sizeof(quint32));
    }

    while( files.size() > s ) // очищаем очередь файлов
    {
        StFile* f = files.dequeue();
        free(f->file);
        delete f;
    }
    static_size = files.size();
}
/******************************************************************
*  Возврат строки с данными в формате csv
*  Вход: нет
*  Выход: данные
******************************************************************/
QString ModelParser_File::csvData(void)
{
    QString str = "";
    StFile* f = files.last();

    str += f->time.toString("hh:mm:ss:zzz") + ";";
    str += QString().sprintf("%d;",f->_time);

    str += QString().sprintf("%o(8);",Piv()->st_n_group.b_addr); // Базовый адрес
    str += QString().sprintf("%o;",f->group); // Группа (oct)
    str += QString().sprintf("%d;",f->size); // Размер
    str += QString().sprintf("%ld;",f->numpp); // № пп

    return str;
}
/******************************************************************
*  Обновление данных в массиве double (для графика)
*  Вход: нет
*  Выход: нет
******************************************************************/
void ModelParser_File::dblDataUpdate(void)
{
    double *d = const_cast<double*>(this->dblData());
    StFile* f = files.last();

    *(d+0) = (double)Piv()->st_n_group.b_addr;
    *(d+1) = (double)f->group;
    *(d+2) = (double)f->size;
    *(d+3) = (double)f->numpp;
}



