#include "modelparser_filegroup.h"

ModelParser_FileGroup::ModelParser_FileGroup(A429_Channel *ch, PivData *pd, ModelParserBase *mpb) :
    ModelParserBase(ch,pd,mpb)
{
    this->strl_header_v.clear();

    this->strl_header_v.append("Идентификатор группы (oct)");
    this->strl_header_v.append("Целостность ФО");
    this->strl_header_v.append("№ пп (для отладки)");

    Size(10+1); // буфер файлов
    fg = filegroup.dequeue(); // выделяем промежуточный указатель
    static_size--;

    numpp = 0x0; // номер файла

    this->dblDataInit();
}
ModelParser_FileGroup::~ModelParser_FileGroup(void)
{
    this->Size(0); // очищаем буфер файлов
    free(fg->file);
    delete fg->errors;
    delete fg; // удаляем промежуточный файл
}
/******************************************************************
*  Получить кол-во столбцов
*  Вход: индекс родителя
*  Выход: кол-во
******************************************************************/
int ModelParser_FileGroup::columnCount(const QModelIndex&) const
{
    return filegroup.size();
}
/******************************************************************
*  Получить данные заголовков представления
*  Вход: секция, ориентация, роль
*  Выход: данные
******************************************************************/
QVariant ModelParser_FileGroup::headerData(int section, Qt::Orientation orientation, int role) const
{
    if( role == Qt::DisplayRole )
    {
        if( orientation == Qt::Vertical )
            return this->strl_header_v.at(section);
        if( orientation == Qt::Horizontal )
        {
            int size = filegroup.size();
            int col = size - section - 1;
            if( col < 0 || col >= size || col >= static_size )
                return QVariant();

            return filegroup.at(col)->time.toString("hh:mm:ss:zzz");
        }
    }
    return QVariant();
}

/******************************************************************
*  Вывод данных
*  Вход: индекс, роль
*  Выход: значение
******************************************************************/
QVariant ModelParser_FileGroup::data(const QModelIndex& index, int role) const
{
    int size = filegroup.size();
    int col = size - index.column() - 1;
    if( col < 0 || col >= size || col >= static_size )
        return QVariant();

    if( role == Qt::DisplayRole )
    {
        switch( index.row() )
        {
        case 0: // Идентификатор группы
            return QString().sprintf("%o",filegroup[col]->id);
        case 1: // Целостность группы
            return filegroup[col]->ctrl?"ОК":"Ошибка";
        case 2: // для отладки
            return QString().sprintf("%ld(ф:%ld)",this->filegroup[col]->numpp,this->filegroup[col]->file_numpp);
        default:
            return QVariant();
        }
    }
    else if( role == Qt::BackgroundRole )
    {
        if( !this->filegroup[col]->ready && (index.row() == 0 || index.column() != 0) )
            return QColor(200,200,200);

        if(  !this->filegroup[col]->ctrl ) // Целостность группы
            return Qt::red;
        else
            return Qt::green;
    }
    else if( role == Qt::ToolTipRole )
    {
        switch( index.row() )
        {
        case 0: // Идентификатор группы
        {
            int idg = this->filegroup[col]->id;
            return QString().sprintf("oct: %o\nhex: 0x%x\ndec: %d",idg,idg,idg);
        }
        case 1: // Целостность группы
            if( !this->filegroup[col]->ctrl )
            {
                QString str = "";
                for( int i=0;i<this->filegroup[col]->errors->size();i++ )
                    str += this->filegroup[col]->errors->at(i) + "\n";

                return str;
            }
            else
                return QVariant();
        default:
            return QVariant();
        }
    }
    else
        return QVariant();

    return QVariant();
}
/******************************************************************
*  Разбор группы файла
*  Вход: нет
*  Выход: нет
******************************************************************/
void ModelParser_FileGroup::slotUpdateInternalData(A429_Word *)
{
    ModelParser_File* mpf = (ModelParser_File*)sender();

    bool is_update = false; // флаг обновления данных

    for( int i=0;i<mpf->Size();i++ )
    {
        ModelParser_File::StFile* f = mpf->File(i);
        if( !f )continue;

        if( f->group != pivdata->st_n_group.id ) // не наша группа, продолжаем
            continue;

        // формируем свойства группы
        fg->id = f->group;
        fg->ctrl = false;
        fg->time = f->time;
        fg->_time = f->_time;
        fg->size = f->size;
        fg->file_numpp = f->numpp;


        bool group_exist = false;
        for( int j=0;j<filegroup.size() && !group_exist;j++ )
            group_exist = filegroup.at(j)->file_numpp == fg->file_numpp; // группа уже сохранена
        if( group_exist )
            continue;

        this->mutex.lock();
        // переписываем файл (хранить указатель нельзя, т.к. структура его содержащая може быть вытолкнута из очереди в ModelParser_File)
        memcpy((void*)fg->file,(void*)f->file,fg->size*4);

        fg->errors->clear();
        this->FileCheck(fg); // контроль файла

        fg->numpp = ++numpp;
        fg->ready = f->ready;

        filegroup.enqueue(fg); // выстраиваем очередь
        emit this->signalAddedInternalQueue(fg->_time,fg->time);
        fg = filegroup.dequeue();

        this->mutex.unlock();

        is_update = true;
        if( this->RecToFile() )
            emit this->signalWriteToFile(this->csvData());
    }

    if( is_update )
    {
        emit this->signalUpdatedInternalData(0); // сигнал изменения данных
        this->counter = 4; // счетчик актуальности данных
        if( !this->Stop() )
            emit this->layoutChanged(); // обновить представления
    }
}
/******************************************************************
*  Сброс данных
*  Вход: нет
*  Выход: нет
******************************************************************/
void ModelParser_FileGroup::slotResetInternalData(void)
{
    QMutexLocker(&this->mutex);

    int s = filegroup.size();
    while( --s >= 0 )
        filegroup[s]->ready = false;

    emit this->layoutChanged();
}
/******************************************************************
*  Проверка целостности файлового блока
*  Вход: нет
*  Выход: нет
******************************************************************/
void ModelParser_FileGroup::FileCheck(FileGroup *ptr_fg)
{
    quint32 wrd = 0x0;
    for( int j=1;j<ptr_fg->size;j++ ) // разбор файла, начиная с 1-го слова
    {
        wrd = ptr_fg->file[j]; // берем слово
        quint32 addr = (wrd >> pivdata->st_n_group.b_addr_offset) & pivdata->st_n_group.b_addr_mask; // адрес слова

        if( (addr - pivdata->parent->st_n_group.b_addr) != (quint32)j ) // адрес слова должен отличаться от базового адреса на j
            ptr_fg->errors->append(QString().sprintf("Смещение '%d' - неверный адрес",j));
    }
    ptr_fg->ctrl = ptr_fg->errors->isEmpty();
}
/******************************************************************
*  Получить указатель на группу по индексу
*  Вход: индекс
*  Выход: указатель на группу
******************************************************************/
ModelParser_FileGroup::FileGroup* ModelParser_FileGroup::Group(int i)
{
    QMutexLocker(&this->mutex);

    if( i >= filegroup.size() )
        return 0x0;

    return filegroup[i];
}

/******************************************************************
*  Получить размер буфера файлов
*  Вход: нет
*  Выход: размер буфера
******************************************************************/
int ModelParser_FileGroup::Size(void)
{
    QMutexLocker(&this->mutex);
    return this->filegroup.size();
}
/******************************************************************
*  Установить размер буфера файлов
*  Вход: размер
*  Выход: нет
******************************************************************/
void ModelParser_FileGroup::Size(int s)
{
    QMutexLocker(&this->mutex);

    while( filegroup.size() < s ) // заполняем очередь файлами-пустышками
    {
        filegroup.enqueue(new FileGroup);
        memset(filegroup.last(),0x0,sizeof(FileGroup));
        filegroup.last()->file = (quint32*)calloc(this->Piv()->parent->st_n_group.size_max,sizeof(quint32));
        filegroup.last()->errors = new QStringList();
    }

    while( filegroup.size() > s ) // очищаем очередь файлов
    {
        FileGroup* f = filegroup.dequeue();
        free(f->file);
        delete f->errors;
        delete f;
    }
    static_size = filegroup.size();
}
/******************************************************************
*  Возврат строки с данными в формате csv
*  Вход: нет
*  Выход: данные
******************************************************************/
QString ModelParser_FileGroup::csvData(void)
{
    QString str = "";
    FileGroup* group = filegroup.last();

    str += group->time.toString("hh:mm:ss:zzz") + ";";
    str += QString().sprintf("%d;",group->_time);

    str += QString().sprintf("%o;",group->id); // Идентификатор группы (oct)

    for(int i=0;i<group->errors->size();i++) // Целостность ФО
        str += group->errors->at(i) + "   ";
    str += ";";

    str += QString().sprintf("%ld(ф:%ld);",group->numpp,group->file_numpp); // запись №пп

    return str;
}
/******************************************************************
*  Обновление данных в массиве double (для графика)
*  Вход: нет
*  Выход: нет
******************************************************************/
void ModelParser_FileGroup::dblDataUpdate(void)
{
    double *d = const_cast<double*>(this->dblData());
    FileGroup* group = filegroup.last();

    *(d+0) = (double)group->id;
    *(d+1) = (double)group->errors->size();
    *(d+2) = (double)group->numpp;
}
