#include "modelparser_param.h"

ModelParser_Param::ModelParser_Param(A429_Channel *ch, PivData *pd) :
    ModelParserBase(ch,pd)
{
    this->strl_header_v.append("Адрес");
    this->strl_header_v.append("Частота следования слова");
    for(int i=0;i<this->pivdata->children.size();i++)
    {
        this->strl_header_v.append(this->pivdata->children.at(i)->info.Name);
        if( this->pivdata->children.at(i)->st_n_elp_gbit.t == PivData::st_N_ELP_GBIT::GB && \
            this->pivdata->children.at(i)->st_n_elp_gbit.fnc == PivData::st_N_ELP_GBIT::N && \
            this->pivdata->children.at(i)->info.type == PivData::N_ELP_GBIT ) // простая группировка бит, надо отображать через редактор
        {
            this->rows_open_persistent_editor.insert(i + 2,true);
        }
        else
        {
            this->rows_open_persistent_editor.insert(i + 2,false);
        }
    }

    Size(11);
    p = params.dequeue();
    static_size--;

    this->dblDataInit();
}

ModelParser_Param::~ModelParser_Param()
{
    this->Size(0); // очищаем буфер
    QMutexLocker(&this->mutex);
    delete p; // удаляем промежуточный указатель
}

/******************************************************************
*  Обновление данных
*  Вход: указатель на пришедшее в канал слово
*  Выход: нет
******************************************************************/
void ModelParser_Param::slotUpdateInternalData(A429_Word *wptr)
{
    bool is_update = false; // флаг обновления данных

    if( this->Stop() ) // обновлять данные не надо, выходим (!!!! временно, для разгрузки потока!)
        return;

    if( this->channel->Ready() || !this->channel->Saveable() ) // данные в канале есть или включена пауза
    {
        quint32 wrd = wptr->GetData();

        p->id = (wrd >> this->pivdata->st_n_param.offset) & this->pivdata->st_n_param.mask;

        if( p->id != pivdata->st_n_param.id ) // адрес не тот, выходим
            return;

        quint32 time = wptr->GetTimeMS();

        if( p->_time > time ) // параметр старый, выходим
            return;

        p->ready = wptr->GetReady();

        if( !p->ready ) // нет готовности параметра, выходим
            return;

        this->mutex.lock();
        quint32 lasttime = params.last()->_time;
        this->mutex.unlock();
        if( lasttime >= time ) // параметр уже в буфере параметров, выходим
            return;

        p->word = wrd;
        p->time = wptr->GetTime();
        p->_time = time;

        float per = (time - params.last()->_time);
        if( per == 0.0 )
            p->f = 1000/0.01;
        else
            p->f = 1000/per;

        this->mutex.lock();
        params.enqueue(p); // добавляем в очередь
        emit this->signalAddedInternalQueue(p->_time,p->time);
        p = params.dequeue();
        this->mutex.unlock();

        is_update = true;
        if( this->RecToFile() )
            emit this->signalWriteToFile(this->csvData());
    }

    if( is_update )
    {
        this->counter = 3;
        if( !this->Stop() )
            emit this->layoutChanged();
    }
}
/******************************************************************
*  Сброс данных
*  Вход: нет
*  Выход: нет
******************************************************************/
void ModelParser_Param::slotResetInternalData(void)
{
    QMutexLocker(&this->mutex);

    int s = params.size();
    while( --s >= 0 )
        params[s]->ready = false;

    emit this->layoutChanged();
}
/******************************************************************
*  Вывод данных
*  Вход: индекс, роль
*  Выход: значение
******************************************************************/
QVariant ModelParser_Param::data(const QModelIndex& index, int role) const
{
    // фильтруем роли
    if( role != Qt::DisplayRole && role != Qt::BackgroundRole && role != Qt::ToolTipRole )
        return QVariant();

    int size = params.size();
    int col = size - index.column() - 1;
    if( col < 0 || col >= size || size > static_size )
        return QVariant();

    if( index.row() == 0 ) // строка Адрес
    {
        if( role == Qt::DisplayRole )
            return QString().sprintf("A%o(8)",this->pivdata->st_n_param.id);
        else if( role == Qt::BackgroundRole )
        {
            if( !params[col]->ready )
                return QColor(200,200,200);
            else
                return QVariant();
        }
        else
            return QVariant();
    }
    else if( index.row() == 1 ) // строка Контроль частоты следования
    {
        if( role == Qt::DisplayRole )
            return QString().sprintf("%.2f Гц",params[col]->f);
        else if( role == Qt::BackgroundRole )
        {
            if( !params[col]->ready && index.column() ) // неготовые данные красим серым, начиная со второго столбца
                return QColor(200,200,200);

            if( !this->pivdata->st_n_param.f )
                return QVariant();

            if( (float)this->pivdata->st_n_param.f > params[col]->f )
                return Qt::yellow;
            else
                return Qt::green;
        }
        else
            return QVariant();
    }
    else
    {
        // получаем указатель на элемент параметра
        PivData* pd = this->pivdata->children.at(index.row()-2);

        if( !params[col]->ready && index.column() && role == Qt::BackgroundRole ) // неготовые данные красим серым, начиная со второго столбца
            return QColor(200,200,200);

        if( pd->info.type == PivData::N_ELP_GBIT ) // элемент слова
        {
            if( this->rows_open_persistent_editor[index.row()] && role == Qt::DisplayRole ) // на отображение выдается параметр через редактор
            {
                emit this->signalDisplayToLabel(index,Fnc_GBIT(pd,params[col]->word,role).toString()); // сигнал редактору
                return QVariant();
            }
            else // обычный вывод
                return Fnc_GBIT(pd,params[col]->word,role); // обычный вывод
        }
        else if( pd->info.type == PivData::N_ELP_BIT ) // признак
        {
            return Fnc_BIT(pd,params[col]->word,role);
        }
        else
            return "Элемент не распознан";
    }
}
/******************************************************************
*  Получить кол-во столбцов
*  Вход: индекс родителя
*  Выход: кол-во
******************************************************************/
int ModelParser_Param::columnCount(const QModelIndex&) const
{
    return params.size();
}
/******************************************************************
*  Получить данные заголовков представления
*  Вход: секция, ориентация, роль
*  Выход: данные
******************************************************************/
QVariant ModelParser_Param::headerData(int section, Qt::Orientation orientation, int role) const
{
    if( role == Qt::DisplayRole )
    {
        if( orientation == Qt::Vertical )
            return this->strl_header_v.at(section);
        if( orientation == Qt::Horizontal )
        {
            int size = params.size();
            int col = size - section - 1;
            if( col < 0 || col >= size || size > static_size )
                return QVariant();

            return params[col]->time.toString("hh:mm:ss:zzz");
        }
    }
    return QVariant();
}
/******************************************************************
*  Получить размер буфера параметров
*  Вход: нет
*  Выход: размер буфера
******************************************************************/
int ModelParser_Param::Size(void)
{
    QMutexLocker(&this->mutex);
    return this->params.size();
}
/******************************************************************
*  Установить размер буфера параметров
*  Вход: размер
*  Выход: нет
******************************************************************/
void ModelParser_Param::Size(int s)
{
    QMutexLocker(&this->mutex);

    while( params.size() < s ) // заполняем очередь пустышками
    {
        params.enqueue(new StParam);
        memset(params.last(),0x0,sizeof(StParam));
    }

    while( params.size() > s ) // очищаем очередь файлов
    {
        StParam* f = params.dequeue();
        delete f;
    }
    static_size = params.size();
}
/******************************************************************
*  Возврат строки с данными в формате csv
*  Вход: нет
*  Выход: данные
******************************************************************/
QString ModelParser_Param::csvData(void)
{
    QString str = "";
    StParam* param = params.last();

    str += param->time.toString("hh:mm:ss:zzz") + ";";
    str += QString().sprintf("%d;",param->_time);

    str += QString().sprintf("%o(8);",param->id); // запись адреса
    str += QString().sprintf("%.2f;",param->f); // запись частоты следования слова
    for(int i=0;i<this->pivdata->children.size();i++)
    {
        PivData* pd = this->pivdata->children.at(i); // получаем указатель на элемент параметра

        if( pd->info.type == PivData::N_ELP_GBIT ) // элемент слова
            str += Fnc_GBIT(pd,param->word,Qt::DisplayRole,false).toString() + ";";
        else if( pd->info.type == PivData::N_ELP_BIT ) // признак
            str += Fnc_BIT(pd,param->word,Qt::DisplayRole).toString() + ";";
        else
            str += "Не распознано;";

        str = str.replace(".",",");
    }

    return str;
}
/******************************************************************
*  Обновление данных в массиве double (для графика)
*  Вход: нет
*  Выход: нет
******************************************************************/
void ModelParser_Param::dblDataUpdate(void)
{
    double *d = const_cast<double*>(this->dblData());
    StParam* param = params.last();

    *(d+0) = (double)param->id;
    *(d+1) = param->f;

    for(int i=0;i<this->pivdata->children.size();i++)
    {
        PivData* pd = this->pivdata->children.at(i); // получаем указатель на элемент параметра

        if( pd->info.type == PivData::N_ELP_GBIT ) // элемент слова
            *(d+2+i) = Fnc_GBIT(pd,param->word,ModelParserBase::DoubleRole).toDouble();
        else if( pd->info.type == PivData::N_ELP_BIT ) // признак
            *(d+2+i) = Fnc_BIT(pd,param->word,ModelParserBase::DoubleRole).toDouble();
        else
            *(d+2+i) = 0.0;
    }
}

