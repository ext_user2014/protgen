#include "parser.h"

Parser::Parser(MyItemModel *model, QWidget *parent) :
    QWidget(parent)
{
    this->srcmodelpiv = model;

    this->parent = (QTabWidget*)parent;
    this->parent->addTab(this,"Анализатор");

    this->setAttribute(Qt::WA_DeleteOnClose);

    // разметка элементов управления
    QGridLayout* gl = new QGridLayout(this);
    gl->setMargin(5);
    gl->setSpacing(5);

    // кнопки
    this->pb_close = new QPushButton("Закрыть",this);
    connect(pb_close,SIGNAL(clicked()),this,SLOT(deleteLater()));
    this->pb_state = new QPushButton("В отдельном окне",this);
    connect(pb_state,SIGNAL(clicked()),this,SLOT(slotStateWidgetChange()));
    // надпись, что парсер не запущен
    this->l_main = new QLabel(this);
    this->l_main->setHidden(true);
    this->pb_pausa = new QPushButton("Пауза",this);
    connect(pb_pausa,SIGNAL(clicked()),this,SLOT(Pausa()));
    this->pb_plot = new QPushButton("График...",0);
    connect(pb_plot,SIGNAL(clicked()),this,SLOT(slotShowPlot()));

    // дерево элементов ПИВ для парсера
    QScrollArea *sa_twp = new QScrollArea(this);
    QGridLayout *gl_sa_twp = new QGridLayout(sa_twp);
    sa_twp->setLayout(gl_sa_twp);
    tw_parser = new QTreeView(sa_twp);
    tw_parser->setHidden(true);
    tw_parser->setWordWrap(true);
    tw_parser->setSelectionMode(QAbstractItemView::ExtendedSelection);
    tw_parser->setBaseSize(350,600);
    gl_sa_twp->addWidget(tw_parser,0,0);
    gl_sa_twp->setRowStretch(0,1);

    // прогресс бар
    pbar_loadparser = new QProgressBar(sa_twp);
    l_loadparser = new QLabel("",sa_twp);
    gl_sa_twp->addWidget(pbar_loadparser,1,0);
    gl_sa_twp->addWidget(l_loadparser,2,0);

    // элементы для записи в файл
    this->le_file = new QLineEdit(QDateTime::currentDateTime().toString("yyyy-MM-dd_HH-mm")+".csv",this);
    this->pb_file = new QPushButton("Записывать в файл...",this);
    connect(this->pb_file,SIGNAL(clicked()),this,SLOT(slotFileSelect())); // обработка нажатия на кнопку
    this->pb_filerecstop = new QPushButton("Старт запись",this);
    connect(this->pb_filerecstop,SIGNAL(clicked()),this,SLOT(slotRec())); // обработка нажатия на кнопку
    this->pb_settingsfile = new QPushButton("Настройка...",this);
    this->pb_settingsfile->setHidden(true); // !!! настроек пока нет!!!
    pb_select = new QPushButton("Выделить всё",this);
    connect(pb_select,SIGNAL(clicked()),this,SLOT(slotSelectForRec()));

    // панель кнопок и настроек
    QWidget* toolbar1 = new QWidget(this);
    QWidget* toolbar2 = new QWidget(this);
    QGridLayout* gltb1 = new QGridLayout(toolbar1);
    QGridLayout* gltb2 = new QGridLayout(toolbar2);
    gltb1->addWidget(pb_pausa,0,0);
    gltb1->addWidget(pb_state,0,1);
    gltb1->addWidget(pb_close,0,2);
    gltb1->addWidget(pb_plot,0,3);
    gltb2->addWidget(pb_file,0,0);
    gltb2->addWidget(le_file,0,1);
    gltb2->addWidget(pb_filerecstop,0,2);
    gltb2->addWidget(pb_settingsfile,0,3);
    gltb2->addWidget(pb_select,0,4);

    // гуи обеспечение таблиц парсера
    sa_table_parser = new QScrollArea(this); // область прокрутки, родитель - виджет парсера
    QWidget* sawidget = new QWidget(sa_table_parser); // виджет области прокрутки
    gl_table_parser = new QGridLayout(sawidget); // компоновщик области прокрутки
    sawidget->setLayout(gl_table_parser); // устанавливаем компоновщик виджету обл. прокрутки
    sa_table_parser->setWidget(sawidget); // установка виджета обл. прокрутки
    this->sa_table_parser->setHidden(true); // скрываем область прокрутки

    // сплиттер
    QSplitter *splitter = new QSplitter(Qt::Horizontal,this);
    splitter->addWidget(sa_twp);
    splitter->addWidget(sa_table_parser);
    splitter->setSizes(QList<int>()<<350<<this->parentWidget()->width()-350);

    // компонуем гуи
    gl->addWidget(toolbar1,0,0,1,2);
    gl->addWidget(toolbar2,1,0,1,2);
    gl->addWidget(l_main,2,0,1,2);
    gl->addWidget(splitter,2,0,1,2);
    gl->setRowStretch(2,1);

    // открываемся пользователю
    this->parent->setCurrentIndex(this->parent->count()-1);
    this->state = false;

    // инициализация указателей на внешние данные
    this->srcdata = 0x0;
    this->srcpiv = 0x0;
    this->modelpiv = 0x0;

    file = new QFile();

    thparser = new QThread(this);
    thparser->exit();

    file->setParent(0);
    file->moveToThread(thparser);
}

Parser::~Parser(void)
{
    slotClear(); // очищаем парсер

    this->tw_parser->setModel(0x0); // сбрасываем модель с представления (иначе сигсев)

    disconnect(); // удаляем связки с слотами настоящего экземпляра класса

    if( this->modelpiv ) // удаляем прокси-модель источника ПИВ, если она создана
        delete this->modelpiv;

    // закрываем файл
    this->file->close();
    delete this->file;

    this->list_plot.clear();

    thparser->exit(); // стоп поток
    if( !thparser->wait(100) ) // ожидание остановки потока 100 мс
        thparser->terminate(); // не дождались, аварийно закрываем поток (потеря данных не важна)

    thparser->deleteLater();
}

/******************************************************************
*  Слот смены режима окна
*  Вход: нет
*  Выход: нет
******************************************************************/
void Parser::slotStateWidgetChange(void)
{
    if( !this->state )
    {
        this->pb_state->setText("Как вкладка");
        this->setParent(0);

        if( this->srcpiv )
            this->setWindowTitle(QString().number(this->srcdata->Num()+1) + ": " + this->srcpiv->Path());
        else
            this->setWindowTitle("Анализатор");

        this->show();
    }
    else
    {
        this->pb_state->setText("В отдельном окне");
        this->setParent(this->parent);

        if( this->srcpiv )
            this->parent->addTab(this,QString().number(this->srcdata->Num()+1) + ": " + this->srcpiv->Path());
        else
            this->parent->addTab(this,"Анализатор");

        this->parent->setCurrentIndex(this->parent->count()-1);
    }
    this->state = !this->state;
}
/******************************************************************
*  Настройка парсера
*  Вход: источник ПИВ, источник данных
*  Выход: нет
******************************************************************/
void Parser::Setup(PivData* psrc, A429_Channel* dsrc)
{
    this->srcdata = dsrc;
    this->srcpiv = psrc;

    if( !this->srcdata || !this->srcpiv ) // нет нужных указателей
    {
        this->tw_parser->setHidden(true);
        this->sa_table_parser->setHidden(true);
        this->pb_pausa->setHidden(true);
        this->pb_state->setHidden(true);
        this->pbar_loadparser->setHidden(true);
        this->le_file->setHidden(true);
        this->pb_file->setHidden(true);
        this->pb_filerecstop->setHidden(true);
        this->pb_select->setHidden(true);
        this->pb_plot->setHidden(true);

        this->l_main->setHidden(false);
        this->l_main->setText("Не хватает исходных данных (нет данных канала и/или ПИВ)");

    }
    else // настраиваем парсер
    {
        this->tw_parser-> setHidden(false);
        this->sa_table_parser->setHidden(false);
        this->l_main->setHidden(true);

        connect(srcpiv,SIGNAL(destroyed()),this,SLOT(close())); // привязываемся к источнику ПИВ, если он удален, то анализатор д.б. закрыт автоматически

        // запуск прокси-модели ПИВ
        this->modelpiv = new ProxyModelPivForParser(this->srcpiv); // передаем указатель на "линию"
        this->modelpiv->setSourceModel(this->srcmodelpiv); // установка модели ПИВ
        connect(modelpiv,SIGNAL(signalSetRootIndex(QModelIndex)),tw_parser,SLOT(setRootIndex(QModelIndex))); // для установки корневого элемента

        this->tw_parser->setModel(this->modelpiv); // установка прокси-модели в представление парсера
        this->tw_parser->expandAll(); // разворачиваем дерево ПИВ

        connect(this->tw_parser,SIGNAL(clicked(QModelIndex)),this,SLOT(slotShowParserTable(QModelIndex))); // выбор элемента ПИВ для парсинга

        // формируем название вкладки
        QString str = "";
        str = QString().number(this->srcdata->Num()+1) + ": " + this->srcpiv->Path();
        this->parent->setTabText(this->parent->indexOf(this),str);

        // подключаем слоты парсера к сигналам модели ПИВ (для отслеживания изменений)
        connect(this->srcmodelpiv,SIGNAL(layoutAboutToBeChanged()),this,SLOT(slotClear())); // ПИВ собирается меняться - очищаемся
        connect(this->srcmodelpiv,SIGNAL(layoutChanged()),this,SLOT(slotCreate())); // ПИВ изменился - восстанавливаемся

        this->pbar_loadparser->setMaximum(this->srcpiv->Size());
        this->pbar_loadparser->setMinimum(0);
        this->pbar_loadparser->setValue(0);
        l_loadparser->setText("Старт загрузки анализатора");

        this->slotCreate(); // инициализация гуи парсера

        QString str_dir = dir.currentPath() + "/" + QDateTime::currentDateTime().toString("yyyy-MM-dd_HH-mm"); // генерим имя папки для сохранения
        str_dir = dir.convertSeparators(dir.absoluteFilePath(str_dir)); // сохраняем абсолютный путь к директоии, в которой будут находится файлы
        le_file->setToolTip("Папка для сохранения: " + str_dir);

        QString strf = FileNameCtrl(srcpiv->info.Name); // проверка имени файла на недопустимые символы
        this->le_file->setText( "k" + QString::number(this->srcdata->Num()+1) +
                                QDateTime::currentDateTime().toString("_yyyy-MM-dd_HH-mm") +
                                "_" +
                                strf +
                                ".csv" ); // генерим имя файла
    }
}
/******************************************************************
*  Создание ядра парсера и компоновка его гуи
*  Вход: нет
*  Выход: нет
******************************************************************/
void Parser::slotCreate(void)
{
    bool ch_pausa = this->srcdata->Saveable();
    this->srcdata->Saveable(false); // пауза в канале, пока создаем гуи

    // рекурсивно создаем таблицы парсера
    this->CreateTv(this->srcpiv);

    // добавляем таблицы парсера в компоновщик виджетов
    for( int i=0;i<this->list_wp.size();i++ )
        this->gl_table_parser->addWidget(list_wp.at(i),i,0);

    // восстанавливаем выделенные ранее элементы
    this->tw_parser->selectAll(); // все выделяем
    QModelIndexList mil = this->tw_parser->selectionModel()->selectedIndexes(); // получаем список индексов
    this->tw_parser->selectionModel()->reset(); // сброс выделенных элементов
    for( int i=0;i<mil.size();i++ )
    {
        QModelIndex mi = this->modelpiv->mapToSource(mil.at(i)); // получаем индекс исходной модели
        PivData* pdtmp = (PivData*)mi.internalPointer(); // получем указатель на элемент ПИВ из индекса
        if( list_selectedpd.contains(pdtmp->Path()) ) // проверяем, был ли ранее выделен данный элемент
            this->tw_parser->selectionModel()->select(mil.at(i),QItemSelectionModel::Select);
    }

    this->slotShowParserTable(QModelIndex()); // отображаем таблицы парсера

    l_loadparser->setText("Запуск потоков анализатора...");
    thparser->start();
    while( !thparser->isRunning() )
        this->thread()->wait(100);
    l_loadparser->setText("Анализатор загружен");

    emit this->signalParserEndReload();
    this->setHidden(false);

    this->srcdata->Saveable(ch_pausa); // восстанавливаем состояние канала
}
/******************************************************************
*  Очистка ядра парсера
*  Вход: нет
*  Выход: нет
******************************************************************/
void Parser::slotClear(void)
{
    this->setHidden(true);

    emit this->signalParserBeginReload();

    this->l_loadparser->setText("Очистка...");
    thparser->exit(); // стоп поток
    if( !thparser->wait(100) ) // ожидание остановки потока 100 мс
        thparser->terminate(); // не дождались, аварийно закрываем поток (потеря данных не важна)

    qDeleteAll(this->list_wp);
    this->list_wp.clear();

    qDeleteAll(this->list_modelparser);
    list_modelparser.clear();
}
/******************************************************************
*  Рекурсивное создание таблиц и моделей парсера
*  Вход: элемент ПИВ, модель-источник данных (при mpb = 0 источник - канал)
*  Выход: нет
******************************************************************/
void Parser::CreateTv(PivData* pd, ModelParserBase* mpb)
{
    for( int i=0;i<pd->children.size();i++ )
    {
        pbar_loadparser->setValue(pbar_loadparser->value()+1);
        l_loadparser->setText("Парсинг '" + pd->children[i]->info.Name + "'");

        WidgetParser* p = 0x0;//new WidgetParser(); // виджет парсера

        ModelParserBase* mp = 0x0; // модель анализатора
        ModelParserBase* friendmp = 0x0; // родная модель анализатора (используется для файла)

        PivData* pdtopfile = pd->children[i]->TopPDOfType(PivData::N_GROUP); // ищем сверху группу для дальнейшего определения модели
        ModelParserBase::ModelType mt;

        switch( pd->children[i]->info.type )
        {
        case PivData::N_GROUP: // элемент ПИВ - группа
            if( pdtopfile ) // группа в группе
            {
                if( pdtopfile->st_n_group.t == PivData::st_N_GROUP::LINK ) // группа в файле
                {
                    if( pd->children[i]->st_n_group.t == PivData::st_N_GROUP::LINK ) // группа файла (субфайл)
                        mt = ModelParserBase::MT_SUBFILE;
                    else // группа в файле
                        mt = ModelParserBase::MT_GROUP_IN_FILE;
                }
                else // группа в группе
                    mt = ModelParserBase::MT_GROUP_IN_GROUP;
            }
            else // группа в канале
            {
                if( pd->children[i]->st_n_group.t == PivData::st_N_GROUP::LINK ) // файл
                    mt = ModelParserBase::MT_FILE;
                else // группа
                    mt = ModelParserBase::MT_GROUP;
            }

            switch( mt )
            {
            case ModelParserBase::MT_GROUP:
            case ModelParserBase::MT_GROUP_IN_GROUP:
            case ModelParserBase::MT_GROUP_IN_FILE:
                mp = new ModelParser_Group(this->srcdata,pd->children[i]);
                p = new WidgetParser(); // виджет парсера (создаем заранее)
                p->tv->setSpan(1,0,1,mp->strl_header_h.size());
                p->tv->setSpan(2,0,1,mp->strl_header_h.size());
                p->flag_rec->setHidden(true);
                p->colcount->setHidden(true);
                break;
            case ModelParserBase::MT_FILE:
                mp = new ModelParser_File(this->srcdata,pd->children[i]);
                friendmp = mp;
                break;
            case ModelParserBase::MT_SUBFILE:
                mp = new ModelParser_FileGroup(this->srcdata,pd->children[i],mpb);
                friendmp = mp;
                break;
            default:
                //delete p;
                continue;
            }
            mp->model_type = mt;
            break;
        case PivData::N_PARAM: // элемент ПИВ - параметр
            if( pdtopfile ) // параметр в группе
            {
                if( pdtopfile->st_n_group.t == PivData::st_N_GROUP::LINK ) // параметр в файле (или в группе файла)
                    mt = ModelParserBase::MT_PARAM_IN_FILE;
                else // параметр в группе
                    mt = ModelParserBase::MT_PARAM_IN_GROUP;
            }
            else // параметр вне группы
                mt = ModelParserBase::MT_PARAM;

            switch( mt )
            {
            case ModelParserBase::MT_PARAM_IN_FILE:
                mp = new ModelParser_FileParam(this->srcdata,pd->children[i],mpb);
                friendmp = mp;
                break;
            case ModelParserBase::MT_PARAM:
            case ModelParserBase::MT_PARAM_IN_GROUP:
                mp = new ModelParser_Param(this->srcdata,pd->children[i]);
                break;
            default:
                //delete p;
                continue;
            }
            mp->model_type = mt;
            break;
        default:
            //delete p;
            continue;
        }

        if( !p ) // виджет заранее не создан, создаем его
            p = new WidgetParser(); // виджет парсера

        p->tv->setModel(mp); // установка модели
        p->name->setText(mp->Piv()->Path().right(mp->Piv()->Path().length() - this->srcpiv->Path().length() - 1)); // устанавливаем имя параметра
        p->tv->setColumnWidth(0,200);
        connect(mp,SIGNAL(signalDisplayToLabel(QModelIndex,QString)),p->mpdb,SLOT(slotUpdateLabels(QModelIndex,QString))); // цепляем делегату сигнал вывода на лейбл
        mp->OpenPersistentEditor(p->tv); // насилком открываем редактор делегата
        this->list_wp.append(p); // добавляем виджет парсера в общий список

        connect(this,SIGNAL(signalParserBeginReload()),mp,SLOT(slotParserBeginReload())); // цепляем событие перезагрузки парсера
        connect(this,SIGNAL(signalParserEndReload()),mp,SLOT(slotParserEndReload())); // цепляем событие перезагрузки парсера
        connect(p->colcount,SIGNAL(valueChanged(int)),mp,SLOT(Size(int))); // цепляем событие изменения кол-ва столбцов
        mp->Size(p->colcount->value()); // начальная предустановка кол-ва столбцов
        this->list_modelparser.append((ModelParserBase*)mp); // добавляем с список указателей

        mp->setParent(0);
        mp->moveToThread(thparser); // обособляем модель в подготовленный поток

        this->CreateTv(pd->children.at(i),friendmp); // рекурсивное создание моделей парсера для дочерних элементов ПИВ
    }
}
/******************************************************************
*  Слот отображения таблиц парсера
*  Вход: индекс прокси-модели источника ПИВ
*  Выход: нет
******************************************************************/
void Parser::slotShowParserTable(QModelIndex)
{
    bool ch_pausa = this->srcdata->Saveable();
    this->srcdata->Saveable(false); // пауза в канале, пока создаем гуи

    list_selectedpd.clear(); // очищаем список выделенных элементов

    // определяем список указателей на элементы ПИВ для отображение парсером
    QModelIndexList mil = this->tw_parser->selectionModel()->selectedIndexes();

    for( int i=0;i<mil.size();i++ ) // заполняем список выделенных элементов
    {
        QModelIndex mi = this->modelpiv->mapToSource(mil.at(i));
        list_selectedpd.append(((PivData*)mi.internalPointer())->Path());
    }

    this->pbar_loadparser->setValue(0);
    this->pbar_loadparser->setMinimum(0);
    this->pbar_loadparser->setMaximum(list_modelparser.size()-1);

    this->sa_table_parser->widget()->setHidden(true); // скрываем для ускорения ГУИ

    this->l_loadparser->setText("Обновление размеров по контексту");
    slotResizeTableParser(); // пересчитываем размеры таблиц и виджетов парсера

    this->l_loadparser->setText("Активация анализатора");
    // отображаем таблицы парсера для выбранных элементов ПИВ
    int h = 0;
    for( int i=0;i<this->list_modelparser.size();i++ )
    {
        this->pbar_loadparser->setValue(i);

        PivData* pdtmp = this->list_modelparser.at(i)->Piv();
        if( list_selectedpd.contains(pdtmp->Path()) )
        {
            this->l_loadparser->setText("Активирую: '" + pdtmp->info.Name + "'");
            this->list_modelparser[i]->Stop(false);

            list_wp[i]->setHidden(false);
            h += list_wp[i]->height() + 10; // подсчет суммы высот для отображаемых таблиц парсера
        }
        else
        {
            this->l_loadparser->setText("Деактивирую: '" + pdtmp->info.Name + "'");
            this->list_wp.at(i)->setHidden(true);
            this->list_modelparser[i]->Stop(true);
        }
    };

    this->sa_table_parser->widget()->setGeometry(0,0,2000,h+50); // подгон высоты виджета области прокрутки
    this->sa_table_parser->widget()->setHidden(false);

    this->l_loadparser->setText("Готово");

    this->srcdata->Saveable(ch_pausa); // восстанавливаем состояние канала
}
/******************************************************************
*  Нажатие кнопки паузы по приему данных каналов
*  Вход: нет
*  Выход: нет
******************************************************************/
void Parser::Pausa()
{
    this->srcdata->Saveable(!this->srcdata->Saveable());

    if( !this->srcdata->Saveable() )
        this->pb_pausa->setText("Старт");
    else
        this->pb_pausa->setText("Пауза");
}
/******************************************************************
*  Слот записи данных из канала channel в файл
*  Вход: строка формата csv
*  Выход: нет
******************************************************************/
void Parser::slotWriteToFile(QString str)
{
    ModelParserBase* mp = (ModelParserBase*)sender(); // источник сигнала

    QTextStream ts(this->file); // организуем поток записи

    if( first_wtf ) // запись заголовка
    {
        first_wtf = false;
        for( int i=0;i<this->list_wp.size();i++ ) // запись строки имен параметров
        {
            if( !this->list_wp.at(i)->flag_rec->isChecked() || this->list_wp.at(i)->isHidden() ) // пропускаем невидимые и не помеченные на регистрацию параметры
                continue;

            ts << this->list_wp.at(i)->name->text().remove('"');
            ts << ";;"; // под колонку Время
            for( int j=0;j<this->list_modelparser.at(i)->strl_header_v.size();j++ )
                ts << ";";
        }
        ts << "\n"; // переводим строку

        for( int i=0;i<this->list_wp.size();i++ ) // запись строки заголовков параметров
            if( this->list_wp.at(i)->flag_rec->isChecked() )
                ts << this->list_modelparser.at(i)->csvTitle().remove('"');
        ts << "\n"; // переводим строку

        this->stroka.clear(); // очщаем строку
    }

    if( this->stroka.keys().contains(mp) ) // от этого источника ячейка уже заполнена
    {
        for( int i=0;i<this->list_wp.size();i++ ) // запись строки
        {
            if( !this->list_wp.at(i)->flag_rec->isChecked() || this->list_wp.at(i)->isHidden() ) // пропускаем невидимые и не помеченные на регистрацию параметры
                continue;

            if( this->stroka.keys().contains(this->list_modelparser.at(i)) ) // есть, что записывать
                ts << this->stroka.value(this->list_modelparser.at(i));
            else // ячейка пуста, записываем пустую ячейку
            {
                ts << ";;"; // под колонку Время
                for( int j=0;j<this->list_modelparser.at(i)->strl_header_v.size();j++ ) // под кол-во элементов параметра
                    ts << ";";
            }
        }
        ts << "\n"; // переводим строку
        this->stroka.clear(); // очщаем строку
    }

    this->stroka.insert(mp,str.remove('"')); // сохраняем ячейку
}
/******************************************************************
*  Слот нажатия на кнопку записи
*  Вход: нет
*  Выход: нет
******************************************************************/
void Parser::slotRec(void)
{
    if( this->pb_filerecstop->text() == "Стоп запись" )
    {
        this->pb_filerecstop->setText("Старт запись");
        this->le_file->setEnabled(true);

        for( int i=0;i<this->list_wp.size();i++ ) // циклический просмотр списка виджетов парсеров
        {
            if( this->list_wp.at(i)->isHidden() ) // виджет скрыт, его не регистрируем
                continue;

            this->list_wp.at(i)->flag_rec->setEnabled(true); // записи, разблокируем флаг регистрации от изменений
            this->list_modelparser.at(i)->RecToFile(false); // снимаем флаг записи в файл в модели парсера

            disconnect(this->list_modelparser.at(i),SIGNAL(signalWriteToFile(QString)),this,SLOT(slotWriteToFile(QString))); // отцепляем слот записи в файл
        }

        this->file->close();
    }
    else
    {
        if( !this->file->isOpen() ) // файл закрыт?
        {
            QString str_dir = le_file->toolTip().right(le_file->toolTip().size() - QString("Папка для сохранения: ").size());
            if( !dir.exists(str_dir) )
                dir.mkdir(str_dir);

            le_file->setText(FileNameCtrl(le_file->text()));
            QString strf = str_dir + "/" + le_file->text();
            file->setFileName(dir.convertSeparators(strf)); // установка имени файла
            if( !this->file->open(QIODevice::WriteOnly | QIODevice::Append) ) // открываем файл на добавление
                return;
        }

        first_wtf = true; // выставляем флаг первичной записи после нажатия на "Старт запись"

        this->pb_filerecstop->setText("Стоп запись");
        this->le_file->setEnabled(false); // идет запись, блокируем изменение имени фала

        for( int i=0;i<this->list_wp.size();i++ ) // циклический перебор виджетов парсеров
        {
            if( this->list_wp.at(i)->isHidden() ) // виджет скрыт - продолжаем перебор
                continue;

            this->list_wp.at(i)->flag_rec->setEnabled(false); // идет запись, блокируем изменение флага регистрации

            if( this->list_wp.at(i)->flag_rec->isChecked() ) // флаг регистрации установлен, меняем модель и цепляем слот
            {
                this->list_modelparser.at(i)->RecToFile(true); // устанавливаем флаг записи в файл в модели парсера
                connect(this->list_modelparser.at(i),SIGNAL(signalWriteToFile(QString)),this,SLOT(slotWriteToFile(QString)));
            }
        }
    }
}
/******************************************************************
*  Слот нажатия на кнопку сохранения
*  Вход: нет
*  Выход: нет
******************************************************************/
void Parser::slotFileSelect(void)
{
    // выбираем файл
    QString strfile = le_file->text();
    strfile = QFileDialog::getSaveFileName(0,"Выберите файл",le_file->text(),"Текст CSV (*.csv)");

    if( strfile.isEmpty() ) // ничего не выбрано, выходим
        return;

    QFileInfo f_inf;
    f_inf.setFile(strfile);

    this->le_file->setText(FileNameCtrl(f_inf.fileName()));
    this->le_file->setToolTip("Папка для сохранения: " + f_inf.absolutePath());
}
/******************************************************************
*  Слот нажатия на кнопку График...
*  Вход: нет
*  Выход: нет
******************************************************************/
void Parser::slotShowPlot(void)
{
    PlotWidget* plot = new PlotWidget(thparser);
    connect(this,SIGNAL(destroyed()),plot,SLOT(close())); // удаляем график при закрытии анализатора
    connect(this,SIGNAL(signalParserBeginReload()),plot,SLOT(close())); // удаляем график при перезагрузке анализатора

    this->list_plot.append(plot);

    plot->show(); // показываемсователюя польз
}
/******************************************************************
*  Слот выделения всех таблиц парсера для записи в файл (прставка галок)
*  Вход: нет
*  Выход: нет
******************************************************************/
void Parser::slotSelectForRec(void)
{
    QPushButton *pb = (QPushButton*)sender();
    bool sel = pb->text() == "Выделить всё";

    // выделяем или снимаем выделение
    if( sel )
        pb->setText("Снять выделенное");
    else
        pb->setText("Выделить всё");

    for( int i=0;i<this->list_wp.size();i++ ) // циклический перебор виджетов парсеров
        if( !this->list_wp.at(i)->isHidden() ) // виджет виден
            this->list_wp.at(i)->flag_rec->setChecked(sel);
}
/******************************************************************
*  Контроль имени файла для записи
*  Вход: имя файла
*  Выход: откорректированное имя файла
******************************************************************/
QString Parser::FileNameCtrl(QString fn)
{
    QString strf = fn;
    strf = strf.replace('/',"-");
    strf = strf.replace('\\',"-");
    strf = strf.replace(':',"-");
    strf = strf.replace('*',"-");
    strf = strf.replace("?","-");
    strf = strf.replace('"',"-");
    strf = strf.replace('<',"-");
    strf = strf.replace('>',"-");
    strf = strf.replace('|',"-");

    return strf;
}
/******************************************************************
*  Обновление высоты таблицы и виджета парсера
*  Вход: нет
*  Выход: нет
******************************************************************/
void Parser::slotResizeTableParser(void)
{
    for( int i=0;i<list_modelparser.size();i++ )
    {
        int htv = 20;
        for( int r=0;r<list_modelparser[i]->strl_header_v.size();r++ )
            htv += list_wp[i]->tv->rowHeight(r)+10;
        list_wp[i]->tv->setFixedHeight(htv);
        list_wp[i]->setFixedHeight(htv+10);
    }
}
