#ifndef MODELPARSER_FILE_H
#define MODELPARSER_FILE_H

#include "modelparserbase.h"


class ModelParser_File : public ModelParserBase
{
public:
    ModelParser_File(A429_Channel *ch, PivData *pd);
    ~ModelParser_File(void);

    virtual int columnCount(const QModelIndex&) const;           // количество столбцов
    virtual QVariant headerData(int, Qt::Orientation, int) const; // установка заголовков

    virtual QVariant data(const QModelIndex&, int) const;  // выборка данных по роли

    struct StFile
    {
        quint32 base_word;
        quint32 group;
        quint32 size;
        QTime time;
        quint32 _time;
        bool ready;
        bool small_buffer; // флаг нехватки буфера для размещения файла
        quint32* file; // файл
        ulong numpp;
    };

    ModelParser_File::StFile* File(int i);

    virtual int Size(void);

    virtual QString csvData(void);

public slots:
    virtual void slotUpdateInternalData(A429_Word*);
    virtual void slotResetInternalData(void);

    virtual void Size(int s);

private:
    int static_size;

    QQueue<StFile*>files;

    ulong numpp;

    StFile* file; // статически выделенная временная переменная

    virtual void dblDataUpdate(void);
};

#endif // MODELPARSER_FILE_H
