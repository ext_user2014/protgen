#ifndef MODELPARSER_FILEGROUP_H
#define MODELPARSER_FILEGROUP_H

#include "modelparser_file.h"

class ModelParser_FileGroup : public ModelParserBase
{
    Q_OBJECT
public:
    ModelParser_FileGroup(A429_Channel *ch, PivData *pd, ModelParserBase* mpb);
    ~ModelParser_FileGroup(void);

    virtual int columnCount(const QModelIndex&) const;           // количество столбцов
    virtual QVariant headerData(int, Qt::Orientation, int) const; // установка заголовков

    virtual QVariant data(const QModelIndex&, int) const;  // выборка данных по роли

    struct FileGroup
    {
        quint32 id;
        QTime time;
        quint32 _time;
        bool ctrl;
        quint32* file;
        int size;
        QStringList* errors;
        ulong numpp;
        ulong file_numpp;
        bool ready;
    };

    ModelParser_FileGroup::FileGroup* Group(int i);

    virtual int Size(void);

    virtual QString csvData(void);

public slots:
    virtual void slotUpdateInternalData(A429_Word*); // ф-я разбора группы файла
    virtual void slotResetInternalData(void);

    virtual void Size(int s);

private:
    int static_size;

    QQueue<FileGroup*> filegroup;

    ulong numpp;

    void FileCheck(FileGroup* ptr_fg); // ф-я проверки целостности файлового блока

    FileGroup* fg; // статически выделенная временная переменная

    virtual void dblDataUpdate(void);
};

#endif // MODELPARSER_FILEGROUP_H
