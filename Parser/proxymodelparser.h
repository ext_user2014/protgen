#ifndef PROXYMODELPARSER_H
#define PROXYMODELPARSER_H

#include <QSortFilterProxyModel>
#include "../pivdata.h"

class ProxyModelPivForParser : public QSortFilterProxyModel
{
    Q_OBJECT
public:
    explicit ProxyModelPivForParser(PivData* pivsrc, QObject *parent = 0);
    virtual ~ProxyModelPivForParser(void){}

    bool filterAcceptsRow(int source_row, const QModelIndex &source_parent) const;
    bool filterAcceptsColumn(int source_column, const QModelIndex &source_parent) const;
    Qt::ItemFlags flags(const QModelIndex &index) const;

    QModelIndex root_index;
signals:
    void signalSetRootIndex(QModelIndex)const;

public slots:

private:
    PivData* piv;



};

#endif // PROXYMODELPARSER_H
