#ifndef MODELPARSERBASE_H
#define MODELPARSERBASE_H

#include <QAbstractTableModel>
#include <QStringList>
#include "../pivdata.h"
#include "../SrcData/a429_channel.h"

class ModelParserBase : public QAbstractTableModel
{
    Q_OBJECT
public:
    ModelParserBase(A429_Channel*,PivData*,ModelParserBase* mpb = 0x0);
    virtual ~ModelParserBase(void);

    virtual int rowCount(const QModelIndex&) const;              // количество строк
    virtual int columnCount(const QModelIndex&) const;           // количество столбцов
    virtual QVariant headerData(int, Qt::Orientation, int) const; // установка заголовков

    Qt::ItemFlags flags(const QModelIndex &index) const;                        // установка флагов

    QStringList mimeTypes(void) const; // Список миме типов
    QMimeData* mimeData(const QModelIndexList &indexes) const; // запрос данных при перетаскивании

    virtual QVariant data(const QModelIndex&, int) const{return QVariant();}  // выборка данных по роли

    virtual PivData* Piv(void)const {return pivdata;} // источник ПИВ

    enum ModelType // типы модели
    {
        MT_FILE             = 0x1,
        MT_SUBFILE          = 0x2,
        MT_GROUP            = 0x4,
        MT_GROUP_IN_GROUP   = 0x8,
        MT_GROUP_IN_FILE    = 0x10,
        MT_PARAM            = 0x20,
        MT_PARAM_IN_FILE    = 0x40,
        MT_PARAM_IN_GROUP   = 0x80,
        MT_ALL_FILE = MT_FILE | MT_SUBFILE | MT_GROUP_IN_FILE | MT_PARAM_IN_FILE
    };

    enum Role
    {
        DoubleRole = Qt::UserRole + 10
    };

    QStringList strl_header_v; // вертикальные заголовки
    QStringList strl_header_h; // горизонтальные заголовки

    ModelType model_type; // тип модели
    A429_Channel* channel; // канал
    PivData* pivdata; // ПИВ

    int counter; // внутренний счетчик
    QMutex mutex; // сериализатор потоков

    QVariant Fnc_SIG(PivData* pd,quint32 w,int r)const;

    QVariant Fnc_BIT(PivData* pd,quint32 w,int r)const;
    QVariant Fnc_GBIT(PivData* pd,quint32 w,int r,bool si = true)const;

    QVariant Fnc_GBIT_CSR(PivData* pd,quint32 w,int r,bool si = false)const;
    QVariant Fnc_GBIT_K(PivData* pd,quint32 w,int r)const;
    QVariant Fnc_GBIT_CONST(PivData* pd,quint32 w,int r)const;
    QVariant Fnc_GBIT_VALUES(PivData* pd,quint32 w,int r)const;
    QVariant Fnc_GBIT_DDK(PivData* pd,quint32 w,int r,bool si = false)const;


    virtual int Size(void) = 0; // размер внутренней очереди проанализированных данных

    virtual QString csvTitle(void); // заголовок последних данных в формате csv
    virtual QString csvData(void); // последние данные в формате csv

    void dblDataInit(void){dbl_data = (double*)calloc(strl_header_v.size(),sizeof(double));}
    const double* dblData(void) {return dbl_data;} // последние данные в формате двойной точности

    // возврат и установка флага записи в файл
    bool RecToFile(void){return rec_to_file;}
    void RecToFile(bool b){rec_to_file = b;}

    QMap<int,bool> rows_open_persistent_editor; // карта флагов необходимости постоянного отображения редактора
    void OpenPersistentEditor(QTableView*); // отображение редактора для необходимых полей

    // флаг останова обновления данных представления
    void Stop(bool bstop){b_stop = bstop;ConnectToSrcData(!b_stop);}
    bool Stop(void){return b_stop;}

public slots:
    virtual void Size(int s) = 0;

    virtual void slotUpdateInternalData(A429_Word*) = 0; // обновление данных при приходе на канал слова
    virtual void slotResetInternalData(void) = 0; // сброс данных

    virtual void dblDataUpdate(void) = 0;

    void slotParserBeginReload(void){b_stop = true;}
    void slotParserEndReload(void){b_stop = false;}
signals:
    void signalUpdatedInternalData(A429_Word*); // сигал окончания обновления данных в потомственном классе
    void signalAddedInternalQueue(double td, QTime t); // сигал окончания добавление данных в внутреннюю очередь данных

    void signalTimer500ms(void); // сигал таймера

    void signalWriteToFile(QString);

    void signalDisplayToLabel(QModelIndex, QString) const; // сигнал отправки данных на лейбл для отображения

private:
    void timerEvent(QTimerEvent *ev);
    int mytimer;

    bool rec_to_file; // флаг записи в файл

    bool b_stop; // флаг останоки обновления данных представления

    double *dbl_data; // последние данны в формате двойной точности (массив размера strl_header_v.size())

    bool src_is_friend; // флаг, что источник данных - родной класс (актуально для ФО)
    ModelParserBase* friend_class; // родной класс
    void ConnectToSrcData(bool yes); // соединить свои слоты к сигналам источника данных
};

#endif // MODELPARSERBASE_H
