#include "modelparserbase.h"

ModelParserBase::ModelParserBase(A429_Channel* ch, PivData* pd, ModelParserBase *mpb) :
    QAbstractTableModel(0)
{
    channel = ch;
    pivdata = pd;

    b_stop = true;

    rec_to_file = false;

    src_is_friend = (mpb != 0x0);
    friend_class = mpb;

    connect(pivdata,SIGNAL(destroyed()),this,SLOT(deleteLater())); // разрушаемся синхронно с  ПИВ

    mytimer = this->startTimer(500); // служебный таймер
    counter = 0; // служеный таймер

    rows_open_persistent_editor.clear();

    dbl_data = 0x0;

    connect(this,SIGNAL(signalAddedInternalQueue(double,QTime)),SLOT(dblDataUpdate()));
}
ModelParserBase::~ModelParserBase(void)
{
    disconnect();
    this->strl_header_h.clear();
    this->strl_header_v.clear();

    if( dbl_data )
        free(dbl_data);
}
/******************************************************************
*  Получить кол-во строк
*  Вход: индекс родителя
*  Выход: кол-во
******************************************************************/
int ModelParserBase::rowCount(const QModelIndex&) const
{
    return this->strl_header_v.size();
}
/******************************************************************
*  Получить кол-во столбцов
*  Вход: индекс родителя
*  Выход: кол-во
******************************************************************/
int ModelParserBase::columnCount(const QModelIndex&) const
{
    return this->strl_header_h.size();
}
/******************************************************************
*  Получить данные заголовков представления
*  Вход: секция, ориентация, роль
*  Выход: данные
******************************************************************/
QVariant ModelParserBase::headerData(int section, Qt::Orientation orientation, int role) const
{
    if( role == Qt::DisplayRole )
    {
        if( orientation == Qt::Vertical )
            return this->strl_header_v.at(section);
        if( orientation == Qt::Horizontal )
        {
            if( this->strl_header_h.size() > section )
                return this->strl_header_h.at(section);
            else
                return QVariant();
        }
    }
    return QVariant();
}
/******************************************************************
*  Тик таймера
*  Вход: событие таймера
*  Выход: нет
******************************************************************/
void ModelParserBase::timerEvent(QTimerEvent *ev)
{
    if( ev->timerId() != mytimer )
        return;

    if( !counter )
    {
        if( this->channel->Saveable() && !this->b_stop ) // пауза не нажата
            this->slotResetInternalData();
    }
    else
        counter--;

    emit this->signalTimer500ms();
}
/******************************************************************
*  Получить знак
*  Вход: ПИВ, слово ПК, роль представления
*  Выход: +1 или -1
******************************************************************/
QVariant ModelParserBase::Fnc_SIG(PivData* pd,quint32 w,int r) const
{
    int sig = 1;
    PivData *pds = pd->ChildPDOfType(PivData::N_ELP_BIT);
    if( pds ) // по признаку
        sig = (Fnc_BIT(pds,w,r).toInt()<0)?-1:1;
    else
    {
        pds = pd->ChildPDOfType(PivData::N_ELP_GBIT);
        if( pds ) // по значению группы бит
            sig = (Fnc_GBIT(pds,w,r,false).toInt()<0)?-1:1;
    }
    return sig;
}
/******************************************************************
*  Получить значения бита
*  Вход: ПИВ, слово ПК, роль представления
*  Выход: значение бита
******************************************************************/
QVariant ModelParserBase::Fnc_BIT(PivData* pd,quint32 w,int r)const
{
    bool value = (w >> pd->st_n_elp_bit.offset) & 0x1;
    if( r == Qt::DisplayRole )
    {
        if( value )
            return pd->st_n_elp_bit.v_1;
        else
            return pd->st_n_elp_bit.v_0;
    }
    else if ( r == Qt::BackgroundRole )
    {
        if( value )
            return pd->st_n_elp_bit.color1;
        else
            return pd->st_n_elp_bit.color0;
    }
    else if( r == Qt::ToolTipRole )
    {
        return "<b>" + pd->st_n_elp_bit.v_0ss + "</b> - 0\n<b>" + pd->st_n_elp_bit.v_1ss + "</b> - 1";
    }
    else if( r == ModelParserBase::DoubleRole ) // роль для графика
    {
        return (double)value;
    }

    return QVariant();
}
/******************************************************************
*  Получить значение группы бит
*  Вход: ПИВ, слово ПК, роль представления, необходимость вывода единиц измерения
*  Выход: зачение группы бит
******************************************************************/
QVariant ModelParserBase::Fnc_GBIT(PivData* pd, quint32 w, int r, bool si)const
{
    if( pd->info.type != PivData::N_ELP_GBIT )
        return "Ошибка вызова функции Fnc_GBIT";

    switch(pd->st_n_elp_gbit.t)
    {
    case PivData::st_N_ELP_GBIT::GB:
    {
        if( (r == Qt::DisplayRole) || (r == ModelParserBase::DoubleRole) )
        {
            switch(pd->st_n_elp_gbit.fnc)
            {
            case PivData::st_N_ELP_GBIT::N: // Ф-ии нет, занчит выводим потраха в виде html-ки (не для графика)
            {
                if( r == Qt::DisplayRole )
                {
                    QString str = "";
                    for( int i=0;i<pd->children.size();i++ )
                    {
                        if( !str.isEmpty() )
                            str += "<br>";

                        str += pd->children.at(i)->info.Name + ": ";
                        if( pd->children.at(i)->info.type == PivData::N_ELP_BIT ) // признак
                        {
                            QColor color(Fnc_BIT(pd->children.at(i),w,Qt::BackgroundRole).toString());
                            str += "<span style=\"background-color:" + color.name() + "\">";
                            str += Fnc_BIT(pd->children.at(i),w,Qt::DisplayRole).toString()+"</span>";
                        }
                        else
                            str += "<div>" + Fnc_GBIT(pd->children.at(i),w,r,si).toString() + "</div>";
                    }
                    return str;
                }
                else if( r == ModelParserBase::DoubleRole ) // для графика (алгоритм мутный надо думать!!!)
                {
                    int val = 0;
                    for( int i=0;i<pd->children.size();i++ )
                    {
                        if( pd->children.at(i)->info.type == PivData::N_ELP_BIT ) // признак
                            val |= Fnc_BIT(pd->children.at(i),w,Qt::DisplayRole).toInt() << i;
                        else
                            val |= Fnc_GBIT(pd->children.at(i),w,r,si).toInt() << i;
                    }
                    return (double)val;
                }
                else
                    return 0.0;
            }
            case PivData::st_N_ELP_GBIT::SUM: // сумма
            {
                float val = 0;
                for( int i=0;i<pd->children.size();i++ )
                    if( pd->children.at(i)->info.type == PivData::N_ELP_GBIT )
                        val += Fnc_GBIT(pd->children.at(i),w,r,false).toFloat();
                if( si && (r == Qt::DisplayRole) )
                    return QString::number(val) + pd->st_n_elp_gbit.si;
                else
                    return val;
            }
            case PivData::st_N_ELP_GBIT::SUB: // разность
            {
                float val = 0;
                for( int i=0;i<pd->children.size();i++ )
                {
                    if( pd->children.at(i)->info.type == PivData::N_ELP_GBIT )
                    {
                        if( !i )
                            val = Fnc_GBIT(pd->children.at(i),w,r,false).toFloat();
                        else
                            val -= Fnc_GBIT(pd->children.at(i),w,r,false).toFloat();
                    }
                }
                if( si && (r == Qt::DisplayRole) )
                    return QString::number(val) + pd->st_n_elp_gbit.si;
                else
                    return val;
            }
            case PivData::st_N_ELP_GBIT::MULT: // перемножение
            {
                float val = 1;
                for( int i=0;i<pd->children.size();i++ )
                    if( pd->children.at(i)->info.type == PivData::N_ELP_GBIT )
                        val *= Fnc_GBIT(pd->children.at(i),w,r,false).toFloat();
                if( si && (r == Qt::DisplayRole) )
                    return QString::number(val) + pd->st_n_elp_gbit.si;
                else
                    return val;
            }
            case PivData::st_N_ELP_GBIT::DIV: // деление
            {
                float val = 0;
                for( int i=0;i<pd->children.size();i++ )
                {
                    if( pd->children.at(i)->info.type == PivData::N_ELP_GBIT )
                    {
                        if( !i )
                            val = Fnc_GBIT(pd->children.at(i),w,r,false).toFloat();
                        else
                        {
                            float tmp = Fnc_GBIT(pd->children.at(i),w,r,false).toFloat();
                            if( tmp == 0.0 )
                                val /= 0.0000001;
                            else
                                val /= tmp;
                        }
                    }
                }
                if( si && (r == Qt::DisplayRole) )
                    return QString::number(val) + pd->st_n_elp_gbit.si;
                else
                    return val;
            }
            case PivData::st_N_ELP_GBIT::CON: // сцепка
            {
                QString val = "";
                for( int i=0;i<pd->children.size();i++ )
                    if( pd->children.at(i)->info.type == PivData::N_ELP_GBIT )
                        val += Fnc_GBIT(pd->children.at(i),w,r).toString();
                if( si )
                    return val + pd->st_n_elp_gbit.si;
                else
                    return val;
            }
            case PivData::st_N_ELP_GBIT::FUN: // функция
            {
                return "Ф-я в разработке";
            }
            default:
                return "Ошибка вызова функции";
            }
        }
        return QVariant();
    }
    case PivData::st_N_ELP_GBIT::CSR:
    {
        return Fnc_GBIT_CSR(pd,w,r,si);
    }
    case PivData::st_N_ELP_GBIT::K:
    {
        return Fnc_GBIT_K(pd,w,r);
    }
    case PivData::st_N_ELP_GBIT::CONST:
    {
        return Fnc_GBIT_CONST(pd,w,r);
    }
    case PivData::st_N_ELP_GBIT::VALUES:
    {
        return Fnc_GBIT_VALUES(pd,w,r);
    }
    case PivData::st_N_ELP_GBIT::DDK:
    {
        return Fnc_GBIT_DDK(pd,w,r,si);
    }
    default:
        return "Ошибка вызова функции";
    }

    return QVariant();
}
/******************************************************************
*  Получить значение на основе ЦСР
*  Вход: ПИВ, слово ПК, роль представления
*  Выход: значение
******************************************************************/
QVariant ModelParserBase::Fnc_GBIT_CSR(PivData *pd, quint32 w, int r, bool si)const
{
    if( r == Qt::DisplayRole || r == ModelParserBase::DoubleRole )
    {
        int sig = Fnc_SIG(pd,w,r).toFloat();
        quint32 data = (sig<0)?(~w):w;
        float cmr = (float)pd->st_n_elp_gbit.val*2 / (float)((pd->st_n_elp_gbit.mask << 1) & (~pd->st_n_elp_gbit.mask));
        data = (data >> pd->st_n_elp_gbit.offset) & pd->st_n_elp_gbit.mask;
        float val = cmr * (float)data;
        val *= sig;

        if( si && (r == Qt::DisplayRole) )
            return QString().number(val) + pd->st_n_elp_gbit.si;
        else
            return val;
    }

    return QVariant();
}
/******************************************************************
*  Получить значение на основе коэффициента
*  Вход: ПИВ, слово ПК, роль представления
*  Выход: значение
******************************************************************/
QVariant ModelParserBase::Fnc_GBIT_K(PivData *pd, quint32 w, int r)const
{
    if( r == Qt::DisplayRole || r == ModelParserBase::DoubleRole )
    {
        return (float)((w >> pd->st_n_elp_gbit.offset) & pd->st_n_elp_gbit.mask) * pd->st_n_elp_gbit.val * \
                Fnc_SIG(pd,w,r).toFloat();
    }

    return QVariant();
}
/******************************************************************
*  Проверка на константность
*  Вход: ПИВ, слово ПК, роль представления
*  Выход: сообщение о равенстве константе
******************************************************************/
QVariant ModelParserBase::Fnc_GBIT_CONST(PivData *pd, quint32 w, int r)const
{
    quint32 val = (w >> pd->st_n_elp_gbit.offset) & pd->st_n_elp_gbit.mask;
    bool b = ((quint32)pd->st_n_elp_gbit.val) == val;
    if( r == Qt::DisplayRole )
    {
        if(b)
            return "OK";
        else
            return "Нарушение ПИВ";
    }
    else if( r == Qt::BackgroundRole )
    {
        if(b)
            return Qt::green;
        else
            return Qt::red;
    }
    else if( r == Qt::ToolTipRole )
    {
        return "<b>Красный</b> - нарушение ПИВ\n<b>Зеленый</b> - ОК";
    }
    else if( r == ModelParserBase::DoubleRole )
    {
        if(b)
            return 1;
        else
            return 0;
    }

    return QVariant();
}
/******************************************************************
*  Получить значение из списка значений
*  Вход: ПИВ, слово ПК, роль представления
*  Выход: значение
******************************************************************/
QVariant ModelParserBase::Fnc_GBIT_VALUES(PivData *pd, quint32 w, int r)const
{
    quint32 val = (w >> pd->st_n_elp_gbit.offset) & pd->st_n_elp_gbit.mask;

    if( r == Qt::DisplayRole )
    {
        if( pd->st_n_elp_gbit.values.contains(val) )
        {
            QString str = pd->st_n_elp_gbit.values.value(val);
            if( str.contains("&&") )
                return str.left(str.indexOf("&&"));
            else
                return str;
        }
        else
            return "Значение не найдено";
    }
    else if( r == Qt::BackgroundRole )
    {
        if( !pd->st_n_elp_gbit.values.contains(val) )
            return Qt::white;
        else
        {
            QString str_ss = pd->st_n_elp_gbit.values.value(val);
            if( str_ss.contains("&&") )
                str_ss = str_ss.right(str_ss.size()-str_ss.indexOf("&&")-2);
            else
                return Qt::white;

            QRegExp rg;
            rg.setPattern("background-color: ([a-z]{1,});");
            rg.indexIn(str_ss); // парсим текст
            return QColor(rg.cap(1));

        }
    }
    else if( r == ModelParserBase::DoubleRole )
    {
        return (double)val;
    }

    return QVariant();
}
/******************************************************************
*  Получить значение ДДК
*  Вход: ПИВ, слово ПК, роль представления, необходимость вывода ед. измерения
*  Выход: ДДК
******************************************************************/
QVariant ModelParserBase::Fnc_GBIT_DDK(PivData *pd, quint32 w, int r, bool si)const
{
    if( r == Qt::DisplayRole || r == ModelParserBase::DoubleRole )
    {
        float val = 0.0;
        for( int i=0;i<pd->children.size();i++ )
        {
            if( pd->children.at(i)->info.type == PivData::N_ELP_GBIT )
                val += Fnc_GBIT(pd->children.at(i),w,r,false).toFloat();
        }
        if( si && (r == Qt::DisplayRole) )
            return QString::number(val) + pd->st_n_elp_gbit.si;
        else
            return val;
    }

    return QVariant();
}
/******************************************************************
*  Получить заголовок для файла формата csv
*  Вход: нет
*  Выход: строка
******************************************************************/
QString ModelParserBase::csvTitle(void)
{
    QString str = "";

    str += "Время;";
    str += "Время;";
    for( int i=0;i<strl_header_v.size();i++ )
        str += strl_header_v.at(i) + ";";

    return str;
}
/******************************************************************
*  Получить данные для файла формата csv
*  Вход: нет
*  Выход: строка
******************************************************************/
QString ModelParserBase::csvData(void)
{
    QString str = "";

    str += ";;"; // для колонок Время
    for( int i=0;i<strl_header_v.size();i++ )
        str += ";";

    return str;
}
/******************************************************************
* Установка флагов
* Вход: индекс записи
* Выход: флаги записи
******************************************************************/
Qt::ItemFlags ModelParserBase::flags(const QModelIndex &index) const
{
    if (!index.isValid())
        return Qt::ItemIsEnabled | Qt::ItemIsDragEnabled;

    return QAbstractItemModel::flags(index) |  Qt::ItemIsDragEnabled;
}
/******************************************************************
* Список MIME типов для модели данных
* Вход: нет
* Выход: список миме
******************************************************************/
QStringList ModelParserBase::mimeTypes(void) const
{
    QStringList sl;
    sl << MIME_STRUCT_MP_FOR_PLOT;
    return sl;
}
/******************************************************************
* Запрос данных при перетаскивании
* Вход: список модельных индексов
* Выход: указатель на миме данные
******************************************************************/
QMimeData* ModelParserBase::mimeData(const QModelIndexList &indexes) const
{
    QMimeData *mimeData = new QMimeData();
    QByteArray encodedData;

    QDataStream stream(&encodedData, QIODevice::WriteOnly);

    for( int i=0; i<indexes.size(); i++ ) // перечисление всех перетаскиваемых индексов
    {
        if (indexes.at(i).isValid())
            stream << indexes.at(i).row() << (quint64)this;
    }

    mimeData->setData(MIME_STRUCT_MP_FOR_PLOT, encodedData);

    return mimeData;
}
/******************************************************************
* Открытие редакора в предсиавлении
* Вход: элемент-представление
* Выход: нет
******************************************************************/
void ModelParserBase::OpenPersistentEditor(QTableView* tv)
{
    foreach ( int r, rows_open_persistent_editor.keys() ) // перебор строк
        if( rows_open_persistent_editor[r] ) // есть необходимость в редакторе
            for( int i=0;i<this->columnCount(QModelIndex());i++ ) // перебор по столбцам
                tv->openPersistentEditor(index(r,i)); // открываем редактор по индексу
}

/******************************************************************
* Cоединить свои слоты к сигналам источника данных
* Вход: соединить/разъединить
* Выход: нет
******************************************************************/
void ModelParserBase::ConnectToSrcData(bool yes)
{
    if( yes )
    {
        if( src_is_friend ) // приоритет обработки данных от родного класса
        {
            connect(friend_class,SIGNAL(signalUpdatedInternalData(A429_Word*)),this,SLOT(slotUpdateInternalData(A429_Word*)));
        }
        else // обработка данных из канала
        {
            connect(channel,SIGNAL(DataAdded(A429_Word*)),this,SLOT(slotUpdateInternalData(A429_Word*)));
            connect(channel,SIGNAL(signalChannelEmpty()),this,SLOT(resetInternalData()));
        }
    }
    else
    {
        if( !(model_type & MT_ALL_FILE) )
        {
            if( src_is_friend ) // приоритет обработки данных от родного класса
            {
                disconnect(friend_class,SIGNAL(signalUpdatedInternalData(A429_Word*)),this,SLOT(slotUpdateInternalData(A429_Word*)));
            }
            else // обработка данных из канала
            {
                disconnect(channel,SIGNAL(DataAdded(A429_Word*)),this,SLOT(slotUpdateInternalData(A429_Word*)));
                disconnect(channel,SIGNAL(signalChannelEmpty()),this,SLOT(resetInternalData()));
            }
        }
    }
}
