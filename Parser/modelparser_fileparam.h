#ifndef MODELPARSER_FILEPARAM_H
#define MODELPARSER_FILEPARAM_H

#include "modelparser_filegroup.h"
#include "modelparser_param.h"


class ModelParser_FileParam : public ModelParserBase
{
    Q_OBJECT
public:
    ModelParser_FileParam(A429_Channel *ch, PivData *pd, ModelParserBase* mpb);
    ~ModelParser_FileParam(void);

    virtual int columnCount(const QModelIndex&) const;           // количество столбцов
    virtual QVariant headerData(int, Qt::Orientation, int) const; // установка заголовков

    virtual QVariant data(const QModelIndex&, int) const;  // выборка данных по роли

    struct FileParam
    {
        quint32 id;
        QTime time;
        quint32 _time;
        quint32 word;
        ulong numpp;
        ulong group_numpp;
        bool ready;
    };

    virtual int Size(void);

    virtual QString csvData(void);

public slots:
    virtual void slotUpdateInternalData(A429_Word*); // ф-я разбора параметра группы файла
    virtual void slotResetInternalData(void);

    virtual void Size(int s);

private:
    int static_size;

    QQueue<FileParam*> fileparam;

    ulong numpp;

    FileParam* fp; // временное значение (статичное выделение памяти под промежуточное значение)

    virtual void dblDataUpdate(void);
};

#endif // MODELPARSER_FILEPARAM_H
