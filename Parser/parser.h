#ifndef PARSER_H
#define PARSER_H

#include "../cfg.h"
#include "../pivdata.h"
#include "../SrcData/a429_word.h"
#include "../SrcData/a429_channel.h"
#include "../PivEditor/MyItemModel.h"
#include "proxymodelparser.h"
#include <QDir>
#include <QFileInfo>

#include "modelparser_param.h"
#include "modelparser_group.h"
#include "modelparser_file.h"
#include "modelparser_filegroup.h"
#include "modelparser_fileparam.h"
#include "modelparser_delegatebase.h"
#include "../Plot/plotwidget.h"

#include <QScrollArea>
#include <QProgressBar>

class Parser : public QWidget
{
    Q_OBJECT
public:
    explicit Parser(MyItemModel* model,QWidget *parent = 0);
    virtual ~Parser(void);

    void Setup(PivData* psrc, A429_Channel* dsrc); // настройка парсера

signals:
    void signalParserBeginReload(void);
    void signalParserEndReload(void);

public slots:
    void slotStateWidgetChange(void); // слот переключения режима виджета (в отдельном окне)

    void slotCreate(void);
    void slotClear(void);

    void slotShowParserTable(QModelIndex);

    void Pausa(void); // пауза в отображении данных на каналах

    void slotWriteToFile(QString);
    void slotRec(void);
    void slotFileSelect(void);
    void slotSelectForRec(void);

    void slotShowPlot(void);

    void slotResizeTableParser(void);
private:
    QTabWidget* parent; // родитель

    QGridLayout* gl_table_parser; // компоновщик таблиц парсера
    QScrollArea* sa_table_parser; // область прокрутки для таблиц парсера

    bool state; // режим окна или вкладки

    // указатели на внешние данные (!!! НЕ удалаять эти указатели)
    PivData* srcpiv; // источник ПИВ (линии)
    A429_Channel* srcdata; // источник данных
    MyItemModel* srcmodelpiv; // модель данных

    ProxyModelPivForParser* modelpiv; // прокси-модель данных

    class WidgetParser : public QWidget // виджет парсера
    {
    public:
        QTableView* tv; // таблица парсера
        QLabel* name; // путь к параметру

        class mySpinBox : public QSpinBox
        {
            //Q_OBJECT
        public:
            explicit mySpinBox(QWidget* parent) : QSpinBox(parent)
            {
                this->lineEdit()->setFocusPolicy(Qt::ClickFocus); // фокус по клику
            }
            virtual void wheelEvent(QWheelEvent *event)
            {
                event->accept(); // блокируем перемотку значения
            }
        }*colcount; // выбор кол-ва стобцов таблицы парсера

        QCheckBox* flag_rec; // флажок регистрации

        QGridLayout* layout; // компоновщик

        ModelParser_DelegateBase* mpdb;

        WidgetParser() : QWidget(0)
        {
            layout = new QGridLayout(this);

            tv = new QTableView(this);
            tv->verticalHeader()->setResizeMode(QHeaderView::ResizeToContents);
            tv->horizontalHeader()->setResizeMode(QHeaderView::Interactive);
            tv->setDragEnabled(true);
            tv->setDragDropMode(QTableView::DragOnly);

            name = new QLabel(this);
            name->setWordWrap(true);
            name->setFixedWidth(200);

            colcount = new mySpinBox(this);
            colcount->setMinimum(1);
            colcount->setMaximum(50);
            colcount->setValue(5);
            colcount->setToolTip("Кол-во столбцов\nтаблицы анализатора");

            flag_rec = new QCheckBox("Регистрировать",this);

            layout->addWidget(name,0,0);
            layout->addWidget(colcount,1,0);
            layout->addWidget(flag_rec,2,0);
            layout->addWidget(tv,0,1,4,1);
            this->setHidden(true);

            mpdb = new ModelParser_DelegateBase();
            tv->setItemDelegate(mpdb);
        }
        ~WidgetParser()
        {
            delete tv;
            delete name;
            delete colcount;
            delete flag_rec;

            delete layout;

            delete mpdb;
        }
    };
    QList<WidgetParser*> list_wp; // список виджетов парсеров

    QList<ModelParserBase*> list_modelparser; // модель анализатора

    QStringList list_selectedpd; // выделенные элементы ПИВ

    QThread *thparser; // поток моделей анализатора

    // элементы управления
    QPushButton* pb_close; // кнопка закрыть
    QPushButton* pb_state; // кнопка смены режима вкладки/окна
    QLabel* l_main; // поле для сообщений
    QTreeView* tw_parser; // дерево элементов ПИВ для парсера
    QPushButton* pb_pausa;
    QPushButton* pb_plot; // вызов окна графика

    void CreateTv(PivData* pd, ModelParserBase* mpb = 0x0); // создание гуи парсера

    QProgressBar* pbar_loadparser; // прогресс бар загрузки парсера
    QLabel* l_loadparser; // для прогресс бар загрузки парсера

    // Для сохранения данных в файл
    QFile* file; // файл для записи
    QDir dir;
    QLineEdit* le_file;
    QPushButton* pb_file;
    QPushButton* pb_filerecstop;
    QPushButton *pb_select;
    QMap<ModelParserBase*,QString> stroka; // текущаяя строка для записи в файл
    QPushButton* pb_settingsfile; // кнопка вызова настроек записи в файл
    bool first_wtf; // флаг первой записи после нажатия на кнопку "Старт"


    QList<PlotWidget*> list_plot; // графики

    QString FileNameCtrl(QString); //контроль имени файла для записи

};

#endif // PARSER_H
