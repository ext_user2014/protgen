#ifndef MODELPARSER_GROUP_H
#define MODELPARSER_GROUP_H

#include "modelparserbase.h"

class ModelParser_Group : public ModelParserBase
{
    Q_OBJECT
public:
    ModelParser_Group(A429_Channel *ch, PivData *pd);

    virtual QVariant data(const QModelIndex&, int) const;  // выборка данных по роли

    virtual int Size(void);

public slots:
    virtual void slotUpdateInternalData(A429_Word*);
    virtual void slotResetInternalData(void);

    virtual void Size(int);

private:
    struct StGroup
    {
        quint32 addr;
        quint32 m;
        quint8 mb;
        bool exist;
    }; QList<StGroup> list_addrexist;  // !!!только для PivData::N_GROUP (НЕ файл)

    virtual void dblDataUpdate(void) {}
};

#endif // MODELPARSER_GROUP_H
