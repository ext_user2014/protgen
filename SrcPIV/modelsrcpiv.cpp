#include "modelsrcpiv.h"

ModelSrcPIV::ModelSrcPIV(MyItemModel *src, QObject *parent) :
    QSortFilterProxyModel(parent), model_from_editor(0x0), model_from_files(0x0)
{
    srcpivtype = SPT_PivEditor; // по умолчанию источник - редактор ПИВ
    model_from_editor = src;

    model_from_files = new MyItemModel(this);

    slotChangeSrcPIVType(srcpivtype);
}
ModelSrcPIV::~ModelSrcPIV(void)
{
    delete model_from_files;
}

/******************************************************************
*
*  Фильтр строк
*
******************************************************************/
bool ModelSrcPIV::filterAcceptsRow(int source_row, const QModelIndex &source_parent) const
{
    if( !source_parent.isValid() )
        return true;

    PivData* pd = (PivData*)source_parent.internalPointer();
    if( !pd ) // все корневые узлы
        return true;

    if(  pd->children.at(source_row)->info.type > PivData::N_LINE ) // фильтруем параметры и их элементы
        return false;

    return true;

}
/******************************************************************
*
*  Фильтр столбцов
*
******************************************************************/
bool ModelSrcPIV::filterAcceptsColumn(int source_column, const QModelIndex &source_parent) const
{
    if( !source_parent.isValid() )
        return true;

    if( source_column >= 2 )
        return false;

    return true;
}
/******************************************************************
*
*  Флаги элементов
*
******************************************************************/
Qt::ItemFlags ModelSrcPIV::flags(const QModelIndex &index) const
{
    if (!index.isValid())
        return Qt::ItemIsEnabled | Qt::ItemIsDropEnabled;

    return QAbstractItemModel::flags(index) |  Qt::ItemIsDragEnabled;

}
/******************************************************************
*  Формирование данных при перетаскивании (переопределение ф-ии исходной модели)
*  вход: список модельных индексов
*  выход: указатель на MIME данные
******************************************************************/
QMimeData* ModelSrcPIV::mimeData(const QModelIndexList &indexes) const
{
    QMimeData *mimedata = new QMimeData();

    QByteArray encodedData;

    QDataStream stream(&encodedData, QIODevice::WriteOnly);

    for( int i=0; i<indexes.size(); i++ ) // перечисление всех перетаскиваемых индексов
    {
        if (indexes.at(i).isValid())
        {
            QModelIndex mi = this->mapToSource(indexes.at(i)); // отображаем индекс прокси модели в модель-источник
            if( !mi.column() )
            {
                stream << (quint64)mi.internalPointer(); // зашиваем указатель на PivData (он внутри модельного индекса)
                stream << (quint64)this->sourceModel(); // зашиваем указатель на модель-источник (нужно для парсера)
            }
        }
    }

    mimedata->setData(MIME_PTR_PIVDATA, encodedData);
    return mimedata;

}

/******************************************************************
*  слот добавления фала ПИВ
*  вход: нет
*  выход: нет
******************************************************************/
void ModelSrcPIV::slotAddFile(void)
{
    QString strfile = QFileDialog::getOpenFileName(0,"Выберите файл","","*.*");

    if( strfile.isEmpty() )
        return;

    if( !model_from_files->LoadData(strfile) )
        QMessageBox(QMessageBox::Warning,"Загрузка ПИВ","Неверный формат файла",QMessageBox::Ok).exec();
}

/******************************************************************
*  слот смены типа источника ПИВ
*  вход: тип источника
*  выход: нет
******************************************************************/
void ModelSrcPIV::slotChangeSrcPIVType(int type)
{
    srcpivtype = (SrcPIVTipe)type;

    switch (srcpivtype)
    {
    case SPT_File:
        this->setSourceModel(model_from_files);
        break;
    case SPT_PivEditor:
        this->setSourceModel(model_from_editor);
        break;
    default:
        break;
    }
    emit layoutChanged();
}



