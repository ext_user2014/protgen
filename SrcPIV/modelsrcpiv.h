#ifndef MODELSRCPIV_H
#define MODELSRCPIV_H

#include <QSortFilterProxyModel>
#include "cfg.h"
#include "pivdata.h"
#include "../PivEditor/MyItemModel.h"

class ModelSrcPIV : public QSortFilterProxyModel
{
    Q_OBJECT
public:
    explicit ModelSrcPIV(MyItemModel* src, QObject *parent = 0);
    ~ModelSrcPIV(void);

    bool filterAcceptsRow(int source_row, const QModelIndex &source_parent) const;
    bool filterAcceptsColumn(int source_column, const QModelIndex &source_parent) const;

    Qt::ItemFlags flags(const QModelIndex &index) const; // установка флагов

    QMimeData* mimeData(const QModelIndexList &indexes) const; // запрос данных при перетаскивании

    enum SrcPIVTipe{SPT_PivEditor, SPT_File};

public slots:
    void slotChangeSrcPIVType(int type);
    void slotAddFile(void);
private:
    SrcPIVTipe srcpivtype; // тип источника

    MyItemModel* model_from_editor;
    MyItemModel* model_from_files;
};

#endif // MODELSRCPIV_H
