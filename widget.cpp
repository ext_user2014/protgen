#include "widget.h"

Widget::Widget(QWidget *parent) :
    QWidget(parent)/*,
    ui(new Ui::Widget)*/
{
    ui = new Ui_Widget;
    ui->setupUi(this);

    // создаем валидатор для списка IP адресов и портов
    QRegExpValidator* validator = new QRegExpValidator(this);
    validator->setRegExp(QRegExp("[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}:[0-9]{1,5}"));
    ui->cbIPPort->setValidator(validator);
    ui->cbIPPort->setToolTip("Вводить так: IP:port. Например: 192.168.0.1:2014");

    // Создание и настройка редактора ПИВ
    Views v;
    v.filename = ui->teFile;
    v.help = ui->teHelp;
    v.pathelement = ui->teElementPath;
    v.pivtable = ui->tabvPivList;
    v.pivtree = ui->tvPivList;
    v.proptable = ui->tablewProp;
    v.savetype = ui->cbSaveAs;
    this->piveditor = new PivEditor(v,this);
    connect(this->ui->pbOpen,SIGNAL(clicked()),this->piveditor,SLOT(Load())); // загрузка
    connect(this->ui->pbSave,SIGNAL(clicked()),this->piveditor,SLOT(slotSave())); // сохранение
    connect(this->ui->pbAddPL,SIGNAL(clicked()),this->piveditor,SLOT(AddItemClick())); // добавление
    connect(this->ui->pbRemPL,SIGNAL(clicked()),this->piveditor,SLOT(RemItemClick())); // удаление

    // создание отдельного потока для источников данных (актуально для сетевого источника)
    this->th_srcdata = new QThread();
    this->th_srcdata->start();

    // создание и настройка сетевого источника данных
    this->srcdatatcp = new SrcDataTCP(this);
    connect(this->ui->cbIPPort,SIGNAL(editTextChanged(QString)),this->srcdatatcp,SLOT(SetHostIPPort(QString))); // установка адреса и порта
    connect(this->srcdatatcp,SIGNAL(ChangeStatus(QString)),this->ui->teIPstatus,SLOT(append(QString))); // отслеживания статуса соединения
    connect(this->ui->pbConnect,SIGNAL(clicked()),this->srcdatatcp,SLOT(ConnectToHost())); // соединение с сервером
    connect(this->ui->pbSendQ,SIGNAL(clicked()),this->srcdatatcp,SLOT(slotDebugStart())); // старт отладки
    ui->cbIPPort->addItems(this->srcdatatcp->slist_ipp); // загружаем список ранее подключенных серверов
    connect(this->srcdatatcp,SIGNAL(signalConnected(QString)),this,SLOT(slotAddIPPort(QString))); // сохранение адреса и порта соединенного сервера
    connect(this,SIGNAL(ShowDebug(bool)),this->srcdatatcp,SLOT(slotDebugShow(bool))); // отображение/скрытие отладочной инфы

    // создание и настройка файлового источника данных
    this->srcdatafile = new SrcDataFile(this);
    this->srcdatafile->SetListWidget(this->ui->lwFiles);
    connect(this->ui->pbSrcOpenFile,SIGNAL(clicked()),this->srcdatafile,SLOT(AddFileToList())); // добавить файл
    connect(this->ui->pbSrcCloseFile,SIGNAL(clicked()),this->srcdatafile,SLOT(RemFileFromList())); // убрать файл

    // создание и настройка модели источника данных
    this->modelsrcdata = new ModelSrcData(this);
    this->modelsrcdata->SetSrcDataPointer(this->srcdatatcp); // установка сетевого источника (значение по умолчанию)
    this->modelsrcdata->SetSrcDataPointer(this->srcdatafile); // установка файлового источника
    connect(this->ui->cbConnectType,SIGNAL(currentIndexChanged(int)),this->modelsrcdata,SLOT(SetSrcDataType(int))); // смена источника данных
    ui->tvSettingAnalizator->setModel(this->modelsrcdata); // устанавливаем модель
    ui->tvSettingAnalizator->setItemDelegate(new DelegateSrcData(this->modelsrcdata)); // устанавливаем делегата
    ui->tvSettingAnalizator->setDropIndicatorShown(true);
    ui->tvSettingAnalizator->setDragEnabled(true);
    ui->tvSettingAnalizator->setAcceptDrops(true);
    ui->tvSettingAnalizator->setDragDropMode(QAbstractItemView::DragDrop);
    connect(this->srcdatatcp,SIGNAL(ChangeStatus()),this->modelsrcdata,SIGNAL(layoutChanged())); // трансформируем сигнал изменения статуса в сигнал обновления представлений
    // снижаем нагрузку на ЦП!!! connect(this->srcdatatcp,SIGNAL(MessRcv()),this->modelsrcdata,SIGNAL(layoutChanged())); // трансформируем сигнал приемника в сигнал обновления представлений
    timer250ms.start(250);
    connect(&timer250ms,SIGNAL(timeout()),this->modelsrcdata,SIGNAL(layoutChanged())); // трансформируем сигнал таймера в сигнал обновления представлений

    // настройка источника ПИВ
    srcpivdata = new ModelSrcPIV(piveditor->model,this);
    connect(ui->cbSrcPIV,SIGNAL(currentIndexChanged(int)),srcpivdata,SLOT(slotChangeSrcPIVType(int))); // цепляем смену типа источника ПИВ
    connect(ui->pbSrcPIVAddFile,SIGNAL(clicked()),srcpivdata,SLOT(slotAddFile())); // цепляем кнопку добавления файла ПИВ
    ui->tvPIVfromEditor->setModel(srcpivdata);
    ui->tvPIVfromEditor->setDragEnabled(true);
    ui->tvPIVfromEditor->setDragDropMode(QAbstractItemView::DragOnly);
    ui->tvPIVfromEditor->setColumnWidth(0,200);
    ui->tvPIVfromEditor->expandAll();
    ui->tvPIVfromFiles->setModel(srcpivdata);
    ui->tvPIVfromFiles->setDragEnabled(true);
    ui->tvPIVfromFiles->setDragDropMode(QAbstractItemView::DragOnly);
    ui->tvPIVfromFiles->setColumnWidth(0,200);

    // обособление источников данных в отдельный поток
    this->srcdatatcp->setParent(0);
    this->srcdatatcp->moveToThread(this->th_srcdata);
}

Widget::~Widget()
{
    th_srcdata->exit();
    if( !th_srcdata->wait(1000) )
        th_srcdata->terminate();

    delete piveditor;  // редактор ПИВ
    delete srcdatafile; // источник данных из файла
    delete modelsrcdata; // модель источника данных
    delete srcpivdata;

    counters.clear(); // счетчики

    srcdatatcp->slist_ipp.clear(); // очищаем список серверов
    for( int i=0;i<ui->cbIPPort->count();i++ ) // сохраняем список серверов
        srcdatatcp->slist_ipp.append(ui->cbIPPort->itemText(i));

    delete srcdatatcp; // источник данных по сети
    delete th_srcdata; // поток источника данных

    delete ui;
}

void Widget::closeEvent(QCloseEvent *ev)
{
    emit this->IsClosed();
    QWidget::closeEvent(ev);
}

void Widget::keyPressEvent(QKeyEvent *ev)
{
    // отображение отладочной страницы
    static QString str = "";
    str += ev->text();

    if( str.contains("debug") )
    {
        emit ShowDebug(true);
        str = "";
    }
    if( str.contains("no") )
    {
        emit ShowDebug(false);
        str = "";
    }
}

void Widget::on_pbOpenMonSelCh_clicked()
{
    QModelIndexList mil = this->ui->tvSettingAnalizator->selectionModel()->selectedIndexes();
    if( !mil.size() )
    {
        QMessageBox(QMessageBox::Warning,"Монитор канала А429","Выделите хотя бы один канал для просмотра",QMessageBox::Ok).exec();
        return;
    }

    MonitorA429* mon = new MonitorA429(&mil, this->ui->tabWidget);
    connect(this,SIGNAL(IsClosed()),mon,SLOT(close())); // удаляем монитор при закрытии главного окна
    connect(this->srcdatatcp,SIGNAL(disconnected()),mon,SLOT(close())); // удаляем монитор при потере соединения
}

void Widget::on_pbOpenParSelCh_clicked()
{
    QModelIndexList list_index = ui->tvSettingAnalizator->selectionModel()->selectedIndexes();

    if( !list_index.size() )
    {
        QMessageBox(QMessageBox::Warning,"Анализатор канала А429","Выделите хотя бы один канал для анализа",QMessageBox::Ok).exec();
        return;
    }

    for( int i=0;i<list_index.size();i++ )
    {
        QModelIndex mi = list_index[i];
        if( mi.column() )
            continue;

        Parser* par = new Parser(this->modelsrcdata->GetSrcPivModel(mi),this->ui->tabWidget);
        par->Setup(this->modelsrcdata->GetSrcPiv(mi),this->modelsrcdata->GetSrcData(mi));
        connect(this,SIGNAL(IsClosed()),par,SLOT(close())); // удаляем анализатор при закрытии главного окна
        connect(this->srcdatatcp,SIGNAL(disconnected()),par,SLOT(close())); // удаляем анализатор при потере соединения
        connect(this->piveditor,SIGNAL(signalBeginAddOrRem()),par,SLOT(slotClear())); // начало редактирование = очищаем анализатор
        connect(this->piveditor,SIGNAL(signalEndAddOrRem()),par,SLOT(slotCreate())); // окончание реактирования = восстанавливаем анализатор
    }

}


