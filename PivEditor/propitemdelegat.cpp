/*****************************************************************

    Делегат свойств элемента ПИВ

    Мордвинкин Ю.Ю.
    07.11.2013

*****************************************************************/
#include "propitemdelegat.h"

PropItemDelegat::PropItemDelegat(QObject *parent) :
    QItemDelegate(parent)
{
}
/******************************************************************
*
*  Создание редактора делегата
*
******************************************************************/
QWidget* PropItemDelegat::createEditor(QWidget *parent, const QStyleOptionViewItem &, const QModelIndex &index) const
{
    switch( this->pd_source->info.type )
    {
    case PivData::N_PIV:
    {
        return new QLineEdit(parent);
    }
    case PivData::N_LINE:
    {
        // QStringList field = (QStringList() << "Тип линии" << "Разрядность" << "Номер линии" << "Описание"); // поля
        switch( index.row() )
        {
        case 0: // Тип линии
        {
            QComboBox* cb = new QComboBox(parent);
            cb->addItems(this->pd_source->st_n_line.t_map.values());
            return cb;
        }
        case 1: // Разрядность
        {
            QComboBox* cb = new QComboBox(parent);
            if( this->pd_source->st_n_line.t == PivData::st_N_LINE::A429 )
            {
                cb->addItem("32");
            }else if( this->pd_source->st_n_line.t == PivData::st_N_LINE::RK )
            {
                cb->addItem("1");
            }else if( this->pd_source->st_n_line.t == PivData::st_N_LINE::MKIO )
            {
                cb->addItem("8");
                cb->addItem("32");
            }
            return cb;
        }
        case 2: // Номер линии
        case 3: // Описание
        {
            return new QLineEdit(parent);
        }
        }
        break;
    }
    case PivData::N_GROUP:
    {
        //        QStringList field = (QStringList() << "Тип группы" << "Идентификатор ID" << "Смещение ID" << "Маска ID"
        //                             << "Базовый адрес" << "Смещение адреса" << "Маска адреса"
        //                             << "Максимальный размер группы" << "Смещение размерности" << "Макса размерности" << "Контроль целостности");
        switch( index.row() )
        {
        case 0: // Тип группы
        {
            QComboBox* cb = new QComboBox(parent);
            cb->addItems(this->pd_source->st_n_group.t_map.values());
            return cb;
        }
        case 1: // Идентификатор ID
        {
            QTreeWidget* tw = new QTreeWidget(parent);
            tw->setColumnCount(3);
            tw->setFixedSize(300,200);
            tw->setHeaderLabels(QStringList()<<"oct"<<"hex"<<"bin");
            for( uint i=0; i<=pd_source->parent->st_n_group.id_mask; i++)
                new QTreeWidgetItem(tw,QStringList()<<QString::number(i,8)<<QString::number(i,16)<<QString::number(i,2));
            connect(tw,SIGNAL(doubleClicked(QModelIndex)),tw,SLOT(close()));
            return tw;
        }
        case 2: // Смещение ID
        case 5: // Смещение адреса
        case 8: // Смещение размерности
        {
            QComboBox* cb = new QComboBox(parent);
            int c = this->pd_source->TopPDOfType(PivData::N_LINE)->st_n_line.r; // разрядность линии
            for( int i=0; i<c; i++ ) // ограничиваем список в соответствии с разрядностью линии
                cb->addItem(QString::number(i+1));
            return cb;
        }
        case 7: // Максимальный размер группы
        {
            QComboBox* cb = new QComboBox(parent);
            if( this->pd_source->TopPDOfType(PivData::N_LINE)->st_n_line.t == PivData::st_N_LINE::A429 ) // ограничения А429 по адресам
            {
                int c = 256; // объём адресного пространства А429
                for( int i=0; i<=c; i++ ) // ограничиваем список в соответствии с типом линии
                    cb->addItem(QString::number(i));
            }
            return cb;
        }
        case 3: // Маска ID
        case 6: // Маска адреса
        case 9: // Макса размерности
        {
            QTableWidget* tw = new QTableWidget(parent);
            tw->setColumnCount(2);
            tw->setRowCount(1);
            tw->setFixedSize(330,200);
            tw->setHorizontalHeaderLabels(QStringList()<<"bin"<<"hex");
            tw->setCellWidget(0,0,new QLineEdit(tw));
            tw->setCellWidget(0,1,new QLineEdit(tw));
            tw->setColumnWidth(0,200);
            tw->setColumnWidth(1,80);
            tw->setCellWidget(0,1,new QLineEdit(tw));

            connect(((QLineEdit*)tw->cellWidget(0,0)),SIGNAL(textChanged(QString)),this,SLOT(Str2to16(QString)));
            connect(((QLineEdit*)tw->cellWidget(0,1)),SIGNAL(textChanged(QString)),this,SLOT(Str16to2(QString)));
            connect(this,SIGNAL(SetText2(QString)),((QLineEdit*)tw->cellWidget(0,0)),SLOT(setText(QString)));
            connect(this,SIGNAL(SetText16(QString)),((QLineEdit*)tw->cellWidget(0,1)),SLOT(setText(QString)));

            connect(tw,SIGNAL(doubleClicked(QModelIndex)),tw,SLOT(close()));
            return tw;
        }
        case 4: // Базовый адрес
        {
            QTreeWidget* tw = new QTreeWidget(parent);
            if( this->pd_source->TopPDOfType(PivData::N_LINE)->st_n_line.t == PivData::st_N_LINE::A429 ) // ограничения А429 по адресам
            {
                tw->setColumnCount(3);
                tw->setFixedSize(300,200);
                tw->setHeaderLabels(QStringList()<<"oct"<<"hex"<<"bin");
                for( int i=0; i<256; i++)
                    new QTreeWidgetItem(tw,QStringList()<<QString::number(i,8)<<QString::number(i,16)<<QString::number(i,2));
                connect(tw,SIGNAL(doubleClicked(QModelIndex)),tw,SLOT(close()));
            }
            return tw;
        }
        case 10: // Контроль целостности
        {
            return new QCheckBox(parent);
        }
        }
        break;
    }
    case PivData::N_PARAM:
    {
        //        QStringList field = (QStringList() << "Частота обновления" << "Идентификатор (Адрес)" << "Смещение" << "Маска"
        //                                           << "Контроль наличия");
        switch( index.row() )
        {
        case 0: // Частота обновления
        {
            return new QLineEdit(parent);
        }
        case 3: // Маска
        {
            QTableWidget* tw = new QTableWidget(parent);
            tw->setColumnCount(2);
            tw->setRowCount(1);
            tw->setFixedSize(330,200);
            tw->setHorizontalHeaderLabels(QStringList()<<"bin"<<"hex");
            tw->setCellWidget(0,0,new QLineEdit(tw));
            tw->setCellWidget(0,1,new QLineEdit(tw));
            tw->setColumnWidth(0,200);
            tw->setColumnWidth(1,80);
            tw->setCellWidget(0,1,new QLineEdit(tw));

            connect(((QLineEdit*)tw->cellWidget(0,0)),SIGNAL(textChanged(QString)),this,SLOT(Str2to16(QString)));
            connect(((QLineEdit*)tw->cellWidget(0,1)),SIGNAL(textChanged(QString)),this,SLOT(Str16to2(QString)));
            connect(this,SIGNAL(SetText2(QString)),((QLineEdit*)tw->cellWidget(0,0)),SLOT(setText(QString)));
            connect(this,SIGNAL(SetText16(QString)),((QLineEdit*)tw->cellWidget(0,1)),SLOT(setText(QString)));

            connect(tw,SIGNAL(doubleClicked(QModelIndex)),tw,SLOT(close()));
            return tw;
        }
        case 1: // Идентификатор (Адрес)
        {
            QTreeWidget* tw = new QTreeWidget(parent);
            if( this->pd_source->TopPDOfType(PivData::N_LINE)->st_n_line.t == PivData::st_N_LINE::A429 ) // ограничения А429 по адресам
            {
                tw->setColumnCount(3);
                tw->setFixedSize(300,200);
                tw->setHeaderLabels(QStringList()<<"oct"<<"hex"<<"bin");
                for( int i=0; i<256; i++)
                    new QTreeWidgetItem(tw,QStringList()<<QString::number(i,8)<<QString::number(i,16)<<QString::number(i,2));
                connect(tw,SIGNAL(doubleClicked(QModelIndex)),tw,SLOT(close()));
            }
            return tw;
        }
        case 2: // Смещение
        {
            QComboBox* cb = new QComboBox(parent);
            int c = this->pd_source->TopPDOfType(PivData::N_LINE)->st_n_line.r; // разрядность линии
            for( int i=0; i<c; i++ ) // ограничиваем список в соответствии с разрядностью линии
                cb->addItem(QString::number(i+1));
            return cb;
        }
        case 4: // Контроль наличия
        {
            return new QCheckBox(parent);
        }
        }
        break;
    }
    case PivData::N_ELP_GBIT:
    {
        //        QStringList field = (QStringList() << "Тип объединения" << "Смещение" << "Маска" << "Опорное значение"
        //                                           << "Значения" << "функция" << "Единица измерения" << "Расширенная функция");
        switch( index.row() )
        {
        case 0: // Тип объединения
        {
            QComboBox* cb = new QComboBox(parent);
            cb->addItems(this->pd_source->st_n_elp_gbit.t_map.values());
            return cb;
        }
        case 1: // Смещение
        {
            QComboBox* cb = new QComboBox(parent);
            int c = this->pd_source->TopPDOfType(PivData::N_LINE)->st_n_line.r; // разрядность линии
            for( int i=0; i<c; i++ ) // ограничиваем список в соответствии с разрядностью линии
                cb->addItem(QString::number(i+1));
            return cb;
        }
        case 2: // Маска
        {
            QTableWidget* tw = new QTableWidget(parent);
            tw->setColumnCount(2);
            tw->setRowCount(1);
            tw->setFixedSize(330,200);
            tw->setHorizontalHeaderLabels(QStringList()<<"bin"<<"hex");
            tw->setCellWidget(0,0,new QLineEdit(tw));
            tw->setCellWidget(0,1,new QLineEdit(tw));
            tw->setColumnWidth(0,200);
            tw->setColumnWidth(1,80);
            tw->setCellWidget(0,1,new QLineEdit(tw));

            connect(((QLineEdit*)tw->cellWidget(0,0)),SIGNAL(textChanged(QString)),this,SLOT(Str2to16(QString)));
            connect(((QLineEdit*)tw->cellWidget(0,1)),SIGNAL(textChanged(QString)),this,SLOT(Str16to2(QString)));
            connect(this,SIGNAL(SetText2(QString)),((QLineEdit*)tw->cellWidget(0,0)),SLOT(setText(QString)));
            connect(this,SIGNAL(SetText16(QString)),((QLineEdit*)tw->cellWidget(0,1)),SLOT(setText(QString)));

            connect(tw,SIGNAL(doubleClicked(QModelIndex)),tw,SLOT(close()));
            return tw;
        }
        case 3: // Опорное значение
        case 6: // Единица измерения
        case 7: // Расширенная функция
        {
            return new QLineEdit(parent);
        }
        case 4: // Значения
        {
            QTableWidget* tw = new QTableWidget(parent);
            tw->setMinimumSize(420,200);
            tw->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Preferred);
            tw->setColumnCount(3);
            tw->setColumnWidth(0,50);
            tw->setColumnWidth(2,200);
            tw->setHorizontalHeaderLabels(QStringList()<<"HEX" << "BIN" <<"Значение");
            tw->setRowCount(this->pd_source->st_n_elp_gbit.mask+1);
            for( int i=0; i<tw->rowCount(); i++ )
            {
                tw->setCellWidget(i, 0, new QLabel(QString::number(i,16),tw));
                tw->setCellWidget(i, 1, new QLabel(QString::number(i,2),tw));
                tw->setCellWidget(i, 2, new LineEditSS(tw));
            }
            connect(tw,SIGNAL(doubleClicked(QModelIndex)),tw,SLOT(close()));

            return tw;
        }
        case 5: // функция
        {
            QComboBox* cb = new QComboBox(parent);
            cb->addItems(this->pd_source->st_n_elp_gbit.fnc_map.values());
            return cb;
        }
        }
        break;
    }
    case PivData::N_ELP_BIT:
    {
        //        QStringList field = (QStringList() << "Смещение (№ бита)" << "Значение 1" << "Значение 0");
        switch( index.row() )
        {
        case 0: // Смещение (№ бита)
        {
            QComboBox* cb = new QComboBox(parent);
            int c = this->pd_source->TopPDOfType(PivData::N_LINE)->st_n_line.r; // разрядность линии
            for( int i=0; i<c; i++ ) // ограничиваем список в соответствии с разрядностью линии
                cb->addItem(QString::number(i+1));
            return cb;
        }
        case 1: // Значение 1
        case 2: // Значение 0
        {
            return new LineEditSS(parent);
        }
        }
        break;
    }
    }
    return 0;
}
/******************************************************************
*
*  загрузка данных в редактор делегата
*
******************************************************************/
void PropItemDelegat::setEditorData(QWidget *editor, const QModelIndex &index) const
{
    switch( this->pd_source->info.type )
    {
    case PivData::N_PIV:
    {
        QLineEdit* le = (QLineEdit*)editor;
        le->setText(this->pd_source->st_n_piv.desc);
        break;
    }
    case PivData::N_LINE:
    {
        // QStringList field = (QStringList() << "Тип линии" << "Разрядность" << "Номер линии" << "Описание"); // поля
        switch( index.row() )
        {
        case 0: // Тип линии
        {
            QComboBox* cb = (QComboBox*)editor;
            cb->setCurrentIndex((int)this->pd_source->st_n_line.t);
            break;
        }
        case 1: // Разрядность
        {
            QComboBox* cb = (QComboBox*)editor;
            cb->setCurrentIndex(cb->findText(QString::number(this->pd_source->st_n_line.r)));
            break;
        }
        case 2: // Номер линии
        {
            QLineEdit* le = (QLineEdit*)editor;
            le->setText(QString::number(this->pd_source->st_n_line.n));
            break;
        }
        case 3: // Описание
        {
            QLineEdit* le = (QLineEdit*)editor;
            le->setText(this->pd_source->st_n_line.desc);
            break;
        }
        }
        break;
    }
    case PivData::N_GROUP:
    {
        //        QStringList field = (QStringList() << "Тип группы" << "Идентификатор ID" << "Смещение ID" << "Маска ID"
        //                             << "Базовый адрес" << "Смещение адреса" << "Маска адреса"
        //                             << "Максимальный размер группы" << "Смещение размерности" << "Макса размерности" << "Контроль целостности");
        switch( index.row() )
        {
        case 0: // Тип группы
        {
            QComboBox* cb = (QComboBox*)editor;
            cb->setCurrentIndex((int)this->pd_source->st_n_group.t);
            break;
        }
        case 1: // Идентификатор ID
        {
            QTableWidget* tw = (QTableWidget*)editor;  // !!! ВНИМАНИЕ: здесь преобразование QTreeWidget в QTableWidget для уменьшения кода
            tw->setCurrentCell(this->pd_source->st_n_group.id,0);
            break;
        }
        case 2: // Смещение ID
        {
            QComboBox* cb = (QComboBox*)editor;
            cb->setCurrentIndex(this->pd_source->st_n_group.id_offset);
            break;
        }
        case 5: // Смещение адреса
        {
            QComboBox* cb = (QComboBox*)editor;
            cb->setCurrentIndex(this->pd_source->st_n_group.b_addr_offset);
            break;
        }
        case 8: // Смещение размерности
        {
            QComboBox* cb = (QComboBox*)editor;
            cb->setCurrentIndex(this->pd_source->st_n_group.size_offset);
            break;
        }
        case 7: // Максимальный размер группы
        {
            QComboBox* cb = (QComboBox*)editor;
            cb->setCurrentIndex(this->pd_source->st_n_group.size_max);
            break;
        }
        case 3: // Маска ID
        {
            QTableWidget* tw = (QTableWidget*)editor;
            ((QLineEdit*)tw->cellWidget(0,0))->setText(QString::number(this->pd_source->st_n_group.id_mask,2));
            ((QLineEdit*)tw->cellWidget(0,1))->setText(QString::number(this->pd_source->st_n_group.id_mask,16));
            break;
        }
        case 6: // Маска адреса
        {
            QTableWidget* tw = (QTableWidget*)editor;
            ((QLineEdit*)tw->cellWidget(0,0))->setText(QString::number(this->pd_source->st_n_group.b_addr_mask,2));
            ((QLineEdit*)tw->cellWidget(0,1))->setText(QString::number(this->pd_source->st_n_group.b_addr_mask,16));
            break;
        }
        case 9: // Макса размерности
        {
            QTableWidget* tw = (QTableWidget*)editor;
            ((QLineEdit*)tw->cellWidget(0,0))->setText(QString::number(this->pd_source->st_n_group.size_mask,2));
            ((QLineEdit*)tw->cellWidget(0,1))->setText(QString::number(this->pd_source->st_n_group.size_mask,16));
            break;
        }
        case 4: // Базовый адрес
        {
            QTableWidget* tw = (QTableWidget*)editor;  // !!! ВНИМАНИЕ: здесь преобразование QTreeWidget в QTableWidget для уменьшения кода
            if( this->pd_source->TopPDOfType(PivData::N_LINE)->st_n_line.t == PivData::st_N_LINE::A429 ) // ограничения А429 по адресам
                tw->setCurrentCell(this->pd_source->st_n_group.b_addr,0);
            break;
        }
        case 10: // Контроль целостности
        {
            QCheckBox* chb = (QCheckBox*)editor;
            chb->setChecked(this->pd_source->st_n_group.link_control);
        }
        }
        break;
    }
    case PivData::N_PARAM:
    {
        //        QStringList field = (QStringList() << "Частота обновления" << "Идентификатор (Адрес)" << "Смещение" << "Маска"
        //                                           << "Контроль наличия");
        switch( index.row() )
        {
        case 0: // Частота обновления
        {
            QLineEdit* le = (QLineEdit*)editor;
            le->setText(QString::number(this->pd_source->st_n_param.f));
            break;
        }
        case 3: // Маска
        {
            QTableWidget* tw = (QTableWidget*)editor;
            ((QLineEdit*)tw->cellWidget(0,0))->setText(QString::number(this->pd_source->st_n_param.mask,2));
            ((QLineEdit*)tw->cellWidget(0,1))->setText(QString::number(this->pd_source->st_n_param.mask,16));
            break;

        }
        case 1: // Идентификатор (Адрес)
        {
            QTableWidget* tw = (QTableWidget*)editor;  // !!! ВНИМАНИЕ: здесь преобразование QTreeWidget в QTableWidget для уменьшения кода
            if( this->pd_source->TopPDOfType(PivData::N_LINE)->st_n_line.t == PivData::st_N_LINE::A429 ) // ограничения А429 по адресам
                tw->setCurrentCell(this->pd_source->st_n_param.id,0);
            break;
        }
        case 2: // Смещение
        {
            QComboBox* cb = (QComboBox*)editor;
            cb->setCurrentIndex(this->pd_source->st_n_param.offset);
            break;
        }
        case 4: // Контроль наличия
        {
            QCheckBox* chb = (QCheckBox*)editor;
            chb->setChecked(this->pd_source->st_n_param.control);
        }
        }
        break;
    }
    case PivData::N_ELP_GBIT:
    {
        //        QStringList field = (QStringList() << "Тип объединения" << "Смещение" << "Маска" << "Опорное значение"
        //                                           << "Значения" << "функция" << "Единица измерения" << "Расширенная функция");
        switch( index.row() )
        {
        case 0: // Тип объединения
        {
            QComboBox* cb = (QComboBox*)editor;
            cb->setCurrentIndex((int)this->pd_source->st_n_elp_gbit.t);
            break;
        }
        case 1: // Смещение
        {
            QComboBox* cb = (QComboBox*)editor;
            cb->setCurrentIndex(this->pd_source->st_n_elp_gbit.offset);
            break;
        }
        case 2: // Маска
        {
            QTableWidget* tw = (QTableWidget*)editor;
            ((QLineEdit*)tw->cellWidget(0,0))->setText(QString::number(this->pd_source->st_n_elp_gbit.mask,2));
            ((QLineEdit*)tw->cellWidget(0,1))->setText(QString::number(this->pd_source->st_n_elp_gbit.mask,16));
            break;
        }
        case 3: // Опорное значение
        {
            QLineEdit* le = (QLineEdit*)editor;
            le->setText(QString::number(this->pd_source->st_n_elp_gbit.val));
            break;
        }
        case 6: // Единица измерения
        {
            QLineEdit* le = (QLineEdit*)editor;
            le->setText(this->pd_source->st_n_elp_gbit.si);
            break;
        }
        case 7: // Расширенная функция
        {
            QLineEdit* le = (QLineEdit*)editor;
            le->setText(this->pd_source->st_n_elp_gbit.fun);
            break;
        }
        case 4: // Значения
        {
            QTableWidget* tw = (QTableWidget*)editor;
            for( int i=0; i<tw->rowCount(); i++ )
            {
                LineEditSS* ed = (LineEditSS*)tw->cellWidget(i,2);
                if( this->pd_source->st_n_elp_gbit.values.contains(i) )
                {
                    QString str = this->pd_source->st_n_elp_gbit.values.value(i);
                    if( str.contains("&&") )
                    {
                        ed->setText(str.left(str.indexOf("&&")));
                        ed->setStyleSheet(str.right(str.size()-str.indexOf("&&")-2));
                    }
                    else
                    {
                        ed->setText(str);
                    }
                }
                else
                {
                    ed->setText("");
                }
            }
            break;
        }
        case 5: // функция
        {
            QComboBox* cb = (QComboBox*)editor;
            cb->setCurrentIndex((int)this->pd_source->st_n_elp_gbit.fnc);
            break;
        }
        }
        break;
    }
    case PivData::N_ELP_BIT:
    {
        //        QStringList field = (QStringList() << "Смещение (№ бита)" << "Значение 1" << "Значение 0");
        switch( index.row() )
        {
        case 0: // Смещение (№ бита)
        {
            QComboBox* cb = (QComboBox*)editor;
            cb->setCurrentIndex(this->pd_source->st_n_elp_bit.offset);
            break;
        }
        case 1: // Значение 1
        {
            LineEditSS* le = (LineEditSS*)editor;
            le->setText(this->pd_source->st_n_elp_bit.v_1);
            le->setStyleSheet(this->pd_source->st_n_elp_bit.v_1ss);
            le->color = this->pd_source->st_n_elp_bit.color1;
            break;
        }
        case 2: // Значение 0
        {
            LineEditSS* le = (LineEditSS*)editor;
            le->setText(this->pd_source->st_n_elp_bit.v_0);
            le->setStyleSheet(this->pd_source->st_n_elp_bit.v_0ss);
            le->color = this->pd_source->st_n_elp_bit.color0;
            break;
        }
        }
        break;
    }
    }

}
/******************************************************************
*
*  Изъятие данных из редактора делегата и запись их в модель данных
*
******************************************************************/
void PropItemDelegat::setModelData(QWidget *editor, QAbstractItemModel *, const QModelIndex &index) const
{
    switch( this->pd_source->info.type )
    {
    case PivData::N_PIV:
    {
        QLineEdit* le = (QLineEdit*)editor;
        this->pd_source->st_n_piv.desc = le->text();
        break;
    }
    case PivData::N_LINE:
    {
        // QStringList field = (QStringList() << "Тип линии" << "Разрядность" << "Номер линии" << "Описание"); // поля
        switch( index.row() )
        {
        case 0: // Тип линии
        {
            QComboBox* cb = (QComboBox*)editor;
            this->pd_source->st_n_line.t = (PivData::st_N_LINE::en)cb->currentIndex();
            break;
        }
        case 1: // Разрядность
        {
            QComboBox* cb = (QComboBox*)editor;
            this->pd_source->st_n_line.r = cb->currentText().toUInt();
            break;
        }
        case 2: // Номер линии
        {
            QLineEdit* le = (QLineEdit*)editor;
            this->pd_source->st_n_line.n = le->text().toUInt();
            break;
        }
        case 3: // Описание
        {
            QLineEdit* le = (QLineEdit*)editor;
            this->pd_source->st_n_line.desc = le->text();
            break;
        }
        }
        break;
    }
    case PivData::N_GROUP:
    {
        //        QStringList field = (QStringList() << "Тип группы" << "Идентификатор ID" << "Смещение ID" << "Маска ID"
        //                             << "Базовый адрес" << "Смещение адреса" << "Маска адреса"
        //                             << "Максимальный размер группы" << "Смещение размерности" << "Макса размерности" << "Контроль целостности");
        switch( index.row() )
        {
        case 0: // Тип группы
        {
            QComboBox* cb = (QComboBox*)editor;
            this->pd_source->st_n_group.t = (PivData::st_N_GROUP::en)cb->currentIndex();
            break;
        }
        case 1: // Идентификатор ID
        {
            QTableWidget* tw = (QTableWidget*)editor;  // !!! ВНИМАНИЕ: здесь преобразование QTreeWidget в QTableWidget для уменьшения кода
            if( tw->currentRow()!=-1 )
                this->pd_source->st_n_group.id = tw->currentRow();
            break;
        }
        case 2: // Смещение ID
        {
            QComboBox* cb = (QComboBox*)editor;
            this->pd_source->st_n_group.id_offset = cb->currentIndex();
            break;
        }
        case 5: // Смещение адреса
        {
            QComboBox* cb = (QComboBox*)editor;
            this->pd_source->st_n_group.b_addr_offset = cb->currentIndex();
            break;
        }
        case 8: // Смещение размерности
        {
            QComboBox* cb = (QComboBox*)editor;
            this->pd_source->st_n_group.size_offset = cb->currentIndex();
            break;
        }
        case 7: // Максимальный размер группы
        {
            QComboBox* cb = (QComboBox*)editor;
            this->pd_source->st_n_group.size_max = cb->currentIndex();
            break;
        }
        case 3: // Маска ID
        {
            QTableWidget* tw = (QTableWidget*)editor;
            this->pd_source->st_n_group.id_mask = ((QLineEdit*)tw->cellWidget(0,0))->text().toUInt(0,2);
            break;
        }
        case 6: // Маска адреса
        {
            QTableWidget* tw = (QTableWidget*)editor;
            this->pd_source->st_n_group.b_addr_mask = ((QLineEdit*)tw->cellWidget(0,0))->text().toUInt(0,2);
            break;
        }
        case 9: // Макса размерности
        {
            QTableWidget* tw = (QTableWidget*)editor;
            this->pd_source->st_n_group.size_mask = ((QLineEdit*)tw->cellWidget(0,0))->text().toUInt(0,2);
            break;
        }
        case 4: // Базовый адрес
        {
            QTableWidget* tw = (QTableWidget*)editor;  // !!! ВНИМАНИЕ: здесь преобразование QTreeWidget в QTableWidget для уменьшения кода
            if( this->pd_source->TopPDOfType(PivData::N_LINE)->st_n_line.t == PivData::st_N_LINE::A429 ) // ограничения А429 по адресам
                this->pd_source->st_n_group.b_addr = tw->currentRow();
            break;
        }
        case 10: // Контроль целостности
        {
            QCheckBox* chb = (QCheckBox*)editor;
            this->pd_source->st_n_group.link_control = chb->isChecked();
        }
        }
        break;
    }
    case PivData::N_PARAM:
    {
        //        QStringList field = (QStringList() << "Частота обновления" << "Идентификатор (Адрес)" << "Смещение" << "Маска"
        //                                           << "Контроль наличия");
        switch( index.row() )
        {
        case 0: // Частота обновления
        {
            QLineEdit* le = (QLineEdit*)editor;
            this->pd_source->st_n_param.f = le->text().toUInt();
            break;
        }
        case 3: // Маска
        {
            QTableWidget* tw = (QTableWidget*)editor;
            this->pd_source->st_n_param.mask = ((QLineEdit*)tw->cellWidget(0,0))->text().toUInt(0,2);
            break;
        }
        case 1: // Идентификатор (Адрес)
        {
            QTableWidget* tw = (QTableWidget*)editor;  // !!! ВНИМАНИЕ: здесь преобразование QTreeWidget в QTableWidget для уменьшения кода
            if( this->pd_source->TopPDOfType(PivData::N_LINE)->st_n_line.t == PivData::st_N_LINE::A429 ) // ограничения А429 по адресам
                this->pd_source->st_n_param.id = tw->currentRow();
            break;
        }
        case 2: // Смещение
        {
            QComboBox* cb = (QComboBox*)editor;
            this->pd_source->st_n_param.offset = cb->currentIndex();
            break;
        }
        case 4: // Контроль наличия
        {
            QCheckBox* chb = (QCheckBox*)editor;
            this->pd_source->st_n_param.control = chb->isChecked();
        }
        }
        break;
    }
    case PivData::N_ELP_GBIT:
    {
        //        QStringList field = (QStringList() << "Тип объединения" << "Смещение" << "Маска" << "Опорное значение"
        //                                           << "Значения" << "функция" << "Единица измерения" << "Расширенная функция");
        switch( index.row() )
        {
        case 0: // Тип объединения
        {
            QComboBox* cb = (QComboBox*)editor;
            this->pd_source->st_n_elp_gbit.t = (PivData::st_N_ELP_GBIT::en1)cb->currentIndex();
            break;
        }
        case 1: // Смещение
        {
            QComboBox* cb = (QComboBox*)editor;
            this->pd_source->st_n_elp_gbit.offset = cb->currentIndex();
            break;
        }
        case 2: // Маска
        {
            QTableWidget* tw = (QTableWidget*)editor;
            this->pd_source->st_n_elp_gbit.mask = ((QLineEdit*)tw->cellWidget(0,0))->text().toUInt(0,2);
            break;
        }
        case 3: // Опорное значение
        {
            QLineEdit* le = (QLineEdit*)editor;
            this->pd_source->st_n_elp_gbit.val = le->text().toFloat();
            break;
        }
        case 6: // Единица измерения
        {
            QLineEdit* le = (QLineEdit*)editor;
            this->pd_source->st_n_elp_gbit.si = le->text();
            break;
        }
        case 7: // Расширенная функция
        {
            QLineEdit* le = (QLineEdit*)editor;
            this->pd_source->st_n_elp_gbit.fun = le->text();
            break;
        }
        case 4: // Значения
        {
            QTableWidget* tw = (QTableWidget*)editor;
            for( int i=0; i<tw->rowCount(); i++ )
            {
                QString str = ((LineEditSS*)tw->cellWidget(i,2))->text();
                if( !str.isEmpty() )
                {
                    if( this->pd_source->st_n_elp_gbit.values.contains(i) ) // элемент есть, меняем его, если есть на что
                        this->pd_source->st_n_elp_gbit.values[i] = str + "&&" + ((LineEditSS*)tw->cellWidget(i,2))->styleSheet() + ";";
                    else
                        this->pd_source->st_n_elp_gbit.values.insert(i,str + "&&" + ((LineEditSS*)tw->cellWidget(i,2))->styleSheet() + ";");
                }
                else // пустая строка, элемент надо удалить
                {
                    if( this->pd_source->st_n_elp_gbit.values.contains(i) ) // елемент был, но удаляется
                        this->pd_source->st_n_elp_gbit.values.remove(i); // удаляем элемент
                }
            }
            break;
        }
        case 5: // функция
        {
            QComboBox* cb = (QComboBox*)editor;
            this->pd_source->st_n_elp_gbit.fnc = (PivData::st_N_ELP_GBIT::en2)cb->currentIndex();
            break;
        }
        }
        break;
    }
    case PivData::N_ELP_BIT:
    {
        //        QStringList field = (QStringList() << "Смещение (№ бита)" << "Значение 1" << "Значение 0");
        switch( index.row() )
        {
        case 0: // Смещение (№ бита)
        {
            QComboBox* cb = (QComboBox*)editor;
            this->pd_source->st_n_elp_bit.offset = cb->currentIndex();
            break;
        }
        case 1: // Значение 1
        {
            LineEditSS* le = (LineEditSS*)editor;
            this->pd_source->st_n_elp_bit.v_1 = le->text();
            this->pd_source->st_n_elp_bit.v_1ss = le->styleSheet();
            this->pd_source->st_n_elp_bit.color1 = le->color;
            break;
        }
        case 2: // Значение 0
        {
            LineEditSS* le = (LineEditSS*)editor;
            this->pd_source->st_n_elp_bit.v_0 = le->text();
            this->pd_source->st_n_elp_bit.v_0ss = le->styleSheet();
            this->pd_source->st_n_elp_bit.color0 = le->color;
            break;
        }
        }
        break;
    }
    }
    emit this->signalCloseEditor();
}
