/*****************************************************************

    Модель данных (интерфейс доступа к данным) протокола обмена
    по каналу А429

    Мордвинкин Ю.Ю.
    23.10.2013

*****************************************************************/

#ifndef MYITEMMODEL_H
#define MYITEMMODEL_H

#include "../cfg.h"
#include "../pivdata.h"
#include "propitemdelegat.h"

class MyItemModel : public QAbstractItemModel
{
    Q_OBJECT
public:
    explicit MyItemModel(QObject *parent = 0);
    ~MyItemModel();

    // функции интерфейса
    QModelIndex index(int row, int column, const QModelIndex &parent) const;    // получение индекса модели
    QModelIndex parent(const QModelIndex &child) const;                         // получение индекса вышестоящего узла
    int rowCount(const QModelIndex &parent = QModelIndex()) const;              // количество строк
    int columnCount(const QModelIndex &parent = QModelIndex()) const;           // количество столбцов
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const;  // выборка данных по роли
    Qt::ItemFlags flags(const QModelIndex &index) const;                        // установка флагов
    QVariant headerData(int section, Qt::Orientation orientation, int role) const; // установка заголовков
    bool setData(const QModelIndex &index, const QVariant &value, int role);    // установка данных записи
    bool insertRow(int row, const QModelIndex &parent);                     // добавление данных
    bool removeRow(int row, const QModelIndex &parent);                     // удаление данных

    Qt::DropActions supportedDropActions() const; // поддерживаемые типы вставки
    QStringList mimeTypes(void) const; // Список миме типов
    QMimeData* mimeData(const QModelIndexList &indexes) const; // запрос данных при перетаскивании
    bool dropMimeData(const QMimeData *data, Qt::DropAction action, int row, int column, const QModelIndex &parent); // вставка данных при перетаскивании

    // открытые переменные
    PivDataList _pdl_data;          // список корневых узлов

    bool notsave; // данные не сохранены

signals:
    void mess(QString str);
public slots:

    bool LoadData(QString filename);         // загрузка данных
    bool SaveData(QString filename,PivData *pd = NULL);         // выгрузка данных

    void slotSetNotSave(void){notsave = true;} // установщик флага изменения данных

private:

    // вспомогательные (закрытые) функции
    int findRow(PivData *node) const; // поиск строки по данным
    // закрытые переменные
    QStringList columns_header;     // заголовки столбов

    quint64 __id; // служебные данные
};

#endif // MYITEMMODEL_H
