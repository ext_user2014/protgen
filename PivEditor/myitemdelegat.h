/*****************************************************************

    Делегат для модели данных

    Мордвинкин Ю.Ю.
    27.10.2013

*****************************************************************/

#ifndef MYITEMDELEGAT_H
#define MYITEMDELEGAT_H


#include "../cfg.h"
#include "../pivdata.h"


class MyItemDelegat : public QItemDelegate
{
    Q_OBJECT
public:
    MyItemDelegat(QObject *parent = 0);

    QWidget *createEditor(QWidget *parent, const QStyleOptionViewItem &, const QModelIndex &index) const;    // создание редактора
    void setEditorData(QWidget *editor, const QModelIndex &index) const;         // загрузка данных в редактор
    void setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const;  // загрузка данных из редактора в модель данных

    
signals:
    
public slots:
    
};

#endif // MYITEMDELEGAT_H
