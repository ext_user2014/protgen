/*****************************************************************

    Делегат для модели данных

    Мордвинкин Ю.Ю.
    27.10.2013

*****************************************************************/
#include "myitemdelegat.h"

/******************************************************************
*
*  Конструктор делегата
*
******************************************************************/
MyItemDelegat::MyItemDelegat(QObject *parent) :
    QItemDelegate(parent)
{
}

/******************************************************************
*
*  создание виджета редактора данных
*
******************************************************************/
QWidget *MyItemDelegat::createEditor(QWidget *parent, const QStyleOptionViewItem &, const QModelIndex &index) const
{
    switch( index.column() )
    {
    case 0x0: // наименование
        return new QLineEdit(parent);
    case 0x1: // тип
        return new QComboBox(parent);
    }
    return 0;
}

/******************************************************************
*
*  Загрузка данных в редактор данных
*
******************************************************************/
void MyItemDelegat::setEditorData(QWidget *editor, const QModelIndex &index) const
{
    PivData *pd = (PivData *)index.internalPointer();
    switch( index.column() )
    {
    case 0x0: // наименование
    {
        QLineEdit *ed = (QLineEdit *)editor;
        ed->setText(pd->info.Name);
        break;
    }
    case 0x1: // тип
    {
        QComboBox *cb = (QComboBox *)editor;
        if( !pd->parent )
            cb->addItem(pd->tnode.value(PivData::N_PIV));
        else
        {
            QList<PivData::Nodes> ln = pd->en_t_node.values(pd->parent->info.type);
            for( int i=0; i<ln.size(); i++ )
                cb->addItem(pd->tnode.value(ln.at(i)));
        }
        //cb->setCurrentIndex(pd->info.type);
        break;
    }
    }
}

/******************************************************************
*
*  Считытвание данных из редактора и загрузка их в модель данных
*
******************************************************************/
void MyItemDelegat::setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const
{
    PivData *pd = (PivData *)index.internalPointer();
    switch( index.column() )
    {
    case 0x0: // наименование
    {
        model->setData(index, ((QLineEdit *)editor)->text());
        break;
    }
    case 0x1: // тип
    {
        model->setData(index, (int)pd->tnode.key(((QComboBox *)editor)->currentText()));
        break;
    }
    }
}
