/*****************************************************************

    Модуль редактора ПИВ

    Мордвинкин Ю.Ю.
    01.11.2013

*****************************************************************/
#include "piveditor.h"

PivEditor::PivEditor(Views &v, QObject *parent) :
    QObject(parent)
{
    this->parentwidget = (QWidget*)parent;
    this->views = v;

    this->model = new MyItemModel();  // создаем модель
    this->views.pivtree->setModel(this->model); // установка модели
    this->views.pivtree->setColumnWidth(0,190);
    this->views.pivtree->setSelectionMode(QAbstractItemView::SingleSelection);
    this->views.pivtree->setDragEnabled(true);
    this->views.pivtree->setAcceptDrops(true);
    this->views.pivtree->setDropIndicatorShown(true);
    this->views.pivtree->setDragDropMode(QAbstractItemView::DragDrop);

    this->views.pivtable->setModel(this->model); // установка модели
    this->views.pivtable->setDragEnabled(true);
    this->views.pivtable->setAcceptDrops(true);
    this->views.pivtable->setDropIndicatorShown(true);
    this->views.pivtable->setDragDropMode(QAbstractItemView::DragDrop);
    this->views.pivtable->setColumnWidth(0,190);

    this->delegate = new MyItemDelegat();  // создание делегата модели
    this->views.pivtree->setItemDelegate(this->delegate);   // установка делегата модели
    this->views.pivtable->setItemDelegate(this->delegate);  // установка делегата модели

    this->propertydelegate = new PropItemDelegat();    // создание делегата свойств
    this->views.proptable->setItemDelegate(this->propertydelegate);    // установка делегата

    this->currentmodelindex = QModelIndex();
    // соединение сигналов со слотами
    connect(this->model,SIGNAL(mess(QString)),this,SLOT(ShowHelp(QString))); // отображаем сообщения от модели
    connect(this,SIGNAL(mess(QString)),this,SLOT(ShowHelp(QString))); // отображаем сообщения от редактора

    connect(this->propertydelegate,SIGNAL(signalCloseEditor()),this,SLOT(ShowProperty())); // показ свойств по закрытию редактора делегата свойств
    connect(this->delegate,SIGNAL(closeEditor(QWidget*)),this,SLOT(ShowProperty())); // показ свойств по закрытию редактора делегата

    connect(this->views.pivtree,SIGNAL(clicked(QModelIndex)),this,SLOT(SelectItem(QModelIndex))); // обработка выделения в pivtree
    connect(this->views.pivtable,SIGNAL(clicked(QModelIndex)),this,SLOT(SelectItem(QModelIndex))); // обработка выделения в pivtable

    connect(this->views.pivtree->selectionModel(),SIGNAL(currentChanged(QModelIndex,QModelIndex)),this,SLOT(ShowProperty(QModelIndex)));
    connect(this->views.pivtable->selectionModel(),SIGNAL(currentChanged(QModelIndex,QModelIndex)),this,SLOT(ShowProperty(QModelIndex)));

    connect(this->propertydelegate,SIGNAL(signalCloseEditor()),this->model,SLOT(slotSetNotSave())); // установка флага по закрытию редактора

    // !!!!!!!!! сообщаем об отсутсвии поддержки МКИО и РК
    emit mess("Модель данных: поддержка МКИО и РК отключена!");

    // установка таймера и счетчиков
    this->counters.insert("help",0);
    this->idTimerHelp = this->startTimer(1000);

    // Восстанавливаем состояние редактора
    StaticData::settings->beginGroup("PivEditor");
    QString strfile = StaticData::settings->value("LastOpenFilePIV","").toString();
    StaticData::settings->endGroup();
    if( this->model->LoadData(strfile) )
        this->views.filename->setText(strfile);
}
PivEditor::~PivEditor(void)
{
    if( model->notsave )
    {
        if( QMessageBox(QMessageBox::Warning,"Редактор ПИВ","Структура ПИВ изменена, сохранить перед выходом?",
                QMessageBox::Yes|QMessageBox::No).exec() == QMessageBox::Yes )
        {
            this->slotSave();
        }
    }

    this->disconnect(); // отключаемся от сигнал-слотов

    delete this->model;
    delete this->delegate;
    delete this->propertydelegate;
}

/******************************************************************
*
*  Добавить элемент ПИВ
*
******************************************************************/
void PivEditor::AddItemClick()
{
    emit this->signalBeginAddOrRem();

    QModelIndex ci = this->views.pivtree->currentIndex();
    if( !ci.isValid() )
        this->model->insertRow(this->model->_pdl_data.size(),ci);
    else
        this->model->insertRow(((PivData*)ci.internalPointer())->children.size(),ci);
    this->views.pivtree->setCurrentIndex(ci);

    emit this->signalEndAddOrRem();
}
/******************************************************************
*
*  Удалить элемент ПИВ
*
******************************************************************/
void PivEditor::RemItemClick()
{
    emit this->signalBeginAddOrRem();

    QModelIndex ci = this->views.pivtree->currentIndex(); // текущий индекс в pivtree
    QModelIndex old_ci = this->currentmodelindex; // сохраняем текущий индекс на случай отмены удаления

    if( ci.row() == model->rowCount(ci.parent())-1 ) // удаляется последний элемент?
    {
        this->currentmodelindex = ci.parent();

        this->views.pivtree->setCurrentIndex(this->currentmodelindex);    // меняем текущий индекс дерева
        this->views.pivtable->setRootIndex(this->currentmodelindex); // меняем корневой индекс таблицы
    }
    else
    {
        this->currentmodelindex = ci.model()->index(ci.row()+1,ci.column(),ci.parent());

        this->views.pivtree->setCurrentIndex(this->currentmodelindex);
        this->views.pivtable->setRootIndex(this->currentmodelindex);
    }

    if( !this->model->removeRow(ci.row(),ci.parent()) ) // удаляем текущий элемент
    {
        this->currentmodelindex = old_ci;

        this->views.pivtree->setCurrentIndex(this->currentmodelindex);
        this->views.pivtable->setRootIndex(this->currentmodelindex);
    }

    this->ShowProperty(this->currentmodelindex); // отображаем свойства

    emit this->signalEndAddOrRem();
}
/******************************************************************
*
*  Щелчок мышки на элементе ПИВ
*
******************************************************************/
void PivEditor::SelectItem(const QModelIndex &index)
{
    if( this->views.pivtree == this->parentwidget->focusWidget() )
    {
        if( this->currentmodelindex == index )
        {
            this->currentmodelindex = QModelIndex();
            this->views.pivtree->setCurrentIndex(this->views.pivtree->rootIndex());
            this->views.pivtable->setRootIndex(this->views.pivtree->currentIndex());
        }
        else
        {
            this->currentmodelindex = index;
            this->views.pivtable->setRootIndex(index);
        }
    }
    else if( this->views.pivtable == this->parentwidget->focusWidget() )
    {
        this->currentmodelindex = index;
    }
    this->ShowProperty(this->currentmodelindex);
}
/******************************************************************
*
*  Отображение справки на метке
*
******************************************************************/
void PivEditor::ShowHelp(QString str)
{
    this->views.help->setText("<b>" + str + "<\b>");
    this->counters["help"] = 3;
}

/******************************************************************
*
*  Событие таймера
*
******************************************************************/
void PivEditor::timerEvent(QTimerEvent *ev)
{
    if( this->counters.value("help") )
        this->counters["help"]--;
    if( this->counters.value("file") )
        this->counters["file"]--;

    if( ev->timerId() == this->idTimerHelp && !this->counters.value("help") )
        this->views.help->setText("");

}

/******************************************************************
*
*  Показ свойств элемента
*
******************************************************************/
void PivEditor::ShowProperty(QModelIndex index)
{
    // очищаемся перед созданием таблицы
    this->views.proptable->clear();
    this->propertydelegate->SetPDSource(0);
    this->views.proptable->setColumnCount(0);
    this->views.proptable->setRowCount(0);
    this->views.proptable->reset();
    this->views.pathelement->setText("");

    if( this->views.proptable == NULL || !index.isValid() )
        return;

    PivData *pd = (PivData*)index.internalPointer();
    this->propertydelegate->SetPDSource(pd);
    this->views.proptable->setColumnCount(1);
    this->views.proptable->setHorizontalHeaderLabels(QStringList() << "Значение");
    this->views.proptable->setColumnWidth(0,200);
    this->views.pathelement->setText(pd->Path());

    switch( pd->info.type )
    {
    case PivData::N_PIV:
    {
        this->views.proptable->setRowCount(1);
        this->views.proptable->setVerticalHeaderLabels(QStringList() << "Описание");
        this->views.proptable->setItem(0,0,new QTableWidgetItem());
        this->views.proptable->item(0,0)->setData(Qt::DisplayRole, pd->st_n_piv.desc);
        break;
    }
    case PivData::N_LINE:
    {
        QStringList field = (QStringList() << "Тип линии" << "Разрядность" << "Номер линии" << "Описание"); // поля
        this->views.proptable->setRowCount(field.size());
        this->views.proptable->setVerticalHeaderLabels(field);

        this->views.proptable->setItem(0,0,new QTableWidgetItem());
        this->views.proptable->item(0,0)->setData(Qt::DisplayRole, pd->st_n_line.t_map.value(pd->st_n_line.t));
        this->views.proptable->setItem(0,1,new QTableWidgetItem());
        this->views.proptable->item(0,1)->setData(Qt::DisplayRole, pd->st_n_line.r);
        this->views.proptable->setItem(0,2,new QTableWidgetItem());
        this->views.proptable->item(0,2)->setData(Qt::DisplayRole, pd->st_n_line.n);
        this->views.proptable->setItem(0,3,new QTableWidgetItem());
        this->views.proptable->item(0,3)->setData(Qt::DisplayRole, pd->st_n_line.desc);
        break;
    }
    case PivData::N_GROUP:
    {
        QStringList field = (QStringList() << "Тип группы"
                             << "ID группы oct(hex)"
                             << "№ 1-го бита ID (dec)"
                             << "Маска ID (hex)"
                             << "Базовый Адрес oct(hex)"
                             << "№ 1-го бита Адреса (dec)"
                             << "Маска Адреса (hex)"
                             << "Макс размер гр. (dec)"
                             << "№ 1-го бита р.гр.(dec)"
                             << "Макса р.гр. (hex)"
                             << "Контроль целостности");
        this->views.proptable->setRowCount(field.size());
        this->views.proptable->setVerticalHeaderLabels(field);

        this->views.proptable->setItem(0,0,new QTableWidgetItem());
        this->views.proptable->item(0,0)->setData(Qt::DisplayRole, pd->st_n_group.t_map.value(pd->st_n_group.t));
        this->views.proptable->setItem(0,1,new QTableWidgetItem());
        this->views.proptable->item(0,1)->setData(Qt::DisplayRole, QString().sprintf("%o (%X)",pd->st_n_group.id,pd->st_n_group.id));
        this->views.proptable->setItem(0,2,new QTableWidgetItem());
        this->views.proptable->item(0,2)->setData(Qt::DisplayRole, pd->st_n_group.id_offset+1);
        this->views.proptable->setItem(0,3,new QTableWidgetItem());
        this->views.proptable->item(0,3)->setData(Qt::DisplayRole, QString().sprintf("%X",pd->st_n_group.id_mask));
        this->views.proptable->setItem(0,4,new QTableWidgetItem());
        this->views.proptable->item(0,4)->setData(Qt::DisplayRole, QString().sprintf("A%o (%X)",pd->st_n_group.b_addr,pd->st_n_group.b_addr));
        this->views.proptable->setItem(0,5,new QTableWidgetItem());
        this->views.proptable->item(0,5)->setData(Qt::DisplayRole, pd->st_n_group.b_addr_offset+1);
        this->views.proptable->setItem(0,6,new QTableWidgetItem());
        this->views.proptable->item(0,6)->setData(Qt::DisplayRole, QString().sprintf("%X",pd->st_n_group.b_addr_mask));
        this->views.proptable->setItem(0,7,new QTableWidgetItem());
        this->views.proptable->item(0,7)->setData(Qt::DisplayRole, pd->st_n_group.size_max);
        this->views.proptable->setItem(0,8,new QTableWidgetItem());
        this->views.proptable->item(0,8)->setData(Qt::DisplayRole, pd->st_n_group.size_offset+1);
        this->views.proptable->setItem(0,9,new QTableWidgetItem());
        this->views.proptable->item(0,9)->setData(Qt::DisplayRole, QString().sprintf("%X",pd->st_n_group.size_mask));
        this->views.proptable->setItem(0,10,new QTableWidgetItem());
        this->views.proptable->item(0,10)->setData(Qt::DisplayRole, pd->st_n_group.link_control?"да":"нет");

        // скрытие полей в зависимости от типа группы
        if( pd->TopPDOfType(PivData::N_LINE)->st_n_line.t == PivData::st_N_LINE::A429 ) // находимся в линии А429
        {
            if( pd->st_n_group.t == PivData::st_N_GROUP::NOLINK ) // группа параметров
            {
                this->views.proptable->setRowHidden(1,1); // скрываем ID
                this->views.proptable->setRowHidden(2,1);
                this->views.proptable->setRowHidden(3,1);
                this->views.proptable->setRowHidden(7,1);   // скрываем размер группы
                this->views.proptable->setRowHidden(8,1);
                this->views.proptable->setRowHidden(9,1);
            }
            else // файл
            {
                if( pd->parent->info.type == PivData::N_LINE ) // это первая группа от линии
                {
                    this->views.proptable->setRowHidden(1,1); // скрываем ID, т.к. это описание файла в целом
                }
                else // последующие элементы файла
                {
                    this->views.proptable->setRowHidden(2,1); // скрываем характеристику ID
                    this->views.proptable->setRowHidden(3,1);
                    this->views.proptable->setRowHidden(7,1); // скрываем размер
                    this->views.proptable->setRowHidden(8,1);
                    this->views.proptable->setRowHidden(9,1);
                }
            }
        }
        break;
    }
    case PivData::N_PARAM:
    {
        QStringList field = (QStringList() << "Частота обновления" << "Адрес oct(hex)" << "№ 1-го бита А (dec)" << "Маска A (hex)"
                                           << "Контроль наличия");
        this->views.proptable->setRowCount(field.size());
        this->views.proptable->setVerticalHeaderLabels(field);

        this->views.proptable->setItem(0,0,new QTableWidgetItem());
        this->views.proptable->item(0,0)->setData(Qt::DisplayRole, pd->st_n_param.f);
        this->views.proptable->setItem(0,1,new QTableWidgetItem());
        this->views.proptable->item(0,1)->setData(Qt::DisplayRole, QString().sprintf("A%o (%X)",pd->st_n_param.id,pd->st_n_param.id));
        this->views.proptable->setItem(0,2,new QTableWidgetItem());
        this->views.proptable->item(0,2)->setData(Qt::DisplayRole, pd->st_n_param.offset+1);
        this->views.proptable->setItem(0,3,new QTableWidgetItem());
        this->views.proptable->item(0,3)->setData(Qt::DisplayRole, QString().sprintf("%X",pd->st_n_param.mask));
        this->views.proptable->setItem(0,4,new QTableWidgetItem());
        this->views.proptable->item(0,4)->setData(Qt::DisplayRole, pd->st_n_param.control?"да":"нет");

        break;
    }
    case PivData::N_ELP_GBIT:
    {
        QStringList field = (QStringList() << "Тип группы"
                             << "№ 1-го бита"
                             << "Маска (hex)"
                             << "Опорное значение"
                             << "Значения"
                             << "функция"
                             << "Единица измерения"
                             << "Расширенная функция");

        this->views.proptable->setRowCount(field.size());
        this->views.proptable->setVerticalHeaderLabels(field);
        this->views.proptable->setItem(0,0,new QTableWidgetItem());
        this->views.proptable->item(0,0)->setData(Qt::DisplayRole, pd->st_n_elp_gbit.t_map.value(pd->st_n_elp_gbit.t));
        this->views.proptable->setItem(0,1,new QTableWidgetItem());
        this->views.proptable->item(0,1)->setData(Qt::DisplayRole, pd->st_n_elp_gbit.offset+1);
        this->views.proptable->setItem(0,2,new QTableWidgetItem());
        this->views.proptable->item(0,2)->setData(Qt::DisplayRole, QString().sprintf("%X",pd->st_n_elp_gbit.mask));
        this->views.proptable->setItem(0,3,new QTableWidgetItem());
        this->views.proptable->item(0,3)->setData(Qt::DisplayRole, pd->st_n_elp_gbit.val);
        this->views.proptable->setItem(0,4,new QTableWidgetItem());
        this->views.proptable->item(0,4)->setData(Qt::DisplayRole, "Список");
        this->views.proptable->setItem(0,5,new QTableWidgetItem());
        this->views.proptable->item(0,5)->setData(Qt::DisplayRole, pd->st_n_elp_gbit.fnc_map.value(pd->st_n_elp_gbit.fnc));
        this->views.proptable->setItem(0,6,new QTableWidgetItem());
        this->views.proptable->item(0,6)->setData(Qt::DisplayRole, pd->st_n_elp_gbit.si);
        this->views.proptable->setItem(0,7,new QTableWidgetItem());
        this->views.proptable->item(0,7)->setData(Qt::DisplayRole, pd->st_n_elp_gbit.fun);

        // скрытие полей в зависимости от типа группы бит
        switch( pd->st_n_elp_gbit.t )
        {
        case PivData::st_N_ELP_GBIT::GB: // в этой группе можно задать только функцию группирования и ед.изм.
        {
            this->views.proptable->setRowHidden(1,1);
            this->views.proptable->setRowHidden(2,1);
            this->views.proptable->setRowHidden(3,1);
            this->views.proptable->setRowHidden(4,1);
            this->views.proptable->setRowHidden(7,1); // т.к. не реализован разбор строки, функция отключена
            break;
        }
        case PivData::st_N_ELP_GBIT::CSR: // для ЦСР достаточно смещения, маски, опорного значения. Для справки оставил ед.изм. и
        {
            this->views.proptable->setRowHidden(4,1);
            this->views.proptable->setRowHidden(5,1);
            this->views.proptable->setRowHidden(7,1);
            break;
        }
        case PivData::st_N_ELP_GBIT::K: // для коэфф достаточно смещения, маски и опорного знач.
        {
            this->views.proptable->setRowHidden(4,1);
            this->views.proptable->setRowHidden(5,1);
            this->views.proptable->setRowHidden(6,1);
            this->views.proptable->setRowHidden(7,1);

            break;
        }
        case PivData::st_N_ELP_GBIT::DDK:
        {
            // убираем всё, кроме типа группы и ед. измерения, т.к. для алгоритма ДДК ничего не надо
            // DDK = GB c функцией CON (сцепить)
            this->views.proptable->setRowHidden(1,1);
            this->views.proptable->setRowHidden(2,1);
            this->views.proptable->setRowHidden(3,1);
            this->views.proptable->setRowHidden(4,1);
            this->views.proptable->setRowHidden(5,1);
            this->views.proptable->setRowHidden(7,1);
            break;
        }
        case PivData::st_N_ELP_GBIT::CONST: // то же, что и коэфф, только другой алгоритм обработки
        {
            this->views.proptable->setRowHidden(4,1);
            this->views.proptable->setRowHidden(5,1);
            this->views.proptable->setRowHidden(6,1);
            this->views.proptable->setRowHidden(7,1);
            break;
        }
        case PivData::st_N_ELP_GBIT::VALUES: // оставляем Значения, Смещение и Маску
        {
            this->views.proptable->setRowHidden(3,1);
            this->views.proptable->setRowHidden(5,1);
            this->views.proptable->setRowHidden(6,1);
            this->views.proptable->setRowHidden(7,1);
            break;
        }
        }


        break;
    }
    case PivData::N_ELP_BIT:
    {
        QStringList field = (QStringList() << "№ бита" << "Значение 1" << "Значение 0");

        this->views.proptable->setRowCount(field.size());
        this->views.proptable->setVerticalHeaderLabels(field);

        this->views.proptable->setItem(0,0,new QTableWidgetItem());
        this->views.proptable->item(0,0)->setData(Qt::DisplayRole, pd->st_n_elp_bit.offset+1);
        this->views.proptable->setItem(0,1,new QTableWidgetItem());
        this->views.proptable->item(0,1)->setData(Qt::DisplayRole, pd->st_n_elp_bit.v_1);
        this->views.proptable->setItem(0,2,new QTableWidgetItem());
        this->views.proptable->item(0,2)->setData(Qt::DisplayRole, pd->st_n_elp_bit.v_0);

        this->views.proptable->item(0,1)->setData(Qt::BackgroundRole,pd->st_n_elp_bit.color1);
        this->views.proptable->item(0,2)->setData(Qt::BackgroundRole,pd->st_n_elp_bit.color0);
        break;
    }
    }
}
/******************************************************************
*
*  Показ свойств элемента (перегруженная функция)
*
******************************************************************/
void PivEditor::ShowProperty(void)
{
    this->ShowProperty(this->currentmodelindex);
}
/******************************************************************
*
*  Сброс внутренних ненужных переменных
*
******************************************************************/
void PivEditor::reset()
{
    this->currentmodelindex = QModelIndex();
    this->ShowProperty(this->currentmodelindex);
}
void PivEditor::Save(SaveType st)
{
    // выбираем файл
    QString strfile = this->views.filename->toPlainText();
    PivData* pd = 0x0;

    switch (st)
    {
    case SV_All:
        if( strfile.isEmpty() ) // ничего не выбрано, выходим
            strfile = QFileDialog::getSaveFileName(0,"Выберите файл",strfile,"*.*");
        break;
    case SV_AsAll:
        strfile = QFileDialog::getSaveFileName(0,"Выберите файл",strfile,"*.*");
        break;
    case SV_SelectedPiv:
        if( !this->views.pivtree->currentIndex().isValid() )
        {
            emit this->mess("Редактор ПИВ: ПИВ не выбран, сохранение отменено");
            return;
        }
        pd = (PivData*)this->views.pivtree->currentIndex().internalPointer();
        if( strfile.isEmpty() ) // ничего не выбрано, выходим
            strfile = QFileDialog::getSaveFileName(0,"Выберите файл",strfile,"*.*");
        break;
    case SV_AsSelectedPiv:
        if( !this->views.pivtree->currentIndex().isValid() )
        {
            emit this->mess("Редактор ПИВ: ПИВ не выбран, сохранение отменено");
            return;
        }
        pd = (PivData*)this->views.pivtree->currentIndex().internalPointer();
        strfile = QFileDialog::getSaveFileName(0,"Выберите файл",strfile,"*.*");
        break;
    }

    if( this->model->SaveData(strfile,pd) )
        this->views.filename->setText(strfile);
}

void PivEditor::Load(void)
{
    // выбираем файл
    QString strfile = QFileDialog::getOpenFileName(0,"Выберите файл","","*.*");

    if( strfile.isEmpty() ) // ничего не выбрано, выходим
    {
        emit this->mess("Редактор ПИВ: файл не выбран, загрузка файла отменена.");
        return;
    }
    if( this->model->LoadData(strfile) )
        this->views.filename->setText(strfile);

    // Сохраняем состояние редактора
    StaticData::settings->beginGroup("PivEditor");
    StaticData::settings->setValue("LastOpenFilePIV",this->views.filename->toPlainText());
    StaticData::settings->endGroup();
}

