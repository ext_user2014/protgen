/*****************************************************************

    Делегат свойств элемента ПИВ

    Мордвинкин Ю.Ю.
    07.11.2013

*****************************************************************/
#ifndef PROPITEMDELEGAT_H
#define PROPITEMDELEGAT_H

#include "../cfg.h"
#include "../pivdata.h"

class PropItemDelegat : public QItemDelegate
{
    Q_OBJECT
public:
    PropItemDelegat(QObject *parent = 0);
    void SetPDSource(PivData* pd) {pd_source = pd;}

    QWidget *createEditor(QWidget *parent, const QStyleOptionViewItem &, const QModelIndex &index) const;    // создание редактора
    void setEditorData(QWidget *editor, const QModelIndex &index) const;         // загрузка данных в редактор
    void setModelData(QWidget *editor, QAbstractItemModel *, const QModelIndex &index) const;  // загрузка данных из редактора в модель данных

signals:
    void SetText2(QString);
    void SetText16(QString);

    void signalCloseEditor(void)const;


public slots:
    void Str2to16(QString str) {emit SetText16(QString().sprintf("%X",str.toUInt(0,2)));}
    void Str16to2(QString str) {emit SetText2(QString::number(str.toUInt(0,16),2));}

private:
    PivData* pd_source; //источник данных
};
/***********************************************************************/
class LineEditSS : public QLineEdit
{
    Q_OBJECT
public:
    QMenu* cmenu_color;

    LineEditSS(QWidget* parent=0):QLineEdit(parent)
    {
        cmenu_color = new QMenu(parent);

        cmenu_color->addAction("Нет");
        cmenu_color->addAction("Зеленый");
        cmenu_color->addAction("Красный");
        cmenu_color->addAction("Желтый");
        cmenu_color->addAction("Голубой");
        connect(cmenu_color,SIGNAL(triggered(QAction*)),SLOT(slotActivated(QAction*)));

        this->setStyleSheet("background-color: white;");
        color.setNamedColor("white");
    }

    QColor color;

public slots:
    void slotActivated(QAction* pAction)
    {
        QString strColor = pAction->text();

        if( strColor == "Зеленый" )
        {
            this->setStyleSheet("background-color: green;");
            color.setNamedColor("green");
        }
        else if( strColor == "Красный" )
        {
            this->setStyleSheet("background-color: red;");
            color.setNamedColor("red");
        }
        else if( strColor == "Желтый" )
        {
            this->setStyleSheet("background-color: yellow;");
            color.setNamedColor("yellow");
        }
        else if( strColor == "Голубой" )
        {
            this->setStyleSheet("background-color: aqua;");
            color.setNamedColor("aqua");
        }
        else
        {
            this->setStyleSheet("background-color: white;");
            color.setNamedColor("white");
        }
    }

protected:
    virtual void contextMenuEvent(QContextMenuEvent* pe)
    {
        cmenu_color->exec(pe->globalPos());
    }

};
/***********************************************************************/
class DelegateLineEditSS : public QItemDelegate
{
    Q_OBJECT
public:
    DelegateLineEditSS(QObject *parent = 0):QItemDelegate(parent){}

    QWidget *createEditor(QWidget *parent, const QStyleOptionViewItem &, const QModelIndex &) const {return new LineEditSS(parent);}
    void setEditorData(QWidget *, const QModelIndex &) const {}
    void setModelData(QWidget *, QAbstractItemModel *, const QModelIndex &) const {}
};
#endif // PROPITEMDELEGAT_H
