/*****************************************************************

    Модель данных (интерфейс доступа к данным) протокола обмена
    по каналу А429

    Мордвинкин Ю.Ю.
    23.10.2013

    16.02.2014: изменена ф-я index, иначе иногда вылетал sigfault
    31.03.2014: изменена функция сохранения в файл

*****************************************************************/
#include "MyItemModel.h"

/******************************************************************
*
*  Конструктор модели
*
******************************************************************/
MyItemModel::MyItemModel(QObject *parent) :
    QAbstractItemModel(parent)
{
    this->columns_header << "Наименование" << "Тип" << "Дата изм.";

    __id = (APPID & 0xFFFF) | ((CLASS_PIVDATA_CURRENT_VER & 0xF) << 16); // первые 16 бита - идентификатор приложения, 4 следующих - версия класса PivData

    notsave = false;
}

/******************************************************************
*
*  Деструктор модели
*
******************************************************************/
MyItemModel::~MyItemModel()
{
    this->disconnect(); // отключаемся от сигнал-слотов

    for(int i=0;i<_pdl_data.size();i++)
        if(_pdl_data.at(i) != NULL)
            delete _pdl_data.at(i);
    _pdl_data.clear();
}
/******************************************************************
*
*  Запрос модельного индекса
*
******************************************************************/
QModelIndex MyItemModel::index(int row, int column, const QModelIndex &parent) const
{
    if ( !this->hasIndex(row, column, parent) ) // проверка существования индека по родителю, строке и столбцу
        return QModelIndex();

    if ( !parent.isValid() ) // запрашивают индексы корневых узлов
    {
        if( row < _pdl_data.size() )
            return this->createIndex(row, column, (void*)_pdl_data.at(row)); // !!!! здесь и везде в createIndex передаём указатель на данные по создаваемому индексу
        else
            return QModelIndex();
    }
    else // запрашивают индекс дочернего узла
    {
        PivData* parentNode = (PivData*)parent.internalPointer(); // получаем указатель на корень
        if( parentNode )
        {
            if( row < parentNode->children.size() )
                return createIndex(row, column, (void*)parentNode->children.at(row)); // возвращаем индекс дочернего узела
            else
                return QModelIndex();
        }
        else
            return QModelIndex();
    }
}
/******************************************************************
*
*  Запрос модельного индекса родительского узла
*
******************************************************************/
QModelIndex MyItemModel::parent(const QModelIndex &child) const
{
    if ( !child.isValid() ) // проверка существования индека
        return QModelIndex();

    // по internalPointer определяем указатель на родителя
    PivData* childNode = (PivData*)(child.internalPointer());
    PivData* parentNode = 0x0;
    int row = -1;

    if( !childNode ) // проверка наличия указателя на ветку
        return QModelIndex();

    if( findRow( childNode ) == -1 ) // проверка ниличя указателя в дереве (условие выполнится, если узел удалён методом delete)
        return QModelIndex();

    if( !childNode->parent ) // проверка наличия родителя ветки
        return QModelIndex();

    parentNode = childNode->parent;

    if( !childNode->parent->parent ) // проверка типа родителя, условие выполнится, если родитель - корень ветки или он в процессе удаления
        row = this->_pdl_data.indexOf(childNode->parent); // получем номер строки в списке корней
    else
        row = childNode->parent->parent->children.indexOf(childNode->parent); // получаем номер строки родителя с списке родетеля родителя :)

    return createIndex(row, 0, (void*)parentNode); // создаем модельный индекс родителя

}
/******************************************************************
*
*  Поиск номера строки по указателю на данные (в списке указателей)
*
******************************************************************/
int MyItemModel::findRow(PivData *node) const
{
    int row = -1;
    if( !this->_pdl_data.contains(node) )    // не нашли указатель в списке узлов
    {
        for( int i=0; i<this->_pdl_data.size(); i++ )
        {
            if( this->_pdl_data.at(i)->Contains(node,&row,0) ) // нашли указатель внутри узлов
                break;
        }
    }
    else
        row = this->_pdl_data.indexOf(node);

    return row;
}
/******************************************************************
*
*  Запрос количества строк
*
******************************************************************/
int MyItemModel::rowCount(const QModelIndex &parent) const
{
    if ( !parent.isValid() )  // запрашивают кол-во строк корневых узлов
        return _pdl_data.size();

    PivData* parentNode = (PivData*)(parent.internalPointer());  // получаем указатель по индексу
    if( this->findRow(parentNode)  == -1 )  // запрос строк у удалённого элемента
        return 0;
    else
        return parentNode->children.size();
}
/******************************************************************
*
*  Запрос количества столбцов
*
******************************************************************/
int MyItemModel::columnCount(const QModelIndex &) const
{
    return this->columns_header.size();
}
/******************************************************************
*
*  Флаги записи
*
******************************************************************/
Qt::ItemFlags MyItemModel::flags(const QModelIndex &index) const
{
    if (!index.isValid())
        return Qt::ItemIsEnabled | Qt::ItemIsDropEnabled;

    if( index.column() == 0 || index.column() == 1  ) // флаг редактирования ставин только на нулевой столбец (наименование)
        return QAbstractItemModel::flags(index) | Qt::ItemIsEditable |  Qt::ItemIsDragEnabled | Qt::ItemIsDropEnabled;
    else
        return QAbstractItemModel::flags(index) |  Qt::ItemIsDragEnabled | Qt::ItemIsDropEnabled;
}
/******************************************************************
*
*  Заголовки столбцов
*
******************************************************************/
QVariant MyItemModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (role != Qt::DisplayRole) // если не для отображения
        return QVariant();

    // для отображения
    if (orientation == Qt::Horizontal && role == Qt::DisplayRole && section < this->columns_header.size())
        return this->columns_header.at(section);

    return QVariant();
}
/******************************************************************
*
*  Запрос данных
*
******************************************************************/
QVariant MyItemModel::data(const QModelIndex &index, int role) const
{
    if ( !index.isValid() )   // проверка индекса
        return QVariant();

    const PivData* node = (PivData*)(index.internalPointer());

    if( role == Qt::DisplayRole )   // данные для отображения?
    {
        switch( index.column() )
        {
        case 0:
            return node->info.Name;
        case 1:
            return node->tnode.value(node->info.type);
        case 2:
            return node->info.dt;
        default:
            return QVariant();
        }
    }
    return QVariant();
}
/******************************************************************
*
*  Заполнение данными
*
******************************************************************/
bool MyItemModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
    if ( !index.isValid() )   // проверка индекса
        return false;


    PivData* node = (PivData*)(index.internalPointer());

    if( role == Qt::EditRole ) // данные от редактора?
    {
        bool exstatus = false;

        emit this->layoutAboutToBeChanged();

        switch( index.column() )
        {
        case 0: // столбец наименования
            if( !value.toString().isEmpty() )
            {
                node->info.Name = value.toString();
                node->updateDateNodes();
                exstatus = true;
                break;
            }
            else
            {
                emit this->mess("Модель данных: имя узла не может быть пустым");
                exstatus = false;
                break;
            }
        case 1: // столбец типа узла
            if( !node->CtrlTypeNode((PivData::Nodes)value.toInt()) )
            {
                emit this->mess("Модель данных: '" + node->info.Name + "' не может быть '" + node->tnode.value((PivData::Nodes)value.toInt()) + "'");
                exstatus = false;
                break;
            }
            node->info.type = (PivData::Nodes)value.toInt();
            node->updateDateNodes();
            exstatus = true;
            break;
        default:
            break;
        }
        emit this->layoutChanged();
        notsave = exstatus; // флаг неоходимости сохранения данных
        return exstatus;
    }

    return false;
}
/******************************************************************
*
*  Добавление данных
*
******************************************************************/
bool MyItemModel::insertRow(int row, const QModelIndex &parent)
{
    PivData* _newpd = NULL;
    if ( !parent.isValid() )   // добавление корневого элемента
    {
        _newpd = new PivData();

        this->beginInsertRows(parent,row,row);
        this->_pdl_data.insert(row,_newpd);
        this->endInsertRows();
    }
    else    // добавление дочернего элемента
    {
        _newpd = (PivData*)parent.internalPointer();

        if ( this->findRow(_newpd) == -1 ) // ветка НЕ найдена
            return false;

        this->beginInsertRows(parent,row,row);
        _newpd->updateDateNodes();
        _newpd = new PivData(_newpd);
        if( !_newpd->parent ) // родитель отказался от потомка
        {
            delete _newpd;
            emit this->mess("Модель данных: запись НЕ добавлена");
        }
        else
            _newpd->parent->children.insert(row,_newpd); // добавляем потомка к родителю в список
        this->endInsertRows();
    }

    emit this->mess("Модель данных: запись добавлена");
    notsave = true; // устанавливаем флаг неоходимости сохранения данных
    return true;
}
/******************************************************************
*
*  Удаление данных
*
******************************************************************/
bool MyItemModel::removeRow(int row, const QModelIndex &parent)
{
    if( row == -1 )
    {
        emit this->mess("Модель данных: нечего удалять");
        return false;
    }

    if ( !parent.isValid() ) // Удаляется корень ветки
    {
        if( row >= this->_pdl_data.size() )
        {
            emit this->mess("Модель данных: нечего удалять");
            return false;
        }

        if( this->_pdl_data[row]->Size() > 5 ) // выдаем сообщение, если размер ветки больше 5
        {
            if( QMessageBox(QMessageBox::Warning,"Модель данных","Ветка содержит больше 5 дочерних элементов, все равно удалить?",
                    QMessageBox::Yes|QMessageBox::No).exec() != QMessageBox::Yes )
            {
                emit this->mess("Модель данных: удаление отменено");
                return false;
            }
        }

        this->beginRemoveRows(parent,row,row);  // сообщаем остальным элементам, что началось удаление записи из можеди
        delete this->_pdl_data[row];
        this->_pdl_data.remove(row); // удаляем запись
        this->endRemoveRows(); // сообщаем остальным записям, что удаление завершено
    }
    else // удаляем дочерний элемент
    {
        PivData* ppd = (PivData*)parent.internalId(); // получем указатель на данные из индекса
        if (  this->findRow(ppd) == -1 ) // ветка НЕ найдена
        {
            emit this->mess("Модель данных: ветка ПИВ не найдена");
            return false;
        }
        if( row >= ppd->children.size() )
        {
            emit this->mess("Модель данных: нечего удалять");
            return false;
        }

        if( ppd->children.at(row)->Size() > 5 ) // выдаем сообщение, если размер ветки больше 5
        {
            if( QMessageBox(QMessageBox::Warning,"Модель данных","Ветка содержит больше 5 дочерних элементов, все равно удалить?",
                    QMessageBox::Yes|QMessageBox::No).exec() != QMessageBox::Yes )
            {
                emit this->mess("Модель данных: удаление отменено");
                return false;
            }
        }

        this->beginRemoveRows(parent,row,row);  // сообщаем остальным элементам, что началось удаление записи из можеди
        ppd->updateDateNodes();
        delete ppd->children[row];   // освобождаем память
        ppd->children.remove(row);
        this->endRemoveRows(); // сообщаем остальным записям, что удаление завершено
    }

    emit this->mess("Модель данных: запись удалена");
    notsave = true; // устанавливаем флаг неоходимости сохранения данных
    return true;
}
/******************************************************************
*
*  Загрузка/выгрузка данных
*
******************************************************************/
bool MyItemModel::LoadData(QString filename)
{
    QFile file;
    // открываем файл
    file.setFileName(filename);
    if( !file.open(QIODevice::ReadOnly) )
    {
        emit this->mess("Модель данных: Файл не открыть, загрузка файла отменена.");
        return false;
    }
    emit this->mess("Модель данных: очищаемся перед загрузкойданных");

    emit this->layoutAboutToBeChanged();

    emit this->mess("Модель данных: загружаем данные");
    // организуем поток записи
    QDataStream ds(&file);
    ds.setVersion(QDataStream::Qt_4_0);
    int node_count = 0;
    quint64 __idtmp = 0;
    ds >> __idtmp;  // считываем служебную информацию
    ds >> node_count;   // считываем 4 байта - кол-во корневых узлов
    if( (this->__id & 0xFFFF) != (__idtmp & 0xFFFF) )
    {
        emit this->mess("Модель данных: формат файла не поддерживается");
        file.close();
        emit this->layoutChanged();
        return false;
    }

    bool iscolorV2 = false;
    if( ((this->__id >> 16) & 0xF) == 2 && ((__idtmp >> 16) & 0xF) == 1 )
    {
        if( QMessageBox(QMessageBox::Warning,"Модель данных","Файл данных имеет старый формат, в котором нет информации о 'цветности' элементов типа 'Признак'. По-умолчанию будет установлен белый цвет для значений 0 и 1. Поменять занчения цвета по-молчанию для открываемого файла?\n\n Нет - красить Признаки белым. \n Да - красить Признаки как раньше (1 - зеленый; 0 - желтый). \n\n Рекомендую для перехода на новый формат файла данных нажать 'Сохранить' в окне редактора ПИВ",
                QMessageBox::Yes|QMessageBox::No).exec() == QMessageBox::Yes )
        {
            iscolorV2 = true;
        }
    }

    int base_size = 0;
    if( _pdl_data.size() )
    {
        if( QMessageBox(QMessageBox::Warning,"Модель данных","Очистить список ПИВ перед загрузкой файла? Если нет, то данные будут добавлены в конец списка.",
                QMessageBox::Yes|QMessageBox::No).exec() == QMessageBox::Yes )
        {
            for(int i=0;i<_pdl_data.size();i++)
                if(_pdl_data.at(i) != NULL)
                    delete _pdl_data.at(i);
            _pdl_data.clear();
            notsave = false; // сбрасываем флаг неоходимости сохранения данных
        }
        else
        {
            base_size = _pdl_data.size();
            notsave = true; // устанавливаем флаг неоходимости сохранения данных
        }
    }

    // загружаем данные в ветви
    for( int i=0;i<node_count;i++ )
    {
        PivData* npd = new PivData();
        npd->ColorV2(iscolorV2);

        this->_pdl_data.append(npd); // создаем ветку
        if( !this->_pdl_data.at(i+base_size)->DataLoad(ds, (__idtmp >> 16) & 0xF) ) // загружаем данные в ветку
        {
            emit this->mess("Модель данных: ошибка загрузки файла ПИВ");
            file.close();
            emit this->layoutChanged();
            return false;
        }
    }
    // закрываем файл
    file.close();
    emit this->mess("Модель данных: файл загружен");
    emit this->layoutChanged();
    return true;
}
bool MyItemModel::SaveData(QString filename, PivData *pd)
{
    QFile file;
    // открываем файл
    file.setFileName(filename);
    if( !file.open(QIODevice::WriteOnly) )
    {
        emit this->mess("Модель данных: Файл не открыть, запись файла отменена.");
        return false;
    }

    emit this->mess("Модель данных: сохраняем файл");
    // организуем поток записи
    QDataStream ds(&file);
    ds.setVersion(QDataStream::Qt_4_0);
    ds << __id; // запись 8 байт - идентификация класса модели

    // выгружаем данные из ветвей
    if( !pd ) // сохраняется все
    {
        ds << _pdl_data.size(); // запись 4-х байт - кол-во корневых узлов
        for( int i=0;i<this->_pdl_data.size();i++ )
            this->_pdl_data.at(i)->DataSave(ds);
    }
    else if( pd->info.type == PivData::N_PIV ) // сохраняется только часть
    {
        ds << (quint32)0x1; // запись 4-х байт - кол-во корневых узлов
        pd->DataSave(ds);
    }
    else
    {
        // закрываем файл
        file.close();
        emit this->mess("Модель данных: файл НЕ сохранён, т.к. не выбран ПИВ");
        return false;
    }

    // закрываем файл
    file.close();
    emit this->mess("Модель данных: файл сохранён");
    notsave = false; // сбрасываем флаг неоходимости сохранения данных
    return true;
}
/******************************************************************
*
*  Доступные режимы перетаскивания
*
******************************************************************/
Qt::DropActions MyItemModel::supportedDropActions() const
{
    if( QApplication::keyboardModifiers() == Qt::ControlModifier )
        return Qt::MoveAction;
    if( QApplication::keyboardModifiers() == Qt::ShiftModifier )
        return Qt::CopyAction;

    return Qt::IgnoreAction;

}
/******************************************************************
*
*  Список MIME типов для модели данных
*
******************************************************************/
QStringList MyItemModel::mimeTypes(void) const
{
    QStringList sl;
    sl << MIME_INDEX_MYITEMMODEL;
    return sl;
}
/******************************************************************
*
*  Запрос данных при перетаскивании
*
******************************************************************/
QMimeData* MyItemModel::mimeData(const QModelIndexList &indexes) const
{
    QMimeData *mimeData = new QMimeData();
    QByteArray encodedData;

    QDataStream stream(&encodedData, QIODevice::WriteOnly);

    for( int i=0; i<indexes.size(); i++ ) // перечисление всех перетаскиваемых индексов
    {
        if (indexes.at(i).isValid())
            stream << (quint64)&indexes.at(i);
    }

    mimeData->setData(MIME_INDEX_MYITEMMODEL, encodedData);
    return mimeData;
}
/******************************************************************
*
*  Вставка данных при перетаскивании
*
******************************************************************/
bool MyItemModel::dropMimeData(const QMimeData *data, Qt::DropAction action, int row, int column, const QModelIndex &parent)
{
    if( action == Qt::IgnoreAction ) // проверка на режим вставки
        return true;

    if( !data->hasFormat(MIME_INDEX_MYITEMMODEL) ) // проверяем тип MIME
        return false;

    if( column > this->columnCount() )  // проверка столбца для вставки
        return false;

    // считываем перетаскиваемые данные
    QByteArray encodedData = data->data(MIME_INDEX_MYITEMMODEL);
    QDataStream stream(&encodedData, QIODevice::ReadOnly);
    QList<QModelIndex*> mipl;
    while (!stream.atEnd())
    {
        quint64 ui = 0;
        stream >> ui;
        QModelIndex* pindex;
        pindex = (QModelIndex*)ui;
        mipl.append(pindex);
    }

    PivData* pd_dst = (PivData*)parent.internalPointer();
    PivData* pd_src = (PivData*)mipl[0]->internalPointer(); // в ячейке 0 milp располагается индекс нулевого столбца

    if( pd_dst == pd_src ) // попытка перетащить себя в себя
        return false;

    bool isinsert = false; // факт вставки элемента
    int insertrow = -1; // строка вставленного элемента (для удаления при неудаче)

    emit this->layoutAboutToBeChanged(); // сообщаем, что модель будет изменена

    // проверка строки для вставки
    int beginRow = -1;
    if (row != -1)  // между строки
    {
        isinsert = this->insertRow(row,parent);
        insertrow = row;
        if( !parent.isValid() )
            pd_dst= this->_pdl_data.at(row);
        else
            pd_dst= pd_dst->children.at(row);
        beginRow = -1;
    }
    else if (parent.isValid())  // падаем на существующий элемент
    {
        beginRow = this->rowCount(parent); // в результате pd_src будет вставлен внутрь pd_dst
    }
    else    // где-то вне элементов
    {
        if( pd_src->info.type == PivData::N_PIV )
        {
            isinsert = this->insertRow(this->_pdl_data.size(),parent);

            insertrow = this->_pdl_data.size()-1;
            pd_dst = this->_pdl_data.last();
            beginRow = -1;
        }
        else
        {
            emit mess("Модель данных: '" + pd_src->info.Name + "' нельзя разместить в корне списка");
            this->layoutChanged();
            return false;
        }
    }

    int remrow = mipl.at(0)->row();
    if( isinsert && parent.internalPointer() == mipl.at(0)->parent().internalPointer() && insertrow <= mipl.at(0)->row() )
        remrow += 1;

    // вставка данных
    if( pd_dst->InsertData(pd_src,beginRow) )
    {
        if( action == Qt::MoveAction )
        {
            if( this->removeRow(remrow,mipl.at(0)->parent()) ) // удаление перемещаемого элемента !!!! ВОТ ЗДЕСЬ ОШИБКА!!!!!!!!!!!!!
                emit mess("Модель данных: данные успешно перемещены");
            else
                emit this->mess("Модель данных: ошибка перемещения (перетаскиваемый элемент не удалён)");
        }
        else
            emit mess("Модель данных: данные успешно скопированы");
    }
    else
    {
        if( isinsert ) // очищаемся, если данные в вставленный элемент не скопировались/не переместились
            this->removeRow(insertrow,parent);
        if( action == Qt::MoveAction )
            emit mess("Модель данных: данные НЕ переместить");
        else
            emit mess("Модель данных: данные НЕ скопировать");
    }

    emit this->layoutChanged(); // сообщаем, что модель изменилась
    return true;
}
