/*****************************************************************

    Модуль редактора ПИВ

    Мордвинкин Ю.Ю.
    01.11.2013

*****************************************************************/
#ifndef PIVEDITOR_H
#define PIVEDITOR_H

#include "../cfg.h"
#include "myitemdelegat.h"
#include "MyItemModel.h"
#include "propitemdelegat.h"
#include "../pivdata.h"

struct Views{
    QTreeView* pivtree;
    QTableView* pivtable;
    QTableWidget* proptable;
    QTextEdit* filename;
    QTextEdit* pathelement;
    QTextEdit* help;
    QComboBox* savetype;

};

class PivEditor : public QObject
{
    Q_OBJECT
public:
    explicit PivEditor(Views &v, QObject *parent = 0);
    ~PivEditor(void);

    enum SaveType
    {
        SV_All = 0,
        SV_AsAll = 1,
        SV_SelectedPiv = 2,
        SV_AsSelectedPiv = 3
    };


    MyItemModel* model; // модель данных
signals:
    void mess(QString str);

    void signalBeginAddOrRem(void);
    void signalEndAddOrRem(void);

public slots:

private slots:
    void SelectItem(const QModelIndex &index);
    void ShowProperty(QModelIndex index);
    void ShowProperty(void);
    void ShowHelp(QString str);
    void reset();
    void timerEvent(QTimerEvent *ev);

    void AddItemClick();
    void RemItemClick();
    void Save(SaveType st);
    void Load(void);

    void slotSave(void){Save((SaveType)this->views.savetype->currentIndex());}


private:
    MyItemDelegat* delegate; // делегат модели
    PropItemDelegat* propertydelegate; // делегат свойств

    QModelIndex currentmodelindex;  // текущий индекс элемента
    int idTimerHelp; // таймер
    Views views; // элементы управления
    QWidget* parentwidget; // виджет-родитель

    QMap<QString, int> counters; // счетчики

};

#endif // PIVEDITOR_H
