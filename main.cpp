#include "widget.h"
#include "cfg.h"
#include <QApplication>

QSettings* StaticData::settings;

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    QTextCodec::setCodecForCStrings(QTextCodec::codecForName("UTF-8"));

    QPixmap p(":/app/screen");
    QSplashScreen* sps = new QSplashScreen(p);
    sps->show();

    a.setApplicationName("КРАПИВА");
    a.setApplicationVersion("1.2.4");
    a.setWindowIcon(QIcon(":/app/icon"));

    // загрузка настроек
    StaticData::settings = new QSettings("krapiva.ini",QSettings::IniFormat,0);
    StaticData::settings->setIniCodec(QTextCodec::codecForName("UTF-8"));
    StaticData::settings->beginGroup("main");
    StaticData::settings->setValue("Name","Комплекс редактора и анализатора протоколов информационного взаимодействия по каналам A429 - КРАПИВА");
    StaticData::settings->setValue("Ver",a.applicationVersion());
    StaticData::settings->setValue("Path",a.applicationFilePath());
    StaticData::settings->setValue("Dir",a.applicationDirPath());
    StaticData::settings->endGroup();

    Widget w;
    w.setWindowTitle(a.applicationName() + " " + a.applicationVersion());
    w.show();

    sps->finish(&w);
    delete sps;

    int rez = a.exec();
    return rez;
}
