#-------------------------------------------------
#
# Project created by QtCreator 2013-10-21T23:21:37
#
#-------------------------------------------------

QT       += core gui network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = КРАПИВА
TEMPLATE = app


SOURCES += main.cpp\
        widget.cpp \
    pivdata.cpp \
    PivEditor/propitemdelegat.cpp \
    PivEditor/MyItemModel.cpp \
    PivEditor/myitemdelegat.cpp \
    PivEditor/piveditor.cpp \
    SrcData/srcdatatcp.cpp \
    SrcData/srcdatafile.cpp \
    Monitor/monitora429.cpp \
    Monitor/modelA429mon.cpp \
    SrcData/a429_channel.cpp \
    SrcData/a429_word.cpp \
    SrcData/a429_device.cpp \
    SrcData/device.cpp \
    Monitor/delegatea429mon.cpp \
    Monitor/modela429monword.cpp \
    Parser/parser.cpp \
    SrcPIV/modelsrcpiv.cpp \
    SrcData/modelsrcdata.cpp \
    SrcData/delegatesrcdata.cpp \
    Parser/proxymodelparser.cpp \
    Parser/modelparserbase.cpp \
    Parser/modelparser_param.cpp \
    Parser/modelparser_group.cpp \
    Parser/modelparser_file.cpp \
    Parser/modelparser_filegroup.cpp \
    Parser/modelparser_fileparam.cpp \
    Parser/modelparser_delegatebase.cpp \
    Plot/mycurve.cpp \
    Plot/myplot.cpp \
    Plot/plotwidget.cpp \
    Plot/datacurve.cpp \
    Plot/styleshemes.cpp

HEADERS  += widget.h \
    pivdata.h \
    cfg.h \
    PivEditor/propitemdelegat.h \
    PivEditor/MyItemModel.h \
    PivEditor/myitemdelegat.h \
    PivEditor/piveditor.h \
    SrcData/srcdatatcp.h \
    SrcData/srcdatafile.h \
    Monitor/monitora429.h \
    Monitor/modelA429mon.h \
    SrcData/a429_channel.h \
    SrcData/a429_word.h \
    SrcData/a429_device.h \
    SrcData/device.h \
    Monitor/delegatea429mon.h \
    Monitor/modela429monword.h \
    Parser/parser.h \
    SrcPIV/modelsrcpiv.h \
    SrcData/modelsrcdata.h \
    SrcData/delegatesrcdata.h \
    Parser/proxymodelparser.h \
    Parser/modelparserbase.h \
    Parser/modelparser_param.h \
    Parser/modelparser_group.h \
    Parser/modelparser_file.h \
    Parser/modelparser_filegroup.h \
    Parser/modelparser_fileparam.h \
    Parser/modelparser_delegatebase.h \
    Plot/mycurve.h \
    Plot/myplot.h \
    Plot/plotwidget.h \
    Plot/datacurve.h \
    Plot/styleshemes.h

INCLUDEPATH += ./../qwt/src
LIBS += -L./../qwt/lib/ -lqwt

FORMS    += widget.ui

RESOURCES += \
    RC/resorce.qrc

OTHER_FILES += \
    RC/Icon_Magn.odp \
    RC/Icon_Zoom.odp \
    RC/Icon_Interval.odp
